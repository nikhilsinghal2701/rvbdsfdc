/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger AdditionalDiscountDetailDeleteFromPWS on Additional_Discount_Detail__c (before delete) {
    
    List<ID> objIDs=new List<ID>(); 
    if(trigger.isBefore && trigger.isDelete){
        
        for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}

          if(objIDs.size()>0)
          {
          if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
          system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
            }
          
          else {
            B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs); 
          }
        }
    }
}