/*************************************************************************************************************
 Author: Santoshi Mishra
 Purpose: This trigger is created on DiscountTable for data processing.
 Created Date : Oct 2011
*************************************************************************************************************/

trigger DistiUpliftTrigger on Disti_Uplift__c (after insert) {
	
	DiscountManagementHelper hlpr = new DiscountManagementHelper();
	hlpr.InsertDistiUpliftChildRecords(Trigger.new);//This method is called by the DistiUplift Table Trigger to create its child records.

}