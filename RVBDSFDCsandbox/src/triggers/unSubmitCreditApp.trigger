trigger unSubmitCreditApp on Application_Task__c (after update) {
	
	List<Id> taskList = new List<Id>();
	for (Application_Task__c appTask : trigger.new) {
		String newStatus = appTask.Status__c;
		//String oldStatus = trigger.oldMap.get(appTask.Id).Status__c;
		//System.debug('oldStatus = ' + oldStatus);
		System.debug('newStatus = ' + newStatus);
		if (newStatus.equalsIgnoreCase('Return for Information')) {	//oldStatus.equalsIgnoreCase('Complete') && 
			taskList.add(appTask.Id);
		}
	}
	System.debug('taskList size = ' + taskList.size());
	List<Credit_Application__c> creditAppList = [Select Id, Submitted__c, Document_Opt_Out__c, Accept_Terms__c from Credit_Application__c where Partner_Application_Task__c In :taskList];
	for (Credit_Application__c creditApp : creditAppList) {
		creditApp.Submitted__c = false;
		creditApp.Accept_Terms__c = false;
		creditApp.Document_Opt_Out__c = false;
		
	}
	update creditAppList;
}