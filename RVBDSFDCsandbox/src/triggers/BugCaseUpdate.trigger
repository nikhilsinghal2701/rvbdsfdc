/*
*Description:Populate Bug # field based on solutions.
*Tracking the bug numbers in the Bug # field, and auto populating the Bug # field 
*will greatly help our case and bug tracking and provide better visibility into the 
*customer impact of specific bugs on our install base.
*Created By:Prashant.Singh@riverbed.com
*Created Date:Sep 28,2010
*/
trigger BugCaseUpdate on Case (before update,before insert,after insert,after update) {
    List<Case> updateList=new List<Case>();
    List<Case> updateList1=new List<Case>();
    Map<ID,List<ID>> csMap = new Map<ID,List<ID>>();
    Map<ID,Solution> solMap;
    set<ID> caseId=new set<ID>();
    set<ID> solutionId=new set<ID>();
    List<ID> solId;
    String delimiter=',';
    List<CaseSolution> csList;
    List<Solution> solList;
    if(trigger.isBefore){
    if(trigger.isUpdate){
    for(Case tempCase:trigger.new){
                if(tempCase.status!=trigger.oldMap.get(tempCase.Id).status){
                    caseId.add(tempCase.Id);
                    updateList.add(tempCase);
                }
            }
            if(updateList.size()>0 && updateList!=NULL){
                csList=[select Id,CaseId,SolutionId from CaseSolution where CaseId IN:caseId];
                for(CaseSolution tempCS:csList){
                    solutionId.add(tempCS.SolutionId);
                    if(csMap.containsKey(tempCS.CaseId)){
                        csMap.get(tempCS.CaseId).add(tempCS.SolutionId);        
                    }else{
                        solId=new List<ID>();
                        solId.add(tempCS.SolutionId);
                        csMap.put(tempCS.CaseId,solId);
                    }
                }
                solMap = new Map<ID,Solution>([select Id,Bug_Number__c from Solution where ID IN:solutionId]);
                if(solMap.size()>0 && solMap!=NULL){
                    for(Case temp:updateList){
                        if(csMap.containsKey(temp.ID)){
                            List<ID> tempSolId=csMap.get(temp.ID);
                            temp.Bug_No__c=null;
                            for(ID tempId:tempSolId){
                                if(solMap.containsKey(tempId)&& solMap.get(tempId).Bug_Number__c!=null){
                                    if(temp.Bug_No__c==null){
                                        //system.debug('bug no.0:'+solMap.get(tempId).Bug_Number__c);
                                        temp.Bug_No__c=solMap.get(tempId).Bug_Number__c;
                                    }else{
                                        //system.debug('bug no.1:'+solMap.get(tempId).Bug_Number__c);                                       
                                        temp.Bug_No__c=temp.Bug_No__c+delimiter+solMap.get(tempId).Bug_Number__c;
                                    }
                                }                       
                            }
                        }                   
                    }
                }
            }           
        }
    }
   if(trigger.isAfter){
        if(trigger.isUpdate){
            for(Case c:trigger.new){
                if(c.AssetId!=trigger.oldMap.get(c.Id).AssetId){
                    updateList.add(c);
                }
            }
        }
        
        if(trigger.isInsert){
            for(Case c:trigger.new){
                if(c.AssetId!=null){
                    updateList.add(c);
                }   
            }
        }
        if(updateList.size()>0 && updateList!=NULL){
            CaseUtil.AssetToCaseUpdateAfter(updateList);
        }
    }
}