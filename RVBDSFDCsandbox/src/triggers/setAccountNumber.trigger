trigger setAccountNumber on Account (before insert) 
{
    /*START: Zeus original code. Commented by prashant.singh@riverbed.com on 10/14/2011
    if((UserInfo.getUserName() == 'mgeldert@zeus.com')
    || (UserInfo.getUserName() == 'mgeldert@riverbed.com.test1'))
    // Odds are this is an API call
    {
        Account a = Trigger.new[0];
        
        if(a.Type == 'Customer EC2')
        {
            a.AccountNumber = Generator.getNewCustomerNumber('A'); // 'A' for Amazon
        }
        else if(a.AccountNumber == null)
        {
            a.AccountNumber = Generator.getNewCustomerNumber('S'); // 'S' for SPLA
        }
    }
    //END:*/
    /*New code for bulk handling.
    ** by prashant.singh@riverbed.com on 10/14/2011
    */
    if(trigger.isBefore && trigger.isInsert && userInfo.getName().equalsIgnoreCase('Zeus Legacy')){
    	ZeusUtility.setAccountNumber(trigger.new);
    	/*
        //if((UserInfo.getUserName() == 'mgeldert@zeus.com')
        //|| (UserInfo.getUserName() == 'mgeldert@riverbed.com.test1')){
            for(Account a: trigger.new){            	
                if(a.Type!=Null && a.Type.equalsIgnoreCase('Customer EC2')){
                    a.AccountNumber = Generator.getNewCustomerNumber('A'); // 'A' for Amazon
                }else if(a.AccountNumber == null){
                    a.AccountNumber = Generator.getNewCustomerNumber('S'); // 'S' for SPLA
                }
            }         
        //} */
    }  
}