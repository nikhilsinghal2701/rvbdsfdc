trigger emailLoginDetails on Developer__c (after insert) 
{
    /* Zeus original code. Commented by prashant.singh@riverbed.com on 10/17/2011
    Developer__c d = Trigger.new[0];
    
    String subj = 'Your Zeus developer account';
    String msg = 'Hi ' + d.FirstName__c + ',\n\n';
    msg += 'Your Zeus developer account has been created.\n\n';
    msg += '\tUsername: ' + d.Email__c + '\n';
    msg += '\tPassword: ' + d.Password__c + '\n';
    msg += '\tExpiry date: ' + d.Expires__c.format() + '\n\n';
    msg += 'You can download the software binaries and license keys from http://www.zeus.com/downloads/developers/downloads.php.\n\n';
    msg += 'Developer licenses are not supported.  If you require the assistance of experienced Zeus consultants ';
    msg += 'as you evaluate Zeus solutions, please commence a managed evaluation at http://www.zeus.com/downloads/traffic-manager.html\n\n';
    msg += 'Regards,\n\nZeus Technology\n';
    
    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
    String[] rcpt = new String[] { d.Email__c };
         
    email.setReplyTo('noreply@zeus.com');
    email.setSaveAsActivity(false);
    email.setToAddresses(rcpt);
    email.setSubject(subj);
    email.setPlainTextBody(msg);
        
    Messaging.SendEmailResult[] sendResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    */
    if(trigger.isAfter && trigger.isInsert){
    	ZeusUtility.sendEmailLoginDetails(trigger.new);
    }
}