/*
Requirement: Support Handling Notes Enhancement upon new case creation.
By: prashant.singh@riverbed.com
date: 12/8/2011
*/

trigger CaseSupportHandlingNotesOnCreate on Case (before insert, before update) {
    /*set<Id> accIds=new set<Id>();
    if(trigger.isBefore){
        for(Case cTemp:trigger.new){
            accIds.add(cTemp.AccountId);
        }
        if(accIds.size()>0){
            Map<Id,Account> accMap=new Map<Id,Account>([select id,Support_Handling_Notes_del__c,Authorizations_Specializations__c from Account where Id IN:accids]);//commented by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
            if(accMap!=null && accMap.size()>0){
                for(Case temp:trigger.new){
                    if(accMap.containsKey(temp.AccountId)){
                        temp.Support_Handling_Notes__c=accMap.get(temp.AccountId).Support_Handling_Notes_del__c;
                        temp.Account_Authorization__c=accMap.get(temp.AccountId).Authorizations_Specializations__c;//added by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
                    }
                }
            }
        }   
    }*/
    if(trigger.isBefore){
        try{
            CaseUtil.setSupportInfoFromAccount(trigger.new, trigger.oldMap);
            CaseUtil.setStaffEngineerRole(trigger.new, trigger.oldMap);
        }catch(Exception e){
            trigger.new[0].addError('An Error occured in CaseSupportHandlingNotesOnCreate trigger:'+e.getMessage());
        }
    }
}