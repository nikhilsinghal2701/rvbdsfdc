trigger CaseSupportHandlingNotesPopulate on Account (after insert, after update) {
    List<Account> casePopulate=new List<Account>();
    List<Case> caseList;
    List<Case> updateCaseList=new List<Case>();
    set<Id> accIds=new set<Id>();
    Map<Id,List<Case>> accCaseMap=new Map<Id,List<Case>>();
    if(trigger.isAfter && trigger.isInsert){
        for(Account acc: trigger.New){
            if(acc.Support_Handling_Notes_del__c!=null){
                casePopulate.add(acc);
            }
        }
    }else if(trigger.isAfter && trigger.isUpdate){
        for(Account acc:trigger.new){
            if(acc.Support_Handling_Notes_del__c!=trigger.oldMap.get(acc.Id).Support_Handling_Notes_del__c){
                //system.debug('***debug1:'+acc.Support_Handling_Notes_del__c);
                //system.debug('***debug2:'+trigger.oldMap.get(acc.Id).Support_Handling_Notes_del__c);
                casePopulate.add(acc);
            }
        }
    }
    if(casePopulate.size()>0){
        for(Account acc:casePopulate){
            accIds.add(acc.Id);
        }
        if(accIds.size()>0){
            List<case> tempList=[select Id, AccountId,Support_Handling_Notes__c,status from case where accountId IN: accIds and (NOT status like 'Closed%')];
            if(tempList!=null && tempList.size()>0){
                for(Case temp:tempList){
                    if(accCaseMap.containsKey(temp.AccountId)){
                        accCaseMap.get(temp.AccountId).add(temp);
                    }else{
                        caseList=new List<Case>();
                        caseList.add(temp);
                        accCaseMap.put(temp.AccountId,caseList);
                    }
                }
            }
        }
        if(accCaseMap.size()>0){
            for(Account acc:casePopulate){
                if(accCaseMap.containsKey(acc.Id)){
                    for(case temp:accCaseMap.get(acc.Id)){
                        temp.Support_Handling_Notes__c=acc.Support_Handling_Notes_del__c;
                        updateCaseList.add(temp);
                    }
                }                   
            }
        }
        if(updateCaseList.size()>0){
            update updateCaseList;
        }       
    }
}