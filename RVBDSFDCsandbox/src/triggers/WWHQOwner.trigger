trigger WWHQOwner on WW_HQ__c (before insert, before update) {
	for (WW_HQ__c o : Trigger.New) {
		String OwnerString = String.valueOf(o.OwnerId);
		if (OwnerString.startsWith('005')) o.AccountOwner__c = o.OwnerId;
	}
}