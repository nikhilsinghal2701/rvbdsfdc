/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
trigger PopulateProductFamily on Product2 (before insert, before update) {
  
    Map<String, String> mapProductFamily = new Map<String, String>();
    Map<String, String> mapFamilyProdSubFamily = new Map<String, String>();
    
    for (Product_Family_Mapping__c mapRecord : [select id, Begins_With__c, Product_Sub_Family__c, 
                                                    Product_Family__c from Product_Family_Mapping__c]) {
        if(mapRecord.Begins_With__c != null )
            mapProductFamily.put(mapRecord.Begins_With__c, mapRecord.Product_Sub_Family__c);
        
        else if(mapRecord.Product_Family__c != null)
            mapFamilyProdSubFamily.put(mapRecord.Product_Family__c, mapRecord.Product_Sub_Family__c);
    }
    
    
    for(Product2 prod :trigger.new) {
       for(String prefix: mapProductFamily.keySet()) {
           
           if(prod.ProductCode!=null && mapProductFamily != null && mapProductFamily.containsKey(prefix) 
                       && prod.ProductCode.startswith(prefix)) {
                
                prod.Product_Sub_Family__c = mapProductFamily.get(prefix); 
                break;   
                
           } else if(mapFamilyProdSubFamily != null && mapFamilyProdSubFamily.containsKey(prod.Family)) {
               prod.Product_Sub_Family__c = mapFamilyProdSubFamily.get(prod.Family); 
               break; 
           }
       }
    }
}