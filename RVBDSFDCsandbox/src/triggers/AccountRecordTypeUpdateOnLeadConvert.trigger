/*
** purpose:
** 1.) Account RecordType Update on lead convert, if record type is 'Support Customer'.
** 2.) Populate the custome address fields of Account from standard address field upon account creation after lead convert.
** 3.) Populate the custom country/state field of lead from standard country/state field.
** by:prashant.singh@riverbed.com
** date:5/23/2012
** Modified on 03/18/2013 for CDH related validation rule bypass during lead conversion, added ConvertLead__c field and update call.
*/
trigger AccountRecordTypeUpdateOnLeadConvert on Lead (before insert, before update, after update){
	// test method in TestReverseBingoProject	
	public List<Country__c> countryList;
	public Map<String,String> countryMap;
	
	if(trigger.isBefore &&  trigger.isInsert){
		countryList=[Select Id,ISO2__c,Name from Country__c];
		countryMap=new Map<String,String>();
		if(countryList.size()>0){
			for(Country__c country:countryList){
				if(!countryMap.containsKey(country.ISO2__c)){
					countryMap.put(country.ISO2__c,country.Name);
				}
			}
		}	
		
			for(Lead lead:trigger.new){
				if(lead.Country!=null){
					if(lead.Country.length()==2){
						if(countryMap.size()>0 && countryMap.containsKey(lead.Country))lead.Country__c=countryMap.get(lead.Country);
					}else{
						lead.Country__c=lead.Country;
					}
				}
				if(lead.State!=null||lead.State!=''){
					//system.debug('***State:'+lead.State);
					lead.OS_State__c=lead.State;
				}
				if(lead.Street != null || lead.Street != ''){
					lead.OS_Address1__c = lead.street;
				}	
				if(lead.city!= null || lead.city !=''){
					lead.OS_City__c = lead.city;
				}
				if(lead.PostalCode != null || lead.PostalCode != ''){
					lead.OS_Postal_Code__c = lead.PostalCode;
				}
			}			
		}else if(trigger.isBefore && trigger.isUpdate){
			List<Lead> updateList = new List<Lead>();
			for(Lead lead:trigger.new){
				if(lead.Country!=null && lead.Country!=trigger.oldMap.get(lead.Id).Country){
					if(lead.Country.length()==2){
						//if(countryMap.size()>0 && countryMap.containsKey(lead.Country))lead.Country__c=countryMap.get(lead.Country);
						updateList.add(lead);	 				
					}else{
						lead.Country__c=lead.Country;
					}
				}				
				if(lead.State!=trigger.oldMap.get(lead.Id).State){
					//system.debug('***State1:'+lead.State);
					lead.OS_State__c=lead.State;
				}
				if(lead.Street != null || lead.Street != ''){
					lead.OS_Address1__c = lead.street;
				}	
				if(lead.city!= null || lead.city !=''){
					lead.OS_City__c = lead.city;
				}
				if(lead.PostalCode != null || lead.PostalCode != ''){
					lead.OS_Postal_Code__c = lead.PostalCode;
				}
			} //end for
			// trigger restructure
			if(!updateList.isEmpty()){
				countryList = [Select Id,ISO2__c,Name from Country__c];
				countryMap = new Map<String,String>();
				if(countryList.size()>0){
					for(Country__c country:countryList){
						if(!countryMap.containsKey(country.ISO2__c)){
							countryMap.put(country.ISO2__c,country.Name);
						}
					}
				}
				for(Lead ld : updateList){
					if(countryMap.size()>0 && countryMap.containsKey(ld.Country))
						ld.Country__c = countryMap.get(ld.Country);
				}
			}
		}
	else if(trigger.isAfter){
		// no bulk processing; will only run from the UI
		List<Account> updAccountList=new List<Account>();
		Boolean isChange=false;
		if (Trigger.new.size() == 1) { 
	  		if (Trigger.old[0].isConverted == false && Trigger.new[0].isConverted == true){ 
	      		// if a new account was created
	    		if (Trigger.new[0].ConvertedAccountId != null) { 
		        	// update the record type of converted account to 'Customer Account' from 'Support Customer' record type.
		        	//Account a = [Select a.Id, a.Name, a.RecordTypeId, a.RecordType.Name From Account a Where a.Id = :Trigger.new[0].ConvertedAccountId];
		        	Account a = [Select a.Id, a.Name,a.RecordTypeId, a.RecordType.Name, a.OS_Address_1__c, a.OS_Address_2__c, a.OS_Address_3__c,
		        				a.OS_City__c, a.OS_State__c, a.D_B_Country__c, a.ISO_Country_Code__c, a.OS_Postal_Code__c 
		        				, a.BillingCity, a.BillingCountry, a.BillingPostalCode, a.BillingState, 
		        				a.BillingStreet,a.ConvertLead__c From Account a Where a.Id = :Trigger.new[0].ConvertedAccountId];
		        	if(a.RecordType.Name.equalsIgnoreCase('Support Customer')){
		        		a.RecordTypeId = [Select r.Id, r.Name, r.SobjectType from RecordType r where r.name='Customer Account' and r.SobjectType='Account'].Id;
		        		isChange=true;	
		        	}else if((a.OS_Address_1__c == null) && (a.OS_City__c == null) && (a.OS_State__c == null) && (a.OS_Postal_Code__c == null)){
	        			a.OS_Address_1__c = a.BillingStreet;
		        		a.OS_City__c = a.BillingCity;
		        		a.OS_State__c = a.BillingState;
		        		a.OS_Postal_Code__c = a.BillingPostalCode;
		        		a.D_B_Country__c = a.BillingCountry;		        				        		
		        		isChange=true;		        		
		        	}
		        	if(isChange==true){
		        		a.ConvertLead__c='Yes';
		        		updAccountList.add(a);
		        	}		        	     		        	 
	      		}
	      		if(updAccountList!=null && updAccountList.size()>0){
	        		try{
	        			update updAccountList;
	        		}catch(Exception e){
	        			trigger.new[0].addError(e.getMessage());
	        		}
	        	}   
	   		}
	  	}
	}
}