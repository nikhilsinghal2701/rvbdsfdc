trigger RenewalContactPermissionAssigment on Contact (after insert , after update) {
    Set<Id> conIds = new Set<Id>();
    for(Contact c : trigger.new){
        if(trigger.isInsert && c.Renewal_Contact__c != null && c.Renewal_Contact__c){
            conIds.add(c.Id);    
        }else if(trigger.isupdate && c.Renewal_Contact__c != trigger.oldmap.get(c.Id).Renewal_Contact__c 
                && c.Renewal_Contact__c != null && c.Renewal_Contact__c){
            conIds.add(c.Id);            
                
       }
    }//end of for loop
    if(!conIds.isEMpty()){
        Set<Id> userIds = new Set<Id>();
        for(User u : [Select id , ContactId from User Where ContactId in :conIds]){
            userIds.add(u.Id);
        }
        if(!userIds.isEmpty()){
            RenewalContactPermissionAssigmentUtil.createPermissionAssig(userIds);
        }
    }
}