trigger setTaskComments on Task (before insert) {
	/*	This trigger generates the comments on the task dynamically to incorporate data from the account and opportunity
		for which the task is created. Current text in comments is as below where merge fields need to be populated at run time.
		An opportunity for {!Account.Name} has been created by {!Opportunity.CreatedBy}.
		The Parent Account is: {!Account.Global_Ultimate_Account_Name__c}
		For details, please click HERE: {!Opportunity.Link}
	*/
	String comments = 'An opportunity for ';
	//Set<Id> accIds = new Set<id>();
	Set<Id> whatIds = new Set<Id>();
	for(Task t : trigger.new){
		String sub = t.subject == null ? '' : t.subject;
		if(sub.equalsIgnoreCase('Global Account Opportunity Alert')){
		//	accIds.add(t.AccountId); 
			whatIds.add(t.whatId);
		}
	}
	//System.debug('accIds = ' + accIds);
	System.debug('whatIds = ' + whatIds);
	if(!whatIds.isEmpty()){
		Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([select id, name, account.name, account.Global_Ultimate_Account_Name__c,createdBy.name 
																from Opportunity where id in :whatids]);
		//Map<Id, Account> accntsMap = new Map<ID, Account>([select id, name, Global_Ultimate_Account_Name__c from Account where id in :accIds]);
		for(Task t : trigger.new){
			if(oppMap.containsKey(t.whatId)){
				comments += oppMap.get(t.whatId).Account.Name + ' has been created by ' + oppMap.get(t.whatId).CreatedBy.Name + '.\n The Parent Account is: ';
				comments += oppMap.get(t.whatId).Account.Global_Ultimate_Account_Name__c + '.\n ';
				/*TODO: Add link for the Opportunity. Need to get the server URL from another class static variable. 
				Holding for now to not make change in another class for just this purpose. 
				String to be added: For details, please click HERE: URL/t.whatId;
				*/ 
				t.Description = comments + '\n' + t.Description;
			}
			comments = 'An opportunity for ';
		}
	}
}