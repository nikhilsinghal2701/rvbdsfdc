/*
    This trigger handles duplicate certification summaries when accounts merge. This trigger finds
    duplicate cert summaries, assigns all certifications to one certification summary, and deletes
    the left over/empty cert summary. 
*/
trigger AccountMergeCertificateSummaryJoin on Account (after update) {
    //check if trigger has already run. This will avoid infinite loop
    if(!AccountMergeCertificateSummaryJoinHelper.hasAlreadyRun()||test.isRunningTest()){
        //get list of accounts that have been updated/joined
        list<string> accountIds = new list<string>();
        for(account a: trigger.new){
            accountIds.add(a.id);
        }
        if(accountIds.size() > 0){
            //get account summaries
            list<Acct_Cert_Summary__c> csList = [Select a.TAP__c, a.Id, a.Account__c 
                                                                                    From Acct_Cert_Summary__c a
                                                                                    where Account__c in:accountIds];
            //make map of Tap__c to list cert summaries
            map<string, list<Acct_Cert_Summary__c>> TapToCSLists = new map<string, list<Acct_Cert_Summary__c>>();                                                                           
            list <Acct_Cert_Summary__c> csSubList;
            for(Acct_Cert_Summary__c cs : csList){
                csSubList = TapToCSLists.get(cs.Tap__c);
                if(csSubList==null)csSubList = new list<Acct_Cert_Summary__c>();
                csSubList.add(cs);
                TapToCSLists.put(cs.Tap__c, csSubList);
            }
            //find duplicate TAP__C for accounts and make map of CertSummaries to be deleted
            //to CertSummaries to be updated/pointed to.
            set<string> TapToCSkeys = TapToCSLists.keyset();
            set<string> acctToCSkeys;
            list<Acct_Cert_Summary__c> CSDeleteList = new list<Acct_Cert_Summary__c>();
            list<Acct_Cert_Summary__c> acctCSList;
            map<string, string> CSdeleteToCSId = new map<string, string>();
            map<string, list<Acct_Cert_Summary__c>> acctToCs;// = new list<string, list<Acct_Cert_Summary__c>>();
            //go thru each TAP list of certs
            for(String k:TapToCSkeys){
                csSubList = TapToCSLists.get(k);
                acctToCs = new map<string, list<Acct_Cert_Summary__c>>();
                //make map of account id to list of Cert Summaries(these are all cert summaries of same TAP.)
                for(Acct_Cert_Summary__c cs: csSubList){
                    acctCSList = acctToCs.get(cs.Account__c);
                    if(acctCSList==null)acctCSList = new list<Acct_Cert_Summary__c>();
                    acctCSList.add(cs);
                    acctToCs.put(cs.Account__c, acctCSList);        
                }
                //look for acounts with more than 1 in list. more than 1 means duplicate
                acctToCSkeys = acctToCs.keySet();
                for(String ac : acctToCSkeys){
                    acctCSList = acctToCS.get(ac);
                    if(acctCSList.size()>1){//if(acctCSList.size()==2)//commented by psingh@riverbed.com on 9/29/2014
                        //CSdeleteToCSId.put(acctCSList[0].id, acctCSList[1].id);//commented by psingh@riverbed.com on 9/29/2014
                        //CSDeleteList.add(acctCSList[0]);//commented by psingh@riverbed.com on 9/29/2014
                        // start: added by psingh@riverbed.com on 9/29/2014
                        String recId=acctCSList[1].id;
                        for(Acct_Cert_Summary__c cs:acctCSList){
                            CSdeleteToCSId.put(cs.Id,recId);
                        }
                    }//end: added by psingh@riverbed.com on 9/29/2014
                }
            }
            //get certifications pointing to Acct_Cert_Summary__c to be deleted and set to new cs id
            Set<String> deleteIds=new Set<string>();// = CSdeleteToCSId.keySet(); //commented by psingh@riverbed.com on 9/29/2014
            //start: added by psingh@riverbed.com on 9/29/2014
            if(CSdeleteToCSId!=null && CSdeleteToCSId.size()>0){
                for(string id:CSdeleteToCSId.keySet()){
                    if(CSdeleteToCSId.containsKey(id) && (id!=CSdeleteToCSId.get(id))){
                        deleteIds.add(id);
                    }
                }
            }//end: added by psingh@riverbed.com on 9/29/2014
            if(deleteIds.size() > 0){
                CSDeleteList=[Select TAP__c, Id, Account__c From Acct_Cert_Summary__c where Id in:deleteIds];//added by psingh@riverbed.com on 09/29/2014
                list<Certificate__c> certList = [Select c.Id, c.Certification_Summary__c 
                                                                                From Certificate__c c 
                                                                                where Certification_Summary__c in :deleteIds];
                for(Certificate__c c : certList){
                    c.Certification_Summary__c = CSdeleteToCSId.get(c.Certification_Summary__c);
                }            
                //set static variable to avoid infinite loop
                AccountMergeCertificateSummaryJoinHelper.setAlreadyRun();
                //update certifications and delete cert summaries
                try{
                    if(certList.size()>0)update certList;           
                    if(CSDeleteList.size()>0)delete CSDeleteList;
                }catch(Exception e){
                    throw e;
                }
            }
        }
    }
}