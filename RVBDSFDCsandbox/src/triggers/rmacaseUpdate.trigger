trigger rmacaseUpdate on RMA__c (after insert,after update) {

  List<RMA__c> updRMAList = new List<RMA__c>();
  //Map<Id,String>  caseMap = new Map<Id,String>();
  Map<Id,List<RMA__c>>  rmaMap = new Map<Id,List<RMA__c>>();
  List<RMA__C> rmaList;  
  List<Case> caselist = New List<Case>();
  Map<Id,Case> caseMap;
  String rma_order_number='';
  Case updCase;
  List<RMA__c> rmaCaseList;
  set<Id> caseId=new Set<Id>();
  
  for(RMA__c rma:trigger.new){
    caseId.add(rma.Case_RMA__c);
  }
  if(caseId.size()>0){
    rmaCaseList=[select Id,RMA_Order_Number__c,case_RMA__c from RMA__c where case_RMA__c IN :caseId and RMA_Order_Number__c!=null ];
    for(RMA__c rma:rmaCaseList){
        if(rmaMap.containsKey(rma.Case_RMA__c)){
            rmaMap.get(rma.Case_RMA__c).add(rma);
        }else{
            rmaList=new List<RMA__c>();
            rmaList.add(rma);
            rmaMap.put(rma.Case_RMA__c,rmaList);
        }
    }
  }
  if(rmaMap!=null && rmaMap.size()>0){
      if(trigger.isAfter && trigger.isInsert){
        for(RMA__c rma:trigger.new){
            if(rmaMap.containskey(rma.Case_RMA__c)){
                for(RMA__c temp: rmaMap.get(rma.Case_RMA__c)){
                    if(rma_order_number==''||rma_order_number==null)rma_order_number=temp.RMA_Order_Number__c;
                    else rma_order_number+=','+temp.RMA_Order_Number__c;
                }
                updCase=new Case(Id=rma.Case_RMA__c,rma__c = rma_order_number);
                caseList.add(updCase);
            }   
        }
      }else if(trigger.isAfter && trigger.isUpdate){
        for(RMA__c rma:trigger.new){
            if(rma.RMA_Order_Number__c!=trigger.oldMap.get(rma.Id).RMA_Order_Number__c){
                for(RMA__c temp: rmaMap.get(rma.Case_RMA__c)){
                    if(rma_order_number==''||rma_order_number==null)rma_order_number=temp.RMA_Order_Number__c;
                    else rma_order_number+=','+temp.RMA_Order_Number__c;
                }
                updCase=new Case(Id=rma.Case_RMA__c,rma__c = rma_order_number);
                caseList.add(updCase);  
            }
        }       
      }
      if(caselist!=null && caselist.size()>0){
        update caseList;
      }
  }
  /* 
   for(Integer s=0; s<Trigger.new.size(); s++) {
         updRMAList.add(Trigger.new[s]);
         caseMap.put(Trigger.new[s].Case_RMA__c,Trigger.new[s].RMA_Order_Number__c);
     }   
   caselist = [SELECT id,RMA__c from case where id in: caseMap.keyset()];   
    for(case c: caselist) {    
    if (c.rma__c == null)
      c.rma__c = caseMap.get(c.id);
    else
      c.rma__c = c.rma__c + ',' + caseMap.get(c.id);
   }
  update caselist;
  */
}