/*************************************************************************************************************
 Author: Santoshi Mishra
 Purpose: This trigger is created on Category_Master__c for data processing.
 Created Date : Oct 201
 Modified By: Anil Madithati
 Date: 08/23/2014
 Purpose: To send an outbound to TIBCO to handle the record deletions ((Part of NPI::3.0))
 Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014
*************************************************************************************************************/
trigger CategoryMasterTrigger on Category_Master__c (after insert,before update,before delete) {
    
    List<ID> objIDs=new List<ID>();
    
    if(trigger.isinsert && trigger.isafter)
    {
    /** This future method is called to add rows in DiscountCategory Detail
      and DistiUplift Detail object on new row insertion in CategoryMaster.*/
      DiscountManagementHelper.updateDiscountTable(trigger.NewMap.KeySet());
       DiscountManagementHelper.updateDistiTable(trigger.NewMap.KeySet());
        
    }
    
    if(trigger.isupdate && trigger.isbefore)
    {
        Set<String> catNames = new Set<String>();
        List<Category_Master__c> catList = new List<Category_Master__c>();
        for(Category_Master__c c : trigger.new)
        {
            Category_Master__c oldc =  trigger.oldMap.get(c.Id);    
            if(c.active__C == false && (c.active__C != oldc.active__C))
            {
            catNames.add(c.Name);
            catList.add(c);
            }
            
        }
        System.debug('CatNames++++++++++'+catNames);
        if(catNames.size()>0)
        {
            List<Product2> productList= [select Category__C,Name,Id from product2 where Category__C in:catNames ];
            Map<String,Integer> CatCount = new Map<String,Integer>();
            System.debug('plist...............'+productList+'prod--'+productList.size());
            if(productList.size()>0)
            {
                for(Product2 p :productList)
                {
                    if(!CatCount.containskey(p.Category__C))
                     CatCount.put(p.Category__C,0);
                    Integer count = CatCount.get(p.Category__C)+1;
                    CatCount.put(p.Category__C,count);
                     
                    
                }
            }
            System.debug('catCount----'+CatCount);  
            for(Category_Master__c c: catList)  
            {
                if(CatCount.get(C.Name)>0)
                {
                    trigger.NewMap.get(c.Id).adderror('You Cannot make this Category inactive as Products are associated with it.');
                }
            }
        }
        
    }
    if(trigger.isBefore && trigger.isDelete){

        for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}

          if(objIDs.size()>0)
          {
          if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
          system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
            }
          
          else {
            B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs); 
          }
        }
    }
}