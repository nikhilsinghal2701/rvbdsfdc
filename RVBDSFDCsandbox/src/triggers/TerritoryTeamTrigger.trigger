trigger TerritoryTeamTrigger on Territory_Team__c (after insert, after update,after delete) {
if(Trigger.isInsert){
        TerritoryPlanShareHelper.assignTerritoryPlanShare(Trigger.new,false,false);
    }
    if(Trigger.isUpdate){
        TerritoryPlanShareHelper.assignTerritoryPlanShare(Trigger.new,true,false);
    }
    if(Trigger.isDelete){
        TerritoryPlanShareHelper.assignTerritoryPlanShare(Trigger.old,false,true);
    }
}