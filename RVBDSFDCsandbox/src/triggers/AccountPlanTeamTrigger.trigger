trigger AccountPlanTeamTrigger on Account_Plan_Team__c (after insert, after update,after delete) {
    if(Trigger.isInsert){
        AccountPlanShareHelper.assignAccountPlanShare(Trigger.new,false,false);
    }
    if(Trigger.isUpdate){
        AccountPlanShareHelper.assignAccountPlanShare(Trigger.new,true,false);
    }
    if(Trigger.isDelete){
        AccountPlanShareHelper.assignAccountPlanShare(Trigger.old,false,true);
    }

}