/*
*Purpose:Trigger will populate the Account_Region field on contact from Account's Region field value.
*Author: Prashant.Singh@riverbed.com
*Date: 08/12/2010

Revision 1: If sticky notes on Contact are updated, sticky notes on cases of that contact will also be updated
Author: Clear Task (Rucha)
Date: 4/2/2013
util.ContactProductSubscription(Trigger.New,accId);//Added by Anil SE Phase 2 for automating Contact Product Subscription based 
on account and linked account asset Oracle Product family.
*/
trigger ContactRegionPopulate on Contact (before insert,before update,after update) {
    set<Id> accId;
    Map<Id,Account> accountMap = new Map<Id,Account>();
    try{
        if(trigger.isBefore){
            accId=new set<Id>();
            for(Contact con:trigger.new){
                if(con.AccountId!=null){
                    accId.add(con.AccountId); 
                }
            }
            if(accId.size()>0){
                accountMap=new Map<Id,Account>([select Id,Name,Region__c,OwnerId from Account where Id in :accId]);
            }
            if(trigger.isInsert){   //before insert process 
                if(accountMap.size()>0){
                    Account acc;
                    for(Contact con:trigger.new){
                        if(con.AccountId!=null){
                            acc=accountMap.get(con.AccountId);
                            con.Account_Region__c=acc.Region__c;
                            //con.OwnerId=acc.OwnerId;
                            if(con.AccountId!=null){
                            accId.add(con.AccountId);
                            }
                        }
                    }
                  util.ContactProductSubscription(Trigger.New,accId);//Added by Anil SE Phase 2 for automating Contact Product Subscription based on account and linked account asset Oracle Product family.
                }
            }
            if(trigger.isUpdate){   //before update process
                if(accountMap.size()>0){
                    Account acc;
                    RVBD_Email_Properties__c settings = RVBD_Email_Properties__c.getInstance('ContactProdSubTriggerOnUpdate');
                    String  runTriggerOnUpdate      = settings.mc_org_wide_email_Id__c;
                    for(Contact con:trigger.new){
                        if(con.AccountId!=null){
                            acc=accountMap.get(con.AccountId);
                            if(acc.Region__c!=trigger.oldMap.get(con.Id).Account_Region__c){
                                con.Account_Region__c=acc.Region__c;    
                            }
                            if(con.AccountId!=null){
                            accId.add(con.AccountId);
                            }
                        }   
                    } 
                    //Requirement isto not to fire the trigger on update, but need one time update during golive trigger all updates 08.23.2014 SE Phase::2. 
                    if(runTriggerOnUpdate=='True')
                	util.ContactProductSubscription(Trigger.New,accId);  
                }
            }       
        }
        if(trigger.isAfter){
            if(trigger.isUpdate){
               Set<Id> contactIdSet = new Set<Id>();
        for(Contact c : Trigger.new){
          if(c.Contact_Sticky_Notes__c != Trigger.oldMap.get(c.Id).get('Contact_Sticky_Notes__c')){
            contactIdSet.add(c.Id);
          }
        }
        if(!contactIdSet.isEmpty()){
          ContactAfterUpdate.updateCaseStickyNotesForContacts(contactIdSet);
        }
        //
            }           
        }
    }catch(Exception e){
        trigger.new[0].addError('An Error occured at trigger ContactRegionPopulate on contact:'+e);
    }
}