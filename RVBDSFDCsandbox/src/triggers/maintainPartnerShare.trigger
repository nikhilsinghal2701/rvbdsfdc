trigger maintainPartnerShare on Account (after update) {

    List<Id> accountIds = new List<Id>();
    for (Account acc : trigger.new) {
        if(acc.RecordTypeId == '012300000000NE5'){//012300000000NE5 is Partner Acount Record type id - fixing tis value to limit the soql queries executed in case acc is not partner acc
            accountIds.add(acc.Id);
        }
    }
    if(!accountIds.isEmpty()){
       // Id profileId = [Select Id from profile where name = 'Partner User - Unregistered'].Id;
        Id profileId='00e70000000wzcg';
        List<User> partnerUsers = [Select Id, Contact.AccountId from User where ContactId != null and Contact.AccountId In :accountIds and ProfileId = :profileId and isActive = true];
    
        List<AccountShare> accountShareList = new List<AccountShare>();
        for (User u : partnerUsers) {
            AccountShare accShare = new AccountShare(AccountAccessLevel = 'Edit',
                AccountId = u.Contact.AccountId, UserOrGroupId = u.Id,
                OpportunityAccessLevel = 'None', ContactAccessLevel = 'Edit');
            accountShareList.add(accShare);
        }
        if (!accountShareList.isEmpty()) {
            insert accountShareList;
        }
    }
}