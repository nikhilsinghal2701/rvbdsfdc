trigger ReassignLead on Lead (before insert, before update, after delete) {
   if(!RecursiveTriggerControl.isExecuted) // to check Recursive Execution added by Raj 9/11/2012
   {
    if(Trigger.isInsert){
           for(Lead l : trigger.new){
                //if(l.createdById != eloquaUser[0].Id){//changed for ticket#65129 by Prashant 2/17/2011
                    l.Eloqua_Rescore_TimeStamp__c = System.now(); 
               // }
            }
    } else if(Trigger.isUpdate){
        // Start: added by Raj  9/6/2012
         // get the values from the Lead_Reassignment_rule__c custom setting. 
        Lead_Reassignment_Rules__c  ldScore, owner, ldSource, ldSourceDet;
        if(Lead_Reassignment_Rules__c.getValues('LeadScore') != null) {
        ldScore = Lead_Reassignment_Rules__c.getValues('LeadScore');
        }
                
        if(Lead_Reassignment_Rules__c.getValues('Owner') != null){
        owner = Lead_Reassignment_Rules__c.getValues('Owner');
        }
        System.debug('lead owner in custom setting : '+owner);
        if(Lead_Reassignment_Rules__c.getValues('LeadSource') != null){
        ldSource = Lead_Reassignment_Rules__c.getValues('LeadSource');
        }
        System.debug('lead source in custom setting : '+ldSource);
        if(Lead_Reassignment_Rules__c.getValues('LeadSourceDetail') != null){
        ldSourceDet = Lead_Reassignment_Rules__c.getValues('LeadSourceDetail');
        }
        
        // End :added by Raj
         String generalLeadId;
        Id eloquaUserId = '00570000001KNMt';
        Id EloquaLeadQuarantineQueueId;
        Id LeadNurtureQueueId;
        //Id generalLeadId;
        Map<Id,Group> groupMap;
        Set<Id> updateLeads = new Set<Id>();         
        List<Lead> unconvertedList = new List<Lead>();
        
        //check whether  lead is converted before querying
        for(Lead newLead : trigger.new){
            if(newlead.isConverted != true)
                unconvertedList.add(newLead);
        }
        
        if(!unconvertedList.IsEmpty()){   
            groupMap =  new Map<Id,Group>( [select id,Name from group where Name in ('Lead Quarantine','Lead Nurture') ]);
            for(Id key : groupMap.keySet()){
                if(groupMap.get(key).Name == 'Lead Quarantine') {   
                    EloquaLeadQuarantineQueueId = key;
                }
                if(groupMap.get(key).Name == 'Lead Nurture') {
                    LeadNurtureQueueId = Key;
                }
            } 
            
               if(RecordTypeIds__c.getInstance('General Leads') != null){
                    RecordTypeIds__c myCS2 = RecordTypeIds__c.getInstance('General Leads');
                    if(myCS2.RecordTypeId__c != null){
                        generalLeadId = myCS2.RecordTypeId__c;
                    }
                }
                System.debug('General lead rec type id = '+generalLeadId);
  
        //generalLeadId= [select id from RecordType where name ='General Leads' and SobjectType = 'Lead'].Id;       
        for(Lead newLead : trigger.new){
            Lead oldLead = trigger.oldMap.get(newLead.Id);
            
         // test if phone has changed, ownerid stays the same
            if((oldLead.ownerId == newLead.ownerId) && (oldLead.Lead_Score__c != newLead.Lead_Score__c)){
                if((generalLeadId == newLead.RecordTypeId)&& (newLead.ownerId == EloquaLeadQuarantineQueueId))//||(newLead.ownerId ==  LeadNurtureQueueId)
                { 
                updateLeads.add(newLead.Id);
                 
                }
            }
        
            // Start: added by Raj 9/6/2012
            if(oldLead.ownerId == newLead.ownerId && !Test.IsRunningTest()){

                if((
               // ((ldScore.value__c != null && newLead.Lead_Score__c != null && ldScore.value__c.contains(newLead.Lead_Score__c)) || (ldSource.value__c !=null && newLead.LeadSource != null && ldSource.value__c.contains(newLead.LeadSource)) || (ldSourceDet.value__c != null && newLead.SourceDetail__c != null && ldSourceDet.value__c.contains(newLead.SourceDetail__c)))
               // && 
                (owner.value__c != null && newLead.ownerId != null && owner.value__c.contains(((String)newLead.ownerId).substring(0,15)) 
                 && (((String)newLead.RecordTypeId).contains(generalLeadId)))
                 )){
                     //added by Ankita to check if current user is eloqua user  - 4/1/2015
                    if(((String)UserInfo.getUserId()).contains(eloquaUserId)){ 
                    updateLeads.add(newLead.Id);
                    system.debug(':::::::::::::::::'+updateLeads);
                    }
                 }
            }
             // End: added by Raj
         
            //added for adding timestamp to rescore leads - Ticket# 54778 - Ankita - 10/4/2010
            if((generalLeadId == newLead.RecordTypeId)&& (!newlead.status.equalsIgnoreCase('Purge'))){ //changed for ticket#65129 by Prashant 2/17/2011 & modified for ticket#85752 on 9/26/2011
                if(Util.hasChanges(Util.leadFieldNamesForRescoring, oldLead, newLead) ){ //|| newLead.Id == Util.masterRecordId
                //set flag
                    newLead.Eloqua_Rescore_TimeStamp__c = System.now(); 
            }
         }
       } 
     }
     if(updateLeads.size()> 0){
       System.debug('::::::::::::::REASSIGN Leads' +updateLeads+system.isFuture());
       if(system.isFuture()){system.debug('::::::::::::::::::::::::Future'); }
       else{  
            //CampignMemberFcrm.updateLeadsForReassignment(updateLeads);
            ReassignLeadTriggerTest.updateLeadsForReassignment(updateLeads); } 
     }
     
    }
    }
}