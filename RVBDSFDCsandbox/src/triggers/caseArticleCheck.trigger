/*
 *	This trigger will filter out cases and set the KCS_Known_vs_New picklist according to KCS standards
 *	TODO: DO NOT RUN this until REST API is fixed
 *		Need to run article batch job and Update 
 */

trigger caseArticleCheck on Case (before update) {

		// map of CaseID & Case object	
		Map<String,Case> caseMap = new Map<String,Case>();
		Map<String,Case> kcsNewCases = new Map<String,Case>();
		Map<String,Case> casesWithArticles= new Map<String,Case>();
		Map<String,Case> noArticleCases = new Map<String,Case>();	
		
		for (case c : trigger.new) {
			if (test.isRunningTest()) {
				//setting it to true to test the batch job
				//c.needKCSRerun__c = true;
			}
			caseMap.put(c.Id,c);		
		}	
		
		// filter out New, Known, N/A		
		for (String caseId : caseMap.keySet()) {
			Case curCase = caseMap.get(caseId);
			
			if (curCase.Related_Article_Count__c > 0 && curCase.Related_Article_Count__c != null) {
				List<InQuira_Case_Info__c> relatedArticles = [SELECT Id, Related_Case__c, Related_InQuira_Article__c FROM InQuira_Case_Info__c WHERE Related_Case__c = :caseId];
				
				// map related_article_ID to current case
				Map<String,Case> relatedIds = new Map<String,Case>();				
				for (InQuira_Case_Info__c kav : relatedArticles) {
					relatedIds.put(kav.Related_InQuira_Article__c,curCase);
				}
				
				List<InQuira_Article_Info__c> articleInfos = [SELECT Id, Article_Created_Date__c FROM InQuira_Article_Info__c WHERE Id IN :relatedIds.keyset()];
											
				// New Case
				for (InQuira_Article_Info__c articleInfo : articleInfos) {																		   
					if (articleInfo.Article_Created_Date__c >= curCase.CreatedDate) {
						kcsNewCases.put(caseId,curCase);
						break;
					}				
				}
				// Known Case
				if (!casesWithArticles.containsKey(caseId) && !kcsNewCases.containsKey(caseId)) {
						casesWithArticles.put(caseId,curCase);
					}				
			}
			
			else {
				// N/A Case
				noArticleCases.put(caseId,curCase);
			}
		}		
			
		for (Case c: kcsNewCases.values()) {
	        c.KCS_Known_vs_New__c = 'New';
	    }
		  	   
	    for (Case c: casesWithArticles.values()) {
	        c.KCS_Known_vs_New__c = 'Known';	  
	    }
	     	   	    
	    for (Case c: noArticleCases.values()) {
	        c.KCS_Known_vs_New__c = 'N/A';	
	    }	    	        	
	
		
}