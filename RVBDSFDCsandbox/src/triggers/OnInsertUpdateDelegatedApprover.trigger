/* Added by prashant.singh@riverbed.com on 10/17/2009
*Trigger will update the Role Name by Delegated Approver roleid
* Send email to delegated approver and attach the email content to the attachment section as Type=Note
*
*/
trigger OnInsertUpdateDelegatedApprover on Delegated_Approver__c ( before insert, before update, after insert, after update) {
    Messaging.SingleEmailMessage[] mails =new List<Messaging.SingleEmailMessage>() ;
    final String APPROVAL_DELEGATION='Approval Delegation';
    final String RIVERBED_APPROVAL_PROCESS='Riverbed Approval Process';
        
    /*
    To check the duplicate delegated user for the same Geo/Region combination along with Active,Start Date 
    and End Date combination.
    By:Prashant.singh@ibm.com
    Date:09/28/2009
    */
    if(trigger.isBefore){
        set<ID> lstUser=new set<ID>();
        set<ID> lstDdetail=new set<ID>();
        set<string> lstRole=new set<string>();
        for(Delegated_Approver__c temp:trigger.new){
            lstUSer.add(temp.User__c);
            lstRole.add(temp.Role__c);
            lstDdetail.add(temp.delegation_detail__c);
        }
        Map<Id,User> mapUsers=new Map<Id,User>([Select Id, UserRoleId from User WHERE Id In:lstUser]);
        set<Id> usrRoleIds=new set<Id>();
        for(User usr:mapUsers.values()){
            usrRoleIds.add(usr.UserRoleId);
        }
        Map<Id,UserRole>  mapRoles=new Map<Id,UserRole>([Select Id, Name from UserRole Where Id In:usrRoleIds]);
        for(Delegated_Approver__c da:trigger.new){
            User uu=mapusers.get(da.User__c);
            UserRole uRole = mapRoles.get(uu.UserRoleId);
            if(uRole!=null) da.Role__c=uRole.Name;
        } 
        /* Start:Commented by prashant.singh@riverbed.com on 03Nov2009  
        List<Delegated_Approver__c> lstTemp=[select User__c,Active__c,End_Date__c,Geo__c,
            Region__c,Role__c,Start_Date__c from Delegated_Approver__c where User__c in :lstUser and Role__c in :lstRole];
        */ //End:Commented by prashant.singh@riverbed.com on 03Nov2009
        
         // Start:Code change by prashant.singh@riverbed.com on 03Nov2009 
        //List<Delegated_Approver__c> lstTemp=[select User__c,Active__c,End_Date__c,Geo__c,
        //    Region__c,Role__c,Start_Date__c from Delegated_Approver__c where User__c in :lstUser];
        List<Delegated_Approver__c> lstTemp=[select User__c,Active__c,End_Date__c,Geo__c,delegation_detail__c,
            Region__c,Role__c,Start_Date__c from Delegated_Approver__c where delegation_detail__c in :lstDdetail and active__c=true];        
         
        //End:Code by prashant.singh@riverbed.com on 03Nov2009  
          if(trigger.isInsert){  
            Integer i=0;
            if(lstTemp.size()>0){
                for(Delegated_Approver__c temp:trigger.new){
                    if(temp.Geo__c!=null && temp.Region__c!=null){
                        if((temp.Active__c==lstTemp[i].Active__c)&&
                           (temp.End_Date__c==lstTemp[i].End_Date__c)&& 
                           (temp.Start_Date__c==lstTemp[i].Start_Date__c)|| (temp.Start_Date__c<=lstTemp[i].End_Date__c)&&
                           (temp.Geo__c.equalsIgnoreCase(lstTemp[i].Geo__c))&&
                           (temp.Region__c.equalsIgnoreCase(lstTemp[i].Region__c))){
                                  temp.addError('You are trying to create duplicate delegated approver record');
                        }
                    }else if(temp.Geo__c==null && temp.Region__c==null){
                        if((temp.Active__c==lstTemp[i].Active__c)&&
                           (temp.End_Date__c==lstTemp[i].End_Date__c)&&
                           (temp.Start_Date__c==lstTemp[i].Start_Date__c)|| (temp.Start_Date__c<=lstTemp[i].End_Date__c)){
                                  temp.addError('You are trying to create duplicate delegated approver record');
                        }    
                    }else if(temp.Geo__c==null){
                        if((temp.Active__c==lstTemp[i].Active__c)&&
                           (temp.End_Date__c==lstTemp[i].End_Date__c)&& 
                           (temp.Start_Date__c==lstTemp[i].Start_Date__c)|| (temp.Start_Date__c<=lstTemp[i].End_Date__c)&&               
                           (temp.Region__c.equalsIgnoreCase(lstTemp[i].Region__c))){
                                      temp.addError('You are trying to create duplicate delegated approver record');
                        }
                    }else if(temp.Region__c==null){
                        if((temp.Active__c==lstTemp[i].Active__c)&&
                           (temp.End_Date__c==lstTemp[i].End_Date__c)&& 
                           (temp.Start_Date__c==lstTemp[i].Start_Date__c)|| (temp.Start_Date__c<=lstTemp[i].End_Date__c)&&
                           (temp.Geo__c.equalsIgnoreCase(lstTemp[i].Geo__c))){
                                  temp.addError('You are trying to create duplicate delegated approver record');
                        }
                    }
                    i++;
                }
            }
          }
    }
    //Added by prashant.singh@riverbed.com on 10/17/2009
    if(trigger.isAfter){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new list<String>();
        List<Note > lstNotes = new List<Note >();
        //todo need to modify to handle bulk operation
        for(Delegated_Approver__c da:trigger.new){
            List<Delegated_Approver__c> daus=[Select d.User__r.Email,d.Delegation_Detail__r.Requester__r.Name,d.User__r.UserRoleId,d.User__r.Name from Delegated_Approver__c d WHERE id =:da.Id];
        
            if(daus.size()>0){
                Delegated_Approver__c dau=daus.get(0);
                toAddresses.add(dau.User__r.Email);
                mail.setToAddresses(toAddresses);
                mail.setSenderDisplayName(RIVERBED_APPROVAL_PROCESS);
                mail.setSubject(APPROVAL_DELEGATION);
                String mailbody='You are the delegated user for '+dau.Delegation_Detail__r.Requester__r.Name+' from date '+da.Start_Date__c.month()+'/'+da.Start_Date__c.day()+'/'+ da.Start_Date__c.year()+' to ' +da.End_Date__c.month()+'/'+da.End_Date__c.day()+'/'+ da.End_Date__c.year();      
                mail.setPlainTextBody(mailbody);
                mails.add(mail);
                
                //Attachments/Notes 
                Note notes=new Note();
                
                String email='From:'+ RIVERBED_APPROVAL_PROCESS+'[mailto:'+dau.User__r.Email+']'+'\n'+
                        'Sent: '+DateTime.now()+ 'To: ['+ dau.User__r.Email +
                        +']\n Subject:'+  APPROVAL_DELEGATION + '\nE-Mail Body:'+mailbody;//Modified email:by prashant.singh@riverbed.com on 03Nov2009
                        
                //notes.Title=APPROVAL_DELEGATION; //Commented by prashant.singh@riverbed.com on 03Nov2009
                notes.Title=dau.User__r.Name+' '+APPROVAL_DELEGATION;
                notes.OwnerId=UserInfo.getUserId();
                notes.Body=email;
                notes.ParentId=da.Delegation_Detail__c;
                //insert notes;
                lstNotes.add(notes);
            }
        }        
        if(lstNotes.size()>0){
            try{
                insert lstNotes;
            }catch(DMLException dme){
                system.debug('Exception::'+dme);
            }
        }
        if(mails.size()>0){
            Messaging.sendEmail(mails);
        }       
    } // End of isAfter
}