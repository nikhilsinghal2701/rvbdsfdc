trigger DistributorResellerType on Distributor_Reseller__c (before insert,before update,after insert,after update) {
    Integer i=0;
    Set<Id> accountIds=new Set<Id>();
    List<Distributor_Reseller__c> lstDistRes=new List<Distributor_Reseller__c>();
    Map<Id,Account> mapAccount=new Map<Id,Account>();
    Map<Id,String> mapRecordType=new Map<Id,String>();
    for(RecordType rType:[Select r.Id, r.Name, r.SobjectType from RecordType r where sobjectType='Account']){
        mapRecordType.put(rType.Id,rType.Name);
    }
    for(Distributor_Reseller__c distRes:trigger.new){
        accountIds.add(distRes.Distributor_Reseller_Name__c);
    }
    for(Account account:[Select a.Id, a.IsPartner, a.Name, a.Partner_Level__c, a.Type, a.RecordTypeId from Account a where a.Id in:accountIds]){
       if((mapRecordType.get(account.RecordTypeId).equalsIgnoreCase('Partner Account'))){
             mapAccount.put(account.Id,account);
        }       
    }
    if(trigger.isBefore){
        if(trigger.isInsert){
           for(Distributor_Reseller__c tempDistRes:trigger.new){
              if(mapAccount.containsKey(tempDistRes.Distributor_Reseller_Name__c)){
                  tempDistRes.Partner_Type__c=mapAccount.get(tempDistRes.Distributor_Reseller_Name__c).Type;
                }
           } 
        }
        if(trigger.isUpdate){
            for(Distributor_Reseller__c upDistRes:trigger.new){
                if(upDistRes.Distributor_Reseller_Name__c!=trigger.old[i].Distributor_Reseller_Name__c){
                    if(mapAccount.containsKey(upDistRes.Distributor_Reseller_Name__c)){
                       upDistRes.Partner_Type__c=mapAccount.get(upDistRes.Distributor_Reseller_Name__c).Type;
                    }
                }else if(upDistRes.Partner_Type__c==null||upDistRes.Partner_Type__c==''){
                    if(mapAccount.containsKey(upDistRes.Distributor_Reseller_Name__c)){
                       upDistRes.Partner_Type__c=mapAccount.get(upDistRes.Distributor_Reseller_Name__c).Type;
                    }
                }
                i++;
            }
        
        }
    }
    if(trigger.isAfter){
    	try{
    		AccountShareHelper asHelper = new AccountShareHelper();
    		asHelper.insertIntoAccountShare(trigger.new);
    	}catch(NullPointerException npe){
    		trigger.new[0].addError('Error: Either Distributor or Reseller is not enabled as partner, kindly contact channels team or System Administrator.');
    	}catch(Exception e){
    		trigger.new[0].addError(e);
    	}
    }
}