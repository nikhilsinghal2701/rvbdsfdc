//*****************************************************************************************
//      Trigger on account: If custom address is null and billing address is not nul      *
//      populate the custom address with billing address                                  *
//                                                                                        *
//     Created on: 11/24/2012                        Modified on: 01/05/2013              *
// *****************************************************************************************
trigger BillingAddPopulateOnCustomAddress  on Account (before insert, before update) {
   
      Map<String,String> countryToISOCodeMap=new Map<String,String>();// mapp of country name to ISO code
      Map<String,String> ISOCodeToCountryMap=new Map<String,String>();// map of ISO code to country name
      Map<String,String> StateMap=new Map<String,String>();
      List<Account> accList = new List<Account>();
      if(trigger.isInsert){
        for(Account acct : Trigger.new){
            //on insert if billing address is not null and custom address is null
           if((acct.OS_Address_1__c==null && acct.OS_City__c==null && 
                acct.OS_State__c==null &&  acct.D_B_Country__c==null  &&
                acct.D_B_Postal_Code__c==null) &&  
              ( acct.BillingStreet !=null || acct.BillingCity !=null || 
               acct.BillingState !=null || acct.BillingPostalCode !=null ||
               acct.BillingCountry !=null  )) {
                    accList.add(acct);
           }
        }
      }else if (trigger.isUpdate){
        for(Account acct : trigger.new){
            Account oldAcc = trigger.oldMap.get(acct.id);
            //on update check if there is change in billing address and custom address is null
            if((acct.BillingStreet !=oldAcc.BillingStreet || acct.BillingCity !=oldAcc.BillingCity || 
               acct.BillingState !=oldAcc.BillingState || acct.BillingPostalCode !=oldAcc.BillingPostalCode  ||
               acct.BillingCountry !=oldAcc.BillingCountry)&& 
               (acct.OS_Address_1__c==null && acct.OS_City__c==null && 
                acct.OS_State__c==null &&  acct.D_B_Country__c==null  &&
                acct.D_B_Postal_Code__c==null)){
                    accList.add(acct);
                }
        }
      }
      
      if(accList.size() > 0){
          for(Country__c ct :  [select name,ISO2__c from Country__c]){
             countryToISOCodeMap.put (ct.Name.toUppercase(),ct.ISO2__c);
             ISOCodeToCountryMap.put (ct.ISO2__c,ct.Name.toUppercase());
          }
          for(States__c st :  [select name,Id__c from States__c]){
             StateMap.put (st.Name.toUppercase(),st.Id__c);
          }
         for(Account acct : accList){
            //Moving Billing Address to CustomAddress
                acct.OS_Address_1__c = acct.BillingStreet;
                acct.OS_City__c = acct.BillingCity;
                acct.OS_Postal_Code__c = acct.BillingPostalCode;
             if(acct.BillingState!=null){ //Added on 1.28.2013 to fix null pointer exception
                 if(StateMap.containsKey(acct.BillingState.toUpperCase())){
                    acct.OS_State__c = StateMap.get(acct.BillingState.toUpperCase());
                 }else {               
                    acct.OS_State__c = acct.BillingState;
                 }
             }
            if(acct.BillingCountry !=null){
             System.debug('Checkpoint1'+acct.BillingCountry+'**'+ ISOCodeToCountryMap.get( acct.BillingCountry.toUpperCase()));   
               if(countryToISOCodeMap.containsKey(acct.BillingCountry.toUpperCase())){
                    acct.D_B_Country__c =acct.BillingCountry;
                    acct.ISO_Country_Code__c = countryToISOCodeMap.get( acct.BillingCountry.toUpperCase());
                }else if (ISOCodeToCountryMap.containsKey(acct.BillingCountry.toUpperCase())){
                      acct.ISO_Country_Code__c = acct.BillingCountry.toUpperCase();
                      acct.D_B_Country__c = ISOCodeToCountryMap.get( acct.BillingCountry.toUpperCase());
            }
            }
        
        }
      }
 }