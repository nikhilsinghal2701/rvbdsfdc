trigger PartnerSelfServiceAccount on Account (before update, after update) {
        if(trigger.isBefore) {
                if(trigger.isUpdate){
                        for (Account a : trigger.new){
                                Account oldRecord = trigger.oldMap.get(a.Id);
                                if (a.IsPartner && !oldRecord.IsPartner)  a.Remaining_Partner_Users__c = a.Partner_User_Count__c;   // by Sukhdeep Singh on 30-Aug-2013 # 140917
                                if (a.Partner_User_Count__c != trigger.oldMap.get(a.Id).Partner_User_Count__c){
                                        Double current = (a.Remaining_Partner_Users__c == null)? 0: a.Remaining_Partner_Users__c;
                                        Double old = (trigger.oldMap.get(a.Id).Partner_User_Count__c  == null)? 0: trigger.oldMap.get(a.Id).Partner_User_Count__c ;
                                        //Integer ld =
                                        a.Remaining_Partner_Users__c =  current - (old - a.Partner_User_Count__c );
                                }
                                //added by Ankita 6/17/2010 for LMS account feed
                                if(a.RecordTypeId == '012300000000NE5' && Util.hasChanges(Util.accFieldNamesForLMS, oldRecord, a)){
                                        //set flag
                                        a.Send_to_LMS__c = true;
                                }
                        }
                        //Update account according to the partner level config setting
                        AccountTriggerHandler.accountUpdateMain(trigger.oldMap, trigger.newMap, false);
                }
        } else {
                if(trigger.isUpdate) {
                        //update opportunity fields based on changes of account
                        AccountTriggerHandler.accountUpdateMain(trigger.oldMap, trigger.newMap, true);
                        AccountTriggerHandler.updateCertSummariesOnChangeOfAccTypeORAuthSpecFields( trigger.oldMap, trigger.new ); //- commented by Rashmi - as this method is not doing anything
                        AccountTriggerHandler.probationEmailNotificationSending(trigger.oldMap, trigger.newMap);
                }
        }

}