/*
*Purpose: Send alert to new hire partner
*By: sukhdeep.singh@riverbed.com
*Date: 11/13/2013
*Ticket # 159409
*/
trigger WelcomeAlertToNewHirePartner on User (after insert) {
    if (trigger.new[0].IsPortalEnabled)
    {   String email = trigger.new[0].email;
        User u1 = [select u.id, u.Name, u.contact.name, u.accountId from User u where u.Id = :trigger.new[0].Id];
        //Contact c1 = [select c.id, c.Name, c.account.name from Contact c where c.Id = :u1.ContactId];
        //Account a1 = [select a.id, a.Name from Account a where a.Id = :c1.AccountId];
        list<User> userList = [select u.id, u.Name from User u where u.accountId = :u1.accountId and u.IsActive = true];    // By Sukhdeep Singh on 13/05/2014 (Ticket# 203477)
        //list<Contact> conList = [select cc.id, cc.Name from Contact cc where cc.account.Id = :u1.accountId];
        if (userList.size() < 2)
        {
            System.ResetPasswordResult sr = System.resetPassword(trigger.new[0].id, true);
            u1.Id = trigger.new[0].Id;
            u1.Temporary_Password__c = sr.getPassword();
        }
    }
     //  IsPortalEnabled after insert
}