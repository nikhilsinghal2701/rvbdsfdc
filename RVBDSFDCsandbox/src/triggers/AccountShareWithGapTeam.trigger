trigger AccountShareWithGapTeam on GAP_Team__c (after insert, after update) {
	if(trigger.isAfter){
		AccountGapTeamVisibility.insertIntoAccountShare(trigger.new);	
	}
}