trigger checkFieldsBeforeReassignment on Case (before update) 
{
    Boolean missingData = false;
    
    for(Case c: trigger.new)
    {
        if((c.Status == 'Development') || (c.Status == '3rd Line'))
        {
            if(c.Platform__c == null)
            {
                missingData = true;
                c.Platform__c.addError('Please add platform');
            }
            
            if(c.Products__c == null)
            {
                missingData = true;
                c.Products__c.addError('Please add product');
            }
         
            if(c.Version__c == null)
            {
                missingData = true;
                c.Version__c.addError('Please add version');
            }
        }
        
        if((c.Status == 'Awaiting bug fix/implementation') && (c.BugNumber__c== null))
        {
            missingData = true;
            c.BugNumber__c.addError('Please add a bug number');
        }
        
        if(missingData) c.addError('Please complete the missing information before changing status...');
    }
}