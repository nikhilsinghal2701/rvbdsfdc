/*
This trigger handles certification resassingments when a contact is moved from account to another.
*/
trigger ContactAccountChange on Contact (after update) {
    
    list<String> contactIdList = new list<String>();
    list<string> accountIdList = new list<String>();
    set<Id>accountIds=new set<Id>();
    list<Certificate__c> certs = new List<Certificate__c>();
    list<Acct_Cert_Summary__c> certSummaries = new List<Acct_Cert_Summary__c>();
    //get list of contacts that have changed account. 
    //also get new account ids
    for(Contact c: trigger.new){
        if((c.AccountId != trigger.oldMap.get(c.id).AccountId)&&c.accountId!=null){
            contactIdList.add(c.id);
            accountIdList.add(c.AccountId);
            accountIds.add(c.AccountId);
            accountIds.add(trigger.oldMap.get(c.id).AccountId);
        }
    }
    if(contactIdList.size() > 0){
    //get certifications
         certs = [Select c.Contact__c, c.Certification_Summary__r.TAP__c, 
                                        c.Certification_Summary__r.Id, c.Certification_Summary__c,
                                        c.Contact__r.AccountId
                                        From Certificate__c c
                                        where Contact__c in :contactIdList];
    }
    if(accountIdList.size() > 0){
    //get certification summaries
        certSummaries = [Select a.TAP__c, a.Id, a.Account__c 
                                                    From Acct_Cert_Summary__c a
                                                    where Account__c in :accountIdList];
    }
    if(certSummaries.size() > 0){
        //make map of maps accountid-->map of Tap__c-->summaryid
        map<string, map<string, string>> accountMap = new map<string, map<string, string>>();
        map<string, string> acctSum;
        for(Acct_Cert_Summary__c acs : certSummaries){
            acctSum = accountMap.get(acs.Account__c);
            if(acctSum == null) acctSum = new map<string, string>();
            acctSum.put(acs.Tap__c, acs.id);
            accountMap.put(acs.Account__c, acctSum);
        }
        if(certs.size() > 0){
            //set new certification summary values on certs. 
            for(Certificate__c c : certs){
                acctSum = AccountMap.get(c.Contact__r.AccountId);
                try{
                    c.Certification_Summary__c = acctSum.get(c.Certification_Summary__r.TAP__c);
                }catch(exception e){
                    e.setmessage('Error: cannot reassign the contact since it contains certification and the destination account does not have certification summary records.');
                    throw e;
                }
            }
            
            //update certs
            update certs;
        }
    }
    if(!accountIds.isEmpty() && accountIds.size()>0){
        //System.debug('yes we came here***********'+accountIds);
        CertificationTriggerHandler.recalculateTotalCertificationOnContactAccountChange(accountIds);
    }
}