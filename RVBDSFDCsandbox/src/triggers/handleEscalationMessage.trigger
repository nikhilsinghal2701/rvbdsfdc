trigger handleEscalationMessage on Case (before update,after update) 
{
    /* New code for bulk handling.
    ** by Prashant.singh@riverbed.com on 10/16/2011 
    */
    if(trigger.isBefore && trigger.isUpdate){
		for(Case newCase:trigger.new){
    		if(newCase.EscalationMessage__c != null && 
	       	((trigger.OldMap.get(newCase.Id).Status != 'Development' && newCase.Status == 'Development') || (trigger.oldMap.get(newCase.Id).Status != '3rd Line' && newCase.Status == '3rd Line'))
	       	){
	       		newCase.EscalationMessage__c = '';
	       }
		}	
	}else if(trigger.isAfter && trigger.isUpdate){
		ZeusUtility.handleEscalationMessage(trigger.new,trigger.oldMap);		
	}
    /* Zeus original code: commented by prashant.singh@riverbed.com on 10/16/2011
    Case oldCase = Trigger.old[0];
    Case newCase = Trigger.new[0];
    
    if(newCase.EscalationMessage__c != null && 
       ((oldCase.Status != 'Development' && newCase.Status == 'Development') || (oldCase.Status != '3rd Line' && newCase.Status == '3rd Line'))
       ) {
        CaseComment cc = new CaseComment();
        cc.ParentId = newCase.Id;
        cc.CommentBody = '*** Escalation message ***\n\n' + newCase.EscalationMessage__c;
        cc.IsPublished = false;
        upsert(cc);
        
        String myId = newCase.Id;
        String ref = 'ref:00D2NO4.' + myId.substring(0, 4) + myId.substring(10, 15) + ':ref';  // LIVE SYSTEM ONLY!

        String recipient = '';
        String urls = 'Cached version: http://devpage-sf-cache.support.cam.zeus.com/dev_page.php?mode=detail&id=' + newCase.CaseNumber + '\n' +
                      'Slow version: http://dev-support.cam.zeus.com/dev_page.php?mode=detail&id=' + newCase.CaseNumber + '\n\n';

        if (newCase.Status == 'Development') {
           recipient = 'dev@zeus.com';
        } else if (newCase.Status == '3rd Line') {
           recipient = 'support-3rd@zeus.com';
           urls = 'SF cached: http://emea-sf-cache.support.cam.zeus.com/' + myId + '\n' +
                  'SF direct: https://emea.salesforce.com/' + myId + '\n\n' + urls;
        }

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        Account a = [ SELECT Name FROM Account WHERE Id = :newCase.AccountId ];
     
        email.setReplyTo('support@zeus.com');
        email.setSaveAsActivity(false);
        String[] rcpt = new String[] { recipient };
        email.setToAddresses(rcpt);
        email.setSubject('Support escalation: ' + newCase.CaseNumber + ' [' + newCase.Subject + '] ');
        
        
        String msg = 'Ticket: ' + newCase.CaseNumber + '\n' +
                'Subject: ' + newCase.Subject + '\n' +
                'Account: ' + a.Name + '\n' +
                'Product: ' + newCase.Product__c + '\n' +
                'Platform: ' + newCase.Platform__c + '\n' +
                'Version: ' + newCase.Version__c + '\n\n' +
                newCase.EscalationMessage__c + '\n\n\n' +
                urls +
                ref;
                
        email.setPlainTextBody(msg);
        Messaging.SendEmailResult[] sendResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
        
        newCase.EscalationMessage__c = '';
    }//End: Zeus original code
    */
}