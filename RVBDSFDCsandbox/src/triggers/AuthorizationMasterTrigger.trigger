/*****************************************
  Purpose : Trigger on Authorizations_Master__c after insert    
  Created By : Rashmi  11/04/2013
  Modified By: Anil Madithati
  Date: 08/23/2014
  Purpose: To send an outbound to TIBCO to handle the record deletions ((Part of NPI::3.0))
  Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 
****************************************/ 
trigger AuthorizationMasterTrigger on Authorizations_Master__c (before delete, after insert) {

    List<ID> objIDs=new List<ID>();
    if( trigger.isAfter){
        AuthorizationMasterTriggerHandler.createCertificationSummaryRecords( trigger.new );
    }
        
    if(trigger.isBefore && trigger.isDelete){
        
        for( ID a:trigger.oldMap.keySet()){objIDs.add(a);}

          if(objIDs.size()>0)
          {
          if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
          system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
            }
          
          else {
            B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs); 
          }
        }
    }
}