/* Rivision History 
Sunil Kumar [PRFT] 10/03/2013: This trigger insert the new record into Account Authroziation object when Certification Summary fields changes as
TAP_Requirement_Satisfied__c equals 'yes.' AND Date_TAP_Req_Satisfied__c field is changed.
Modified by Jaya ( Perficient )
Modifing Method: calling CertRollup.rollupCert
Modifing purpose: to roll up certs to parent account and grand parent account
Modifing version Name:  Rev:1
*********************************/

trigger CertificationSummaryTapQualifyUpdateAccount on Acct_Cert_Summary__c (before update, after update) {
 	
 	// Rev:1 this condation check is to not execute the trigger logic during cert roll up batch process.
 	if(!Constant.disablePartnerCompetencyTrigger){
	 	if( trigger.isUpdate ){
	 		//New code added by Prashant on 01/30/2014
	 		/*if(trigger.isBefore){
				CertificationSummaryTriggerHandler.updateCertificationSummaryRecordsBasedOnTAPRequirementCriteria(trigger.oldMap,trigger.new);// this is not needed as the roll ups are calculated in 'AccountTriggerHandler.cls'
	 		}else*/ if( trigger.isAfter ){
	 			System.debug('oldMap:'+trigger.oldMap);
				System.debug('new:'+trigger.new); 
	 			// Rev:1 either increment or decrement the cert value in the roll up fields of the parent and grandparent.
	 			CertificationSummaryTriggerHandler.certificationRollup(trigger.oldMap, trigger.new);
				// roll down partner competency status from grandparent, parent to child by comparing their roll up fields to the child needed fields. 
				CertificationSummaryTriggerHandler.competencyRollDown(trigger.newMap);
				// End of Rev:1
				System.debug('oldMap:'+trigger.oldMap);
				System.debug('new:'+trigger.new); 
	 			//CertificationSummaryTriggerHandler.upsertAccountAuthorizationRecordsBasedOnTAPRequirementCriteria( trigger.oldMap, trigger.new );//==> account auth creation/ updating is handeled in competencyrolldown call on certRollup class.
	 			CertificationSummaryTriggerHandler.updatePartnerLevelOnRSACountChanged( trigger.oldMap, trigger.new, null);
	 			//CertificationSummaryTriggerHandler.updateAccountPartnerOrComplianceLevelOnCertificationCountChange( trigger.oldMap, trigger.new ); //==> Rev:1 commented as part of method merge updatePartnerLevelOnRSACountChanged = [updatePartnerLevelOnRSACountChanged + updateAccountPartnerOrComplianceLevelOnCertificationCountChange]
				
			}/*else{
				// code to fire on before update
				//CertificationSummaryTriggerHandler.clearTAPReqSatisfiedDate( trigger.new );
			}*/
	 	}
 	}
}