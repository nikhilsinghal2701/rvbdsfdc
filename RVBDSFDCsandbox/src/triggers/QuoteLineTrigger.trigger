/*************************************************************************************************************
 Author: Santoshi Mishra
 Purpose: This trigger is created on Quote_Line_Item__c
  for data processing.
 Created Date : Oct 2011
*************************************************************************************************************/
trigger QuoteLineTrigger on Quote_Line_Item__c (after insert,after update,before insert,before update) {

    if((trigger.isinsert ||trigger.isupdate)&& trigger.isafter)
    {
    /** This method is called to copy opportunity discount detail into Quote on quote insertion..*/
     DiscountManagementHelper.UpdatesDiscountDetailInQuote(trigger.New);
        System.debug('Inside quote line update-----');
           
    }  
    
    if(trigger.isbefore && trigger.isinsert){
        DiscountManagementHelper.beforeInsertUpliftCalculation(trigger.New);
    }
    
    if(trigger.isbefore && trigger.isupdate){
        DiscountManagementHelper.beforeUpdateUpliftCalculation(trigger.New,Trigger.oldMap);
    }

}