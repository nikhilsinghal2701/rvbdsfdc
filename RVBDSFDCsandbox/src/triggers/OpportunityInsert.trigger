trigger OpportunityInsert on Opportunity (after insert) 
{
    //Debug Output for current logged in user 
    System.debug('Current USer --> ' + Userinfo.getUserId());
    
//  if(Trigger.isInsert)
//  {   
        for (Opportunity o : Trigger.new) 
        {
            System.debug('Opportunity Id --> ' + o.id);
            
            // Check if the current looged in user is a part of sales Team 
            // Code needs to be changed Table --> UserTeamMember
/*          
            if (o.OwnerId != Userinfo.getUserId()) 
            {
                try
                {
                    // Add current user as the ISR to the opportunity.
                    Visible_to_Partners__c loggedUser = new Visible_to_Partners__c(Role__c='Logged User',PartnerName__c=Userinfo.getUserId(),Opportunity_Name__c=o.id,Opportunity_Access__c='Edit');
                    System.debug('Registering Partner to Sales Team --> ' + loggedUser);
                    insert loggedUser;
                }
                catch (DmlException e) 
                {
                    System.debug(e.getMessage());
                }
            }
*/      

            System.debug('Registering Partner Id --> ' + o.Registering_Partner__c);
            
            if (o.Registering_Partner__c != NULL)
            {
                   System.debug('Ordering Center Id --> ' + o.Ordering_Center__c);
                   
                   //Insert into Primary Opportunity Partner (Distribution Center)
                   
//                 try
//                  {
                        if (o.Ordering_Center__c != NULL)
                        {
                            //Populate the Distribution center as a PrimaryPartner
//                          try
//                          {
                                Partner op = new Partner(Role='VAR/Reseller',IsPrimary=true,AccountToId =o.Ordering_Center__c,OpportunityId=o.id);
                                System.debug('Opportunity Partner --> ' + op);
                                insert op;
//                          }
//                          catch (DmlException e) 
//                          {
//                              System.debug(e.getMessage());
//                          }
                        /*  
                            // Add to distributor to opportunity Sales Team
                            try
                            {
                                Visible_to_Partners__c vp = new Visible_to_Partners__c(Role__c='Partner OC',PartnerName__c=o.Registering_Partner_User__c,Opportunity_Name__c=o.id,Opportunity_Access__c='Edit');
                                System.debug('Registering Partner to Sales Team --> ' + vp);
                                insert vp;
                            }
                            catch (DmlException e) 
                            {
                                System.debug(e.getMessage());
                            }
                        */  
                        }
                        else
                        {
                            //As for direct reseller order distribution center will be null , populate Reseller Account as Primary Partner
//                          try
//                          {
                            if (o.Registering_Partner__c != o.AccountId)
                                {   
                                    Partner op = new Partner(Role='VAR/Reseller',IsPrimary=true,AccountToId =o.Registering_Partner__c,OpportunityId=o.id);
    //                              System.debug('Oportunity Partner --> ' + op);
                                    insert op;
                                }
//                          }
//                          catch (DmlException e) 
//                          {
//                              System.debug(e.getMessage());
//                          }
                        /*  
                            Visible_to_Partners__c vp = new Visible_to_Partners__c(Role__c='Partner EElse',PartnerName__c=o.Registering_Partner_User__c,Opportunity_Name__c=o.id,Opportunity_Access__c='Read');
                            System.debug('Registering Partner to Sales Team --> ' + vp);
                            
                            try
                            {
                                insert vp;
                            }
                            catch (DmlException e) 
                            {
                                System.debug(e.getMessage());
                            }
                        */  
                        }
//                  }
//                  catch (DmlException e) 
//                  {
//                      System.debug(e.getMessage());
//                  }
    
            }
        }
//  }
}

    /*  
                    ---> No need to add all users with Orders access in Partner Role. This will be managed by Role Hierarchy. Partial Code below <---
                               
                   //Query All contacts who can place "Orders"
                   
                   List<Contact> ContactUser = [Select Id from Contact where AccountId = :dc.Distributor_Ordering_Center__c and PartnerRole__c like '%orders%'];
                    
                    System.debug('ContactUser --> ' + ContactUser);
       
                   //Query all the active users based on the contact records
                   for(integer i=0; i<ContactUser.size(); i++)
                   {
                    List<User> partnerUser = [Select Id from user where ContactId = :ContactUser[i].Id];
                    //new User(OpportunityId = o.id,ContactId = ContactUser[i].ContactID,Role = ContactUser[i].Role);
                    System.debug('PartnerUser --> ' + partnerUser);
                   }
    */
                    
    /*              
                    for (Account a1 : accts) {
                    a1.Parent_Account_Name__c = a.Name;
                    System.Debug('a1= ' + a1 + ', a1.Parent_Account_Name= ' + a1.Parent_Account_Name__c + ', a1.Name= ' + a1.Name);
                    updatedaccts.add(a1);
                    }
                    update updatedaccts;
    */