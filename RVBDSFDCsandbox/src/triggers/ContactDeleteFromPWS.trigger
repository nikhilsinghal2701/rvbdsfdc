trigger ContactDeleteFromPWS on Contact (before delete, after insert, after update) {
    Set<String> fields = new Set<String>{'FirstName', 'LastName', 'Email', 'AccountId', 'User_Profile__c', 'Salutation', 'Title', 'Department', 'Fax', 'Phone', 'MobilePhone', 'AccountId', 'Renewal_Contact__c','PartnerRole__c'};
    Map<Id,Id> rmId = new Map<Id,Id>();
    
    if(trigger.isBefore && trigger.isDelete){
        B2BIntegrationUtil.contactDeleteService(trigger.oldMap.keySet());
        util.ContMergeReTouchChildRecords(trigger.oldMap.keySet()); 
    }else if(trigger.isInsert){
        
        B2BIntegrationUtil.generateContactWSDL(trigger.new, 'Insert', rmId);
    }else if(trigger.isUpdate){
        List<Contact> conList = new List<Contact>();    
        for(Contact c : trigger.new){
            for (String field : fields) {
                if (isChanged(c, trigger.oldMap.get(c.Id), field)) {
                    conList.add(c);
                    break;
                }
            }
        }
        System.debug('conList = :::::' + conList );
        if(conList != null && conList.size()>0){
            B2BIntegrationUtil.generateContactWSDL(trigger.new, 'Update', rmId);
        }
    }
    public Boolean isChanged(Contact c, Contact oldContact, String field) {
        System.debug('field = ' + field);
        Object newVal = c.get(field);
        Object oldVal = oldContact.get(field);
        return (checkNull(newVal) != checkNull(oldVal)); 
        
    }
    
    public String checkNull(Object val) {
        return (val == null) ? '' : String.valueOf(val);
    }


}