trigger LeadPartnerUser on Lead (before insert, before update, after update) {
    RecordType recType;
    Lead oldLead;
    Map<Id,User> mapUser=new Map<Id,User>();
    Map<Id,User> userInfomap =new Map<Id,User>();
    set<Id> contactIds=new set<Id>();
    set<Id> uIds = new Set<Id>();
    //system.debug('Trigger New:'+trigger.new); 
    Map<Id,RecordType> mapRecordType=new Map<Id,RecordType>([select id,name from RecordType where 
                                            SobjectType='lead']);
                                            
    system.debug('RecordType-->'+mapRecordType)     ;
    system.debug('UserType-->'+UserInfo.getUserType())  ;       
    
    if(trigger.isBefore){
        for(Lead lead:trigger.new){
            if(lead.Tier2_Partner_Contact__c!=null) contactIds.add(lead.Tier2_Partner_Contact__c);
            if(lead.Sold_To_Partner_Contact__c!=null) contactIds.add(lead.Sold_To_Partner_Contact__c);
            if(UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                uIds.add(UserInfo.getUserId());
            }
        }
        if(contactIds.size()>0 || uIds.size() > 0){
            for(User usr:[Select u.Id, u.ContactId, u.Name, u.Contact.Account.Name from User u  where u.ContactId in :contactIds or id in :uIds]){
                mapUser.put(usr.ContactId, usr);
                userInfomap.put(usr.Id,usr);
            }
        }   
        System.debug('userinfoMap = ' + userInfomap)    ;                           
        if(trigger.isInsert){
            for(Lead lead:trigger.new){
                if(mapRecordType.containsKey(lead.RecordTypeId)){
                    recType=mapRecordType.get(lead.RecordTypeId);
                    if(recType.Name.equalsIgnoreCase('Partner Recruitment') && UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                        System.debug('Setting preferred disti on partner lead: ' + userInfomap.get(UserInfo.getUserId()).Contact.Account.Name);
                        System.debug('Current User : ' + userInfomap.get(UserInfo.getUserId()));
                        try{
                        lead.Preferred_Distributor__c = userInfomap.get(UserInfo.getUserId()).Contact.Account.Name;
                        System.debug('Preferred_Distributor__c:  ' + lead.Preferred_Distributor__c );
                        }catch(Exception e){
                            System.debug('Exception in setting preferred disti: ' + e.getMessage());
                        }
                    }
                    if(recType.Name.equalsIgnoreCase('Deal Registration - Distributor')){
                        try{
                            //lead.Tier2_User__c=[Select u.Id, u.ContactId, u.Name from User u  where u.ContactId=:lead.Tier2_Partner_Contact__c].Id;
                            if(mapUser.containsKey(lead.Tier2_Partner_Contact__c)){
                                lead.Tier2_User__c=mapUser.get(lead.Tier2_Partner_Contact__c).Id;
                            }
                        }catch(Exception e){
                            system.debug('Exception occurs:'+e);
                        }
                    }else if(recType.Name.equalsIgnoreCase('Deal Registration')){
                        try{
                            //lead.Sold_To_Partner_User__c=[Select u.Id, u.ContactId, u.Name from User u  where u.ContactId=:lead.Sold_To_Partner_Contact__c].Id;
                            if(mapUser.containsKey(lead.Sold_to_Partner_Contact__c)){
                                lead.Sold_To_Partner_User__c=mapUser.get(lead.Sold_to_Partner_Contact__c).Id;
                            }
                            //system.debug('userId:'+mapUser.get(lead.Sold_to_Partner_Contact__c).Id);
                        }catch(Exception e){
                            system.debug('Exception occurs:'+e);
                        }
                    }
                }
            }
        }//End of isInsert
        if(trigger.isUpdate){
        Set<Id> approvedIds = new Set<Id>(); //added by Ankita 3/16 for updating approved lead to clear WF rule queue
            for(Lead lead:trigger.new){
                oldLead=trigger.oldMap.get(lead.Id);
                if(mapRecordType.containsKey(lead.RecordTypeId)){
                    recType=mapRecordType.get(lead.RecordTypeId);
                    /*if(recType.Name.equalsIgnoreCase('Partner Recruitment') && UserInfo.getUserType().equalsIgnoreCase('PowerPartner')){
                        System.debug('Setting preferred disti on partner lead: ' + userInfomap.get(UserInfo.getUserId()).Contact.Account.Name);
                        System.debug('Current User : ' + userInfomap.get(UserInfo.getUserId()));
                        try{
                        lead.Preferred_Distributor__c = userInfomap.get(UserInfo.getUserId()).Contact.Account.Name;
                        System.debug('Preferred_Distributor__c:  ' + lead.Preferred_Distributor__c );
                        }catch(Exception e){
                            System.debug('Exception in setting preferred disti: ' + e.getMessage());
                        }
                    }*/
	            	if(!oldLead.Pass_Lead_to_a_Partner__c && lead.Pass_Lead_to_a_Partner__c && (lead.Lead_Passed_by__c == null) ){
	            		lead.Lead_Passed_by__c=UserInfo.getUserId();//added by Anil/Ankita to capture lead passed by 6/8/2010
	            	}
                    if(recType.Name.equalsIgnoreCase('Deal Registration - Distributor')){
                        if(lead.Tier2_Partner_Contact__c!=oldLead.Tier2_Partner_Contact__c){
                            try{
                                //lead.Tier2_User__c=[Select u.Id, u.ContactId, u.Name from User u  where u.ContactId=:lead.Tier2_Partner_Contact__c].Id;
                                if(mapUser.containsKey(lead.Tier2_Partner_Contact__c)){
                                    lead.Tier2_User__c=mapUser.get(lead.Tier2_Partner_Contact__c).Id;
                                }
                            }catch(Exception e){
                                system.debug('Exception occurs:'+e);
                            }
                        }                       
                    }else if(recType.Name.equalsIgnoreCase('Deal Registration')){
                        if(lead.Sold_To_Partner_Contact__c!=oldLead.Sold_To_Partner_Contact__c){
                            try{
                                //lead.Sold_To_Partner_User__c=[Select u.Id, u.ContactId, u.Name from User u  where u.ContactId=:lead.Sold_To_Partner_Contact__c].Id;
                                if(mapUser.containsKey(lead.Sold_to_Partner_Contact__c)){
                                    lead.Sold_To_Partner_User__c=mapUser.get(lead.Sold_to_Partner_Contact__c).Id;
                                }
                                //system.debug('lead.Sold_To_Partner_User__c::'+lead.Sold_to_Partner_Contact__c);
                            }catch(Exception e){
                                system.debug('Exception occurs:'+e);
                            }
                        }                       
                    }
                    //added by Ankita 12/28/2009 - Disti 1B
                    if(recType.Name.equalsIgnoreCase('Deal Registration') || recType.Name.equalsIgnoreCase('Deal Registration - Distributor')){
                        if(!oldLead.Status.equalsIgnoreCase(lead.Status) && lead.Status.equalsIgnoreCase('Rejected') && 
                            lead.Rejection_Reason__c == null && LeadApprovalActions.country.contains(lead.Country) &&
                            !AddToSalesTeam.PROFILES.contains(UserInfo.getProfileId())){
                            //leadIds.add(l.Id);
                            System.debug('Rejecting lead due to = ' + lead.Rejection_Reason__c);
                            //lead.Rejection_Reason_Given__c = true;
                            lead.addError('Rejection reason is required for rejected deal registrations. Please click the back button and use Reject with reason button to reject the deal.');
                        }
                        if(!oldLead.Status.equalsIgnoreCase(lead.Status) && lead.Status.equalsIgnoreCase('Approved') && 
                            oldLead.Status.equalsIgnoreCase('Pending - More Info') && LeadApprovalActions.country.contains(lead.Country)){
                            //leadIds.add(l.Id);
                            System.debug('Cannot Approve a Lead in procecss ');
                            //lead.Rejection_Reason_Given__c = true;
                            lead.addError('This lead is pending more info from the partner. This cannot be approved until a reponse is received from the partner on the more information request.');
                        }
                        if(!oldLead.Status.equalsIgnoreCase(lead.Status) && (lead.status.equalsIgnoreCase('Approved') || lead.Status.equalsIgnoreCase('Rejected'))){
                            //leadIds.add(l.Id);
                            System.debug('Setting Lead Convert Status = ' + lead.Status);
                            lead.Lead_Convert_Status__c = lead.Status;
                        }
                        //added by Ankita 3/16 for updating approved lead to clear WF rule queue
                        if(!oldLead.Status.equalsIgnoreCase(lead.Status) && lead.status.equalsIgnoreCase('Approved')){
                            approvedIds.add(lead.Id);
                        }
                    }
        
                }//End of 1st If
            }//End of for           
            if(!approvedIds.isEmpty()){
                if(LeadApprovalActions.runOnceApproved()){
                    LeadApprovalActions.updateLeads(approvedIds);
                }
            }
            /*if(!leadIds.isEmpty()){
                LeadApprovalActions.setLeadConvertStatus(leadIds);
            }*/
        }//End of isUpdate  
    }else {//End of isBefore
        //After Update part
    //added to identify if the lead status changed to Submitted, add to set which is then passed on to future method to set  email isr field
        //Map <Id, Lead> leads = new Map <Id, Lead> ();
        Set<Id> leads = new Set<Id>();      
        List<Id> leadRecords = new List<Id>();  
        Set<Id> m_leadRecord = new Set<Id>();    
        m_leadRecord.addAll(leadRecords);        
        Set<Id> leadIds = new Set<Id>(); //used for setting email ISR field
        Set<Id> lIds = new Set<Id>(); //used for setting lead status = submitted : Ankita 3/11
        for(Lead l : trigger.new){
            if(mapRecordType.containsKey(l.RecordTypeId)){
                recType=mapRecordType.get(l.RecordTypeId);
                System.debug('Lead record type = ' + recType);
                if(recType.Name.equalsIgnoreCase('Deal Registration') || recType.Name.equalsIgnoreCase('Deal Registration - Distributor')){
                    if(l.Status.equals('Submitted') && l.Email_ISR__c == null){
                        System.debug('Set Email ISR');
                        leadIds.add(l.Id);
                    }
                    if(l.Auto_Reject_Deal__c){
                        LeadApprovalActions.approvalAction(l.Id, 'Reject', 'Rejecting deal registration request since no response received from partner');
                    }
            //added to add SoldToPartnerUser to the opp sales team - added by Ankita on 12/30/2009 Disti 1B
                    if (l.IsConverted && (l.ConvertedOpportunityId != null)) {                
                        System.debug('put Lead to further handling');                
                        //leads.put(l.Id, l);
                        leads.add(l.id);
                    }            
                    //added by Ankita 3/11 for setting lead status based on checkbox
                    if(l.isSubmitted__c && !l.Status.equalsIgnoreCase('Submitted')){
                        System.debug('Set status = Submitted');
                        lIds.add(l.id);
                    }
                }
                System.debug('Leads size:' + leads.size());        
            }
        }    
        if (leads.size() > 0){            
            if(!AddToSalesTeam.runOnce()){            
                //AddToSalesTeam pVis = new AddToSalesTeam(leads, Trigger.oldMap);
                AddToSalesTeam.addPartnerToSalesTeam(leads);            
                //AddToSalesTeam.flush();      
            }  
        }
        System.debug('LeadIds size = ' + leadIds.size());
        if(!leadIds.isEmpty()){
            if(LeadApprovalActions.runOnce() && !(system.isFuture() || system.isBatch())){
                LeadApprovalActions.setEmailISROnLead(leadIds);
            } else if (LeadApprovalActions.runOnce() && (system.isFuture() || system.isBatch())){
            	
                LeadApprovalActions.nonfuturesetEmailISROnLead(leadIds);
            }
        }
        if(!lIds.isEmpty()){
            System.debug('Setting status to submitted');
            if(LeadApprovalActions.runOnce() && !(system.isFuture() || system.isBatch())){
                LeadApprovalActions.setLeadSubmitted(lIds);
    } else if(LeadApprovalActions.runOnce() && (system.isFuture() || system.isBatch())){
                LeadApprovalActions.nonfuturesetLeadSubmitted(lIds);
    }
}
}
}//End of Trigger