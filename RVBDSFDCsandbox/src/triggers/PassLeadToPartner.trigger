trigger PassLeadToPartner on Lead (before update) {
    Set<Id> ownerIds = new Set<Id>();
    for(Lead l : trigger.new){
        if((l.RecordTypeId == '012300000000V6i') && (l.ownerId != trigger.oldMap.get(l.Id).ownerId)){//if lead is general lead and if owner has changed
            ownerIds.add(l.ownerid);
            ownerIds.add(trigger.oldMap.get(l.id).ownerId);
        }
    }
    Map<Id,User> userMap = new Map<Id,User>([select id, usertype from User where id in :ownerids]);
    
    for(Lead l : trigger.new){
        if(userMap.containsKey(l.ownerId) && userMap.containsKey(trigger.oldMap.get(l.Id).ownerId)){
            if(userMap.get(l.OwnerId).userType.equalsIgnoreCase('PowerPartner') && userMap.get(trigger.oldMap.get(l.Id).ownerId).userType.equalsIgnoreCase('Standard')){
                l.Pass_Lead_to_a_Partner__c = true;
            }
        }
    }
}