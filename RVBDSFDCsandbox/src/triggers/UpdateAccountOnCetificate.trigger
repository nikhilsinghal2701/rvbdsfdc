trigger UpdateAccountOnCetificate on Certificate__c (after insert, after update, before insert, 
before update) {
	if(Trigger.isBefore){
		for(Certificate__c cert:trigger.new){
			if(cert.Expiration_Date__c!=null && cert.Expiration_Date__c<system.today()){
				cert.IsExpired__c=true;
			}else if(cert.Expiration_Date__c!=null && cert.Expiration_Date__c>system.today()){
				cert.IsExpired__c=false;
			}
		}
	}
	if(Trigger.isAfter){
		//Util.updateTotalCertifiedUserOnAccount(trigger.new);
	}
}