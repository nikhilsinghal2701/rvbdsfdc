/*  Trigger to update child account based on parent account if the parent account is a GAP/GDAP account 
    identified by the Global Account Coverage field
    Logic:
    1. if parent account is updated, check if it is a GAP/GDAP account. 
        a. If yes, get all children and update them to set Global Account Alerts field from the parent.
    2. if new child is inserted, get the value of Global Account Alerts field from parent and set on child.      
*/
trigger GlobalAccountAlertsFieldUpdate on Account (After Update,Before Insert) 
{
    Try
    {
        List<Account> updatedChildAccounts = new List<Account>();
        Map<Id,Account> acMap = new Map<Id,Account>();
        Set<Id> parentIds = new Set<Id>();
       If(Trigger.isInsert){//child accnt insert : get parent account 
            for(Account a : trigger.new){
                if(a.parentId != null){
                    parentIds.add(a.ParentId);
                }
            }
            if(parentIds.size()>0){//added by prashant.singh@riverbed.com on 7/12/2012
                acMap = new Map<Id,Account>([select id, Global_Account_Alerts__c, Account_Coverage_Program__c from Account
                                            where id in :parentIds]);
                for(Account a : trigger.new){
                    if(a.ParentId != null){
                        Account pAcc = acMap.get(a.ParentId);
                        if(pAcc.Account_Coverage_Program__c!=null && (pAcc.Account_Coverage_Program__c.equalsIgnorecase('GAP') || pAcc.Account_Coverage_Program__c.equalsIgnorecase('GDAP'))){
                            a.Global_Account_Alerts__c = pAcc.global_Account_Alerts__c;
                        }
                    }
                } 
            }//added by prashant.singh@riverbed.com on 7/12/2012                          
       }
       If(Trigger.isupdate){ //parent account update case
            for(Account a : trigger.new){
            // added a check to verify whether Account_Coverage_Program__c field was updated or not 05.30.2014
                if(a.Account_Coverage_Program__c!=null && Trigger.newMap.get(a.Id).Account_Coverage_Program__c != Trigger.oldMap.get(a.Id).Account_Coverage_Program__c && (a.Account_Coverage_Program__c.equalsIgnoreCase('GAP') || a.Account_Coverage_Program__c.equalsIgnoreCase('GDAP'))){
                    parentIds.add(a.id);
                }
            }
            if(parentIds.size()>0){//added by prashant.singh@riverbed.com on 7/12/2012
                acMap =new Map<Id,Account>([Select id,Global_Account_Alerts__c,ParentID from Account where ParentID in :parentIds]);
                For(Account A : acMap.values()){
                    Account acc = trigger.newMap.get(A.parentId);
                    if(acc.Account_Coverage_Program__c.equalsIgnoreCase('GAP') || acc.Account_Coverage_Program__c.equalsIgnoreCase('GDAP')){
                        A.Global_Account_Alerts__c=acc.Global_Account_Alerts__c;
                        updatedChildAccounts.add(A);
                    }
                }
                update updatedChildAccounts ;
            }//added by prashant.singh@riverbed.com on 7/12/2012
        }
    }
    catch(Exception e)
    {
        System.debug('From GlobalAccountAlertsFieldUpdate Trigger:'+e.getMessage());
    }   
}