trigger changeStatusOnEscalation on Case (after insert, before update, after update) 
{

    if(Trigger.isAfter && Trigger.isInsert) {
        CaseTriggerHelper.doAfterInsert(trigger.new);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate) {
        CaseTriggerHelper.doBeforeUpdate(trigger.new, Trigger.oldMap);
    }
    
    if(Trigger.isAfter && Trigger.isUpdate) {
        CaseTriggerHelper.doAfterUpdate(trigger.new, Trigger.oldMap);
    }
    
    /* START:Original code of zeus. commented by Prashant.singh@riverbed.com on 10/14/2011  
    private void addComment(Id caseId, String msg)
    {
        CaseComment cc = new CaseComment();
        cc.ParentId = caseId;
        cc.CommentBody = msg;
        cc.IsPublished = false;
        
        insert(cc);
    }
    
    Case updatedState = trigger.new[0];
    Case preUpdate = trigger.old[0];    
    
    if(!preUpdate.isEscalated && updatedState.isEscalated)
    {
        if(updatedState.Further_Investigation__c == 'Yes')
        {
            updatedState.Further_Investigation__c = 'No';
            addComment(updatedState.Id, 'No further investigation has been undertaken in the required time');
        }
        
        if((updatedState.Status == 'Awaiting customer')
        || (updatedState.Status == 'Awaiting 3rd party'))
        {
            updatedState.Status = 'Active';
            addComment(updatedState.Id, 'This case has been in an *Awaiting* state for too long. Please chase it up.');
        }
    }
    //END: Original code of zeus*/  
    /* New code for bulk handling.
    ** by Prashant.singh@riverbed.com on 10/14/2011 
    */  
    if(trigger.isBefore && trigger.isUpdate){
        system.debug('Trigger.NEW:'+trigger.new);
        for(Case temp:trigger.new){
            if(trigger.oldMap.containsKey(temp.Id) && !trigger.oldMap.get(temp.Id).isEscalated && temp.IsEscalated){
                if(temp.Further_investigation__c.equalsIgnoreCase('Yes')){
                    temp.Further_investigation__c = 'No';
                }else if(temp.Status.equalsIgnoreCase('Awaiting customer')||temp.Status.equalsIgnoreCase('Awaiting 3rd party')){
                    temp.Status = 'Active';
                }
            }
        }
    }else if(trigger.isAfter && trigger.isUpdate){
        system.debug('***TriggerNew:'+trigger.new);
        system.debug('***TriggerOld:'+trigger.old);
        Map<Id,String> ccUpdateMap=new Map<Id,String>();
        String msg;
        List<Case> updatedState = trigger.new;
        Map<Id,Case> preUpdate = trigger.oldMap;
        for(Case temp:trigger.new){
            if(trigger.oldMap.containsKey(temp.Id) && !trigger.oldMap.get(temp.Id).isEscalated && temp.IsEscalated){
                system.debug('***After-inside changeStatusOnEscalation');
                if(temp.Further_investigation__c.equalsIgnoreCase('No')&& trigger.oldMap.get(temp.Id).Further_investigation__c.equalsIgnoreCase('Yes')){
                    system.debug('***Further-inside changeStatusOnEscalation');
                    msg='No further investigation has been undertaken in the required time';
                    ccUpdateMap.put(temp.Id,msg);
                }else if(temp.Status.equalsIgnoreCase('Awaiting customer')||temp.Status.equalsIgnoreCase('Awaiting 3rd party')&& trigger.oldMap.get(temp.Id).Status.equalsIgnoreCase('Active')){
                    system.debug('***Status-inside changeStatusOnEscalation');
                    msg='This case has been in an *Awaiting* state for too long. Please chase it up.';
                    ccUpdateMap.put(temp.Id,msg);
                }
            }
        }
        if(ccUpdateMap.size()>0){
            ZeusUtility.addComment(ccUpdateMap);
        }
    }    
}