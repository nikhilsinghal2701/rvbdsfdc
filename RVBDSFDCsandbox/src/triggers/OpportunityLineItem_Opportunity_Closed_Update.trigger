/* 
*Description:whenever an opportunity closes (changes to stage 6),to check the checkbox 
*on opportunity products called ‘opportunity closed’ (field name is ‘opportunity_closed__c’).
*Created By:Prashant.Singh@riverbed.com
*Created Date:Nov 01,2010
*/
trigger OpportunityLineItem_Opportunity_Closed_Update on Opportunity (after insert, after update) {
	set<ID> oppIds = new set<ID>();
	List<OpportunityLineItem> oppLineItemlist = new List<OpportunityLineItem>();
	if(trigger.isAfter){
		if(trigger.isUpdate && !(System.IsBatch() || System.IsFuture())){ 
			util.getOpportunityLineItem(trigger.new);
		}					
	}
}