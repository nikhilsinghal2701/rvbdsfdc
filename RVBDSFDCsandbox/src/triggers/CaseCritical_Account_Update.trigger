/***Modified By: Anil Madithati
**Date: 08/23/2014
**Purpose: To call EvalEndDateHTTPCallOut class to Retrieve the latest eval end date from Key Server on asset ((Part of SE::2.0))
*/     
Trigger CaseCritical_Account_Update on case (after insert,after update){
set<ID> NewCaseAcctID=New set<ID>();
set<ID> UpdCaseAcctID=New set<ID>();
Set<Id> Ids=new Set<Id>();
//string RecordtypeId;
list<ID> accountIDs=new List<ID>();
//Used Describe call to get the RecordtypeId to SOQL query By Anil madithati 08.04.2014
  String recordTypeName = 'Critical Account';
  Map<String,Schema.RecordTypeInfo> rtMapByName = Schema.SObjectType.Case.getRecordTypeInfosByName();
  Schema.RecordTypeInfo rtInfo =  rtMapByName.get('Critical Account');
  id recordTypeId = rtInfo.getRecordTypeId();
 //RecordtypeId=[Select r.Id from RecordType r where SobjectType ='Case'and Name='Critical Account'].id;
      
    for(Case c :Trigger.New)
    {   
            if((Trigger.IsInsert) && (c.RecordtypeID==recordTypeId)){
                NewCaseAcctID.add(c.Critical_Account__c);
                accountIDs.add(c.Critical_Account__c);
            }
            else if((Trigger.IsInsert)&& c.assetId!=null){
                system.debug('asset ****'+c.assetId);
                Ids.add(c.assetId); 
            }
            else if((Trigger.Isupdate) && (c.RecordtypeID==recordTypeId) && (c.status=='Closed - Resolved'||c.status=='Closed - Unresolved'||c.status=='Closed - Duplicate' )){
                UpdCaseAcctID.add(c.Critical_Account__c);
                accountIDs.add(c.Critical_Account__c);
            }  
    }

    for (account acc :[select ID, critical_account__c from account where ID in :accountIDs]){
                    
         If( NewCaseAcctID.contains(acc.id)){
            acc.Critical_Account__c=true;
          }
          else{
            acc.Critical_Account__c=false;
          }
          Update acc;
    }
    
    if(Ids.Size()>0){
        if(Test.isRunningTest() && Limits.getFutureCalls() >= Limits.getLimitFutureCalls()){
            system.debug(LoggingLevel.Error, 'Future method limit reached. Skipping...');
         }
                
        else {EvalEndDateHTTPCallOut.parseJSONResponseFuture(Ids);}   
    }
}