/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 // Code Modified by Santoshi Mishra in Dec-2011 to increase the coverage.
@isTest
private class OnboardingTest {
    // Modified by Santoshi on Dec-21 to increase the test coverage of the class CreditApplicationExt
    static testMethod void postLeadConvertUnitTest() {
        Lead l = new Lead(firstname = 'TestFirst', lastName = 'TestLast', Company='TestCompany.com',
            email = 'testfirst.last@testcompany.com',
            Sales_Contact_Last_Name__c = 'testSalesContact',
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC',AnnualRevenue = 1200000, Number_Of_Employees__c = 200, 
            Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', 
            Secondary_Application__c = 'UDP',Phone='4154154156', Street = 'Remington Dr', City = 'San Francisco', 
            Country = 'US', State = 'CA', Preferred_Distributor__c = 'Arrow');

        l.recordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment' Limit 10].Id;
        insert l;
        
        Task_Template__c taskTemplate = new Task_Template__c(name = 'Test template');
        insert taskTemplate;
        
        List<Tasks__c> taskList = new List<Tasks__c>();
        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Credit', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Days', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Legal', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Weeks', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Tax', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Months', Document_ID__c = '123456789012345'));
        insert taskList;
        
        Task_Rule__c taskRule = new Task_Rule__c(name = 'Test Rule', Task_Template__c = taskTemplate.Id,
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC');
        insert taskRule;
        ConvertLeadWS.convertLead(l.Id);
        
        Credit_Application__c capp = new Credit_Application__c();
        insert capp;
        
        ApexPages.StandardController controller = new ApexPages.StandardController(capp);
        CreditApplicationExt ext = new CreditApplicationExt(controller);
        ext.getAccountReferences();
        ext.addAccountReference();
        ext.deleteAccountReference();
        ext.backToTask();
        ext.submitCreditApp();
        ext.getSCreditApp();
        ext.authorizeSubmit();
        ext.getShowNoDocumentCheckbox();
        ext.cancelSubmitCreditApp();
        ext.getCreditAppNotEditable();
        ext.getCreditAppEditable();
        ext.getPartnerUser();
        ext.saveAll();
               
        ApplicationTaskInboxController appController = new ApplicationTaskInboxController();
        appController.getApplicationTasks();
        appController.getDebug();
    }

    static testMethod void postLeadConvertWithNoTemplateUnitTest() {
        Lead l = new Lead(firstname = 'TestFirst', lastName = 'TestLast', Company='TestCompany.com',
            email = 'testfirst.last@testcompany.com',
            Sales_Contact_Last_Name__c = 'testSalesContact',
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC',AnnualRevenue = 1200000, Number_Of_Employees__c = 200, 
            Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', 
            Secondary_Application__c = 'UDP',Phone='4154154156', Street = 'Remington Dr', City = 'San Francisco', 
            Country = 'US', State = 'CA', Preferred_Distributor__c = 'Arrow');

        l.recordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment' Limit 10].Id;
        insert l;
        
        Task_Template__c taskTemplate = new Task_Template__c(name = 'Test template');
        insert taskTemplate;
        
        List<Tasks__c> taskList = new List<Tasks__c>();
        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Credit', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Days', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Legal', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Weeks', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Tax', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Months', Document_ID__c = '123456789012345'));
        insert taskList;
        
        Task_Rule__c taskRule = new Task_Rule__c(name = 'Test Rule', Task_Template__c = taskTemplate.Id,
            Partner_Level__c = 'Gold', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC');
        insert taskRule;
        ConvertLeadWS.convertLead(l.Id);
    }

    static testMethod void deactivateOneOffUserUnitTest() {
        Lead l = new Lead(firstname = 'TestFirst', lastName = 'TestLast', Company='TestCompany.com',
            email = 'testfirst.last@testcompany.com',
            Sales_Contact_Last_Name__c = 'testSalesContact',
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC',AnnualRevenue = 1200000, Number_Of_Employees__c = 200, 
            Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', 
            Secondary_Application__c = 'UDP',Phone='4154154156', Street = 'Remington Dr', City = 'San Francisco', 
            Country = 'US', State = 'CA', Preferred_Distributor__c = 'Arrow');

        l.recordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment' Limit 10].Id;
        insert l;
        
        Task_Template__c taskTemplate = new Task_Template__c(name = 'Test template');
        insert taskTemplate;
        
        List<Tasks__c> taskList = new List<Tasks__c>();
        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Credit', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Days', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Legal', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Weeks', Document_ID__c = '123456789012345'));

        taskList.add(new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Tax', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Months', Document_ID__c = '123456789012345'));
        insert taskList;
        
        Task_Rule__c taskRule = new Task_Rule__c(name = 'Test Rule', Task_Template__c = taskTemplate.Id,
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC');
        insert taskRule;
        ConvertLeadWS.convertLead(l.Id);

        Id accountId = [Select l.ConvertedAccountId From Lead l where Id = :l.Id].ConvertedAccountId;
        try {
            Partner_Application__c partnerApp = [Select Id, Application_Status__c From Partner_Application__c p where p.Account__c = :accountId];
            partnerApp.Application_Status__c = 'Approved';
            update partnerApp;
        } catch(Exception e) {}
        
     User u = [Select Id,AccountId from User where ContactId != null limit 1];
    
        List<String> ids =  new List<String>();
        ids.add(u.id);
       
        DeactivateOneOffUser.changeUserProfile(ids);
        DeactivateOneOffUser.deactivate(ids);
    }



    static testMethod void autoSubmitPartnerAppCategoryUnitTest() {
        Lead l = new Lead(firstname = 'TestFirst', lastName = 'TestLast', Company='TestCompany.com',
            email = 'testfirst.last@testcompany.com',
            Sales_Contact_Last_Name__c = 'testSalesContact',
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC',AnnualRevenue = 1200000, Number_Of_Employees__c = 200, 
            Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', 
            Secondary_Application__c = 'UDP',Phone='4154154156', Street = 'Remington Dr', City = 'San Francisco', 
            Country = 'US', State = 'CA', Preferred_Distributor__c = 'Arrow');

        l.recordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment' Limit 10].Id;
        insert l;
        
        Task_Template__c taskTemplate = new Task_Template__c(name = 'Test template');
        insert taskTemplate;
        
        
        Tasks__c t = new Tasks__c(name = 'Complete Credit Form', Attach_Credit_Application__c = true,
            Task_Category__c = 'Credit', Task_Template__c = taskTemplate.Id, Time__c = 5,
            Time_Unit__c = 'Days', Document_ID__c = '123456789012345');
        insert t;
        
        Task_Rule__c taskRule = new Task_Rule__c(name = 'Test Rule', Task_Template__c = taskTemplate.Id,
            Partner_Level__c = 'One-off', Partner_Type__c = 'VAR', 
            Provides_Level_1_2_Support__c = true,
            Purchase_through_a_Distributor__c = false,
            Geo__c = 'APAC');
        insert taskRule;
        ConvertLeadWS.convertLead(l.Id);

        Id accountId = [Select l.ConvertedAccountId From Lead l where Id = :l.Id].ConvertedAccountId;
        try {
            Partner_Application__c partnerApp = [Select Id, Application_Status__c From Partner_Application__c p where p.Account__c = :accountId];
            
            Map<Id, Partner_Application_Category__c> partnerAppCategories = new Map<Id, Partner_Application_Category__c>([Select Id From Partner_Application_Category__c p where p.Partner_Application__c = :partnerApp.Id]);
            List<Application_Task__c> appTaskList = [Select a.Id, a.Status__c From Application_Task__c a where a.Application_Category__c In :partnerAppCategories.keySet()];
            for (Application_Task__c appTask : appTaskList) {
                appTask.Status__c = 'Complete';
            }
                    
            update appTaskList;
        } catch (Exception e) {
            
        }
            
    }

    static testMethod void maintainPartnerShareUnitTest() {
        DiscountApproval.isTest = true;
        User u = [Select Id, Contact.AccountId from User where ContactId != null And isActive = true limit 1];
        Id accId = u.Contact.AccountId;
        Id profileId = [Select Id from profile where name = 'Partner User - Unregistered'].Id;
        u.profileId = profileId;
        update u;
        //Account acc = new Account(Id = accId);
        //update acc;
    }
}