global class SchedulerOpportunityCountUpdates implements Schedulable
{   
  global void execute(SchedulableContext SC) 
  {  
     BatchOpportunityCountRollup bocr = new BatchOpportunityCountRollup();
     Database.executeBatch(bocr,5);     
  }
}