@isTest
/* 
*Description:Test class for Trigger:'OpportunityLineItem_Opportunity_Closed_Update'.
*Created By:Prashant.Singh@riverbed.com
*Created Date:Nov 03,2010
*/
private class OpportunityLineItem_UpdateTest {
    static testMethod void myUnitTest() {
    	Opportunity testOp1 = new Opportunity(Name='Test',StageName='4 - Selected',CloseDate=Date.newInstance(2011, 01, 01),ownerid=userInfo.getUserId());
        insert testOp1; 
        /*
        Product2 testProduct1 = new Product2(Name = 'Test Stack Product 600x1000');
        testProduct1.CanUseRevenueSchedule = true;
        testProduct1.IsActive = true;   
        testProduct1.RevenueScheduleType = 'Repeat'; 
        testProduct1.RevenueInstallmentPeriod = 'Monthly'; 
        testProduct1.NumberOfRevenueInstallments = 5; 
        insert testProduct1;
        Pricebook2 pricebook = [select Id from Pricebook2 where isStandard = true]; 
       	PricebookEntry testPricebookEntry1 = new PriceBookEntry(UnitPrice = 200,  Product2Id = testProduct1.Id, Pricebook2Id = pricebook.Id,  IsActive =true);           
        insert testPricebookEntry1; */
        PricebookEntry[] testPricebookEntry1=[Select UnitPrice, ProductCode, Product2Id, Pricebook2Id, Name, IsActive, Id From PricebookEntry where ProductCode like '%SHA-07050%' Limit 5];   
        OpportunityLineItem testOpLI1 = new OpportunityLineItem(OpportunityId=testOp1.Id);
        testOpLI1.Opportunity_Closed__c=false;
        testOpLI1.TotalPrice=2230;
        testOpLI1.Quantity=1;
        testOpLI1.Discount_Percentage__c=12;
        testOpLI1.PricebookEntryId = testPricebookEntry1[0].Id;
        insert testOpLI1;
        testOp1.StageName='6 - Order Accepted';
        update testOp1;
    }
}