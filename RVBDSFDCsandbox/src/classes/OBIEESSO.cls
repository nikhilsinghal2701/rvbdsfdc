public class  OBIEESSO {

 
   
    public string username{
  
     get{
        User  user1=[Select u.Username,u.email, u.ContactId, u.Contact.AccountId, u.Contact.FirstName, u.Contact.Id, u.Contact.LastName from User u where Id=:userInfo.getUserId()];
        String serverUrl=URL.getSalesforceBaseUrl().toExternalForm()+'/services/Soap/u/27.0/'+UserInfo.getOrganizationId();
        Blob input = Blob.valueOf('username'+'='+user1.username+'sessionId'+'='+userInfo.getSessionId()+'serverUrl'+'='+serverUrl);
        String encodedtoken=EncodingUtil.base64Encode(input);
        return encodedtoken;
        }
    set;
    }
    public string rvbdEmail{
    get{
    String rvbdEmail;
     RVBD_Email_Properties__c rvbdEmailProp=[Select id,name,Server_URL__c from RVBD_Email_Properties__c where name=:'OBISSO'];
      if(rvbdEmailProp.Server_URL__c!=null)
        rvbdEmail=rvbdEmailProp.Server_URL__c;      
     return rvbdEmail;
     }
     set;
    } 
    public OBIEESSO(){     
    }   
     //Test Method for OBIEESSO Controller 
   public static testMethod void testOBIEESSO(){
        Profile profile = [SELECT Name,id FROM Profile where Name=:'System Administrator'];
        List<User> user=[Select name,id from User where ProfileId=:profile.id AND isActive=:true];
        User me=user[0];
        RVBD_Email_Properties__c rvbdEmailProp=new RVBD_Email_Properties__c();
        rvbdEmailProp.name='OBISSO';
        rvbdEmailProp.Server_URL__c='http://rvbd-365-oap07.itciss.com:9704/analytics-sfdc';
        insert rvbdEmailProp;
        system.runas(me){
        PageReference pageRef = Page.OBIEEIframe;
         Test.setCurrentPage(pageRef);
         OBIEESSO osso=new OBIEESSO();
         string x = osso.username;
         string y = osso.rvbdEmail;
       
        }  
    }    
}