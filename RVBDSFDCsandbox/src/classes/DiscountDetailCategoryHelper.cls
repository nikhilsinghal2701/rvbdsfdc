/*
    Class: DiscountDetailCategoryHelper
    Purpose: checks every qoute line Item for the rangee from custom settings DiscountDetailCategoryRange__c  
    Created Date: 9/12/2014
    Author: Jaya
    Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014
*/
public without sharing class DiscountDetailCategoryHelper {
    
    public static final String DEAL_DESK = 'Deal Desk Queue'; 
    public static final String QUOTE_APPROVED_STATUS = 'Approved';
    public static final String QUOTE_PENDING_STATUS = 'Pending Approval';
    public static final String DEAL_DESK_APPROVED = 'Approved';
    public static final String DEAL_DESK_PENDING = 'Pending';
    /*
        Check for Discount Range and High risk SKU
    */
    public static List<Quote_Line_Item__c> validateDiscountRange(List<Quote_Line_Item__c> quoteLineItemList){
        system.debug(LoggingLevel.INFO,'method invoked***');
        List<Quote_Line_Item__c> qlis = new List<Quote_Line_Item__c>();
        Map<Id,Id> hrSKUMap = new Map<Id,Id>();
        hrSKUMap = getHighRiskSKU();
       
        system.debug(LoggingLevel.INFO,'hrSKUMap:'+ hrSKUMap);
        for(Quote_Line_Item__c qli: quoteLineItemList){
            
            DiscountDetailCategoryRange__c ddRange = new DiscountDetailCategoryRange__c();
            
            if(qli.Category__c=='B' || qli.Category__c=='K'){
                string cat= getCategory(qli);
                system.debug(LoggingLevel.INFO,'getCategory***'+ getCategory(qli));
                ddRange = cat!= null ? DiscountDetailCategoryRange__c.getValues(cat):ddRange;
                system.debug(LoggingLevel.INFO,' ddRange***'+ ddRange);
            
            //Decimal distiApprovedDiscount= qli.D_Non_Standard_Discount__c != null? qli.D_Non_Standard_Discount__c:qli.D_Standard_Discount__c+qli.Additional_Discount__c;
            
                Decimal distiApprovedDiscount = getDistiApprovedDiscount(qli);
                String categoryRange = ddRange != null && ddRange.Range__c != null ? ddRange.Range__c : '';
                system.debug(LoggingLevel.INFO,'categoryRange***'+categoryRange);
                system.debug(LoggingLevel.INFO,'distiApprovedDiscount***'+ distiApprovedDiscount);
                
           
            if(categoryRange != '' && categoryRange !=null ){
                
                system.debug(LoggingLevel.INFO,'inside****');
                String[] splitRange = new List<String>();
                splitRange = categoryRange.split('-');
                Decimal min = Decimal.valueOf(splitRange[0].trim());
                Decimal max = Decimal.valueOf(splitRange[1].trim());
                
                system.debug(LoggingLevel.INFO,'min ****::'+ min);
                system.debug(LoggingLevel.INFO,'max ****::' + max);
                //if(qli.Disti_Approved_Discount__c >= min && qli.Disti_Approved_Discount__c <= max){ //By Anil 11.04.2014
                if(distiApprovedDiscount >= min && distiApprovedDiscount <= max){
                   
                    qli.VSOE_NON_Compliant__c = qli.VSOE_NON_Compliant__c==True?False:False;
                }
                else
                if((distiApprovedDiscount < min && distiApprovedDiscount!=0) || distiApprovedDiscount > max){
                    qli.VSOE_NON_Compliant__c = true;
                    system.debug(LoggingLevel.INFO,'Compliant = true ****::');
                   //qli.VSOE_NON_Compliant__c = false;
                } 
                else
                if(distiApprovedDiscount==0 && qli.VSOE_NON_Compliant__c == true ){
                    qli.VSOE_NON_Compliant__c = False;
                    system.debug(LoggingLevel.INFO,'Compliant = true ****::');
                   //qli.VSOE_NON_Compliant__c = false;
                } 
            }
            }
               if(qli.Category__c==null && qli.VSOE_NON_Compliant__c == true){
                  qli.VSOE_NON_Compliant__c = False;
                  system.debug(LoggingLevel.INFO,'Blank Category****::');
               }
            
                // high risk flag set
                if(!hrSKUMap.isEmpty()){
                system.debug(LoggingLevel.INFO,'inside HRSK****:');
                qli.Is_High_Risk_SKU__c = hrSKUMap.containskey(qli.Product2__c) ? true : false;
                }
                qlis.add(qli);      
            
        }
        system.debug(LoggingLevel.INFO,'qlis****'+qlis);
        return qlis;
    }
    public static Map<Id,Id> getHighRiskSKU(){
        Map<Id,Id> hrSKUMap = new Map<Id,Id>();
        for(High_Risk_SKU__c hrsku: [SELECT Product__c FROM High_Risk_SKU__c WHERE Product__c != null]){
            hrSKUMap.put(hrsku.Product__c,hrsku.Product__c);
        }
        return hrSKUMap;
    }
    
    public static string getCategory(Quote_Line_Item__c ql){
        string cat;
        system.debug(LoggingLevel.INFO,'inside getCategory****:'+ ql);
        if(ql.UOM__c=='Year'|| ql.UOM__c=='YR'){
            
            if(ql.Config_Quantity__c == 0){
               cat=ql.Category__c;
            }
            
            else
            if(ql.Config_Quantity__c ==1){
            cat=ql.Category__c+1;
            }
            else
            if(ql.Config_Quantity__c ==2){
            cat=ql.Category__c+2;
            }
            else
            if(ql.Config_Quantity__c ==3){
            cat=ql.Category__c+3;
            }
            
            else 
            if(ql.Config_Quantity__c >= 4){
            cat= ql.Category__c+4;
            }
        }
        return cat;
        }
        
    public static Decimal getDistiApprovedDiscount(Quote_Line_Item__c ql){
        
        Decimal distiApprovedDiscount ;
        
        if(ql.D_Non_Standard_Discount__c != null){
            distiApprovedDiscount = ql.D_Non_Standard_Discount__c;
        }  
        else 
        if(ql.D_Standard_Discount__c != null && ql.Additional_Discount__c!=null){
            distiApprovedDiscount = ql.D_Standard_Discount__c+ql.Additional_Discount__c;
        }
         else
         if(ql.D_Standard_Discount__c != null){
             distiApprovedDiscount = ql.D_Standard_Discount__c;
         }
         else
         if(ql.Additional_Discount__c !=null){
            distiApprovedDiscount = ql.Additional_Discount__c;
         }
         else{
             distiApprovedDiscount=null;
         }
          return distiApprovedDiscount;
    }
    /* validate quote on update of quote during approval process.*/
    public static Map<Id,String> dealDeskValidate(Map<Id,Quote__c> newQuoteMap, Map<Id,Quote__c> oldQuoteMap){
        // get Deal Desk queue members.
        Map<Id,String> quoteValidationMap = new Map<Id,String>();
        Map<Id,Id> gmMap = new Map<Id,Id>();
        gmMap = getQueueMebers();
        Id currentUserId = UserInfo.getUserId();
        for(Quote__c quote: newQuoteMap.values()){
            // if Quote status is changed to 
            System.debug('quote not approved:'+quote.Discount_Status__c);
            if(quote.Discount_Status__c == QUOTE_APPROVED_STATUS){              
                if(quote.Deal_Desk_Status__c != null && quote.Deal_Desk_Status__c.equalsIgnoreCase(DEAL_DESK_PENDING) && !gmMap.containsKey(currentUserId)){
                    //quote.Is_DealDesk_Approved__c = true;
                    quoteValidationMap.put(quote.Id,System.Label.Quote_Not_Approved_By_DealDesk);
                    System.debug('Calling PI if the user is not DealDesk');
                    getApprovalProcess(quote.Id);
                }
                if(quote.Deal_Desk_Status__c != null && quote.Deal_Desk_Status__c.equalsIgnoreCase(DEAL_DESK_PENDING) && gmMap.containsKey(currentUserId)){
                    quote.Deal_Desk_Status__c = DEAL_DESK_APPROVED;
                    System.debug('Calling PI if the user is DealDesk');
                    getApprovalProcess(quote.Id);
                }
            }
        }
        System.debug('quoteValidationMap:'+quoteValidationMap);
        return quoteValidationMap;
    }
    /*get the status of the Approval process*/
    public static void getApprovalProcess(Id objectId){
        List<ProcessInstance> pi = [SELECT Id, Status FROM ProcessInstance WHERE TargetObjectId = :objectId];
        System.debug('pi:'+pi);
    }
    /*submitting record to the next approver who is either [deal Desk or the sales approver]*/
    public void submitToNetxApprover(Id objectId,List<Id> nextApporves){
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval automatically using Trigger');
        req1.setObjectId(ObjectId);
        req1.setNextApproverIds(nextApporves);
        Approval.ProcessResult result = Approval.process(req1);
    }
    public static Map<Id,Id> getQueueMebers(){
        Map<Id,Id> gmMap = new Map<Id,Id>();
        for(GroupMember gm: [SELECT GroupId,UserOrGroupId FROM GroupMember where Group.Name =: DEAL_DESK]){
            gmMap.put(gm.UserOrGroupId,gm.UserOrGroupId);
        }
        return gmMap;
    }
    
}