public class CCI_MDF_SSO{	
	//private static string ENDPOINT='https://qastaging.ccionline.biz/sso/riverbedsso.aspx?';  //QA  
	private static string ENDPOINT='https://cims.ccionline.biz/sso/riverbedsso.aspx?';    //UAT
    //private static string ENDPOINT;https://uat.ccionline.biz/sso/riverbedsso.aspx
    private static string USER_ID;
	private static string ACCOUNT_ID;
	private string requestURL;
	private String responseURL = '';
    private String frameHeight = '0px';
    private String styleValue = 'block';
    private static Boolean isTestMethod=false;
    PageReference retPage;
    User user;
    
    public CCI_MDF_SSO(){
		USER_ID=userInfo.getUserId().substring(0,15);
		user=[Select  u.ContactId, u.Contact.Account.Id, u.Id, Partner_Path_SSO_Token__c from User u where u.Id=:userInfo.getUserId()];
		if(user!=NULL){
			if(user.Contact.Account.Id == null ){//&& user.MDF_User_Role_CCI__c != null
				ACCOUNT_ID = 'internal';
			}else{
				ACCOUNT_ID = ((String) user.contact.Account.Id).substring(0,15);
			}
		}
    }  
    /*    
    public PageReference doLogin(){
        try{
            string token = 'userid='+USER_ID+'&accountid='+ACCOUNT_ID+'&debug=on';
            system.debug('>>>token:'+token);
            retPage = new PageReference(checkResponse(ENDPOINT,token));
        }catch(Exception e){
            system.debug('Exception:'+e);
        } 
		return retPage;		
    }
   
    private string checkResponse(String urlStr , String body){   
        requestURL = urlStr+body;
        return requestURL;
    }
    */
    
    public PageReference doLogin(){
    	PageReference retPage;
        try{
            string token = 'userid='+USER_ID+'&accountid='+ACCOUNT_ID+'&debug=on';
         //   string token = 'userid='+USER_ID+'&accountid='+ACCOUNT_ID;
            system.debug('>>>token:'+token);
            //checkResponse1(ENDPOINT,token);
            retPage = new PageReference(checkResponse(ENDPOINT,token));
        }catch(Exception e){
            system.debug('Exception:'+e);
        } 
        return retPage;		
    }
    
    public PageReference doAmazonLogin(){
    	PageReference retPage;
        try{
        	String url = 'https://amazoncportal.riverbed.com/index.php?fuseaction=home.sso&partner_path_sso_token='+user.Partner_Path_SSO_Token__c+'&external_userID='+USER_ID+'&organizationID='+UserInfo.getOrganizationId();
            //string token = 'userid='+USER_ID+'&accountid='+ACCOUNT_ID+'&debug=on';
         //   string token = 'userid='+USER_ID+'&accountid='+ACCOUNT_ID;
            system.debug('>>>token:'+url);
            //checkResponse1(ENDPOINT,token);
            retPage = new PageReference(url);
        }catch(Exception e){
            system.debug('Exception:'+e);
        } 
        return retPage;		
    }
    
    private void checkResponse1(String urlStr , String body){   
        responseURL = ENDPOINT+ body;
        frameHeight = '875px';
    }
    
    private String checkResponse(String urlStr , String body){   
        responseURL = ENDPOINT+ body;
        System.debug('response url = ' + responseURL);
        //frameHeight = '875px';
        return responseURL;
    }
    
    public String getResponseURL(){
        return responseURL;
    }    
    public String getFrameHeight(){
        return frameHeight;
    }    
    public void setFrameHeight(String strHeight){
        this.frameHeight = strHeight;
    }
        
    //Test Method for CCI_MDF_SSO Controller 
    static testMethod void testCCI_MDF_SSO(){
        isTestMethod=true;
        CCI_MDF_SSO cms=new CCI_MDF_SSO();
        cms.doLogin();
    }
}