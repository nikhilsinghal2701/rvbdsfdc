public with sharing class UserManagerManagement {
	public Boolean display=false;    
    public User user1,user2;
    private List<User> lstUser=new List<User>();
    private List<GenreicObject> lstGobject;
    private List<GenreicObject> users = new List<GenreicObject>();
    
    /* This is the constructor
    	by:prashant.singh@riverbed.com
    */
    public UserManagerManagement(ApexPages.StandardController controller) {       
    	//this.user1=(User)controller.getRecord();
       Id id = ApexPages.currentPage().getParameters().get('id');       
       user1 = (id == null) ? new User() : [select id, name, User_Manager__c, User_Manager__r.Id, User_Manager__r.Name from User WHERE id = :id];
       user2 = (id == null) ? new User() : [select id, name, User_Manager__c, User_Manager__r.Id, User_Manager__r.Name from User WHERE id = :id];
    }
    /* getter/setter methods for the properties of User sObject
    	by:prashant.singh@riverbed.com
    */
    public void setUser1(User value){
        this.user1=value;        
    } 
    public User getUser1(){
       return user1;
    }
    
    public void setUser2(User value){
        this.user2=value;        
    } 
    public User getUser2(){
         return user2;
    }
    public Boolean getDisplay(){
    	return display;
    }
    
    /* This mehod gives the list of users associated with a choosen user manager
    	by:prashant.singh@riverbed.com
    */
    public void getAllUser(){             
        //if(user1!=null){
        if(user1.User_Manager__c==null){
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a user.');
            ApexPages.addMessage(msg);
        }else{   
            lstUser=[select id, name, User_Manager__c, User_Manager__r.Id, User_Manager__r.Name from User where User_Manager__c=:user1.User_Manager__c];
        	if(lstUser.size()>0){
	        	display=true;
	        	lstGobject = listUsers(lstUser);
	        	users=lstGobject;
        	}else{
	        	display=false;
	        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'This Manager is not associated with any user.');
	            ApexPages.addMessage(msg);
        	}
        }        
    }

    /* This mehod return the list of user on the basis of user manager
    	by:prashant.singh@riverbed.com
    */
    private List<GenreicObject> listUsers(List<User> lstUser) {
        List<GenreicObject> lstGenreicObj = new List<GenreicObject>();               
        for(User user:lstUser){
            GenreicObject obj = new GenreicObject();
            obj.Action = false; 
            obj.name = user.name;
            obj.userId = user.Id;
            obj.IsGrayBox ='grayBox';
            lstGenreicObj.add(obj);
        }
        return lstGenreicObj;
    }
    /* This method replace the existing manager of a user to a new manager.
    	manager gets changed when a user check the checkbox.
    	by:prashant.singh@riverbed.com
    */
    public PageReference replaceManager(){
    	string var='user';
    	User tempUser;
    	List<User> lstUpdatedUser=new List<User>();
    	Map<Id,User> mapUser = new Map<Id,User>();
    	system.debug('user2.User_Manager__c: '+user2.User_Manager__c);
    	if(user2.User_Manager__c==null){
    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Replace By User is not selected.');
	        ApexPages.addMessage(msg);	
    	}else {//if(user2.User_Manager__c!=null||user2.User_Manager__c!=''){
    		for(User user:lstUser){
    			mapUser.put(user.id,user);
    		}
	    	for(GenreicObject obj:users){
	    		if(obj.Action){
	    			if(mapUser.containsKey(obj.userId)){
	    				tempUser=mapUser.get(obj.userId);
	    				tempUser.User_Manager__c=user2.User_Manager__c;
		    			lstUpdatedUser.add(tempUser); 
	    			}	
	    		}
	    	}
	    	if(lstUpdatedUser.size()<1){
	    		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Please select a user.');
		        ApexPages.addMessage(msg);
    		}
	    	if(lstUpdatedUser.size()>0){
	    		try{
	    			update lstUpdatedUser;
	    			if(lstUpdatedUser.size()>1)
	    				var='users';
	    			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, 'Manager of below '+var +' is replaced.');
	            	ApexPages.addMessage(msg);
	    		}catch(DMLException dme){
	    			system.debug('DML Exception occurs:'+dme.getMessage());
	    		}catch(Exception e){
	    			system.debug('General Exception occurs:'+e.getMessage());
	    		}    		
	    	}
    	}
        return null;
    }
    /* setter/getter for the GenericObject properties
    	by:prashant.singh@riverbed.com
    */    
    public GenreicObject[] getUsers() { return users; }
    public void setLocations(GenreicObject[] userList) { this.users = userList; }
    
    /* Class contains property to manage properties of users
    	by:prashant.singh@riverbed.com
    */
    public class GenreicObject
    {
        public String name{
            get { return name;}
            set { name = value;}
        }
        public Boolean Action{
            get { return Action;}
            set { Action = value;}
        }
        public String IsGrayBox{
            get{ return IsGrayBox;}
            set{ IsGrayBox = value;}
        }
        public String userId{
        	get { return userId;}
            set { userId = value;}
        }           
     }
}