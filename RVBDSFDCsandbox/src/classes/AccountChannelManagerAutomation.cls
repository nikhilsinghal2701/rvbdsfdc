public class AccountChannelManagerAutomation {
    /*
    Purpose:Automation of CAM/CMM on the basis of Geo,Region and Country
    Author:Prashant Singh
    Date:11June2010
    Update:25June2010
    Update Reason:
    1.) If the override checkbox is checked on the account, even if the account geo/region changes, 
    	the CAM/CMM values should not be updated 
	2.)If the account.type != VAR or Distributor, the account CAM and CMM values 
		should not be updated either on account update or the mapping change
	Update:03Jan2013
	Update Reason: Ticket#131020 -- CAM/CMM needs to be populated without checking Partner_Status1__c value, 
				earlier checking for 'Good Standing' as value.
    */
    static List<Channel_Manager_Assignments__c> channelMan;
    static List<Account> partnerAccount;
    /**
    *   Process updates or new trigger events. Populate CAM/CMM on the basis of Geo,Region and Country.
    *   
    *   @param triggerNew the trigger context new List 
    *   @param triggerOldMap the trigger context old Map 
    *   @return void
    */
    public static void populateManager(List<Account> newTrigger, Map<Id,Account>oldMap){
        partnerAccount=getPartnerAccount(newTrigger);
//        if(channelMan.size()>0){ 
        	try{
            if(oldMap==null){
		        channelMan=[select Id, Geo__c, Region__c, Country__c,Channel_Marketing_Manager__c, 
		                    Channel_Account_Manager__c from Channel_Manager_Assignments__c];
                for(Account acc:partnerAccount){
                    for(Channel_Manager_Assignments__c channel:channelMan){
                        if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)&& channel.Country__c==null){
                            acc.Channel_Account_Manager__c=channel.Channel_Account_Manager__c;
                            acc.Channel_Marketing_Manager__c=channel.Channel_Marketing_Manager__c;
                        }else if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)
                                && channel.Country__c.contains(acc.BillingCountry)){
                            acc.Channel_Account_Manager__c=channel.Channel_Account_Manager__c;
                            acc.Channel_Marketing_Manager__c=channel.Channel_Marketing_Manager__c;
                        }   
                    }                   
                }
            }else if(oldMap!=null){
            	channelMan=[select Id, Geo__c, Region__c, Country__c,Channel_Marketing_Manager__c, 
			                    Channel_Account_Manager__c from Channel_Manager_Assignments__c];
                for(Account acc:partnerAccount){
                    if((acc.Geographic_Coverage__c!=oldMap.get(acc.Id).Geographic_Coverage__c)||(acc.RecordTypeId!=oldMap.get(acc.Id).RecordTypeId)
                        ||(acc.Region__c!=oldMap.get(acc.Id).Region__c)||(acc.BillingCountry!=oldMap.get(acc.Id).BillingCountry)
                        ||(acc.Partner_Status1__c != oldMap.get(acc.Id).Partner_Status1__c)/*((acc.Partner_Status1__c != oldMap.get(acc.Id).Partner_Status1__c) && acc.Partner_Status1__c.equalsIgnoreCase('Good Standing'))*/){//commenting condition for ticket#131020			        	
                        for(Channel_Manager_Assignments__c channel:channelMan){
                            if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)&& channel.Country__c==null){
                                acc.Channel_Account_Manager__c=channel.Channel_Account_Manager__c;
                                acc.Channel_Marketing_Manager__c=channel.Channel_Marketing_Manager__c;
                            }else if(acc.Geographic_Coverage__c.equalsIgnoreCase(channel.Geo__c)&& acc.Region__c.equalsIgnoreCase(channel.Region__c)
                                    && channel.Country__c.contains(acc.BillingCountry)){
                                acc.Channel_Account_Manager__c=channel.Channel_Account_Manager__c;
                                acc.Channel_Marketing_Manager__c=channel.Channel_Marketing_Manager__c;
                            }
                        }   
                    }
                }
            }
        	}catch(Exception e){
        		System.debug('Exception in setting CAM/CMM: ' + e.getMessage());
        	}       
  //      }
    }
    /**
    *   Extracting partner account from the new List.
    *   
    *   @param triggerNew the trigger context new List 
    *   @return void
    */
    private static List<Account> getPartnerAccount(List<Account> newTrigger){
        List<Account>pAccount=new List<Account>();
        //Partner Account record type id
        Id rTypeId = '012300000000NE5AAM';
        //RecordType rType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
        for(Account acc:newTrigger){
            if(acc.RecordTypeId==rTypeId && (acc.Type=='VAR'||acc.Type=='Distributor')&& acc.Override_CAM_or_CMM_updates__c!=TRUE)pAccount.add(acc);
        }
        return pAccount;
    }
    
    /**
	*	Code coverate test method > 90%
	*	@return void
	*/
	static  testMethod void coverage(){
		RecordType rType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
		Account patAcc1=new Account();
        patAcc1.RecordTypeId=rType.Id;
        patAcc1.name='stpAccount';
        patAcc1.Type='Distributor';
        patAcc1.Industry='Education';
        patAcc1.Geographic_Coverage__c='EMEA';
        patAcc1.Region__c='Central Europe';
        patAcc1.BillingCountry='';
        insert patAcc1;
        
        Account patAcc2=new Account();
        patAcc2.RecordTypeId=rType.Id;
        patAcc2.name='tier2Account';
        patAcc2.Type='VAR';
        patAcc2.Industry='Education';
        patAcc2.Geographic_Coverage__c='Americas';
        patAcc2.Region__c='Latin America';
        patAcc2.BillingCountry='Argentina';
        insert patAcc2;
        
        patAcc1.Geographic_Coverage__c='APAC';
        patAcc1.Region__c='India';
        update patAcc1;
	}    
}