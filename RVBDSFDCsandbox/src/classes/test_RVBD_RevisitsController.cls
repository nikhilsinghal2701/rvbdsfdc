@isTest(SeeAllData=true)
public class test_RVBD_RevisitsController {

    static testmethod void testRVBD_RVBD_RevisitsController(){
        
        RVBD_RevisitsController rc = new RVBD_RevisitsController();
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        List<Lead> leadlst = new List<Lead>();
        for(integer i=0; i<10; i++){
        Lead ld = new Lead( Lastname = 'TEST', Company = 'RVBD', Status='In Process', Email='test@rvbd.com', Lead_Score__c = 'B', Revisit_Time__c = datetime.now(), concierge_Rep__c = thisUser.Id, LeadSource='Email');
        leadlst.add(ld);
        }
        if(leadlst.size()>0) 
        insert leadlst;
        
       	List<Contact> conlst = new List<Contact>();
        for(integer i=0; i<10; i++){
        Contact con = new Contact( Lastname = 'TEST', Email='test@rvbd.com', Phone='123-456-6789', Revisit_Time__c = datetime.now(), concierge_Rep__c = thisUser.Id, Contact_Score__c = 'B');
        conlst.add(con );
        }
        if(conlst.size()>0) 
        insert conlst;


		Test.startTest();
        System.runAs(thisUser)
        {
        rc.refresh();
        rc.getLeads();
        rc.getContacts();
        
        rc.searchLeadsContacts();
        //System.assertEquals(10,rc.lstlead.Size()); 
        rc.flipSortOrder();
        rc.selectedTab='Leads';
        rc.first();
        rc.last();
        rc.previous();
        rc.next();
        Boolean next, prev; 
        next = rc.hasNext;
        prev = rc.hasPrevious;
        
        rc.selectedTab='Contacts';
        rc.first();
        rc.last();
        rc.previous();
        rc.next();
        next = rc.hasNext;
        prev = rc.hasPrevious;
        
        Integer n,m,o,p,x,y;
        n=rc.totalLeadRecords;
        m=rc.totalConRecords;
        o=rc.leadPageNo;
        p=rc.conPageNo;
        x=rc.noLeadPages;
        y=rc.noConPages;
        }
    }
    
}