public with sharing class EmailSupportSitePasswordController {
	public Opnet_Group_Id__c opId;
	
	public Opnet_Group_Id__c getOpId(){
		return opId;
	}
	public void setOpId(Opnet_Group_Id__c val){
		opId = val;		
	}
	private String resetUrl;
	
	/*public EmailSupportSitePasswordController(){
		opId = [Select Contact__c, Contact__r.Email, Id, OPNET_SendPasswordResetEmail__c from Opnet_Group_Id__c where Id=:opId.Id];
		
		if(opId.OPNET_SendPasswordResetEmail__c){
			resetPwd();
		}
	}*/
	
	private void resetPwd(){
		String resetUrl = '';
		opId = [Select Contact__c, Contact__r.Email, Id from Opnet_Group_Id__c where Id=:opId.Id];
		
		String pwd = String.valueOf(Math.abs(Crypto.getRandomInteger()));
	    		Integer i=0;
	    		while(pwd.length() < 8 && i <5){//If password length is less than 8, regenerate the random number, attempt 5 times
	    			pwd = String.valueOf(Math.abs(Crypto.getRandomInteger()));
	    			i++;
	    		}
	    		
	    		if(i<5){
	    			Contact conToUpdate = new Contact(Id=opId.Contact__c);
		    		conToUpdate.PasswordHash__c = EncodingUtil.Base64Encode(Crypto.generateDigest('MD5',Blob.valueOf(pwd.substring(0,8))));
		    		conToUpdate.PasswordChange__c = true;
		    		conToUpdate.OPNET_PasswordResetEmailSent__c = true;
		    		update conToUpdate;		    		 
		    		resetUrl = 'https://login.riverbed.com/auth_submit.htm?username=' + opId.Contact__r.Email + '&riverbed.com&token=' + pwd;
	    		}
	   
	}
	
	public String getResetPwdUrl(){	
		resetPwd();	
	    return resetUrl;
	}
}