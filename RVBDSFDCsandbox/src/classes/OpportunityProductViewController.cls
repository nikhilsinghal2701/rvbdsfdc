/*	This class is used to override the standard View link for the Opportunity Product
	If the current user is a reseller, they will be redirected to a custom page where they do 
	not have visibility to the Sales Price of the product.
	For a non-reseller user, the standard view page for the product will be displayed.
	Author: Ankita
	Date: 2/2/2010
	Project: Disti 1C 
*/ 
public class OpportunityProductViewController {
	 
	
	public OpportunityLineItem item {get; set;}
	static Set<Id> resellerProfileIds = new Set<Id>{'00e70000000vPmm','00e70000000vPmw','00e70000000vPn6'};

	
	public OpportunityProductViewController(ApexPages.StandardController controller){
		Id prodId = controller.getId();
		List<OpportunityLineItem> i = [select id, OpportunityId, PriceBookEntry.Product2Id, PriceBookEntry.ProductCode,
					ListPrice, Quantity, R_Sales_Price__c, R_Total_Price__c, description  
					from OpportunityLineItem where id = :prodId ];
			if(!i.isempty())			
		this.item = i[0];		
	}
	
	public PageReference onLoad(){
		if(resellerProfileIds.contains(UserInfo.getProfileId())){
			//return Page.ViewOppProduct;
			return null;
		}else{
			return new PageReference('/'+item.Id+'?nooverride=1');  
		}
		
	}
	
	/**static testmethod void testProductView(){
		//create new account
        Account a = new account (name = 'no match',BillingState='Birmingham',BillingCountry='AF');
        insert a;
		
		//get a reseller user
		//User u = [select Id,Contact.AccountId from User where profileid in :resellerProfileIds and isactive = true limit 1];  // Commneted there is no users in the resellerProfileIds list code failing
	    User u = [select Id,Contact.AccountId from User where profileid='00e70000000vPmr' and isactive = true limit 1];
        Opportunity opp = new Opportunity();
        opp.CloseDate = System.today();
        opp.Name = 'Test Opp';  
        opp.StageName = '0 - Prospect';        
        opp.accountId = a.id;
        opp.Channel__c='Dstributor';
		opp.Tier2__c = u.Contact.AccountId;
		opp.Tier2_Partner_User__c = u.Id;
		insert opp;
		
		PriceBookEntry pbe = [select id from Pricebookentry where isactive = true limit 1];
		OpportunityLineItem item = new OpportunityLineItem(OpportunityId = opp.Id, Quantity = 1, PriceBookEntryId = pbe.id, unitprice = 100);
		insert item;
		//test as reseller
		System.runAs(u){
			PageReference pref = Page.ViewProduct;
	        Test.setCurrentPage(pref);
			OpportunityProductViewController con = new OpportunityProductViewController(new ApexPages.StandardController(item));
			PageReference p = con.onLoad();
		}
		//test as admin
		PageReference pref1 = Page.ViewProduct;
        Test.setCurrentPage(pref1);
		OpportunityProductViewController con1 = new OpportunityProductViewController(new ApexPages.StandardController(item));
		PageReference p1 = con1.onLoad(); 
	}
	*/
}