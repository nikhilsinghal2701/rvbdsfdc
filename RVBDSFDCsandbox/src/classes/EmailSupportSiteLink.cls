/*********************
Class: EmailSupportSiteLink
Description: One time class to email support site link to certain contacts
Author: Clear Task (Rucha) 4/1/2013

Rev1: Rucha 6/20/2013 - To set fields on Contacts to whom emails were sent successfully
**********************/
public with sharing class EmailSupportSiteLink implements Database.Batchable<sObject>{
    private Id fromEmailId;   
    private String query;
    private String emailBody;   
    private String emailSentDate;
    private String headerImgUrl;
    
    public EmailSupportSiteLink(String q){
        //Get from address
        fromEmailId = RVBD_Support_Site_Properties__c.getInstance('Support Site Restriction').Org_Wide_Default_Email_Id__c;
        headerImgUrl = RVBD_Email_Properties__c.getInstance('rvbd_header_image_url').Server_URL__c;        
        query = q;          
    }
    
    public Database.Querylocator start(Database.BatchableContext bc){           
        /*String query = 'Select Email, (Select Id From Opnet_Group_IDs__r) ' +
                        'From Contact ' +
                        'Where Support_Role__c=\'Support Access\' ' +
                            'and PasswordHash__c=null ' +
                            'and Email != null';*/
                            
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> contactList){ 
    	Map<Id,Opnet_Group_Id__c> conToGroupIdMap = new Map<Id,Opnet_Group_Id__c>();
    	List<Opnet_Group_Id__c> grpIdList = new List<Opnet_Group_Id__c>();
    	for(Opnet_Group_Id__c ogid : [Select Id,Name, Opnet_username__c,Contact__c,Opnet_Account_Group_Name__c,OPNET_SupportSiteLinkSent__c From Opnet_Group_ID__c where Contact__c in :contactList]){
    		Opnet_Group_Id__c ogidToUpdate = conToGroupIdMap.get(ogid.Contact__c);
    		if(ogidToUpdate == null && ogid.OPNET_SupportSiteLinkSent__c==false){
    			ogidToUpdate = new Opnet_Group_Id__c(Id=ogid.Id,OPNET_SendSupportSiteLink__c=true);
    			conToGroupIdMap.put(ogid.Contact__c,ogidToUpdate);
    		}
    		
    		if(ogid.OPNET_SupportSiteLinkSent__c){
    			grpIdList.add(ogid);
    		}    		
    	}
    	
    	for(Opnet_Group_Id__c c : grpIdList){
    		if(conToGroupIdMap.get(c.Contact__c)!=null){
    			conToGroupIdMap.remove(c.Contact__c);
    		}
    	}
    	
    	system.debug('Num contacts: ' + conToGroupIdMap.values().size());
    	update conToGroupIdMap.values();
    	
       /* List<Messaging.SingleEmailMessage> emailsToSendList = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage email;
        Map<Id,List<Opnet_Group_ID__c>> contactToGrpIdsMap = new Map<Id,List<Opnet_Group_Id__c>>();
          for(Opnet_Group_Id__c ogid : [Select Id,Name, Opnet_username__c,Contact__c,Opnet_Account_Group_Name__c From Opnet_Group_ID__c where Contact__c in :contactList]){
             List<Opnet_Group_Id__c> ogidList = contactToGrpIdsMap.get(ogid.Contact__c);
             if(ogidList  == null){
                 ogidList = new List<Opnet_Group_Id__c>();
             }
             ogidList.add(ogid);
             contactToGrpIdsMap.put(ogid.Contact__c,ogidList);
         }
        
        //Create Email list
        Map<Id,Messaging.SingleEmailMessage> contactToEmailMap = new Map<Id,Messaging.SingleEmailMessage>();
        for(sObject obj : contactList){
            Contact c = (Contact)obj;
            if(contactToGrpIdsMap.get(c.Id) != null){                            
                email = new Messaging.SingleEmailMessage();                     
                email.setOrgWideEmailAddressId(fromEmailId);                                    
                email.setHtmlBody(createEmailMessage(c.Email,contactToGrpIdsMap.get(c.Id)));                        
                email.setToAddresses(new List<String>{c.Email});
                email.setSubject('OPNET to Riverbed Support Site Account Transition Information');
                if(!(Label.Email_to_Testers_Switch.equalsIgnoreCase('none'))){
                    email.setCcAddresses(Label.Email_to_Testers_Switch.split(';'));
                }
                emailsToSendList.add(email);
                contactToEmailMap.put(c.Id,email);
            }
        }
        
        System.debug('Num eligible contacts: ' + contactToEmailMap.size());
        
        //Send emails
        if(!emailsToSendList.isEmpty()){                    
            List<Messaging.SendEmailResult> smeResult = Messaging.sendEmail(emailsToSendList,false);
             //Added by Rucha on 6/20/2013 to mark contact to whom emails were sent successfully
	        Integer i = 0;
	        List<Contact> emailsSentSuccessfully = new List<Contact>();
	        for(Messaging.SendEmailResult sme : smeResult){
	        	if(sme.isSuccess()){
	        		String sentEmailBody = emailsToSendList.get(i).getHtmlBody();
	        		Id conId;
	        		for(Id contactId : contactToEmailMap.keySet()){ 
	        			Messaging.SingleEmailMessage emailSent = contactToEmailMap.get(contactId);
	        			if(emailSent.getHtmlBody().equals(sentEmailBody)){
	        				emailsSentSuccessfully.add(new Contact(Id=contactId,OPNET_SupportSiteLinkSent__c=true));
	        				break;
	        			}
	        		}        		
	        	}        	
	        	i++;
	        }	        
	        
	        if(!emailsSentSuccessfully.isEmpty()){
	        	update emailsSentSuccessfully;
	        }//End added by Rucha on 6/20/2013
        }*/
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
    
    private String createEmailMessage(String cEmail,List<Opnet_Group_ID__c> opnetDetailsList){
        String opnetGrpDetails = '';
        
        for(Opnet_Group_ID__c ogid : opnetDetailsList)
           opnetGrpDetails += '<tr style="font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;"><td valign="top" width="49%">' + ogid.Opnet_username__c + '</td><td width="2%" /><td valign="top" width="49%">' + ogid.Opnet_Account_Group_Name__c + '</td></tr>';               
            
        String emailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"/><title>Email Template #6</title><style type="text/css">#outlook a {padding:0;}body{width:600px !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:auto; padding:10px 0;} .ExternalClass {width:100%;} .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}table td {border-collapse: collapse;}</style></head>' +
                            '<body><table width="600" cellpadding="0" cellspacing="0" border="0" id="backgroundTable"><tr><td><!--header table--><table border="0" cellpadding="0" cellspacing="0" ><tr><td width="100%"><img style="outline:none; text-decoration:none; text-align:left;" src="' + headerImgUrl + '" alt="Riverbed" border=0 width=600 height=60" ></td></tr></table></td></tr><tr><td><!--email body table--><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr><td width="400" style="background-color:#ffffff;font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;padding:10px; " ><!--left column email body-->' + 
                        '<p>Dear Customer,</p>' +
                        '<p>Riverbed is excited to welcome you to our Support community.  We look forward to working with you. </p>' +
                        '<p>Your Riverbed Support Site account username is:  ' + cEmail  + '</p>' +
                        '<p>A password reset email will be sent to you shortly from <br /><a href="mailto:noreply-techsupport@riverbed.com">noreply-techsupport@riverbed.com</a>. '+
                          'If you do not receive it, you may reset your password by accessing the <a href="https://support.riverbed.com/index.htm">Riverbed Support Site</a> and clicking on the ‘Get a Riverbed Support Login’ link. </p>' +
                        '<p>All customers are receiving a Riverbed support account and username so they may access support applications.</p> ' +
                        '<p>Your previous OPNET Support account(s), as listed below, has been merged into a Riverbed Support account:</p>' +
                        '<table><tr style="font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;"><td width="49%"><span style="text-decoration:underline">OPNET Support Username</span></td><td width="2%" /><td width="49%"><span style="text-decoration:underline">OPNET Group ID</span></td></tr>' +
                        opnetGrpDetails + 
                        '</table><p>Sincerely,<br/>Riverbed Technical Support<br/><a href="https://support.riverbed.com" style="color:#e55603;">support.riverbed.com</a></p></td>' +
                                '<td valign="top"><!--right column table--><table width="180" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;color:#ffffff;text-align:left;font-size:12px;table-layout:fixed;"><tr><td height="1" > </td></tr>' +
                                '<!--orange button--><tr><td style="border-color:#da7c0c; border-style:solid; border-width:1px;background: #ff6600;background: -webkit-gradient(linear, left top, left bottom, from(#faa51a), to(#ff6600));background: -moz-linear-gradient(top, #faa51a, #f47a20);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#faa51a\', endColorstr=\'#ff6600\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;overflow: hidden;" ><a href="https://login.riverbed.com/login_support.htm" target ="_blank" title="View Your Account" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;">View Your Account</a></td></tr><tr><td height="1"></td></tr>' +
                                '<!--black button #1--><tr><td style="border-color:#000; border-style:solid; border-width:1px;background: #333;background: -webkit-gradient(linear, left top, left bottom, from(#666), to(#000));background: -moz-linear-gradient(top, #666, #000);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#666666\', endColorstr=\'#000000\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;"><a href="https://support.riverbed.com/index.htm" target ="_blank" title="Visit Support" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;">Visit Support</a></td></tr>' +
                                '<tr><td height="1"></td></tr><!--black button #2--><tr><td  style="border-color:#000; border-style:solid; border-width:1px; background: #333;background: -webkit-gradient(linear, left top, left bottom, from(#666), to(#000));background: -moz-linear-gradient(top, #666, #000);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#666666\', endColorstr=\'#000000\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;"><a href="mailto:support@riverbed.com"  target ="_blank" title="Email Support" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;" >Email Support</a></td></tr>' +
                                '</table></td><!--end of right column table--></tr></table></td><!--end email body table--></tr>' +
                                '<tr><td style=" font-family:Arial, sans-serif;text-align:left;font-size:9px;line-height:1.3em;padding:8px;border-color:#e55603;border-top-style:solid;border-top-width:medium; ">' +
                                '<!-- footer-->' +
                                'Copyright &copy;2012 Riverbed Technology, Inc. All rights reserved.<br /> ' +
                                '<a href="https://support.riverbed.com/contact/index.htm" style="color:#e55603;">Contact Support </a> | ' +
                                '<a href="http://www.riverbed.com/us/contact/" style="color:#e55603;">Contact Riverbed</a> | ' +
                                '<a href="http://www.riverbed.com/us/privacy_policy.php" style="color:#e55603;">Privacy Policy</a> | <a href="http://www.riverbed.com/us/legal_notices.php"style="color:#e55603;">Legal Notices </a> | <a href="http://www.riverbed.com/us/sitemap.php" style="color:#e55603;">Site Map </a></td></tr></table></body></html>';
                        
        return emailBody;
    }
    
    @isTest(SeeAllData=true)
    static void testEmailSupportSiteLink(){ 
        List<Contact> contactList = [Select Id from Contact where Support_Role__c='Support Access' and PasswordHash__c=null and Email!=null limit 5];
        
        List<Opnet_Group_ID__c> ogidList = new List<Opnet_Group_ID__c>();
        
        String contactIds = '';
        for(Contact c : contactList){
            ogidList.add(new Opnet_Group_ID__c(Contact__c=c.Id));
            contactIds += '\'' + c.Id + '\',';
        }
        if(contactIds!=''){
            contactIds = '(' + contactIds.substring(0,contactIds.length()-1) + ')';
        }
        
        insert ogidList;
                
        String query = 'Select Email, (Select Id,Name, Opnet_username__c From Opnet_Group_IDs__r) ' +
                        'From Contact ' +
                        'Where Support_Role__c=\'Support Access\' ' +
                            'and PasswordHash__c=null ' +
                            'and Email != null ' +
                            'and Id in ' + contactIds;
        EmailSupportSiteLink es = new EmailSupportSiteLink(query);
        Test.startTest();
        Database.executeBatch(es);
        Test.stopTest();        
    }
}