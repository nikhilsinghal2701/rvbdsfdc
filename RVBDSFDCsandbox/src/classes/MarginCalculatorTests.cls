@isTest
private class MarginCalculatorTests {

	static testmethod void testMarginCalculation(){
		Quote__c q = DiscountApprovalTestHlp.createQuote(null);
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(q);
		MarginCalculatorController con = new MarginCalculatorController(sc);
		con.addProducts();
		con.removeProducts();
		con.addSelectedProducts();
		con.back();
		for(Quote_Line_Item__c item : con.quoteLines){
			System.debug('Disti Discount = ' + item.Disti_Approved_Discount__c);
			item.Scenario_Quantity__c = 2;
		}
		con.calculate();
		con.email();
		con.hideNonCOGSItems();
		con.reset();
		con.searchProducts();
		con.sendEmail();
		con.showNonCOGSItems();
		
	}
	static testmethod void testMarginCalculationExtn(){
		Quote__c q = DiscountApprovalTestHlp.createQuote(null);
		ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(q);
		MarginCalculatorPageExtension con = new MarginCalculatorPageExtension(sc);
	}
	static testmethod void unitTest(){
    	Account a = [select id from Account limit 1];
    	Case c = new Case(accountid = a.id );
    	ApexPages.StandardController rba = New ApexPages.StandardController(c);
    	PendingCases pc = new PendingCases(rba);
    }
}