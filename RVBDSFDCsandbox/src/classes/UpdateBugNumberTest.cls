@isTest
private class UpdateBugNumberTest {
	/*
	*Description:Test method for BugCaseUpdate trigger.
	*Created By:Prashant.Singh@riverbed.com
	*Created Date:Sep 30,2010
	*/
    static testMethod void bugCaseUpdateUnitTest() {
        Case newCase=new case();
        newCase.Status='New';
		insert newCase;
		
		Solution newSolution = new Solution();
		newSolution.Bug_Number__c='1234';
		newSolution.OwnerId=userinfo.getUserId();
		newSolution.SolutionName='1234-test';
		insert newSolution;
		
		CaseSolution cs=new CaseSolution(caseId=newCase.Id,SolutionId=newSolution.Id);
		insert cs;
		
		newCase.Status='Working';
		update newCase;
        
    }

    /*
	*Description:Test method for UpdateBugNumberWS class.
	*Created By:Prashant.Singh@riverbed.com
	*Created Date:Sep 30,2010
	*/

    static testMethod void updateBugNumberWSUnitTest() {
    	Case newCase=new case();
        newCase.Status='New';
		insert newCase;
		
		Solution newSolution1 = new Solution();
		newSolution1.Bug_Number__c='1234';
		newSolution1.OwnerId=userinfo.getUserId();
		newSolution1.SolutionName='1234-test';
		insert newSolution1;
		
		Solution newSolution2 = new Solution();
		newSolution2.Bug_Number__c='5678';
		newSolution2.OwnerId=userinfo.getUserId();
		newSolution2.SolutionName='5678-test';
		insert newSolution2;
		
		CaseSolution cs1=new CaseSolution(caseId=newCase.Id,SolutionId=newSolution1.Id);
		insert cs1;
		CaseSolution cs2=new CaseSolution(caseId=newCase.Id,SolutionId=newSolution2.Id);
		insert cs2;
		String result=UpdateBugNumberWS.updateBugNumber(newCase.Id);
    }

}