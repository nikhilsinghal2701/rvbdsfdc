/*********************************************************
Class: DiscountApprovalVFTemplateHlpCtrl
This is controller for component DiscountApprovalVfTemplateHlp. This class has all the methods 
that are needed to dynamically pull data for the quote and receipient from several objects
Author:Rucha - 5/13/2013
Modification: adding description table. to the email template 
Modification version: Rev:1
Modified Date : 9/22/2014
Modified by: Jaya.
Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 
*********************************************************/
global without sharing class DiscountApprovalVFTemplateHlpCtrl {
    public Quote__c relatedToQuote{get;set;}    
    public Static final String QUOTE_R_STATUS = 'Required';
    private DiscountApprovalEmailHlp disApprHlp;
    //public String quoteId {get;set;}    // Added by Sukhdeep Singh
    global DiscountApprovalVFTemplateHlpCtrl(){//Constructor
        disApprHlp = DiscountApprovalEmailHlp.getInstance();        
    }
    public List<ProcessInstanceHistory> approvalHistory;
    Quote__c quote;
    public List<ProcessInstanceHistory> getApprovalSteps() {    // Added by Sukhdeep Singh
      /*if (quoteId != null) {//commented by Prashant on 1/1/2014
        Quote__c quote = [Select Id, (Select TargetObjectId, SystemModstamp, StepStatus, RemindersSent, ProcessInstanceId, OriginalActorId, IsPending, IsDeleted, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessSteps order by SystemModstamp desc) from Quote__c where Id = :quoteId];
        return quote.ProcessSteps;
      }
      return new List<ProcessInstanceHistory> ();*/
      //Added by Prashant on 1/1/2014 to fix approval history issue in NSD approval email
      if(approvalHistory==null && relatedToQuote!=null){
        approvalHistory=new List<ProcessInstanceHistory>();
        quote = [Select Id, (Select TargetObjectId, SystemModstamp, StepStatus, RemindersSent, ProcessInstanceId, OriginalActorId, IsPending, IsDeleted, Id, CreatedDate, CreatedById, Comments, ActorId From ProcessSteps order by SystemModstamp desc) from Quote__c where Id !=null And Id =:relatedToQuote.Id];
      }
      if(quote!=null)approvalHistory.addAll(quote.ProcessSteps);
      return approvalHistory;
    }
    
    public String marginStatus{//To display margin status to approvars
        get{        
            return disApprHlp.getMarginStatusInfo(relatedToQuote);
        }
        set;    
    }
    
    
    public List<Discount_Detail__c> discountDetails;
    public List<Discount_Detail__c> getDiscountDetails(){//To display those discount details for which NSD is requested
        if(discountDetails == null){
            Map<String,String> qliCategoryMap = createCategoriesonQlisMap();
            discountDetails = new List<Discount_Detail__c>();
            for(Discount_Detail__c dd : [Select Category_Master__r.Name,Category_Master__r.Description__c,Discount__c,Special_Discount__c,Delta_Discount__c,
                                         Uplift__c,Disti_Special_Discount__c, Additional_Discount__c, Full_Discount__c,Quote__r.Referral_Deal__c,Quote__r.Referral_Fee__c
                                         From Discount_Detail__c 
                                         Where Quote__c!=null And Quote__c = :relatedToQuote.Id 
                                         And IsOpp__c=false Order By Category_Master__r.Name]){
                if(qliCategoryMap.get(dd.Category_Master__r.Name) != null && dd.Special_Discount__c != null && dd.Special_Discount__c > 0)
                    discountDetails.add(dd);
            }           
        }
        return discountDetails;
    }
    /*Rev:1 descriptive details  OR Reason for Approval*/
    public Map<String,Set<String>> descriptiveDetails; 
    public Map<String,Set<String>> getDescriptiveDetails(){
        if(discountDetails == null){
            // retrive reasons from approval
            Map<String,String> reasons = new Map<String,String>();
            reasons = getReason4Approval();
            descriptiveDetails = new Map<String,Set<String>>();
            Map<String,String> categoryMasterNameDescriptionMap = new Map<String,String>();// to get th descriptive name for category
            categoryMasterNameDescriptionMap = getRelatedCategory(relatedToQuote.Id);
            Map<String,String> vsoeRange = new Map<String,String>();// vsoe range is a custom settings that defines the range to be compared with.
            vsoeRange = getVSOERange();
            for(Quote_Line_Item__c qli : [SELECT Quote__c,Quote__r.Blanket__c,Category__c,Quote__r.Discount_Status__c,VSOE_NON_Compliant__c,
                                            Is_High_Risk_SKU__c
                                         FROM Quote_Line_Item__c 
                                         WHERE Quote__c!=null AND Quote__c = :relatedToQuote.Id
                                         Order BY Category__c]){
                // retriving category description based on category of the qli.                             
                String categoryDesc = qli.Category__c != null && categoryMasterNameDescriptionMap.containsKey(qli.Category__c) 
                                            ? categoryMasterNameDescriptionMap.get(qli.Category__c) : '';
                categoryDesc = qli.Category__c +'-'+categoryDesc;
               if(qli.Quote__r.Discount_Status__c.equalsIgnoreCase(QUOTE_R_STATUS)){
                    Set<String> ddPLDSet = new set<String>();
                    String str1 = reasons.get('pld');//'Discounts on the following Product Discount Category is greater than Program Level Disocunts:';
                    if(descriptiveDetails.containskey(str1)){
                        ddPLDSet.addAll(descriptiveDetails.get(str1));
                    }
                    ddPLDSet.add(categoryDesc);// adding category
                    descriptiveDetails.put(str1,ddPLDSet);
                }
                if(qli.VSOE_NON_Compliant__c){
                    Set<String> ddPDCSet = new Set<String>();
                    String str2 = reasons.get('vsoe');//'Non-VSOE compliant software';
                    if(descriptiveDetails.containskey(str2)){
                        ddPDCSet.addAll(descriptiveDetails.get(str2));
                    }
                    ddPDCSet.add(categoryDesc);// adding category
                    descriptiveDetails.put(str2,ddPDCSet);
                }
                if(qli.Is_High_Risk_SKU__c){
                    Set<String> ddELASet = new Set<String>();
                    String str3 = reasons.get('ela');//'Bill of Materials includes SKUs with inherent risk - $1 ELA or PS SKUs';
                    if(descriptiveDetails.containskey(str3)){
                        ddELASet.addAll(descriptiveDetails.get(str3));
                    }
                    ddELASet.add(categoryDesc);// adding category
                    descriptiveDetails.put(str3,ddELASet);
                }
                if(qli.Quote__r.Blanket__c){
                    Set<String> ddBLANKETSet = new Set<String>();
                    String str4 = reasons.get('blanket');//'Request is for a Blanket Discount';
                    if(descriptiveDetails.containskey(str4)){
                        ddBLANKETSet.addAll(descriptiveDetails.get(str4));
                    }
                    ddBLANKETSet.add(categoryDesc);// adding category
                    descriptiveDetails.put(str4,ddBLANKETSet);
                }
            }           
        }
        return descriptiveDetails;
    }// end of Rev:1
    /* Rev:1 deal Econimics */
    public List<DiscountDetailsCtrl.OppDiscountDetails> discountDealEconomics;
    public Boolean blanketSwitch {get;set;}
    public Decimal SumTotalNetAmt {get;set;}
    public Decimal SumTotalListAmt {get;set;}
    public List<DiscountDetailsCtrl.OppDiscountDetails> getDiscountDealEconomics(){
        discountDealEconomics = new List<DiscountDetailsCtrl.OppDiscountDetails>();
        ApexPages.StandardController ctrl = new ApexPages.StandardController(relatedToQuote);
        DiscountDetailsCtrl rec = new DiscountDetailsCtrl(ctrl);
        discountDealEconomics = rec.discountDetails;
        blanketSwitch = rec.blanketSwitch;
        SumTotalNetAmt = rec.SumTotalNetAmt;
        SumTotalListAmt = rec.SumTotalListAmt;
        return discountDealEconomics;
    }
    public Map<String,String> getRelatedCategory(Id QuoteId){
        Map<String,String> categoryMasterNameDescriptionMap = new Map<String,String>();
         for(Discount_Detail__c dd : [Select Category_Master__r.Name,Category_Master__r.Description__c,Discount__c,Special_Discount__c,Delta_Discount__c,
                                         Uplift__c,Disti_Special_Discount__c, Additional_Discount__c, Full_Discount__c,Quote__r.Referral_Deal__c,Quote__r.Referral_Fee__c
                                         From Discount_Detail__c 
                                         Where Quote__c!=null And Quote__c = :QuoteId]){
             categoryMasterNameDescriptionMap.put(dd.Category_Master__r.Name,dd.Category_Master__r.Description__c);
        }
        return categoryMasterNameDescriptionMap;
    }
    /*get vsoe ranges*/
    public Map<String,String> getVSOERange(){
        Map<String,String> vsoeRangeMap = new Map<String,String>();
        for(DiscountDetailCategoryRange__c dr: DiscountDetailCategoryRange__c.getAll().values()){
            String range = dr.Range__c.substring(0,dr.Range__c.IndexOf('-'))+'% to '+ dr.Range__c.substring(dr.Range__c.IndexOf('-')+1,dr.Range__c.length())+'%';
            vsoeRangeMap.put(dr.Name,range);
        }
        return vsoeRangeMap;
    }
    /*get reason for approval  */
    public Map<String,String> getReason4Approval(){
        Map<String,String> reason = new Map<String,String>();
        for(ReasonsForApproval__c rfa: ReasonsForApproval__c.getAll().values()){
            reason.put(rfa.Name,rfa.Reason__c);
        }
        return reason;
    }
    
   // end Of Rev:1
    public String partnerName{//To get quote's opportunity's partner name
        get{
            try{
                OpportunityPartner partner = [Select Id,AccountTo.Name from OpportunityPartner Where OpportunityId=:relatedToQuote.Opportunity__c AND IsPrimary = true];
                return partner.AccountTo.Name;
            }
            catch(Exception e){
                return null;
            }
            
        }
        set;
    }
    
    public Decimal overallDiscount{//To get overall discount
        get{
            return disApprHlp.getDiscount(getDiscountDetails());
        }
        set;
    }
    
    public String getRSM(){
        relatedToQuote = [Select RSM__r.Name from Quote__c Where Id = :relatedToQuote.Id];
        return relatedToQuote.RSM__r.Name;
    }
    
    public String getISR(){
        relatedToQuote = [Select Opportunity__r.Primary_ISR__r.Name from Quote__c Where Id = :relatedToQuote.Id];
        return relatedToQuote.Opportunity__r.Primary_ISR__r.Name;
    }   
    
    /*public String oracleCategories{//To get all product families/oracle categories on quote line items related the quote
        get{
            Map<Id,List<String>> quoteToOrclCatMap = DiscountApprovalRouting.getInstance().getProductFamiliesforQuotes(new List<Quote__c>{relatedToQuote});
            oracleCategories = '';      
            
            if(quoteToOrclCatMap.get(relatedToQuote.Id) != null){
                for(String orclCat : quoteToOrclCatMap.get(relatedToQuote.Id)){
                    oracleCategories += orclCat + ',';
                }
                if(oracleCategories != ''){
                    oracleCategories = oracleCategories.substring(0,oracleCategories.length()-1);
                }
            }
            return oracleCategories;
        }
        set;
    }*/
    /*Rev:1 wrapper class for descriptive Details or Reason for Approval*/
    public class DescriptiveDetail{
        public String categoryStr {get;set;}
        public String categoryDes {get;set;}
    }
    private Map<String,String> createCategoriesonQlisMap(){//To get categories from all the line items on the quote
        Map<String,String> qliCatMap = new Map<String,String>();
        for(Quote_Line_Item__c qli : [Select Category__c from Quote_Line_Item__c where Quote__c=:relatedToQuote.Id]){
            qliCatMap.put(qli.Category__c,qli.Category__c);
        }
        return qliCatMap;
    }
}