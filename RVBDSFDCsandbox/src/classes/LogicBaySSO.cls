/* LogicBay SSO implementation, replacing s-control by Apex/VF Page
** By: prashant.singh@riverbed.com
** Date: 2/15/2012
*/
public with sharing class LogicBaySSO {
    private static string algorithmName='MD5';
    private static string SECRET='riverbed';
    private static string EMAIL,ORGNAME;
    //private static string ENDPOINT='http://lbapp01.logicbay.com/riverbed/login/sso.jsp';  //test environment
    private static string ENDPOINT='http://riverbed.logicbay.com/riverbed/login/sso.jsp'; //Prod environment  
    private static string GMTTIME;
    private String responseURL = '';
    private PageReference retPage;
    private User user;
    
    public LogicBaySSO(){     
    }
    public PageReference loginAuthentication(){
      try{
          GMTTIME=getTimeInGMT();
          //System.debug(LoggingLevel.INFO,'GMT Time = '+ GMTTIME);
          //EMAIL=[select Email from user where id=:userInfo.getUserId()].Email;
          user=[Select u.email, u.ContactId, u.Contact.AccountId, u.Contact.FirstName, u.Contact.Id, u.Contact.LastName from User u where Id=:userInfo.getUserId()];
          if(user!=null){
              EMAIL=user.Email;
              ORGNAME=[Select Name from Account where id=:user.Contact.AccountId].Name;
              string digestString = EMAIL+SECRET+GMTTIME;
              //System.debug(LoggingLevel.INFO,'digestString = '+ digestString);
              Blob myDigest = Crypto.generateDigest(algorithmName, Blob.valueOf(digestString));
              string myDigestString = EncodingUtil.ConvertToHex(myDigest);
              string token=isNull(myDigestString);
              //system.debug(LoggingLevel.INFO,'TOKEN:'+token);
              //system.debug('Original Token:'+token);
              retPage = new PageReference(checkResponse(ENDPOINT,token));
          }
      }catch(Exception e){
        system.debug('Exception:'+e);
      }
        return retPage;                   
    }
    
    private string checkResponse(String urlStr , String body){   
        responseURL = urlStr+'?username='+EMAIL+'&acntname='+getURLEncode(UserInfo.getOrganizationName())+'&code='+body+'&firstname='+
        getURLEncode(user.Contact.FirstName)+'&lastname='+getURLEncode(user.Contact.LastName)+'&orgname='+getURLEncode(ORGNAME)+'&orgcode='+((String)user.Contact.AccountId).substring(0,15)+'&SFDCUserId='+((String)user.Id).substring(0,15);
        //system.debug('ResponseURL:'+responseURL);
        return responseURL;
    }      
    
    private string getTimeInGMT(){
        DateTime d = System.now();
        String zero='0';
        String month=''+d.monthGmt();
        if(month.length()==1) month=zero+month;
        String day=''+d.dayGmt();
        if(day.length()==1)day=zero+day;
        String hour=''+d.hourGmt();
        if(hour.length()==1)hour=zero+hour;
        String minute=''+d.minuteGmt();
        if(minute.length()==1) minute=zero+minute;
        String second=''+d.secondGmt();
        if(second.length()==1)second=zero+second;
        String timeStamp = ''+month+day+d.yearGmt()+hour;
        String timeFormat = d.formatGmt(timeStamp);
        return timeFormat;
    } 
    
    private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return getURLEncode(originalStr);
    }  
    private string getURLEncode(string originalStr){
        string encodeStr=EncodingUtil.urlEncode(originalStr, 'UTF-8');
        //system.debug('>>> String:'+encodeStr);
        return encodeStr;
    }
    
    //Test Method for LogicBaySSO Controller 
    static testMethod void testLogicBaySSO(){
        LogicBaySSO lbs=new LogicBaySSO();
        lbs.loginAuthentication();
    }    
}