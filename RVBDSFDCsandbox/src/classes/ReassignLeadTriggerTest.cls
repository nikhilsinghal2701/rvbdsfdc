public with sharing class ReassignLeadTriggerTest {

    @future
    public static void updateLeadsForReassignment(Set<Id> leadIds){
    //public static void updateLeadsForReassignment(List<Lead> leads){
        List<Lead> updatedLeads = new List<Lead>();
        for(Lead newLead : [select id from Lead where id in :leadIds]){
        //for(Lead newLead : leads){
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule= true;
            newLead.setOptions(dmo);
            updatedLeads.add(newLead);
        }
        //jsADM.jsADM_ContextProcessController.isInFutureContext = true; //added by Ankita 7/18/2011 for Jigsaw ADM pkg    // uninstalling with 'Jigsaw Advanced Data Management (Managed)'
        RecursiveTriggerControl.isExecuted = true; // added by Raj 9/11/2012 to control the recurssive execution.
        update updatedLeads;
        system.debug('::::::::::::::::::::::Assignment'+updatedLeads);
    }

    static testmethod void testLeadRessignment(){
        RecordType recType = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        User eloquaUser = [select id, name from User where name = 'Eloqua Administrator'];
        Group nurtureQueue = [select id from group where name ='Lead Nurture' limit 1];
        //create lead
        Lead newLead;
        test.startTest();
        System.runAs(eloquaUser){
            newLead = new Lead(firstname = 'New Lead', lastname = 'lead', company = 'new company', ownerid = nurtureQueue.id,
                                leadsource = 'Web - Riverbed.com', recordTypeid = recType.id );
            insert newLead;
            
        }
        Lead l = [select id, ownerid,Lead_Score__c from Lead where id = :newLead.id];
        l.Lead_score__c = 'B';
        update l;
        Test.stopTest();
        //System.assertEquals(quarantineQueue.id,l.ownerid);
    }

}