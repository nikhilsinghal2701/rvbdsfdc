/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CertRollupBatchTest {

    static testMethod void myUnitTest() {
        Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        TestDataFactory tdf = new TestDataFactory();
         RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
        List<String> authMaster = new List<String>{'Comp-AD','Comp-W','Comp-SD','Comp-PM'};
        //List<Authorizations_Master__c> list_of_AuthMaster = tdf.createAuthMaster(authMaster);
        // these records are needed for auth creation
       // insert list_of_AuthMaster;
        Account grandParent = tdf.createAccount();
        insert grandParent;
        Account parentAcct = tdf.createAccount();
        parentAcct.ParentId = grandParent.Id;
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        Set<Id> acctIds = new Set<Id>{grandParent.Id,parentAcct.Id,acct.Id};
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        // create certs for general competency
        // create certificate master records
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
            }
        }
        // Create Partner level config records
        List<PartnerLevelConfig__c> partnerConfigList = new List<PartnerLevelConfig__c>();
        //partnerConfigList.add(new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'REGULAR',Number_Of_Competencies__c = 1,Type__c = 'VAR'));
        partnerConfigList.add(new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR'));
        insert partnerConfigList;
        /*List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,
                                                                new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Federal'});
    List<Account> accList = TestingUtilPP.createAccounts(1, true, new Map<String, Object>{'Partner_Level__c'=>'Authorized', 'Type' =>'VAR','Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});*/
        Test.startTest();
            insert certs;
            System.debug('---:'+tdf.retriveCertSummary(parentAcct.Id));
            Error_Log__c logError = CompetencyRollDown.logError('recordId', 'Test class msg', 'CertRollupTest', '');
            // batch class call
            CertRollupBatch crb = new CertRollupBatch();
            Database.executeBatch(crb);
            //CertRollupBatchHelper.getRelatedAccounts(acctIds);
        Test.stopTest();
        // assert methods
        for(Acct_Cert_Summary__c pc: tdf.retriveCertSummary(parentAcct.Id))
        {
            System.assert(pc.Roll_Up_RSS__c >=0);
            System.assert(pc.Roll_Up_RTSS__c >=0);
            System.assert(pc.Roll_Up_RCSA_RCSP__c >= 0);
        }
        List<Authorizations_Master__c> list_of_AuthMaster = tdf.createAuthMaster(authMaster);
        insert list_of_AuthMaster;
        Set<Id> masterIds = new Set<Id>();
        for(Authorizations_Master__c am: list_of_AuthMaster){
            masterIds.add(am.Id);
        }
        List<Account_Authorizations__c> list_of_Authos = new List<Account_Authorizations__c>();
        for(Authorizations_Master__c aMaster: list_of_AuthMaster){
            list_of_Authos.add(new Account_Authorizations__c(Authorization_Master__c = aMaster.Id,
                                                Account__c = acct.Id,Status__c = 'Active'));
        }
        Set<Id> accIds = new Set<Id>{acct.Id};
        AccountAuthorizationTriggerHandler.syncUpdateAuthorizationSpecializationFieldOnAccount(list_of_Authos,masterIds,accIds);
        AccountAuthorizationTriggerHandler.sendEmailNotification(list_of_Authos, list_of_Authos, 'update');
    }
}