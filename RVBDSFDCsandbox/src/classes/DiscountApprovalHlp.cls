/*******************************************************
Rev1 Clear Task- Rucha  4/30/2012 Added condition to make Discount Reason mandatory
********************************************************/
public without sharing class DiscountApprovalHlp {
    private static final String APPROVE = 'Approve';
    private static final Set<Id> EMAIL_USER_IDS = new Set<Id>{'00570000001KkSG', '005S0000000QZXn'};
    
    public class DiscountApprovalException extends Exception {}
    
    public static void initializeQuotes(List<Quote__c> quotes) {
        Set<Id> oppIds = extractOpportunityIds(quotes);
        Map<Id,Opportunity> oppsById = new Map<Id,Opportunity>([SELECT Id, Competitors__c, OwnerId FROM Opportunity WHERE Id IN :oppIds]);
        for (Quote__c quote : quotes) {
            Opportunity opp = oppsById.get(quote.Opportunity__c);
            if (opp != null) {
                quote.Competition__c = opp.Competitors__c;
                //q.RSM__c=o.OwnerId; //Commented on 11/12/2009 by prashant.singh@riverbed.com
                populateApprovers(quote, opp.OwnerId);
            }
        } 
    }
    
    /**
     * Populates all Approver fields on specified quote with specified
     * user.
     */       
    public static void populateApprovers(Quote__c quote, Id approverId) {
        if (approverId != null) {
            for (Integer i=0;i<10;i++) {
                quote.put('Approver' + (i + 1) + '__c', approverId);
            } 
        }
    }
    
    /**
     * Populates all Approver fields on specified quote with specified
     * user.
     */       
    public static void clearApprovers(Quote__c quote) {
        for (Integer i=0;i<10;i++) {
            quote.put('Approver' + (i + 1) + '__c', null);
        } 
    }
    
    /**
     * Sets all Approved User fields (1-10) on specified quote to specified value.
     */
    public static void setApprovedUsers(Quote__c quote, Boolean value) {
        for (Integer i=0 ;i<10;i++) {
            quote.put('Approved_User' + (i + 1) + '__c', value);
         } 
    }
    
    public static NSD_Approval_History__c createApprovalRecord(Quote__c quote, DateTime stamp, String action, String approvers) {
        return createApprovalRecord(UserInfo.getUserId(), UserInfo.getUserName(), quote, stamp, action, approvers);
    }
    
    public static NSD_Approval_History__c createApprovalRecord(Id userId, String userName, Quote__c quote, DateTime stamp, String action, String approvers) {
        Set<Id> approverIds = DiscountApprovalHlp.extractApproverIds(quote);
        return new NSD_Approval_History__c(Approver__c=userId,Approver_Name__c=userName,Quote__c=quote.Id,Stamp__c=stamp,Action__c=action,Assigned_Approvers__c=approvers,Assigned_Ids__c=toString(approverIds));
    }
    
    public static Set<Id> extractApproverIds(List<Quote__c> quotes) {
        Set<Id> result = new Set<Id>();
        for (Quote__c quote : quotes) {
            result.addAll(extractApproverIds(quote));
        }
        return result;
    }
    
    public static Set<Id> extractApproverIds(Quote__c quote) {
        Set<Id> approverIds = new Set<Id> ();
        approverIds.add(quote.Approver9__c);
        approverIds.add(quote.Approver8__c);
        approverIds.add(quote.Approver7__c);
        approverIds.add(quote.Approver6__c);
        approverIds.add(quote.Approver5__c);
        approverIds.add(quote.Approver4__c);
        approverIds.add(quote.Approver3__c);
        approverIds.add(quote.Approver2__c);
        approverIds.add(quote.Approver1__c);
        approverIds.add(quote.Approver10__c);
        return approverIds;
    }
    
    //Start: Added by prashant.singh@riverbed.com/10/12/2012/SFDC outage Project
    public static Boolean validateIsManual(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotesById){
        Boolean isManual=false;
        for(Quote__c quote:quotes){
            if(quote.NSD_Override__c!=null){
                isManual=true;
            }
        }
        return isManual;
    }
    
    public static void approveRecord(List<Quote__C> quotes){//added on 10/25/2012
        for(Quote__c qt:quotes){
            if(qt.NSD_Override__c != null && qt.Discount_Status__c == QuoteApprovalHlp.STATUS_REQUIRED){
                qt.Discount_Status__c=QuoteApprovalHlp.STATUS_APPROVED;
            }
        }
    }
    
    public static void approveRecord(List<Quote__C> newQuote, Map<Id,Quote__c>oldMap){//added on 10/25/2012
        List<ProcessInstance> pInstance;
        List<ProcessInstanceWorkitem>  workitem;
        Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
        Map<Id,Quote__c> quoteMap=new Map<Id,Quote__C>();
        Map<Id,ProcessInstance> pInstanceMap = new Map<Id,ProcessInstance>();
        Map<Id,Id> pWorkItemMap = new Map<Id,Id>();
        List<Id>quoteIds=new List<Id>();
        for(Quote__c temp:newQuote){
            if(temp.Id!=null && temp.Discount_Status__c==QuoteApprovalHlp.STATUS_PENDING)
                quoteMap.put(temp.id,temp);
                quoteIds.add(temp.Id);
        }
        if(quoteMap.size()>0){
            //pInstance = [Select p.TargetObjectId, p.Status, p.Id,(Select Id, ActorId, IsDeleted From Workitems) From ProcessInstance p where targetobjectid IN:quoteIds and status='Pending'];
            pInstance = new List<ProcessInstance>([select Id,TargetObjectId from ProcessInstance where Status = 'Pending' and TargetObjectId IN:quoteIds]);
        }
        
        if(pInstance!=null && pInstance.size()>0){
            set<Id> pIds=new set<Id>();
            for(ProcessInstance temp:pInstance){
                pIds.add(temp.Id);
                pInstanceMap.put(temp.TargetObjectId,temp);
            }
            if(pIds.size()>0)
                workitem = new List<ProcessInstanceWorkitem>([select Id, ProcessInstanceId from ProcessInstanceWorkitem where ProcessInstanceId IN:pIds]);
            if(workitem.size()>0)
                for(ProcessInstanceWorkitem temp:workitem){
                    pWorkItemMap.put(temp.ProcessInstanceId,temp.Id);
                }   
        }
        for(Quote__c qt:newQuote){
            if(qt.Id!=null && quoteMap.containsKey(qt.Id)){                        
                req2.setComments('Cancelling and approved manualy');
                req2.setWorkitemId(pWorkItemMap.get(pInstanceMap.get(qt.Id).Id));
                req2.setAction('Removed');
                Approval.ProcessResult result2 = Approval.process(req2);
            }
        }
    }
    
    @future
    public static void offlineApproved(Set<Id> quoteId){
        List<Quote__c> quoteList=[Select id,Discount_Status__c,NSD_Override__c from Quote__c where Id IN:quoteId];
        List<Quote__c> updateQuote;
        if(quoteList!=Null && quoteList.size()>0){
            updateQuote=new List<Quote__c>();
            for(Quote__c qt:quoteList){
                if(qt.Discount_Status__c==QuoteApprovalHlp.STATUS_CANCELLED && qt.NSD_Override__c!=Null){
                    qt.Discount_Status__c=QuoteApprovalHlp.STATUS_APPROVED;
                    updateQuote.add(qt);
                }
            }
        }
        if(updateQuote!=Null && updateQuote.size()>0){
            update updateQuote;
        }
    }
    //End: Added by prashant.singh@riverbed.com/10/12/2012/SFDC outage Project
     
    public static Boolean validateSubmittedQuotes(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotesById) { 
        Map<Id,DateTime> lastUpdatedByOppId = new Map<Id,DateTime>();
        Map<Id,Quote__c> pendingQuotesById = new Map<Id,Quote__c>();
        Map<Id,Boolean> quoteRSMApproverMap = new Map<Id,Boolean>();
        Boolean valid = true;
        
        Set<Id> oppIds = new Set<Id>();
        for (Quote__c quote : quotes) {
            Quote__c oldQuote = oldQuotesById.get(quote.Id);
            if ((quote.Discount_Status__c == QuoteApprovalHlp.STATUS_PENDING) && (quote.Discount_Status__c != oldQuote.Discount_Status__c)) {
                quote.Submitter__c = UserInfo.getUserId();
                pendingQuotesById.put(quote.Id, quote);
                oppIds.add(quote.Opportunity__c);  
            }
        }
        
        if (!pendingQuotesById.isEmpty()) {
            for (Quote__c oppQuote : [SELECT  Id, LastModifiedDate, Discount_Status__c, Opportunity__c, RSM__c, RSM__r.Approver__c FROM Quote__c WHERE Opportunity__c IN :oppIds]) {
                DateTime lastUpdated  = lastUpdatedByOppId.get(oppQuote.Opportunity__c);        
                if (lastUpdated == null || lastUpdated < oppQuote.LastModifiedDate){
                    lastUpdatedByOppId.put(oppQuote.Opportunity__c, oppQuote.LastModifiedDate);
                }
                quoteRSMApproverMap.put(oppQuote.Id, oppQuote.RSM__r.Approver__c)  ;             
            }
            
            for (Quote__c quote : pendingQuotesById.values()) {
                DateTime lastUpdated = lastUpdatedByOppId.get(quote.Opportunity__c);
                Quote__c oldQuote = oldQuotesById.get(quote.Id);
                Boolean isRSMApprover = quoteRSMApproverMap.get(quote.Id);
                if (lastUpdated != null && lastUpdated > quote.LastModifiedDate) {
                    quote.addError('You may only submit the last modified Quote on an Opportunity.');
                    valid = false;
                } else if (quote.RSM__c == null) {
                    quote.addError('You must select an RSM before submitting.');
                    valid = false;
                } else if (quote.Discount_Code__c == null) {
                    //added by Ankita (2/3/2010) to ensure that Discount Code is selected before quote is submitted for approval
                    quote.addError('You must select a Discount Code before submitting');
                } else if(quote.Discount_Reason__c == null){//added by Rucha (4/30/2013)
                    quote.addError('You must select a Discount Reason before submitting');                  
                }else if (!isRSMApprover && quote.Quote_Total_Only_Std_Discount__c >= Double.valueOf(Label.LV_CutOff)){
                    quote.addError('This quote may not be approved because the approval chain could not be calculated.');
                }
            }    
        }
        return valid;
    }
    
    public static void createHistory(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotesById) { 
        List<NSD_Approval_History__c> hists = new List<NSD_Approval_History__c>();
        
        if (!isEmailUserId(UserInfo.getUserId())) {
            for (Quote__c quote : quotes) {
                Quote__c oldQuote = oldQuotesById.get(quote.Id);
                if ((quote.Discount_Status__c == QuoteApprovalHlp.STATUS_PENDING
                        || quote.Discount_Status__c == QuoteApprovalHlp.STATUS_CANCELLED
                        || quote.Discount_Status__c == QuoteApprovalHlp.STATUS_REJECTED)
                        && quote.Discount_Status__c != oldQuote.Discount_Status__c) {
                    if (quote.Approver__c  != null) {
                        hists.add(createApprovalRecord(quote, DateTime.now(), quote.Discount_Status__c, quote.Approver__c));
                    }               
                }
            }
        }
        
        if (!hists.isEmpty()) {
            insert hists;
        }   
    }
    
    public static void shareToPartner(Map<Id,Quote__c> newQuotesById, Map<Id,Quote__c> oldQuotesById) {
        Account partnerAccount;
        String roleName;    
        Map <String, Id> accMap = new Map <String, Id>();
        Map <Id, String> roleMap = new Map <Id, String>();
        Map <Id, Id> groupMap   = new Map <Id, Id>();   
        Set <Id> accIds = new Set <Id>();
        Map <Id, sObject> accounts = null;
        Map <Id, Id> partnerAccs = new Map <Id, Id>();
        Map <Id, Account> partnerMap = new Map <Id, Account>();
        Set <Id> oIds = new Set <Id>();
        Map <Id, Id> oppQuote = new Map <Id, Id> ();
        List <Quote__Share> newShares = new List <Quote__Share> ();
        Set <Id> oldGroupIds = new Set <Id> ();
        Set <Id> oldShareQIds = new Set <Id> ();
    
        for (Quote__c q: newQuotesById.values()){ 
            Quote__c oldq = oldQuotesById.get(q.Id);
            if (q.Share_To_Partner__c == true
                && oldq.Share_To_Partner__c != true){
                oppQuote.put(q.Opportunity__c, q.Id);
            }else if(q.Share_To_Partner__c == false
                && oldq.Share_To_Partner__c == true){
                oldShareQIds.add(q.Id);
                oppQuote.put(q.Opportunity__c, q.Id);
            }
        }
            
        if (oppQuote.size() > 0){
            for (OpportunityPartner o : [Select o.OpportunityId, o.IsPrimary, o.AccountToId 
                    From OpportunityPartner o 
                    where o.OpportunityId in : oppQuote.keyset()
                    and  o.IsPrimary = true]){
                partnerAccs.put(o.AccountToId, o.OpportunityId);
            }
            
            for (Account a: [Select IsPartner, Name, Id , Type
                                    from Account 
                                    where Id IN : partnerAccs.keyset() 
                                    and IsPartner = true]){
                Id oId =  partnerAccs.get(a.Id);
                Id qId = oppQuote.get(oId);
                partnerMap.put(qId, a);     
            }
                                                                                                        
            for (Quote__c q: newQuotesById.values()){
                
                partnerAccount = partnerMap.get(q.Id);
                
                if (partnerAccount != null) {
                    //if the account name is really long, truncate the role name
                    if (partnerAccount.Name.length() > 62) {
                        roleName = partnerAccount.Name.substring(0,62)+' Partner User';
                    } else {
                        roleName = partnerAccount.Name+' Partner User';
                    }
                    System.debug('Rolename='+rolename);
                    
                    accMap.put(rolename, q.Id);
                }
            }
            for (UserRole ur : [Select Id, Name 
                                from UserRole 
                                where Name IN : accMap.keyset()]){
                                    
                roleMap.put(ur.Id, ur.Name);
            }
        
            
            for (Group pg : [select Id, RelatedId
                                from Group 
                                where RelatedId IN : roleMap.keyset() 
                                and Type='Role']){
                                    
                roleName = roleMap.get(pg.RelatedId);
                Id qId = accMap.get(roleName);
                if (oldShareQIds.contains(qId)){
                    oldGroupIds.add(pg.Id);
                }else {
                    //added by Ankita for Quote visibility to partners
                    String accessLevel = 'Read';
                    if(partnerMap.containsKey(qId)){
                        String accType = partnerMap.get(qId).Type == null ? '' : partnerMap.get(qId).Type; 
                        if(accType.equalsIgnoreCase('Distributor')){
                            accessLevel = 'Edit';
                        }
                    }
                    Quote__Share qShare = new Quote__Share(UserOrGroupId=pg.Id, RowCause='manual',ParentId=qId,AccessLevel=accessLevel);
                    newShares.add(qShare);
                }
            }   
            
            List <Quote__Share> oldShares = [select Id, UserOrGroupId 
                                            from Quote__Share 
                                            where UserOrGroupId IN: oldGroupIds
                                            and RowCause='manual' 
                                            and ParentId IN : oldShareQIds 
                                            and AccessLevel='Read'];
            
            insert newShares;   
            delete oldShares;
        }       
    }
    
    public static Map<Id,Opportunity> loadOpportunitiesByIds(Set<Id> oppIds) {
        return new Map<Id,Opportunity>([SELECT Discount_Demos__c, 
                                               Discount_Category__c, 
                                               DiscountSupport__c, 
                                               DiscountSpare__c, 
                                               DiscountService__c, 
                                               DiscountProduct__c,
                                               DiscountSupport_1__c,
                                               DiscountSupport_2__c,
                                               DiscountSupport_3__c,
                                               Approved_Discount_A__c,
                                               Approved_Discount_B__c, 
                                               Approved_Discount_C__c,
                                               Approved_Discount_D__c,
                                               Approved_Discount_E__c
                                              // Approved_Quote_A__c,/* Commneted to free up the look up fields By Pramod/Ankita 12.17.2013*/
                                              // Approved_Quote_B__c, 
                                              // Approved_Quote_C__c,
                                              // Approved_Quote_D__c, Deleting the field as part of Cleanup and repurposing it per Julie
                                               //Approved_Quote_E__c
                                               FROM Opportunity WHERE Id IN :oppIds]);
    }
    
    public static Map<Id,User> loadUsersById(Set<Id> userIds) {
        return new Map<Id,User>([SELECT Name, Geo__c, Region__c FROM User WHERE Id IN :userIds]);
    }

    public static User getUser(String email, Set<Id> userIds){   
        List<User> users ;
        if(DiscountApprovalEmail.isTest==true)
        {
            Set<String>UsersList = new Set<String>();
            
            for(Id ids:userIds)
            {
                String str=ids;
                if(Str.length()==18)
                str=str.substring(0,str.length()-3);
                UsersList.add(str);
            }
             users = [SELECT Name FROM User WHERE Id IN :UsersList ];
        }
        else
         users = [SELECT Name FROM User WHERE Id IN :userIds AND Email = :email];
        if (users!=null && users.size() > 0){
            return users[0];
        }
        return null;
    }

    public static User getUser(Id uId){
        return [Select u.Name, u.Id from User u where u.Id = : uId];
    }

    public static boolean isEmailUserId(Id userId){
        if (EMAIL_USER_IDS.contains(userId)){
            return true;    
        } else {
            return false;   
        }
    }
    
    private static String toString(Set <Id> oIds){
        String result = '';
        for (Id oId : oIds){
            result += oId + '\n';
        }   
        return result;
    }
    
    /**
     * Returns set of Opportunity IDs referenced by specified quotes.
     */
    public static Set<Id> extractOpportunityIds(Quote__c[] quotes) {
        Set<Id> ids = new Set<Id>();
        for (Quote__c quote : quotes) {
            ids.add(quote.Opportunity__c);
        }
        return ids;
    }
    
    /**
     * Returns set of User IDs that represent RSMs on specified quotes.
     */
    public static Set<Id> extractRSMs(Quote__c[] quotes) {
        Set<Id> ids = new Set<Id>();
        for (Quote__c quote : quotes) {
            if (quote.RSM__c != null) {
                ids.add(quote.RSM__c);
            }
        }
        return ids;
    }
    
    /**
     * Updates Approved User fields on specified quote to specified value.
     */
    public static void updateApprovedUsers(Quote__c quote, Boolean value){
        for (Integer i=0;i<10;i++) {
            quote.put('Approved_User' + (i + 1) + '__c', value);
         } 
     }
     
    /**
     *sets  quote object approval user flag to true when user moves form one step to another 
     */
    private static void  setApprovedUserFlag(sObject q){
        for (Integer i = 0 ; i <10; i++){
            if (q.get('Approved_User' + (i + 1) + '__c') == false) {
                q.put('Approved_User' + (i + 1) + '__c',true);
                break;
            }
        } 
    }
     
    public static void setRSM(List<Quote__c> quotes){
        Set<Id> oppIds = extractOpportunityIds(quotes);
        Map<Id,Opportunity> oppsById = new Map<Id,Opportunity>([SELECT OwnerId FROM Opportunity WHERE Id IN :oppIds]);
        for (Quote__c quote :quotes) {
            Opportunity opp = oppsById.get(quote.Opportunity__c);
            if (opp != null && quote.RSM__c == null){
                quote.RSM__c = opp.OwnerId;//added by Ankita 2/3/2010 for Defaulting the RSM to Opportunity owner
            }
        } 
    }
    
    public static void setSegmentOnQuote(List<Quote__c> quotes){
        Set<Id> rsmIds = new Set<Id>();
        //Map<Id,Opportunity> oppsById = new Map<Id,Opportunity>([SELECT OwnerId FROM Opportunity WHERE Id IN :oppIds]);
        for(Quote__c q : quotes){
            rsmIds.add(q.RSM__c);
        }
        Map<Id,String> rsmSegmentMap = new Map<Id,String>();
        for(User u : [select Id, Segment__c from User where id in :rsmIds]){
            rsmSegmentMap.put(u.Id,u.segment__c);
        }
        for (Quote__c q :quotes) {
            String qSeg = q.Segment__c != null ? q.Segment__c : '';
            if(qSeg.equalsIgnoreCase('Not linked to any org')){
                q.Segment__c = rsmSegmentMap.get(q.RSM__c);
            }
            
        } 
    }
    
    //Added by Rucha (5/28/2013)
    public static void populateProductCategory(List<Quote_Line_Item__c> qliList){
        Set<Id> productIdSet = new Set<Id>();
        
        for(Quote_Line_Item__c qli : qliList){
            if(qli.Product2__c != null){
                productIdSet.add(qli.Product2__c);
            }
        }      
        
        List<Product_Oracle_Category__c> pocList = [Select Oracle_Category__c,Product__c,Oracle_Category__r.Name
                                                        From Product_Oracle_Category__c 
                                                        Where Product__c in :productIdSet];
       
        
                                                 
        for(Quote_Line_Item__c qli : qliList){
            Map<String,String> catMap = new Map<String,String>();
            String catList ='';
            for(Product_Oracle_Category__c  poc : pocList){               
                if(qli.Product2__c == poc.Product__c && poc.Oracle_Category__c!=null && !(catMap.containsKey(poc.Oracle_Category__r.Name))){
                    catList += poc.Oracle_Category__r.Name + ',';
                    catMap.put(poc.Oracle_Category__r.Name,poc.Oracle_Category__r.Name);
                }
            }
            
            if(catList != ''){
                catList = catList.substring(0,catList.length()-1);
                qli.Oracle_Category__c = catList;
            }           
        }
    }    
    //
}