global class AccountHierarchy implements Database.Batchable<sobject>, Database.Stateful
{
	public String query;
	global String error;
	global database.queryLocator start(Database.BatchableContext BC) 
	{ 
	        return database.getQueryLocator(query);
	} 
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{ 
		List<Account> accountList = scope;
		Set<String> parentOSKey = new Set<String>(); 
		Set<String> ultimateOSKey = new Set<String>();
		Set<String> isoCodeSet = new Set<String>();
		Map<String, Id> osKeyAccountIdMap = new Map<String, Id>();
		Map<String, Id> ultimateOSKeyAccountIdMap = new Map<String, Id>();
		List<Account> updateAccountList = new List<Account>();
		//Set<String> updateAccountSet = new Set<String>();
		//List<Account> parentAccountList = new List<Account>();
		boolean isUpdate = false;
		try 
		{
			for(Account a :accountList)
			{
				if(a.OS_Parent_Key_ID__c != null)
				parentOSKey.add(a.OS_Parent_Key_ID__c);
				if(a.OS_Ultimate_Parent_Key_ID__c != null)
				ultimateOSKey.add(a.OS_Ultimate_Parent_Key_ID__c);
								
			}
			System.Debug('accountList ==>> '+accountList);
			System.Debug('parentOSKey ==>> '+parentOSKey);
			System.Debug('ultimateOSKey ==>> '+ultimateOSKey);
			if(parentOSKey.size() != 0 || ultimateOSKey.size() != 0)
			{
				for(Account a : [SELECT Id, OneSource__OSKeyID__c,OS_Ultimate_Parent_Key_ID__c FROM Account WHERE (OneSource__OSKeyID__c IN : parentOSKey OR OneSource__OSKeyID__c IN : ultimateOSKey)])
				{
					System.Debug('a ==>> '+a);
					System.Debug('a.OneSource__OSKeyID__c ==>> '+a.OneSource__OSKeyID__c);
					if(a.OneSource__OSKeyID__c != null)
					{
						if(parentOSKey.contains(a.OneSource__OSKeyID__c))
						osKeyAccountIdMap.put(a.OneSource__OSKeyID__c, a.Id);
						else if(ultimateOSKey.contains(a.OneSource__OSKeyID__c))
						ultimateOSKeyAccountIdMap.put(a.OneSource__OSKeyID__c, a.Id);
						
					}
				}
			}	
				
				System.Debug('osKeyAccountIdMap ==>> '+osKeyAccountIdMap);
				System.Debug('ultimateOSKeyAccountIdMap ==>> '+ultimateOSKeyAccountIdMap);
				
					for(Account a :accountList)
					{
						isUpdate = false;
						if(a.OS_Parent_Key_ID__c != null && osKeyAccountIdMap.get(a.OS_Parent_Key_ID__c) != null)
						{
							System.Debug('OS_Parent_Key_ID__c Match Found ');
							Id parentId = osKeyAccountIdMap.get(a.OS_Parent_Key_ID__c);
							if(a.Id != parentId)
							{
								a.ParentId = parentId;
							    isUpdate = true;
							}
							System.Debug('OS_Parent_Key_ID__c -> isUpdate ==>> '+isUpdate);
						}
						else if(a.OS_Ultimate_Parent_Key_ID__c != null && ultimateOSKeyAccountIdMap.get(a.OS_Ultimate_Parent_Key_ID__c) != null)
						{
							System.Debug('OS_Ultimate_Parent_Key_ID__c Match Found ');
							Id parentId = ultimateOSKeyAccountIdMap.get(a.OS_Ultimate_Parent_Key_ID__c);
							if(a.Id != parentId)
							{
								a.ParentId = parentId;
							    isUpdate = true;
							}
							System.Debug('OS_Ultimate_Parent_Key_ID__c -> isUpdate ==>> '+isUpdate);
						}
							
						if(isUpdate)
						{
							//updateAccountSet.add(a.Id);
							updateAccountList.add(a);
						}
					}
					
					/*for(Account a : parentAccountList)
					{
						if(!updateAccountSet.contains(a.Id))
						{
							isUpdate = false;
							if(isUpdate)
							{
								updateAccountSet.add(a.Id);
								updateAccountList.add(a);
							}
						}
					}*/
					if(updateAccountList.size() != 0)
					{
						Database.SaveResult[] srList = Database.update(updateAccountList, false);
						for(Database.SaveResult sr : srList)
						{
							for(Database.Error e : sr.getErrors())
							error += '\n ' + e.getMessage();
						}
					}
				//}
				
		}
		catch(Exception e)
		{
			error += e.getMessage();
		}
	}
	global void finish(Database.BatchableContext BC)
	{
		AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =: BC.getJobId()];
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toEmailAddresses = new String[] {'anil.madithati@riverbed.com','Paru.Mahesh@riverbed.com','Prerana.Gupta@riverbed.com'};
        mail.setToAddresses(toEmailAddresses);
        mail.setSubject('BatchJob Status' + a.Status);
        mail.setHTMLBody
        ('Total No of Batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}