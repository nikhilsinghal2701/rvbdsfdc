public class UpdateLeadStatusOnNewTaskTest{
	public static Boolean isTest=false;
	
	/*
	*Description:Test method for UpdateLeadStatusOnNewTask trigger.
	*Created By:Prashant.Singh@riverbed.com
	*Created Date:Sep 26,2010
	*/
    static testMethod void leadStatusUpdateUnitTest() {
    	isTest=true;
        Lead lead=new lead();
        lead.FirstName='test';
        lead.LastName='ench1';
        lead.Status='Open';
        lead.LeadSource='Advertisement';
        lead.Company='test';
        lead.OwnerId=userinfo.getUserId();
        lead.RecordTypeId=[select Id from RecordType where sObjectType ='Lead' and Name='General Leads'].Id;
        insert lead;
        Task newTask = new Task();
		newTask.Status = 'Completed';
		newTask.Subject='Call';
		newTask.ActivityDate= System.today();
		newTask.OwnerId=lead.OwnerId;
		newTask.whoId=lead.Id;
		insert newTask;
		
		lead.Status='Open';		
		update lead;
		
		Task newTask1 = new Task();
		newTask1.Status = 'Completed';
		newTask1.Subject='Call';
		newTask1.ActivityDate= System.today();
		newTask1.OwnerId=lead.OwnerId;
		newTask1.whoId=lead.Id;
		insert newTask1;		
    }

    /*
	*Description:Test method for CreateTaskWS class.
	*Created By:Prashant.Singh@riverbed.com
	*Created Date:Sep 26,2010
	*/

    static testMethod void createFollowUpTaskOnLeadUnitTest() {
    	string days='2';
    	Lead lead=new lead();
        lead.FirstName='test';
        lead.LastName='ench1';
        lead.Status='Open';
        lead.LeadSource='Advertisement';
        lead.Company='test';
        lead.OwnerId=userinfo.getUserId();
        lead.RecordTypeId=[select Id from RecordType where sObjectType ='Lead' and Name='General Leads'].Id;
        insert lead;
        String result=CreateTaskWS.createTask(days, lead.Id, lead.OwnerId);
    }
}