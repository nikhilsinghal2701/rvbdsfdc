public class MassRejectionAction{    
    static set<ID> leadId =new set<ID>();    
    public static void massLeadRejection(){
        List<Lead> rejLead=[Select l.Id,l.Name,l.CreatedDate,l.LeadSource,l.OwnerId,l.RecordTypeId,l.RecordType.Name, l.Status from Lead l where l.RecordType.Name in('Deal Registration','Deal Registration - Distributor') AND l.CreatedDate < 2010-01-01T01:02:03Z AND l.Status='Submitted' limit 50];
        for(Lead lead:rejLead){
            leadId.add(lead.Id);
        }
        if(leadId.size()>0) massReject(leadId);
    }
    public static void massReject(Set<Id> recId){        
        List<ProcessInstance> pInstance = [Select p.TargetObjectId, p.Status, p.Id,(Select Id, ActorId, IsDeleted From Workitems order by SystemModStamp limit 1),(Select Id, StepStatus From Steps order by SystemModStamp limit 1) From ProcessInstance p where targetobjectid in :recId];
        // Instantiate the new ProcessWorkitemRequest object and populate it
        for(ProcessInstance pi:pInstance){
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Rejecting request.');
            req2.setAction('Reject');
            if(pi.Workitems.size() > 0){
                req2.setWorkitemId(pi.Workitems[0].Id);
                // Submit the request for rejection
                Approval.ProcessResult result2 = Approval.process(req2);
            }
        }
    }
    //Test Method for MassRejectionAction Controller     
    static testMethod void testMassRejectionAction(){
        /*
        Set<Id> lId=new Set<Id>();        
        List<Lead> rejLead=[Select l.Id,l.Name,l.CreatedDate,l.LeadSource,l.OwnerId,l.RecordTypeId,l.RecordType.Name, 
        l.Status from Lead l where l.RecordType.Name in('Deal Registration','Deal Registration - Distributor') AND  
        l.CreatedDate < 2010-01-01T01:02:03Z AND l.Status='Submitted' limit 5];
        system.debug('Test Method Lead:'+rejLead);        
        for(Lead lead:rejLead){            
            lId.add(lead.Id);
        }        
        test.starttest();
        MassRejectionAction.massReject(lId);
        test.stoptest();*/
        test.starttest();
        MassRejectionAction.massLeadRejection(); 
        test.stoptest();         
    }
    
}