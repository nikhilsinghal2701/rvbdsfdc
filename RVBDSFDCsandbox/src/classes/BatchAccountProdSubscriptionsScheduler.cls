global class BatchAccountProdSubscriptionsScheduler implements Schedulable 
{
  global void execute(SchedulableContext SC) 
  { 
    BatchAccountProductSubscriptions accProdSubscription = new BatchAccountProductSubscriptions();
    database.executebatch(accProdSubscription);
    }
}