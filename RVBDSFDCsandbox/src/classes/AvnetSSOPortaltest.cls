@isTest
private class AvnetSSOPortaltest
{
 static testMethod void testMe() {
 
  PageReference pageRef = Page.AvnetSSO_OpportunityList;
  List<OpportunityWrapper> oppList = new List<OpportunityWrapper>();
  List<OpportunityWrapper> oppLists = new List<OpportunityWrapper>();
  Test.setCurrentPage(pageRef);
 AvnetSSOPortalController controller = new AvnetSSOPortalController();
  //User u = [select id from User where name='Richard Harvey']; 
    Account a = new Account(name = 'account');
    insert a;
    
    Contact c = new Contact (accountid = a.Id, lastname = 'con', email='test@test.com');
    insert c;
    
    a.IsPartner = true;
    update a;
    
   Profile p=[select id,name from profile where name='PRM - Distributor - Admin'];      
   
   User newPartner = new User(alias = 'standt', email='standarduser@testorg.com', 
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
								    contactId = c.Id, localesidkey='en_US', profileid = p.Id, 
								    timezonesidkey='America/Los_Angeles', username='standarduser23@testorg.com');   

  insert newPartner;
  controller.stage = ''       ;
  controller.acc = '';
 System.runAs(newPartner) {
 controller.getStages();
 controller.search();
 //oppList = controller.showList;
 oppLists = controller.selectedopp; 
 controller.ow[1].checked = true;
 controller.updateopp();
 controller.back();
 controller.stage = '4 - Selected';
 controller.search();
}

        OpportunityWrapper cw = new OpportunityWrapper();
        System.assertEquals(cw.checked,false);      
 
        OpportunityWrapper cw2 = new OpportunityWrapper(new Opportunity (name='Test1'));
        System.assertEquals(cw2.ops.name,'Test1');
        System.assertEquals(cw2.checked,false);       
 
    }
    }