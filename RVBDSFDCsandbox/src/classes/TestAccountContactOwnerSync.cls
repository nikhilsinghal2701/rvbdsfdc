@isTest
private class TestAccountContactOwnerSync {

    static testMethod void BatchAccountContactOwnerSync() {
        /*RecordType cRecType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Customer Account'];
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType.Id;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        Contact con=new Contact();
        con.AccountId=cusAcc.Id;
        con.FirstName='cus';
        con.LastName='Contact';
        con.Email='test@test.com';
        con.Support_Role__c='Support Access';
        insert con;
        Profile profile=[select id,name from profile where name='System Administrator'];      
        User user1=new User();
        user1.FirstName='test1';
        user1.LastName='umm1';
        user1.Alias='tu1';
        user1.Email='test101@test1.com';
        user1.Username='test101@test1.com';
        user1.EmailEncodingKey='ISO-8859-1';
        user1.CurrencyIsoCode='USD';
        user1.TimeZoneSidKey='America/Los_Angeles';
        user1.LocaleSidKey=userInfo.getLocale();
        user1.LanguageLocaleKey='en_US';
        user1.ProfileId=profile.id;
        insert user1;
        system.runAs(user1){
            Contact con1=new Contact();
            con1.AccountId=cusAcc.Id;
            con1.FirstName='cus';
            con1.LastName='Contact';
            con1.Email='test@test.com';
            con1.Support_Role__c='Support Access';
            insert con1;
            cusAcc.OwnerId=user1.Id;
            update cusAcc;
            //test of BatchAccountContactOwnerSync class
           // Test.startTest(); 
                //BatchAccountContactOwnerSync.isTest=true;           
                //Id batchProcessId=Database.executeBatch(new BatchAccountContactOwnerSync(),1);
           // Test.stopTest();*/
            BatchAccountContactOwnerSync bc = new BatchAccountContactOwnerSync();
            bc.query = 'SELECT Id, OwnerId,(Select Id,OwnerId from Contacts) from Account limit 200';
            Test.startTest();
            Database.executeBatch(bc, 200);
            Test.stopTest();
        }       
    }