/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class TestSupportSiteAccessRestriction {
/*
    static testMethod void accountUtilUnitTest() {
       AccountUtil.isTest=true;
        RecordType cRecType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Customer Account'];
        Asset testAsset =[select SupportStartDate__c,Support_End_Date__c from Asset  where SupportStartDate__c!= null AND Support_End_Date__c!= null limit 1];
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType.Id;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        System.debug('Customer account::::::::::::::::::::'+cusAcc);
        
        RecordType pRecType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
        Account patAcc=new Account();
        patAcc.RecordTypeId=pRecType.Id;
        patAcc.name='tier2Account';
        patAcc.Partner_Status1__c='Good Standing';
        patAcc.Type='VAR';
        patAcc.Industry='Education';
        patAcc.Geographic_Coverage__c='Americas';
        patAcc.Region__c='Latin America';
        patAcc.BillingCountry='Argentina';
        insert patAcc;
        System.debug('Partner account::::::::::::::::::::'+patAcc);
        
        Product2 prod=[Select p.Id, p.Name, p.ProductCode from Product2 p where p.ProductCode='SHA-05010'];
        
        
        Asset newAsset=new Asset();
        newAsset.AccountId=cusAcc.Id;
        newAsset.SerialNumber='SD123456789';
        newAsset.Product2Id=prod.Id;
        newAsset.Name='TestSteelhead 5010';
        newAsset.IB_Status__c='Sold';
        newAsset.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset.Support_End_Date__c=Date.newInstance(2013, 4, 11);
        //insert newAsset;
        //System.debug(' new Asset::::::::::::::::::::'+newAsset);
        
        Asset newAsset1=new Asset();
        newAsset1.AccountId=patAcc.Id;
        newAsset1.SerialNumber='SD123456789';
        newAsset1.Product2Id=prod.Id;
        newAsset1.Name='TestSteelhead 5010';
        newAsset1.IB_Status__c='Sold';
        newAsset1.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset1.Support_End_Date__c=Date.newInstance(2013, 4, 11);
        //insert newAsset1;
        
        List<Contact>conList=new List<Contact>();
        Contact con1=new Contact();
        con1.AccountId=cusAcc.Id;
        con1.FirstName='cus';
        con1.LastName='Contact';
        con1.Email='test@test.com';
        con1.Support_Role__c='Support Access';
        conList.add(con1);
        Contact con2=new Contact();
        con2.AccountId=patAcc.Id;
        con2.FirstName='pat';
        con2.lastName='Contact';
        con2.Email='test@test.com';
        con2.Support_Role__c='Support Access';
        conList.add(con2);
        insert conList;
               
        cusAcc.Support_Site_Access_Expiry_Date__c=system.today().addDays(30);
        cusAcc.X30_Day_Notification_Sent__c=true;
        update cusAcc;
        cusAcc.Disable_Support_Method__c='Restricted';
        cusAcc.Reason__c='test code coverage';
        update cusAcc;
        cusAcc.Disable_Support_Method__c='Allowed';
        cusAcc.Reason__c='test code coverage';
        update cusAcc;
        cusAcc.Support_Site_Access_Expiry_Date__c=system.today().addDays(15);
        cusAcc.X15_Day_Notification_Sent__c=true;
        update cusAcc; 
        cusAcc.Disable_Support_Method__c='Automatic';
        cusAcc.Reason__c='test code coverage';
        update cusAcc;
        cusAcc.Support_Site_Access_Expiry_Date__c=system.today();
        cusAcc.Support_Access_Disabled__c=true;
        update cusAcc; 
        
        Asset newAsset2=new Asset();
        newAsset2.AccountId=cusAcc.Id;
        newAsset2.SerialNumber='SD123456789';
        newAsset2.Product2Id=prod.Id;
        newAsset2.Name='TestSteelhead 5010';
        newAsset2.IB_Status__c='Sold - Demo';
        newAsset2.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset2.Support_End_Date__c=Date.newInstance(2013, 4, 11);
        //insert newAsset2;
        
        // updated with testAsset insted of newAsset
        cusAcc.Support_Site_Access_Expiry_Date__c=testAsset.Support_End_Date__c;
        update cusAcc;
        
        patAcc.Disable_Support_Method__c='Automatic';
        patAcc.Partner_Status1__c='Terminated';
        patAcc.Reason__c='test code coverage';
        update patAcc;
        patAcc.Disable_Support_Method__c='Allowed';
        update patAcc;
        patAcc.Disable_Support_Method__c='Restricted';
        update patAcc;
       // updated with testAsset insted of newAsset2;
        patAcc.Support_Site_Access_Expiry_Date__c=testAsset.SupportStartDate__c;
        update patAcc;
        
        Test.StartTest();
        BatchAccountRollups batchAccountRollups=new BatchAccountRollups();
        batchAccountRollups.Query='SELECT Id,RecordTypeId,CreatedDate,Support_Site_Access_Expiry_Date__c from Account WHERE (IsDeleted = False) limit 100';
        ID batchprocessid = Database.executeBatch(batchAccountRollups);
        Test.StopTest();              
    } */  
}