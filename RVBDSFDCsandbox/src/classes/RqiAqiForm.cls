/*
*	This class is used to controll the RQI_AQI_SamplingPage.page
*	Input: SFDC User ID
*	Output: List of Articles for AQI or list of Cases for RQI
*/

public with sharing class RqiAqiForm {
    public String cType {get;set;}
    public String dateRange1 {get;set;}
    public String dateRange2 {get;set;}
       
    public Case caseOwner {get;set;}
    public Boolean isKCSAdmin {get;set;}
    public Boolean isCoach {get;set;}
    public User curUserId;
    public List<Case> caseList;
    public List<InQuira_Article_Info__c> articleList;
    public Map<String,User> teamMembers;
    
    public List<Case> getcaseList(){
        return caseList;
    }
    
    //TODO: get articles 
    public List<InQuira_Article_Info__c> getarticleList(){
        return articleList;
    }
    
	// instantiate class
    public RqiAqiForm(ApexPages.StandardController stdController){
		caseOwner = new Case();
        caseList = new List<Case>();
        articleList = new List<InQuira_Article_Info__c>();
        teamMembers = new Map<String,User>();
        isCoach = false;
        isKCSAdmin = false;
        curUserId = [SELECT Id, KCS_Coach__c,KCS_Knowledge_Administrator__c,KCS_Work_Team__c FROM User WHERE Id =:UserInfo.getUserId()];
        
        // validate Coach or Administrator
        if (curUserId.KCS_Knowledge_Administrator__c == true) {
        	isKCSAdmin = true;
        } else if (curUserId.KCS_Coach__c == true) {
        	getTeamMembers();
        	if (!teamMembers.isEmpty()) {
        		isCoach = true;
        	}
        }
    }
    
    public void getTeamMembers() {
    	if (curUserId.KCS_Work_Team__c != null) {
	    	List<User> members = [SELECT Id,KCS_Work_Team__c FROM User WHERE KCS_Work_Team__c = :curUserId.KCS_Work_Team__c];
	    	for (User u : members) {
	    		teamMembers.put(u.Id,u);
	    	}
    	}
    }
    
    public Pagereference valuesFromUser(){
        cType = apexpages.currentpage().getparameters().get('casesorarticles');
        dateRange1 = apexpages.currentpage().getparameters().get('datestart');
        dateRange2 = apexpages.currentpage().getparameters().get('dateend');

        if (isCoach || isKCSAdmin) {
        	setupCasesOrArticles();
        }
         
        return null;
    }
    
        
    public void setupCasesOrArticles(){
        //TODO: Get Cases for RQI
        if(cType.equals('retCases')){
            //get 6 random closed cases owned by the specified user in the date range specified
            System.debug('Type clicked: '+cType);
            Case currentCase;
            List<Date> usingdates = whichDateRange();
            System.debug('Case Owner ID: '+ caseOwner.Case_Owner__c);
            //when you create the RQIs, you need to make sure to exclude cases with related RQIs
            system.debug('ISADMIN:' + isKCSAdmin);
		if (isKCSAdmin || teamMembers.containsKey(caseOwner.Case_Owner__c)) {	
	            caseList = [
	            	SELECT Id, CaseNumber, ClosedDate, Case_Owner__c 
	            	FROM Case 
	            	WHERE Case_Owner__c = :caseOwner.Case_Owner__c 
	            	AND ClosedDate!=null 
	            	AND ClosedDate >= :usingdates.get(0) 
	            	AND ClosedDate <= :usingdates.get(1) 
	            	AND (Status = 'Closed' OR Status ='Closed - Resolved' OR Status = 'Closed - Resolved (First Time Fix)' 
	            		OR Status = 'Closed - Unresolved')
	            	AND Id NOT IN (SELECT Case__c FROM RQI_Audit__c)
	            	LIMIT 100
	        	];
		}
            Integer totCases = caseList.size();
            System.debug('Total Number of Cases: '+ totCases);
            String test = 'These are the case ids returned ';
            if (totCases>6) {
                //choose 6 random cases from all that were returned
                List<Case> realList=new List<Case>();
                test += 'from case count more than 6: ';
                Integer rand, i;
                for (i=0;i<6;i++) {
                    rand = Math.floor(Math.random() * totCases).intValue();
                    currentCase = caseList.get(rand);
                    realList.add(currentCase);
                    test += currentCase.Id+'  ';
                }
                System.debug(test);
                caseList = new List<Case>();
                caseList.addAll(realList);

            } else if(totCases==0||totCases<0){
                //no cases were found
                //message is shown to the user based on caseList.size if its 0 then they are prompted to search again
            }else {
                //use all cases that were returned
                test+= 'from case count less than 6: ';
                for (Case c: caseList){ 
                    test += c.Id+'  ';
                }
                System.debug(test);
            }
           /*
            *	Button: Get Solutions for AQI
            *	-Retrieve 4 publisehd articles created by the selected user
            *		--Only articles attached to cases will be retrieved		
            *	-Avoid articles already being audited 
            *	-4 Articles will be randomly selected among articles created within the 2 month period.
            */
            
            //TODO: Ctype responds to Get Solutions for AQI
        }else if(cType.equals('retArticles')){
            //get 4 published articles owned by the specified user in the date range specified
            System.debug('Type clicked: '+ cType);
            InQuira_Article_Info__c currentArticle;
            
            // Date range within 2 month period
            List<Date> usingdates = whichDateRange();
            System.debug('Article Owner ID: '+ caseOwner.Case_Owner__c);
            
            //when you create the AQIs, you need to make sure to exclude articles in the process of or have already been audited
            List<String> excludeAlreadyAuditedArticles = new List<String>();
            for(SObject sc :[SELECT Solution_ID__c FROM AQI_Audit__c]){
            	try{
            		String i = (Id)sc.get('Solution_ID__c');
            		excludeAlreadyAuditedArticles.add(i);
            	}catch(Exception e){
            		System.debug('not an id, someone corrupted the data');
            	}
            }
            
            /*TODO: get list of articles that are created by the selected user AND attached to cases
            		from IMWS
            			// Set variable for the user that's selected
            			// get a list of articles created by that user within the past 2 months
            */
            if (isKCSAdmin || teamMembers.containsKey(caseOwner.Case_Owner__c)) {			
	            articleList = [
		            SELECT Id, /*ValidationStatus,*/ CreatedDate, OwnerId, Document_ID__c 
		            FROM InQuira_Article_Info__c
		          	// TODO: need to pull article created Date from REST API and replace SFDC CreatedDate
		            WHERE OwnerId = :caseOwner.Case_Owner__c 
		            AND CreatedDate != null
		            AND CreatedDate >= :usingdates.get(0) 
		            AND CreatedDate <= :usingdates.get(1)
		            AND Document_ID__c !=null 
		            AND Document_ID__c NOT IN :excludeAlreadyAuditedArticles
		            LIMIT 100
	            ];
            }
            Integer totArticles= articleList.size();
            System.debug('Total Number of Articles: '+ totArticles);
            String test2 = 'These are the article ids returned ';
            
            if (totArticles>4) {
                //choose 4 publised articles from all that were returned
                List<InQuira_Article_Info__c> realList2 = new List<InQuira_Article_Info__c>();
                test2 += 'from article count more than 6: ';
                Integer rand, i;
                for (i=0;i<4;i++) {
                    rand = Math.floor(Math.random() * totArticles).intValue();
                    currentArticle = articleList.get(rand);
                    realList2.add(currentArticle);
                    test2 += currentArticle.Id+'  ';
                }
                System.debug(test2);
                articleList = new List<InQuira_Article_Info__c>();
                articleList.addAll(realList2);

            } else if(totArticles==0||totArticles<0){
                //no articles were found
                // message is shown to the user based on articleList.size if its 0 then they are prompted to search again by the VF page
            }else {
                //use all cases that were returned
                test2+= 'from case count less than 4: ';
                for (InQuira_Article_Info__c c :articleList){ 
                    test2 += c.Id+'  ';
                }
                System.debug(test2);
            }
        }else{
        	//TODO: reset the form and have the user try again.
        }
    }
    
    /* 
        function to return the date range the user selected 
        TODO: check which radio button is checked to determine which date range to use instead of checking the daterange fields
        Format currently being passed for both daterange and prevmonths:
        Start Date: 2013-10-12 00:00:00 End Date: 2013-12-12 00:00:00
     */
    public List<Date> whichDateRange(){
        List<Date> useThisDateRange = new List<Date>();
        Date dstart;
        Date dend;
        String[] useDate;
        if (!(dateRange1.equals('')) || !(dateRange2.equals(''))) {
            useDate = dateRange1.split('/');
            dstart = Date.newinstance(Integer.valueOf(useDate[2].trim()),Integer.valueOf(useDate[0].trim()),Integer.valueOf(useDate[1].trim()));
            useDate = dateRange2.split('/');
            dend = Date.newinstance(Integer.valueOf(useDate[2]),Integer.valueOf(useDate[0]),Integer.valueOf(useDate[1]));
            System.Debug('Start Date: '+dstart+' End Date: '+dend);
        } else {
            dend = Date.today().addDays(1);
            dstart = Date.today().addMonths(-2);
            System.Debug('Start Date: '+dstart+' End Date: '+dend);
        }
        useThisDateRange.add(dstart);
        useThisDateRange.add(dend);
        return useThisDateRange;
    }
    
}