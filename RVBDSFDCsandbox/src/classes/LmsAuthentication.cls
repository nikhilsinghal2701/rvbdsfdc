public class LmsAuthentication {
    private static string algorithmName='AES128';
    private static string SECRET_KEY='cfTzz2B1yeeIhyj3GVQjjA==';
    //private static string ENDPOINT='http://riverbed.cte.plateausystems.com:80/plateau/user/ssoLogin.do?pseudo_sso_token=';    
    private static string ENDPOINT;
    private LMS_Settings__c lmsSettings;
    private static string timestamp;
    private static string USER_ID;
    private string responseBody;
    private String responseURL = '';
    //private String frameHeight = '0px';
    //private String styleValue = 'block';
    private static Boolean isTestMethod=false;
    private List<Portlet_Right_Navigation__c> links;
    private Map<String, String> linkMap;
    PageReference retPage;
    
    public LmsAuthentication(){  
        //lmsSettings=LMS_Settings__c.getValues('RVBD_LMS_URL');
        lmsSettings=LMS_Settings__c.getValues('Staging URL');   
    /*  links = [Select p.name, p.Document_Url__c From Portlet_Right_Navigation__c p where Portlet_HeadLine__r.Name = 'LMS'];
        if(!links.isEmpty()){
            linkMap = new Map<String, String>();
            for(Portlet_Right_Navigation__c p : links){
                linkMap.put(p.Name,p.Document_Url__c);
            }
        }*/
    }  
    public PageReference homePageCall(){    	
        try{
            ENDPOINT = lmsSettings.Home_Page__c;       
            string token=tokenAuthentication();
			retPage = new PageReference(checkResponse(ENDPOINT,token));
            /*
            HttpResponse res=lmsAPICall(ENDPOINT,token); 
            checkResponse(ENDPOINT,token);         
            if(!isTestMethod){          
                responseBody=res.getBody();
            }*/
        }catch(Exception e){
            system.debug('Exception:'+e);
        } 
        return retPage; 
    } 
    
    public PageReference curriculaCall(){
        try{
            ENDPOINT = lmsSettings.Curricula_Page__c;
            string token=tokenAuthentication(); 
            retPage = new PageReference(checkResponse(ENDPOINT,token));
            /*                       
            HttpResponse res=lmsAPICall(ENDPOINT,token); 
            checkResponse(ENDPOINT,token);           
            if(!isTestMethod){          
                responseBody=res.getBody();
            }*/
        }catch(Exception e){
            system.debug('Exception:'+e);
        } 
        return retPage; 
    } 
    
    public PageReference learningHistoryCall(){
        try{
            ENDPOINT = lmsSettings.Learning_History_Page__c;
            string token=tokenAuthentication();
            retPage = new PageReference(checkResponse(ENDPOINT,token));
            /*                      
            HttpResponse res=lmsAPICall(ENDPOINT,token);
            checkResponse(ENDPOINT,token);             
            if(!isTestMethod){          
                responseBody=res.getBody();
            }*/
        }catch(Exception e){
            system.debug('Exception:'+e);
        }
        return retPage;  
    } 
    private string tokenAuthentication(){
        timestamp=doDateTimeFormat();
        //system.debug('>>>TimeStamp:'+timestamp);
        USER_ID=userInfo.getUserName();
        //system.debug('User name ='+ USER_ID);
        //USER_ID='ggangadi';
        string dataString = USER_ID+'|'+timestamp.trim();
        //system.debug('>>>dataString:'+dataString);
        Blob cryptoKey = EncodingUtil.base64Decode(SECRET_KEY);
        Blob iv = Blob.valueOf('seedinitvector12');                                    
        Blob encryptedData = Crypto.encrypt(algorithmName, cryptoKey, iv, Blob.valueOf(dataString));     
        String b64EncryptedData = EncodingUtil.base64Encode(encryptedData);
        //system.debug('encryptedString:'+b64EncryptedData);
        string token=isNull(b64EncryptedData);
        //system.debug('>>>TOKEN:'+token);
        return token;
    }  
    /*
    public void tokenAuthentication(){
        try{
            timestamp=doDateTimeFormat();
            system.debug('>>>TimeStamp:'+timestamp);
            //USER_ID=userInfo.getUserId();
            USER_ID='zhoward0';
            string dataString = USER_ID+'|'+timestamp.trim();
            system.debug('>>>dataString:'+dataString);
            Blob cryptoKey = EncodingUtil.base64Decode(SECRET_KEY);
            Blob iv = Blob.valueOf('seedinitvector12');
                                        
            Blob encryptedData = Crypto.encrypt(algorithmName, cryptoKey, iv, Blob.valueOf(dataString));     
            String b64EncryptedData = EncodingUtil.base64Encode(encryptedData);
            system.debug('encryptedString:'+b64EncryptedData);
            string token=isNull(b64EncryptedData);
            system.debug('>>>TOKEN:'+token);
            HttpResponse res=lmsAPICall(ENDPOINT,token);
            checkResponse(ENDPOINT,token);
            if(!isTestMethod){          
                responseBody=res.getBody();
            }
        }catch(Exception e){
            system.debug('Exception:'+e);
        }                           
    }
    */
    private HttpResponse lmsAPICall(String urlStr , String body){       
        HttpResponse response=null;     
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(urlStr+body);
            req.setBody(body);
            req.setMethod('POST');
            if(!isTestMethod)
                response = h.send(req);            
        }catch(System.CalloutException ce){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, ce.getMessage());
            ApexPages.addMessage(msg);
        }catch(Exception e){            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(msg);
        }
        return response;  
    }
   /*
    private void checkResponse(String urlStr , String body){   
        responseURL = urlStr+body;
        system.debug('requestURL:'+urlStr+body);
        system.debug('responseURL:'+responseURL);
        frameHeight = '875px';
    }
    */
    private string checkResponse(String urlStr , String body){   
        responseURL = urlStr+body;
        //system.debug('requestURL1:'+urlStr+body);
        //system.debug('responseURL1:'+responseURL);
        //frameHeight = '875px';
        return responseURL;
    }
    
    public String getResponseURL(){
        return responseURL;
    } 
    /*   
    public String getFrameHeight(){
        return frameHeight;
    }    
    public void setFrameHeight(String strHeight){
        this.frameHeight = strHeight;
    }    
    */
    private string doDateTimeFormat(){
        DateTime dt = DateTime.now();
        //system.debug('Date before formating:'+dt);
        string dateFormat=dt.format('yyyyMMdd HH:mm:ss','America/Los_Angeles');
        return dateFormat.trim();
    } 
        
    private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return getURLEncode(originalStr);
    }  
    private string getURLEncode(string originalStr){
        string encodeStr=EncodingUtil.urlEncode(originalStr, 'UTF-8');
        //system.debug('>>> String:'+encodeStr);
        //return encodeStr;
        return originalStr;
    }
    
    //Test Method for LmsAuthentication Controller 
    static testMethod void testLmsAuthentication(){
        isTestMethod=true;
        LmsAuthentication lms=new LmsAuthentication();
        lms.homePageCall();
        lms.curriculaCall();
        lms.learningHistoryCall();
        //lms.getFrameHeight();
        //lms.getResponseURL();
        //lms.setFrameHeight('0px');
    }
    
}