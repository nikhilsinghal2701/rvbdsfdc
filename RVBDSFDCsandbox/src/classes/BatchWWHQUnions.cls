/****
*This batch class is used to query all WW HQ objects not in a deleted state
*Does an additional query to get Assets, Contacts & Opportunities
*attempts to get all the Riverbed Products Owned
*then all the cloud providers
*then key applications
*then network hardware
*then Network Mgmt
*then storage hardware
*then storage mgmt
*then national var
*then SI
*then SP
*then loops through assets for the Support End Date
*then loops through contacts to get a count
*then loops through opportunities for a last sold to date and sold to partner
*/

global without sharing class BatchWWHQUnions implements Database.Batchable<sObject>, Database.Stateful {

	global String Query;

	global BatchWWHQUnions(){
   
		//Query = 'Select Id, NumberOfEmployees__c, AmountClosedOpportunities__c, FirstClosedOpportunity__c, LastClosedOpportunity__c, NoOfEndOfLifexx10Product__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, TotalStage2or6Opportunities__c, RiverbedProductsOwned__c, CloudProviders__c, KeyApplications__c, NetworkHardware__c, NetworkManagement__c, StorageHardware__c, StorageManagement__c, NationalVAR__c, NationalVARComments__c, SI__c, SIComments__c, SP__c, SPComments__c FROM WW_HQ__c WHERE Id=\'' + 'a1MR0000000AHFr' + '\'';
   		Query = 'Select Id, NumberOfEmployees__c, AmountClosedOpportunities__c, FirstClosedOpportunity__c, LastClosedOpportunity__c, NoOfEndOfLifexx10Product__c, TotalClosedOpportunities__c, TotalOpenOpportunities__c, TotalStage2or6Opportunities__c, RiverbedProductsOwned__c, CloudProviders__c, KeyApplications__c, NetworkHardware__c, NetworkManagement__c, StorageHardware__c, StorageManagement__c, NationalVAR__c, NationalVARComments__c, SI__c, SIComments__c, SP__c, SPComments__c FROM WW_HQ__c WHERE (IsDeleted = False)';
   
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	   	   		
   		List<WW_HQ__c> hqToUpdate = new List<WW_HQ__c>();
   		
   		for(sObject so : scope)
   		{
   			WW_HQ__c hq = (WW_HQ__c)so;
   		
	   		String rpo = '',cp = '',ka = '',nh = '',nm = '',sh = '',sm = '', nv = '', nvc = '', si = '', sic = '', sp = '', spc = '', rd='',
	   		SoldToPartnerType = '', Tier2Type = '';
	   		Integer ref = 0, con = 0;
	   		Date nrd,LastSoldToDate;
   			ID SoldToPartner,Tier2;
		
		   /*   
			*  Update 4: Set the Company field “Assets Under Trial” to the sum of the “Assets Under Trial” field for each linked Account.
			
		   		
		   		AggregateResult lcount =  [ Select WW_HQ__c, Sum(AssetsUnderTrial__c) FROM Account 
		   										WHERE WW_HQ__c = :hq.Id  GROUP BY WW_HQ__c];
		   		
		   		hq.AssetsUnderTrial__c = Integer.ValueOf(lcount.get('expr0'));
				system.debug('\n Company Assets Under Trial :'+Integer.ValueOf(lcount.get('expr0')));
			*/

	   		String sOwners = '';  // Field to capture all related Alias from Accounts related to the Company
			String cOwners = '';  // Field to capture current owner and provide logic so that alias is not repeated
			
			for(Account a:[SELECT Id, Owner.alias, Riverbed_Products_Owned__c,Cloud_Providers__c, Key_Applications__c, Network_Hardware__c, Network_Management__c, Storage_Hardware__c, Storage_Management__c, National_VAR__c, National_VAR_Comments__c, SI__c, SI_Comments__c, SP__c, SP_Comments__c, Reference_Private__c, Reference_Public__c,
				(SELECT Id, Support_End_Date__c from Assets),
		   		(SELECT Id from Contacts),
		   		(SELECT Id, Sold_To_Partner__c, Sold_To_Partner_Type__c, CloseDate, StageName, Partner_Closest_to_Customer_Id__c from Opportunities)			
				from Account WHERE WW_HQ__c = :hq.Id
				ORDER BY Owner.alias])							
			{							
				
				/*
	   			*  Update 7: Create a concatenation of Account.Account Owner Alias to populate field Company.All Related Account Owners (3)
	   			*/
				if (cOwners != a.owner.alias) {
				 		sOwners = sOwners + a.owner.alias+',';
				 		cOwners = a.owner.alias;
				}
					 	
				
				if(a.Riverbed_Products_Owned__c != null)
				{
					for(String s: a.Riverbed_Products_Owned__c.Split(';')){
						if(!rpo.contains(s))
						{
							rpo += s + ';';
						}
					}				
				}
				if(a.Cloud_Providers__c != null)
				{
					for(String s: a.Cloud_Providers__c.Split(';')){
						if(!cp.contains(s))
						{
							cp += s + ';';
						}
					}				
				}	
					
				if(a.Key_Applications__c != null)
				{
					for(String s: a.Key_Applications__c.Split(';')){
						if(!ka.contains(s))
						{
							ka += s + ';';
						}
					}				
				}
					
				if(a.Network_Hardware__c != null)
				{
					for(String s: a.Network_Hardware__c.Split(';')){
						if(!nh.contains(s))
						{
							nh += s + ';';
						}
					}				
				}
				
				if(a.Network_Management__c != null)
				{
					for(String s: a.Network_Management__c.Split(';')){
						if(!nm.contains(s))
						{
							nm += s + ';';
						}
					}				
				}
					
				if(a.Storage_Hardware__c != null)
				{
					for(String s: a.Storage_Hardware__c.Split(';')){
						if(!sh.contains(s))
						{
							sh += s + ';';
						}
					}				
				}
				
				if(a.Storage_Management__c != null)
				{
					for(String s: a.Storage_Management__c.Split(';')){
						if(!sm.contains(s))
						{
							sm += s + ';';
						}
					}				
				}	
				if(a.National_VAR__c != null)
				{
					for(String s: a.National_VAR__c.Split(';')){
						if(!nv.contains(s))
						{
							nv += s + ';';
						}
					}				
				}
					
				if(a.SI__c != null)
				{
					for(String s: a.SI__c.Split(';')){
						if(!si.contains(s))
						{
							si += s + ';';
						}
					}				
				}	
								
				if(a.SP__c != null)
				{
					for(String s: a.SP__c.Split(';')){
						if(!sp.contains(s))
						{
							sp += s + ';';
						}
					}				
				}	
												
				if(a.National_VAR_Comments__c != null)
				{
					nvc += a.National_VAR_Comments__c;
				}
				
				if(a.SI_Comments__c != null)
				{
					sic += a.SI_Comments__c;
				}
					
				if(a.SP_Comments__c != null)
				{
					spc += a.SP_Comments__c;
				}
				
				if(a.Reference_Private__c != null || a.Reference_Public__c != null)
				{
					ref += 1;
					
					if(a.Reference_Private__c != null)
					{
						for(String s: a.Reference_Private__c.Split(';')){
							if(!rd.contains(s))
							{
								rd += s + ';';
							}
						}
					}
					
					if(a.Reference_Public__c != null)
					{
						for(String s: a.Reference_Public__c.Split(';')){
							if(!rd.contains(s))
							{
								rd += s + ';';
							}
						}
					}					
				}
				
				for (Asset ast:a.Assets)
				{
					if(ast.Support_End_Date__c != null && ast.Support_End_Date__c >= system.today())					
					{
						if(nrd == null || (ast.Support_End_Date__c < nrd))
						{
							nrd = ast.Support_End_Date__c;
						}
					}
				}
				
				
				for (Contact c:a.Contacts)
				{
					con += 1;
				}
				
				for (Opportunity opp: a.Opportunities)
				{
					if(opp.StageName.Contains('6') && (LastSoldToDate == null || opp.CloseDate > LastSoldToDate))					
					{
						LastSoldToDate = opp.CloseDate;
						SoldToPartner = opp.Partner_Closest_to_Customer_Id__c;
						//SoldToPartnerType = opp.Sold_To_Partner_Type__c;
						//Tier2 = opp.Tier2__c;
						//Tier2Type = opp.Tier2_Type__c;
					}
				}									  
			}
			if(sOwners!=null)
			{
				integer len = sOwners.length();
				if(len>255)
					len=255;
				sOwners = sOwners.substring(0,len);
			}	
			hq.All_FM_Owners__c = sOwners; // Track the Owner alias from Accounts	
			system.debug('\n All FM Owners :'+sOwners);
			
			hq.RiverbedProductsOwned__c = rpo;
			hq.CloudProviders__c = cp;
			hq.KeyApplications__c =ka;
			hq.NetworkHardware__c = nh;
			hq.NetworkManagement__c = nm;
			hq.StorageHardware__c = sh;
			hq.StorageManagement__c = sm;
			hq.NationalVAR__c =nv;
			hq.NationalVARComments__c = nvc;
			hq.SI__c = si;
			hq.SIComments__c = sic;
			hq.SP__c = sp;
			hq.SPComments__c = spc;	
			hq.CountOfReferenceAccounts__c = ref;
			hq.ReferenceDetails__c = rd;
			hq.NextRenewalDate__c = nrd;
			hq.CountOfContacts__c = con;
			hq.MostRecentSoldToPartner__c = SoldToPartner;
			//hq.MostRecentSoldToPartnerType__c = SoldToPartnerType;
			//hq.MostRecentTier2__c = Tier2;
			//hq.MostRecentTier2Type__c = Tier2Type;			
			
			hqToUpdate.add(hq);
   		}   	
		update hqToUpdate;
								
   }
   
   global void finish(Database.BatchableContext BC){

   }
	
}