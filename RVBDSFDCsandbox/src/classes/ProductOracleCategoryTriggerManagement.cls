/*******************************************************************
Class: ProductOracleCategoryTriggerManagement
This class will manage all the operations that should happen on product oracle categories after any dml operations 
on this object. It will be invoked from ProductOracleCategoryTriggers trigger

Author: Rucha Pradhan
Date: 6/13/2013
********************************************************************/

public with sharing class ProductOracleCategoryTriggerManagement {
    public static void populateCategoryId(List<Product_Oracle_Category__c> pocList){
        Set<String> oracleCatIdSet = new Set<String>();
        Map<String,List<Product_Oracle_Category__c>> pOCMap = new Map<String,List<Product_Oracle_Category__c>>();
        for(Product_Oracle_Category__c poc : pocList){
            String str = poc.PWS_Oracle_category_id__c + '-' + poc.PWS_Category_Set_Id__c;
            oracleCatIdSet.add(str);
            if(pOCMap.containsKey(str)){
                List<Product_Oracle_Category__c> pList = pOCMap.get(str);
                pList.add(poc);
            }else{
                List<Product_Oracle_Category__c> pList = new List<Product_Oracle_Category__c> ();
                pList.add(poc);
                pOCMap.put(str,pList);
            }
        }
        System.debug(LoggingLevel.INFO, 'pOCMap Size = '+ pOCMap.size());
        //Get the sfdc ids corresponding to PWS Oracle Ids
        Map<String,Id> pwsOrclCatToSfdcCatIdMap = new Map<String,Id>();
        for(Oracle_Category__c oc : [SELECT Id,Oracle_Category_Id__c, Oracle_Category_Id_Category_Set_Id__c FROM Oracle_Category__c WHERE Oracle_Category_Id_Category_Set_Id__c in :pOCMap.keySet()]){
            pwsOrclCatToSfdcCatIdMap.put(oc.Oracle_Category_Id_Category_Set_Id__c,oc.Id);
        }
        System.debug(LoggingLevel.INFO, 'pwsOrclCatToSfdcCatIdMap Size = '+ pwsOrclCatToSfdcCatIdMap.size());
        
        //Populate Oracle_Category__c on product Oracle Category with sfdc id corresponding to PWS Oracle Id
        /*for(Product_Oracle_Category__c poc : pocList){
            poc.Oracle_Category__c = pwsOrclCatToSfdcCatIdMap.get(poc.PWS_Oracle_category_id__c);
        }*/
        for(String str : pOCMap.keyset()){
            List<Product_Oracle_Category__c> pList = pOCMap.get(str);
            for(Product_Oracle_Category__c poc : pList){
                if(pwsOrclCatToSfdcCatIdMap.containsKey(str)){
                    System.debug(LoggingLevel.INFO, 'Found Oracle Category');
                    poc.Oracle_Category__c = pwsOrclCatToSfdcCatIdMap.get(str);
                }
            }
        }
    }
}