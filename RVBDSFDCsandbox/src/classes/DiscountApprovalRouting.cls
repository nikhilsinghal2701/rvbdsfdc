/**
 * Non Standard Discount approval routing logic. Provides functionality to manage the approval process 
 * by loading appropriate approvers for a particular step.
 *   
 * @author megar@apprivo.com
 * Modified by SKM - NSD2.0 
 * Modified by Rohit Mehta - 021010
 * Modified by Rucha - For Delta Discounts
 */
public with sharing class DiscountApprovalRouting {
    private static final DiscountApprovalRouting INSTANCE = new DiscountApprovalRouting();
    
    private static Boolean isRSMProcessed=false;
    
    public static final String RSM = 'Regional Sales Manager';
    
    //Dynamic SOQL field names
    public static final String CATEGORY_FIELD = 'Category_';
    public static final String APPROVER_FIELD = 'Approver_';
    public static final String STEP_FIELD = 'Step_';
    
    //To do - Remove
    private static final String SUPPORT_CAT = 'B';
    private static final String REASON_ERROR=  'Discount Reason can not be edited when the discount type is Promotional.';
    
    private DiscountApproverCache discountApprovers;
    private ApprovalMatrixCache approvalMatrix;
    private Map<Id,User> usersById;
    private LineItemData lineItemData;
    
    private DiscountApprovalRouting() {
        discountApprovers = new DiscountApproverCache();
        approvalMatrix = new ApprovalMatrixCache();
    }
    
    /**
     * Singleton factory method.
     */
    public static DiscountApprovalRouting getInstance() {
        return INSTANCE;
    }

    /**
     * Process an approval step by loading in the new set of approvers for the next step (if any) 
     *
     * @param quotes the Trigger newMap from the context
     * @param oldMap the Trigger oldMap from the context
     */
    public void processApprovalStep(Map<Id,Quote__c> newQuotes, Map<Id,Quote__c> oldQuotes) {
        List<Quote__c> quotesInApproval = new List<Quote__c>();
        List<Quote__c> lowValueQuotesInApproval = new List<Quote__c>();
        for (Quote__c quote : newQuotes.values()) {
            if ((quote.Discount_Status__c.equalsIgnoreCase(QuoteApprovalHlp.STATUS_PENDING) || quote.Discount_Status__c.equalsIgnoreCase(QuoteApprovalHlp.STATUS_MORE_INFORMATION))){
                if(quote.Quote_Total_Only_Std_Discount__c >= Double.valueOf(Label.LV_CutOff)) {//Added by Rucha (5/15/2013) so that Discount Approvers get calculated for only high value deals
                    // We ae only interested in quotes that are pending approval or more information
                    quotesInApproval.add(quote);
                }
                else{
                    lowValueQuotesInApproval.add(quote);
                }
            }
        }
        
        if(!lowValueQuotesInApproval.isEmpty()){
            Map<Id,Quote__c> quotesToSendEmailForMap = new Map<Id,Quote__c>();
            for (Quote__c quote : lowValueQuotesInApproval) {
                Quote__c oldQuote = oldQuotes.get(quote.Id);
                if((quote.Discount_Status__c == QuoteApprovalHlp.STATUS_PENDING) && (quote.Discount_Status__c != oldQuote.Discount_Status__c)){
                    quote.Send_Email__c = DiscountApprovalEmailHlp.EmailType.APPROVE.name();
                    quotesToSendEmailForMap.put(quote.Id,quote);
                }               
            }
            if(!quotesToSendEmailForMap.isEmpty()){
                DiscountApprovalEmailHlp.getInstance().processQuotes(quotesToSendEmailForMap);
            }
        }
        if (quotesInApproval.isEmpty()) {
            // Nothing to do
            return;
        }
        
        // Load and index users related to this quote. We'll need this later for handling delegations.
        // TODO: DiscountEmailHlp is doing almost exact same load to send out emails. Share!!!
        Set<Id> userIds = new Set<Id>();
        userIds.addAll(DiscountApprovalHlp.extractRSMs(quotesInApproval));
        userIds.addAll(DiscountApprovalHlp.extractApproverIds(quotesInApproval));
        usersById = DiscountApprovalHlp.loadUsersById(userIds);
        
        // Load and cache past approvers and approval matrix
        discountApprovers.loadByQuoteIds(newQuotes.keySet());
        approvalMatrix.loadByQuoteIds(newQuotes.keySet());
        
        // Pending quotes are quotes that were just submitted for approval
        // Those quotes need special handling (clear previous Discont Approvers, reset Previous Approver field, etc.)
        List<Quote__c> pendingQuotes = new List<Quote__c>();
        List<NSD_Approval_History__c> history = new List<NSD_Approval_History__c>();
        
        for (Quote__c quote : quotesInApproval) {
            Quote__c oldQuote = oldQuotes.get(quote.Id);
            Boolean reset = (quote.Discount_Status__c == QuoteApprovalHlp.STATUS_PENDING) && (quote.Discount_Status__c != oldQuote.Discount_Status__c);
            if (reset) {
                // Verify Approval Routing Matrix is not empty; otherwise, it's going to create problems down the road.
                List<Approvers_Routing_Matrix__c> approvalChain = approvalMatrix.getApprovalChain(quote.Id);
                if (approvalChain.size() == 0) {
                    quote.addError('This quote may not be approved because the approval chain could not be calculated.');
                } else {
                    pendingQuotes.add(quote);
                }
            } else if (quote.Next_Step__c == true) {
                if (!DiscountApprovalHlp.isEmailUserId(UserInfo.getUserId())) {
                    if (quote.All_In__c == false) {
                        Discount_Approver__c approver = new Discount_Approver__c(Approver__c=UserInfo.getUserId(),Quote__c=quote.Id);
                        discountApprovers.addApprover(approver);
                    }
                    history.add(DiscountApprovalHlp.createApprovalRecord(quote, DateTime.now(), quote.Discount_Status__c, quote.Approver__c));                    
                }
                advanceApprover(quote);
        
                // Reset next Next Step flag
                quote.Next_Step__c = false;
                if (quote.Discount_Status__c == QuoteApprovalHlp.STATUS_MORE_INFORMATION) {
                    // Change Discount Status back to Pending Approval in case more informationw as requested during last approval step.
                    quote.Discount_Status__c = QuoteApprovalHlp.STATUS_PENDING;
                }
            }
        }
           
        if (!pendingQuotes.isEmpty()) {
            discountApprovers.deleteApprovers((new Map<Id,Quote__c>(pendingQuotes)).keySet());
            for (Quote__c quote : pendingQuotes) {
                DiscountApprovalHlp.setApprovedUsers(quote, false);
                DiscountApprovalHlp.clearApprovers(quote);
                quote.Previous_Approver__c = null;
                quote.Previous_Delegated_Approver__c = null;
                //commenting so requests can be routed to the RSM
                //skipRSMApproval(quote);
                advanceApprover(quote);
            }
        }
        
        // Flush any pending Discount Approver inserts/updates. 
        discountApprovers.flush();
        
        if (history.size() > 0) {
            insert history;
        }
        
    }

    /**
     * Examines specified quotes and related line items to evaluate whether Discount Status should be Required or Not Required.
     * Approved or Not Required quotes are also updated with discounts from related Opportunity.
     */
    public void processQuotes(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotes) { 
        Map<Id,Quote__c> quotesById = new Map<Id,Quote__c>(quotes);
        LineItemData lineData = loadLineItemData(quotesById.keySet());
        Set<Id> oppIds = DiscountApprovalHlp.extractOpportunityIds(quotes);
        Map<Id,Opportunity> oppsById = DiscountApprovalHlp.loadOpportunitiesByIds(oppIds);
    /**Code Modified by Santoshi Mishra on Nov-2011 to insert the discount values in the discount detail table. */  
        Set<ID> OpptyUpdateSet = new Set<id>(); // this will store the id of the opportunities to be updated.
        Map<ID,ID> quoteUpdateMap= new Map<Id,Id>();
        /**********-Code End************************/
        
        
        for (Quote__c quote : quotesById.values()) {
            String status = quote.Discount_Status__c.toLowerCase();
            String oldStatus = oldQuotes.get(quote.Id).Discount_Status__c;
            Boolean justApproved = QuoteApprovalHlp.isJustApproved(oldQuotes.get(quote.Id), quote);
            if (!(status.equalsIgnoreCase(QuoteApprovalHlp.STATUS_PENDING) || status.equalsIgnoreCase(QuoteApprovalHlp.STATUS_MORE_INFORMATION) || status.equalsIgnoreCase(QuoteApprovalHlp.STATUS_REJECTED) || justApproved)) {
                // Evaluate Discount Status only for quotes that are NOT in Pending Ppproval or Rejected or More Information
                // Quotes that were JUST Approved should be excluded also as we need to give After trigger a chance to update approved discounts on the Oppty.
                Opportunity opp = oppsById.get(quote.Opportunity__c);
                System.assert(opp != null, 'Unable to find Opportunity: ' + quote.Opportunity__c + '; Opportunity Cache: ' + oppsById);
                List<Quote_Line_Item__c> lineItems = lineData.get(quote.Id);
                Map<String,Decimal> discountsByCategory = lineData.getDiscountsByCategory(quote.Id);  
                Map<String,Decimal> discountsByCategorySpecial = lineData.getDiscountsByCategoryNew(quote.Id);//added by psingh@riverbed.com on 08/27/2013
                // Checks if category has discount and >0
                Boolean hasNSD = false;
                if ((discountsByCategory != null) && !discountsByCategory.isEmpty()) {
                    for (Decimal discount : discountsByCategory.values()) {
                        if ((discount != null) && (discount > 0)) {
                            hasNSD = true;
                        }
                    }
                }
                
                quote.Max_Level__c = (hasNSD) ? 1 : 0;
                System.debug(LoggingLevel.INFO,'----'+hasNSD +'--'+QuoteApprovalHlp.isUnderValidPromotion(quote)+ '--'+QuoteApprovalHlp.isApprovedDiscountExceeded(opp, discountsByCategory));
                if (hasNSD) {
                    if (QuoteApprovalHlp.isUnderValidPromotion(quote) || 
                            !QuoteApprovalHlp.isApprovedDiscountExceeded(opp, discountsByCategorySpecial) //|| //modified by psingh@riverbed.com on 08/27/2013
                            //QuoteApprovalHlp.isOnlyApproverRSM(approvalMatrix.getApprovalChain(quote.Id))
                            ) {
                        quote.Discount_Status__c = QuoteApprovalHlp.STATUS_APPROVED;
                    } else {
                        quote.Discount_Status__c = QuoteApprovalHlp.STATUS_REQUIRED;
                        System.debug(LoggingLevel.INFO,'status----'+quote.Discount_Status__c);
                    }
                } else {
                    quote.Discount_Status__c = QuoteApprovalHlp.STATUS_NOT_REQUIRED;
                }
            System.debug(LoggingLevel.INFO,'aappp----------------' +QuoteApprovalHlp.STATUS_APPROVED.equalsIgnoreCase(oldStatus)+'old--' +oldStatus+'---'+QuoteApprovalHlp.STATUS_APPROVED);
                // TODO: Is this right? Should it go off previous status?
                if (opp != null && (QuoteApprovalHlp.STATUS_APPROVED.equalsIgnoreCase(oldStatus) || QuoteApprovalHlp.STATUS_NOT_REQUIRED.equalsIgnoreCase(oldStatus))) {
                  /**Code commented by Santoshi Mishra on Nov-2011 to insert the discount values in the discount detail table. 
                    quote.DiscountSupport_1__c = opp.DiscountSupport_1__c;
                    quote.DiscountSupport_2__c = opp.DiscountSupport_2__c;
                    quote.DiscountSupport_3__c = opp.DiscountSupport_3__c;            
                    quote.Discount_Product__c = opp.DiscountProduct__c;
                    quote.Discount_Support__c = opp.DiscountSupport__c;
                    quote.Discount_Demos__c = opp.Discount_Demos__c;
                    quote.Discount_Training__c = opp.DiscountService__c;
                    quote.Discount_Spare__c = opp.DiscountSpare__c;
                    *************************************************/
                    
                    /**Code Modified by Santoshi Mishra on Nov-2011 to insert the discount values in the discount detail table. */
                   System.debug(LoggingLevel.INFO,'oppp----------------' + Opp+'2222222--'+opp.Id+'33-'+quote+'4444--'+quote.id);
                     OpptyUpdateSet.add(opp.Id);
                     quoteUpdateMap.put(quote.ID,opp.Id);
                    /**********Code End******/
                }
            }
        }
       /**Code Modified by Santoshi Mishra on Nov-2011 to insert the discount values in the discount detail table. */ 
        if(OpptyUpdateSet.size()>0)
        {
             DiscountManagementHelper cls = new DiscountManagementHelper();
             System.debug('Quote Update Map..' +quoteUpdateMap+'-----------'+OpptyUpdateSet);
              System.debug(LoggingLevel.INFO,'oppp ids---------------' + OpptyUpdateSet );
             cls.UpdatesStdDiscountInDisDetail(quoteUpdateMap,OpptyUpdateSet,true,null);  
        }
        /**********Code End******/ 
    }
    
    /**
     * Saves non-standard discounts on approved quotes to the corresponding opportunities.
     * This method takes care to only process quotes where Discont Status just changed to Approved.
     */
    public void saveApprovedDiscountsToOpportunity(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotes) {
    List<Quote__c> approvedQuotes = new List<Quote__c>();
    Set<ID> ApprovedQuoteIds = new Set<Id>();
        Map<Id,Id> quoteVsOpty = new Map<Id,Id>();
        for (Quote__c quote : quotes) {
            Quote__c oldQuote = oldQuotes.get(quote.Id);
            // Only quotes that were just approved and are not under promo should
            // write approved discounts back to the oppty
            System.debug(LoggingLevel.INFO,'dis111111133333333333333'+quote.Discount_Status__c+'---'+oldQuote.Discount_Status__c+'----'+QuoteApprovalHlp.isUnderValidPromotion(quote));
            if ((quote.Discount_Status__c == QuoteApprovalHlp.STATUS_APPROVED) && 
                    (quote.Discount_Status__c != oldQuote.Discount_Status__c) &&
                    !QuoteApprovalHlp.isUnderValidPromotion(quote)) {
                approvedQuotes.add(quote);
                ApprovedQuoteIds.add(quote.Id);
                quoteVsOpty.put(quote.Id,quote.Opportunity__c);
            }
        }
        if (!ApprovedQuoteIds.isEmpty()) {
            Set<Id> oppIds = DiscountApprovalHlp.extractOpportunityIds(approvedQuotes);
            Map<Id,Opportunity> oppsById = DiscountApprovalHlp.loadOpportunitiesByIds(oppIds);
        // Added by Santoshi on Nov -7-2011 for updating opty discount details with the discount details from the approved quotes.
             DiscountManagementHelper cls = new DiscountManagementHelper();
             Map<String,Map<String,Decimal>> QuoteDiscountCategories = new Map<String,Map<String,Decimal>>();
             
             for (Quote__c quote : approvedQuotes) {
                Opportunity opp = oppsById.get(quote.Opportunity__c);
                if (opp != null) {
                    Map<String,Decimal> discountsByCategory = lineItemData.getDiscountsByCategoryNew(quote.Id);//modified by psingh@riverbed.com on 08/27/2013
                    if(!QuoteDiscountCategories.containskey(quote.Id))
                    QuoteDiscountCategories.put(Quote.Id,discountsByCategory);
                System.debug(LoggingLevel.INFO,'dis1111111111----2222222222'+QuoteDiscountCategories+'----'+quote.Id);  
                }
             }  
        //   Map<String,Decimal> discountsByCategory = lineItemData.getDiscountsByCategory(quote.Id);
              cls.UpdatesStdDiscountInDisDetail(quoteVsOpty,oppIds,false,QuoteDiscountCategories);   
            /**loadLineItemData((new Map<Id,Quote__c>(approvedQuotes)).keySet());
            for (Quote__c quote : approvedQuotes) {
                Opportunity opp = oppsById.get(quote.Opportunity__c);
                if (opp != null) {
                    Map<String,Decimal> discountsByCategory = lineItemData.getDiscountsByCategory(quote.Id);
                    QuoteApprovalHlp.updateOpportunityWithApprovedDiscounts(quote, opp, discountsByCategory);
                } else {
                    System.debug('No opportunity found. Quote: ' + quote);
                }
            }
            // Delegate to QuoteApprovalHlp since it's declared "without sharing".
            // Users approving quotes (and therefore updating approved disconts back to oppty)
            // may not have edit rights to the oppty record  
            QuoteApprovalHlp.updateOpportunities(oppsById.values());*/
        }
    }

    public static void processPromotions(Map<Id,Quote__c> newQuotes, Map<Id,Quote__c> oldQuotes) {
        Set<String> codes = new Set<String>();
        for (Quote__c quote : newQuotes.values()) {
            // We are only interested in quotes that have a change in Discount Code and are in eligible status.
            Quote__c oldQuote = oldQuotes.get(quote.Id);
            if (quote.Discount_Code__c != null && (oldQuote.Discount_Code__c != quote.Discount_Code__c) && 
                    QuoteApprovalHlp.PROMOTION_VALID_STATUSES.contains(quote.Discount_Status__c.toLowerCase())) {
                codes.add(quote.Discount_Code__c);
            }   
        }
        System.debug('*** Discount Codes: ' + codes);
        
        if (!codes.isEmpty()) {
            Map<String,Discount_Schedule__c> schedulesByReasonCode = new Map<String,Discount_Schedule__c>();
            for (Discount_Schedule__c schedule: [SELECT Start_Date__c, Reason_Text__c, End_Date__c, Discount_Type__c, Reason_Code__c FROM Discount_Schedule__c WHERE Reason_Code__c IN :codes AND Active__c = true]) {
                schedulesByReasonCode.put(schedule.Reason_Code__c, schedule);                                                          
            }
            System.debug('*** Promotions: ' + schedulesByReasonCode);
            
            for (Quote__c quote: newQuotes.values()){
                Discount_Schedule__c schedule = schedulesByReasonCode.get(quote.Discount_Code__c);
                if (schedule != null) {
                    quote.Promotional__c = true;    
                    quote.Discount_Reason__c  = schedule.Reason_Text__c;              
                    if ((Date.today() >= schedule.Start_Date__c) && (Date.today() <= schedule.End_Date__c)) {
                        quote.Promotion_Expired__c = false;
                    } else{
                        quote.Promotion_Expired__c = true;
                    }   
                    quote.Expires__c =  quote.End_Date__c;  
                } else {
                    quote.Promotional__c = false;   
                    quote.Promotion_Expired__c = false; 
                    quote.Expires__c = null;   
                }
            }      
        }
    }

    public void generateApprovalMatrix(List<Quote__c> quotes, Map<Id,Quote__c> oldQuotes) {
        Map<Id,Id> roleIds = new Map <Id, Id> ();
        Map<Id,Id> parentRoleIds = new Map <Id, Id> ();
        Map<Id,Id> geoVPRoleIds = new Map <Id, Id> ();
        Map<Id,UserRole> roles = new Map<Id,UserRole>();
        List<Quote__c> quotesToProcess = new List<Quote__c>();
        
        Map<Id,Double> quoteToPriceAfterStdDiscount = new Map<Id,Double>();
        for(AggregateResult agr : [Select Quote__c,sum(Disti_Std_Ext_Price__c)qTotal From Quote_Line_Item__c
                                    Where Quote__c in :quotes
                                    Group By Quote__c]){
                                        quoteToPriceAfterStdDiscount.put((Id)agr.get('Quote__c'),(Double)agr.get('qTotal'));
            
        }           

        for (Quote__c quote : quotes) {
            Boolean justApproved = QuoteApprovalHlp.isJustApproved(oldQuotes.get(quote.Id), quote);
            // Do not generatae Approval Matrix for quotes that have no RSM or in Pending Approval, More Information status or that have just been approved.
            if ((quote.RSM__c == null) || 
                    justApproved || 
                    quote.Discount_Status__c.equalsIgnoreCase(QuoteApprovalHlp.STATUS_PENDING) || 
                    quote.Discount_Status__c.equalsIgnoreCase(QuoteApprovalHlp.STATUS_MORE_INFORMATION) ||
                    quoteToPriceAfterStdDiscount.get(quote.Id) < Double.valueOf(Label.LV_CutOff)) {
                continue;
            }
            quotesToProcess.add(quote);
        }

        Set<Id> rsmIds = new Set<Id>();
        Set<Id> quoteIds = new Set<Id>();
        for (Quote__c quote : quotesToProcess){
            rsmIds.add(quote.RSM__c);
            quoteIds.add(quote.Id);
        }
        
        loadLineItemData(quoteIds);
        //Added by rucha (5/13/2013)
        Map<Id,Map<String,List<String>>> quoteToProductFamiliesMap = getProductFamiliesforQuotes(quotesToProcess);  
        
        //Added by Rucha (6/5/2013) for product family routing
        //Create a map of overlay role to corresponding oracle categories which need routing
        Map<String,Map<String,String>> overlayRoleToOracleCategoryMap = getOverlayRoleToOracleCategoryMap();           
        //
        for (Quote__c quote : quotesToProcess) {
            Map<String,Decimal> discountsByCategory = lineItemData.getDiscountsByCategory(quote.Id);            
            // Get all users and roles required to create matrix by the RSM user
            Map<String,UserRoleObj> hierarchyByRoleName = DiscountApproverMatrixHelper.computeHierarchyByRoleName(rsmIds);          
            List<Approvers_Routing_Matrix__c> approvers = DiscountApproverMatrix.createApproversMatrixForQuote(quote, hierarchyByRoleName, discountsByCategory, quoteToProductFamiliesMap.get(quote.Id),overlayRoleToOracleCategoryMap);
            approvalMatrix.putApprovalChain(quote.Id, approvers);
            //System.assert(false, 'MATRIX: ' + [SELECT User__r.Name, Level__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :quote.Id]);
        }
    }
    
    private void skipRSMApproval(Quote__c quote) {
        List<Approvers_Routing_Matrix__c> approvalChain = approvalMatrix.getApprovalChain(quote.Id);
        if (approvalChain[0].Level__c == 0) {
            discountApprovers.addApprover(new Discount_Approver__c(Quote__c=quote.Id,Approver__c=approvalChain[0].User__c));
            DiscountApprovalHlp.populateApprovers(quote, approvalChain[0].User__c);
        }
    }
    
    
    /**
     * Advances specified quote to next approver and returns User ID for that approver. Return null if there are no
     * more approvers.
     */
    private Boolean advanceApprover(Quote__c quote) {
        List<Approvers_Routing_Matrix__c> approvalChain = approvalMatrix.getApprovalChain(quote.Id);
        DiscountApproverCache.QuoteApprovers pastApprovers = discountApprovers.getOrCreateApprovers(quote.Id);
        
        // Delegated Approver is normally stored in Approver1__c field. Regular Approver is stored in Approver2-10.
        Id delegatedApproverId = quote.Approver1__c;
        Id originalApproverId = quote.Approver2__c;
        
        //Discount_Approver__c lastApprover = approvers.getLastApprover(approvalChain);
        quote.Previous_Approver__c = originalApproverId;
        if (delegatedApproverId != originalApproverId) {
            // If Approver1 and Approver2 are not the same there was a delegated approver on this step. Save it.
            quote.Previous_Delegated_Approver__c = delegatedApproverId;
            
            // Also, check if delegated user is in Discount Approvers list AND current user is not the original approver; 
            // If false, add him so we have a record of delegation and we don't ask to approve again later.
            if (!pastApprovers.hasUserApproved(delegatedApproverId) && (originalApproverId != UserInfo.getUserId())) {
                Discount_Approver__c approver = new Discount_Approver__c(Approver__c=delegatedApproverId,Quote__c=quote.Id);
                approver.Comments__c = 'On behalf of ' + usersById.get(originalApproverId).Name;
                discountApprovers.addApprover(approver);
                
                // Now, find the Discount Approval record for original approver, and make a note there.
                pastApprovers.getApproverByUserIdRequired(originalApproverId).Comments__c = 'Delegated to ' + usersById.get(delegatedApproverId).Name;
            }
        } else {
            quote.Previous_Delegated_Approver__c = null;
        }
        
        Discount_Approver__c approver = pastApprovers.createNextApprover(approvalChain);
        if (approver == null) {
            // Everyone in the approval chain has approved; we are done.
            // Setting all Approved User fields to true quits the approval process.
            DiscountApprovalHlp.setApprovedUsers(quote, true);
            return false;
        }
        discountApprovers.addApprover(approver);
        
        DiscountApprovalHlp.populateApprovers(quote, approver.Approver__c);
        if (!quote.Approved_User10__c) {
            User rsmUser = usersById.get(quote.RSM__c);
            // See if the current approver has active delegations
            
            System.debug(LoggingLevel.INFO,'delegates--'+rsmUser+'---'+rsmUser.Region__c+'---'+rsmUser.Geo__c+'---'+quote.Approver1__c);
            Delegated_Approver__c delegatedApprover = DelegatedApproverLoader.getInstance().findDelegatedApprover(quote.Approver1__c, rsmUser.Geo__c, rsmUser.Region__c);
            if (delegatedApprover != null) {
                quote.Approver1__c = delegatedApprover.User__c;
                if (pastApprovers.hasUserApproved(delegatedApprover.User__c)) {
                    // The delegated user has already approved.
                    // Make a note and advance to next approver
                    approver.Comments__c = 'Delegated to ' + delegatedApprover.User__r.Name;
                    return advanceApprover(quote);
                }
            }
        }
        
        // Queue Approval Request email.
        // Emails are not actually sent out until flush() call in After trigger
        DiscountApprovalEmailHlp.getInstance().doApproval(quote);
        return true;
    }
    
    private LineItemData loadLineItemData(Set<Id> quoteIds){
        if ((lineItemData == null) || lineItemData.getQuoteIds().isEmpty()) {
            lineItemData = new LineItemData([SELECT Standard_Discount__c, Quote__c, Config_Quantity__c, Non_Standard_Discount__c, Category__c FROM Quote_Line_Item__c WHERE Quote__c IN: quoteIds]);
        }
        return lineItemData;
    }
    
        
    /**
     * Data structure to hold Line Item data keyed by Quote Id
     */
    private class LineItemData {
        private Map<Id,Map<Id,Quote_Line_Item__c>> lineItems = new Map<Id,Map<Id,Quote_Line_Item__c>>();

        public LineItemData(List<Quote_Line_Item__c> lis){
            for (Quote_Line_Item__c li: lis){
                this.put(li.Quote__c,li);
            }
        }
        
        public void putAll(List<Quote_Line_Item__c> lis){
            for (Quote_Line_Item__c li: lis){
                this.put(li.Quote__c,li);
            }
        }   

        public List<Quote_Line_Item__c> get(Id quoteId) {
            Map<Id,Quote_Line_Item__c> result = lineItems.get(quoteId); 

            if (result != null){
                return result.values();
            }
            return null;
        }
        
        public void put(Id qId, Quote_Line_Item__c li) {
            Map<Id,Quote_Line_Item__c> tmp = lineItems.get(qId);
            if (tmp == null) {
                lineItems.put(qId, new Map<Id, Quote_Line_Item__c>{li.Id => li});
            } else {
                tmp.put(li.Id , li);    
            }           
        } 
        
        public Set<Id> getQuoteIds(){
            return lineItems.keySet();
        }   
         
        /*
        ** this method is to provide delta discount value of Non Standard Discounts to calculate routing matrix.
        */
        public Map<String,Decimal> getDiscountsByCategory(Id quoteId) {
            Map<String,Decimal> discounts = new Map<String,Decimal>(); 
            List<Quote_Line_Item__c> lineItems = get(quoteId);
            if (lineItems == null) {
                System.debug('*** No line items for Quote ID: ' + quoteId + '; Items Map: ' + this.lineItems);
                return discounts;
            }
            
            //Caculate the highest non standard discount for each category
            for (Quote_Line_Item__c item:  lineItems) {
                String cat = item.Category__c;
                Decimal newValue  =  0;
                /*Comment by Pramod 5.21.2009
                The system is kicking quotes back into NSD when an approved discount is changed to a lower discount. 
                If a discount is entered lower than approved or standard don't trigger approvals */
                if (cat != null) {
                    if (item.Non_Standard_Discount__c != null) {
                        Decimal std = (item.Standard_Discount__c == null) ? 0: item.Standard_Discount__c;
                        if (std >= item.Non_Standard_Discount__c) {
                            newValue = item.Non_Standard_Discount__c - std;
                            newValue = (newValue < 0) ? 0 : newValue;
                            newValue = (newValue > 100) ? 100 : newValue; 
                        } else {                            
                            newValue = item.Non_Standard_Discount__c - std;//Modified by Rucha
                        }
                    }
                  
                    if (discounts.get(cat) != null) {
                        Decimal current = discounts.get(cat);
                        if (current < newValue) {
                            discounts.put(cat, newValue);                       
                        }
                    } else {
                        discounts.put(cat, newValue);
                    }
                }
                System.debug(LoggingLevel.INFO,'8888888888888888'+item.Standard_Discount__c+'---'+item.Non_Standard_Discount__c);
            }
            
            System.debug(LoggingLevel.INFO,'8888888888888888'+discounts);
            return discounts;
         }
        //added by psingh@riverbed.com on 08/27/2013 
        /*
        ** this method is to provide absolute value of Non Standard Discounts to set the status of a quote, like Approved,Required, etc.
        */
         public Map<String,Decimal> getDiscountsByCategoryNew(Id quoteId) {
            Map<String,Decimal> discounts = new Map<String,Decimal>(); 
            List<Quote_Line_Item__c> lineItems = get(quoteId);
            if (lineItems == null) {
                System.debug('*** No line items for Quote ID: ' + quoteId + '; Items Map: ' + this.lineItems);
                return discounts;
            }
            
            //Caculate the highest non standard discount for each category
            for (Quote_Line_Item__c item:  lineItems) {
                String cat = item.Category__c;
                Decimal newValue  =  0;
                /*Comment by Pramod 5.21.2009
                The system is kicking quotes back into NSD when an approved discount is changed to a lower discount. 
                If a discount is entered lower than approved or standard don't trigger approvals */
                if (cat != null) {
                    if (item.Non_Standard_Discount__c != null) {
                        Decimal std = (item.Standard_Discount__c == null) ? 0: item.Standard_Discount__c;
                        if (std >= item.Non_Standard_Discount__c) {
                            newValue = item.Non_Standard_Discount__c - std;
                            newValue = (newValue < 0) ? 0 : newValue;
                            newValue = (newValue > 100) ? 100 : newValue; 
                        } else {                            
                            newValue = item.Non_Standard_Discount__c;// - std;//Modified by Rucha;commented by psingh@riverbed.com on 8/20/2013
                        }
                    }
                  
                    if (discounts.get(cat) != null) {
                        Decimal current = discounts.get(cat);
                        if (current < newValue) {
                            discounts.put(cat, newValue);                       
                        }
                    } else {
                        discounts.put(cat, newValue);
                    }
                }
                System.debug(LoggingLevel.INFO,'8888888888888888'+item.Standard_Discount__c+'---'+item.Non_Standard_Discount__c);
            }
            
            System.debug(LoggingLevel.INFO,'8888888888888888'+discounts);
            return discounts;
         }
                  
    }
	
	
    public class ApprovalMatrixCache {
        private Map<Id,List<Approvers_Routing_Matrix__c>> matrixByQuoteId;
        
        public ApprovalMatrixCache() {
            matrixByQuoteId = new Map<Id,List<Approvers_Routing_Matrix__c>>();
        }
        
        public void loadByQuoteIds(Set<Id> quoteIds) {
            if (matrixByQuoteId.isEmpty()) {
                for (Approvers_Routing_Matrix__c matrix : [SELECT User__c, Level__c, Quote__c FROM Approvers_Routing_Matrix__c WHERE Quote__c IN :quoteIds AND Route__c=true AND Level__c != null ORDER BY Level__c]) {
                    List<Approvers_Routing_Matrix__c> matrices = matrixByQuoteId.get(matrix.Quote__c);
                    if (matrices == null) {
                        matrices = new List<Approvers_Routing_Matrix__c>();
                        matrixByQuoteId.put(matrix.Quote__c, matrices);
                    }
                    matrices.add(matrix);
                }
            }
        }
        
        public List<Approvers_Routing_Matrix__c> getApprovalChain(Id quoteId) {
            if (matrixByQuoteId.containsKey(quoteId)) {
                return matrixByQuoteId.get(quoteId);
            } else {
                return new List<Approvers_Routing_Matrix__c>();
            }
        }
        
        public void putApprovalChain(Id quoteId, List<Approvers_Routing_Matrix__c> matrices) {
            matrixByQuoteId.put(quoteId, matrices);
        }
    }
    
    //Added by Rucha (5/13/2012) to get Product Families on Quotes
    public Map<Id,Map<String,List<String>>> getProductFamiliesforQuotes(List<Quote__c> quotesList){
    	Map<Id,Map<String,List<String>>> quotesToProductFamiliesMap = new Map<Id,Map<String,List<String>>>();         
         for(Quote_Line_Item__c qli : [Select Id,Oracle_Category__c,Quote__c,Category__c from Quote_Line_Item__c where Quote__c in :quotesList]){           
            if(qli.Oracle_Category__c != null){                
                Map<String,List<String>> discCatToOrclCatMap = quotesToProductFamiliesMap.get(qli.Quote__c);
                List<String> productCategoryList;
                if(discCatToOrclCatMap == null){
                	discCatToOrclCatMap = new Map<String,List<String>>();
                	productCategoryList = new List<String>();
                }
                else{
                	productCategoryList = discCatToOrclCatMap.get(qli.Category__c);
                	if(productCategoryList == null){
                		productCategoryList = new List<String>();
                	}
                }
                if(qli.Oracle_Category__c.contains(',')){
                    productCategoryList.addAll(qli.Oracle_Category__c.split(','));
                }
                else{
                    productCategoryList.add(qli.Oracle_Category__c);
                }
                discCatToOrclCatMap.put(qli.Category__c,productCategoryList);
                quotesToProductFamiliesMap.put(qli.Quote__c,discCatToOrclCatMap);            
            }
         }
         return quotesToProductFamiliesMap;
    }
    
    //Get map of overlay roles to product families
    public Map<String,Map<String,String>> getOverlayRoleToOracleCategoryMap(){
    Map<String,Map<String,String>> overlayRoleToOracleCategoryMap = new Map<String,Map<String,String>>();
            for(NSDOverlays__c nsdO : NSDOverlays__c.getAll().values()){
                Map<String,String> oracleCategoryMap = overlayRoleToOracleCategoryMap.get(nsdO.Overlay_Role__c);
                if(oracleCategoryMap == null){
                    oracleCategoryMap = new Map<String,String>();
                }                
                oracleCategoryMap.put(nsdO.Oracle_Category__c,nsdO.Oracle_Category__c);            
                overlayRoleToOracleCategoryMap.put(nsdO.Overlay_Role__c,oracleCategoryMap);
            }
      return overlayRoleToOracleCategoryMap;
    }
    //
}