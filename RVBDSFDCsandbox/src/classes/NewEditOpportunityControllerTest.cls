@isTest
private class NewEditOpportunityControllerTest {

				static testMethod void myUnitTest() {
				//Rohit - 01082010
				DiscountApproval.isTest = true;

						List<Account> lstAcc=new List<Account>();
								Opportunity testOpp=new Opportunity();
								Opportunity testOpp1=new Opportunity();
								RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
								RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
								Account cusAcc=new Account();
								cusAcc.RecordTypeId=accCusRT.Id;
								cusAcc.name='CustomeraAAccount';
								cusAcc.Type='Customer';
								cusAcc.Industry='Education';
								lstAcc.add(cusAcc);

								Account stpAcc=new Account();
								stpAcc.RecordTypeId=accRT.Id;
								stpAcc.name='stpAccount';
								stpAcc.Type='Distributor';
								stpAcc.Industry='Education';
								stpAcc.RASP_Status__c='Authorized';
								stpAcc.Authorizations_Specializations__c='RASP;RVSP';
								lstAcc.add(stpAcc);

								Account tier2Acc=new Account();
								tier2Acc.RecordTypeId=accRT.Id;
								tier2Acc.name='stpAccount';
								tier2Acc.Type='VAR';
								tier2Acc.Industry='Education';
								tier2Acc.RASP_Status__c='Authorized';
								tier2Acc.Authorizations_Specializations__c='RASP';
								lstAcc.add(tier2Acc);

								Account tier3Acc=new Account();
								tier3Acc.RecordTypeId=accRT.Id;
								tier3Acc.name='stpAccount';
								tier3Acc.Type='VAR';
								tier3Acc.Industry='Education';
								tier3Acc.RASP_Status__c='Not Reviewed';
								lstAcc.add(tier3Acc);

								Account influAcc=new Account();
								influAcc.RecordTypeId=accRT.Id;
								influAcc.name='stpAccount';
								influAcc.Type='VAR';
								influAcc.Industry='Education';
								influAcc.RASP_Status__c='Not Reviewed';
								lstAcc.add(influAcc);

								insert lstAcc;

								User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
								User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];

								testOpp.AccountId=cusAcc.Id;
								testOpp.Channel__c='Reseler';
								testOpp.Sold_To_Partner__c=stpAcc.Id;
								testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
								testOpp.Primary_ISR__c=primaryISR.Id;
								testOpp.Name='TestOpportunity';
								testOpp.Type='New';
								testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
								testOpp.CloseDate=System.today();
								testOpp.StageName='0-Prospect';
								testOpp.Forecast_Category__c='PipeLine';
								testOpp.Competitors__c='cisco';
								testOpp.LeadSource='Email';
								//testOpp.Support_Provider__c=stpAcc.Id;
								//testOpp.Support_Provided__c='Reseller';
								insert testOpp;
								testOpp.Support_Provider__c=stpAcc.Id;
								testOpp.Support_Provided__c='Reseller';
								update testOpp;
								ApexPages.StandardController sc = New ApexPages.StandardController(testOpp);
								System.currentPageReference().getParameters().put('id',testOpp.id);
								NewEditOpportunityController newController=new NewEditOpportunityController(sc);
								newController.getSupport();
								newController.getSupportBy();
								newController.getSupports();
								newController.providedBy();
								newcontroller.getSelectedOpnetGroupId();

								newController.save2();

								DiscountApproval.run = false;//added by Ankita 1/27/2012

								testOpp1.AccountId=cusAcc.Id;
								testOpp1.Channel__c='Reseler';
								testOpp1.Sold_To_Partner__c=stpAcc.Id;
								testOpp1.Sold_To_Partner_User__c=powerPartner[0].Id;
								testOpp1.Tier2__c=tier2Acc.Id;
								testOpp1.Tier2_Partner_User__c=powerPartner[1].Id;
								testOpp1.Primary_ISR__c=primaryISR.Id;
								testOpp1.Name='TestOpportunity1';
								testOpp1.Type='New';
								testOpp1.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
								testOpp1.CloseDate=System.today();
								testOpp1.StageName='0-Prospect';
								testOpp1.Forecast_Category__c='PipeLine';
								testOpp1.Competitors__c='cisco';
								testOpp1.LeadSource='Email';
								testOpp1.Group_Id_Not_Needed__c=true;
								//testOpp1.Support_Provider__c=tier2Acc.Id;
								//testOpp1.Support_Provided__c='Distributor';
								insert testOpp1;
								testOpp1.Support_Provider__c=tier2Acc.Id;
								testOpp1.Support_Provided__c='Distributor';
								update testOpp1;
								ApexPages.StandardController sc1 = New ApexPages.StandardController(testOpp1);
								System.currentPageReference().getParameters().put('id',testOpp1.id);
								NewEditOpportunityController newController1=new NewEditOpportunityController(sc1);
								newController1.getSupport();
								newController1.getSupportBy();
								newController1.getSupports();
								newController1.providedBy();
								newController1.save2();
								newController1.save3();
								
								// call for percentage coverage
								newController1.setSelectedOpnetGroupId('ttest');
								newController1.setSupport('support');
								newController1.getSupportsBy();
								newController1.setSupportBy('test');
								newController1.getOpportunityAccGroupIds();

				}
}