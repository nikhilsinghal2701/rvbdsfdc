/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest
private class TestB2BIntegration {
    static testmethod void testInsertOppLines(){
        DiscountApproval.isTest = true;
        Test.startTest();
        Opportunity opp = DiscountApprovalTestHlp.createOpp();
       // Pricebook2 p = [select id from Pricebook2 where isactive = true] ;
        Product2 prod = new Product2(Name = 'SteelHead X200', Family = 'Hardware');
        insert prod;
        
        // Get standard price book ID.        
        Id pricebookId = Test.getStandardPricebookId();
       // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        opp.Pricebook2Id = customPB.Id;
        update opp;
        
        Test.stopTest(); 
    }
    
    static testmethod void testAccountDeleteService(){
        B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        try {
        test.starttest();
        delete cusAcc;
        test.stoptest();
         }
        catch (Exception e) {
        system.assertEquals('You can\'t delete this record!', e.getMessage());
        }
        
    }
    
    static testmethod void testContactDeleteService(){
        B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        Contact con=new Contact();
        con.AccountId=cusAcc.Id;
        con.FirstName='cus';
        con.LastName='Contact';
        con.Email='test@test.com';
        con.Support_Role__c='Support Access';
        insert con;
        test.starttest();
        delete con;
        test.stoptest();
    }
    
    static testmethod void testOpportunityDeleteService(){
        B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];
        
        Opportunity opp=new Opportunity();        
        opp.AccountId=cusAcc.Id;
        opp.Channel__c='Direct';
        opp.Tier2_Partner_User__c=powerPartner[1].Id;
        opp.Primary_ISR__c=primaryISR.Id;
        opp.Name='TestOpportunity1';
        opp.Type='New';
        opp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        opp.CloseDate=System.today();
        opp.StageName='0-Prospect';
        opp.Forecast_Category__c='PipeLine';
        opp.Competitors__c='cisco';
        opp.LeadSource='Email';
        insert opp;
        test.starttest();
        delete opp;
        test.stoptest();
    }
    
    static testmethod void testQuoteService(){
        B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];
        
        Opportunity opp=new Opportunity();        
        opp.AccountId=cusAcc.Id;
        opp.Channel__c='Direct';
        opp.Tier2_Partner_User__c=powerPartner[1].Id;
        opp.Primary_ISR__c=primaryISR.Id;
        opp.Name='TestOpportunity1';
        opp.Type='New';
        opp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        opp.CloseDate=System.today();
        opp.StageName='0-Prospect';
        opp.Forecast_Category__c='PipeLine';
        opp.Competitors__c='cisco';
        opp.LeadSource='Email';
        insert opp;
        
        Quote__c qt = new Quote__c();
        qt.name=opp.name;
        qt.Opportunity__c=opp.id;
        qt.RSM__c=primaryISR.Id;
        qt.Discount_Code__c='test';
        qt.Discount_Reason__c='test';
        qt.Deal_Desk_Status__c='Not Required';
        qt.Used_For_Ordering__c=True;
        
        test.starttest();
        insert qt;
        test.stoptest();
    }
    static testmethod void testUserInsertUpdateService(){
        B2BIntegrationUtil.isTestMethod=true;
        Profile profile=[select id,name from profile where name='System Administrator'];      
        User user1=new User();
        user1.FirstName='test1';
        user1.LastName='umm1';
        user1.Alias='tu1';
        user1.Email='test101@test1.com';
        user1.Username='test101@test1.com';
        user1.Internal_Department__c='test123';
        user1.Segment__c='US';
        user1.EmailEncodingKey='ISO-8859-1';
        user1.CurrencyIsoCode='USD';
        user1.TimeZoneSidKey='America/Los_Angeles';
        user1.LocaleSidKey=userInfo.getLocale();
        user1.LanguageLocaleKey='en_US';
        user1.ProfileId=profile.id;
        insert user1;
        system.runAs(user1){
            user1.Street='680 Folsom St';
            user1.City='San Francisco';
            user1.State='CA';
            user1.Country='USA';
            update user1;
        }
    }
    static testMethod void testOpportunityLineItemDeleteService(){
        Opportunity testOp1 = new Opportunity(Name='Test',StageName='4 - Selected',CloseDate=Date.newInstance(2011, 01, 01),ownerid=userInfo.getUserId());
        insert testOp1; 
        PricebookEntry[] testPricebookEntry1=[Select UnitPrice, ProductCode, Product2Id, Pricebook2Id, Name, IsActive, Id From PricebookEntry where ProductCode like '%SHA-07050%' Limit 5];   
        OpportunityLineItem testOpLI1 = new OpportunityLineItem(OpportunityId=testOp1.Id);
        testOpLI1.Opportunity_Closed__c=false;
        testOpLI1.TotalPrice=2230;
        testOpLI1.Quantity=1;
        testOpLI1.Discount_Percentage__c=12;
        testOpLI1.PricebookEntryId = testPricebookEntry1[0].Id;
        insert testOpLI1;
        test.starttest();
        delete testOpLI1;
        test.stoptest();
    }
    static testMethod void testAdditionalDiscountDetailDeleteFromPWS(){
        Discount_Detail__c testdd = new Discount_Detail__c(Name='Test');
        insert testdd; 
        Additional_Discount_Detail__c tetadd = new Additional_Discount_Detail__c(Discount_Detail__c=testdd.id);
        insert tetadd;
        test.starttest();
        delete tetadd;
        test.stoptest();
    }
    static testMethod void testAuthMasterDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
       Authorizations_Master__c authMaster=new Authorizations_Master__c(Name='test',Active_Inactive__c=True,Add_Automatically__c=False,Description__c='Competency in Application Delivery (AD) product line',Type__c='Competency');
       insert authMaster;
       test.starttest();
       delete authMaster;
       test.stoptest();
    }
    static testMethod void testCatMasterDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
       Category_Master__c catMaster=new Category_Master__c(Name='test',Active__c=true,Description__c='test',Group__c='test',Locking_Value__c=0.00,Maximum_Value__c=0.00,Registered_Discount__c=0.00,Standard_Discount__c=0.00,Uplift_Value__c=0.00,Warning_Value__c=0.00);
       insert catMaster;
       test.starttest();
       delete catMaster;
       test.stoptest();
    }
    static testMethod void testDscDetlDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
       Category_Master__c catMaster=new Category_Master__c(Name='test',Active__c=true,Description__c='test',Group__c='test',Locking_Value__c=0.00,Maximum_Value__c=0.00,Registered_Discount__c=0.00,Standard_Discount__c=0.00,Uplift_Value__c=0.00,Warning_Value__c=0.00);
       insert catMaster;
       
       Discount_Detail__c dscDetl=new Discount_Detail__c(Name='test',Discount__c=0.00,Customer_Discount__c=0.00,Date_Approved__c=Date.newInstance(2011, 01, 01),Category_Master__c=catMaster.id,Special_Discount__c=0.00,Uplift__c=0.00,Var_Discount__c=0.00);
       insert dscDetl;
       test.starttest();
       delete dscDetl;
       test.stoptest();
    }
    static testMethod void testprodFmlAuthDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
       Authorizations_Master__c authMaster=new Authorizations_Master__c(Name='test',Active_Inactive__c=True,Add_Automatically__c=False,Description__c='Competency in Application Delivery (AD) product line',Type__c='Competency');
       insert authMaster;
       
       Oracle_Category__c oCat=new Oracle_Category__c(Name='Expand',Active__c=true,Category_Set_Id__c='1607',Category_Set_Name__c='Product Family',Competitive_Take_Out__c=true,Description__c='test',Oracle_Category_Id__c='2356',Oracle_Category_Id_Category_Set_Id__c='X123456');
       insert oCat;
       
       Product_Family_Authorizations__c pfmlAuth=new Product_Family_Authorizations__c(Authorization_Master__c=authMaster.id,Product_Family__c=oCat.id);
       insert pfmlAuth;
       
       test.starttest();
       delete pfmlAuth;
       test.stoptest();
    }
    static testMethod void testLinkActDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        
        Contact con=new Contact();
        con.AccountId=cusAcc.Id;
        con.FirstName='cus';
        con.LastName='Contact';
        con.Email='test@test.com';
        con.Support_Role__c='Support Access';
        insert con;
        
        Linked_Account__c lac=new Linked_Account__c();
        lac.Linked_Account__c=cusAcc.id;
        lac.Agent_Contact__c=con.id;
        lac.Active__c=true;
        lac.Agent__c=true;
        lac.Assets__c=true;
        lac.Cases__c=true;
        lac.Contacts__c=true;
        lac.Expiration__c=Date.newInstance(2011, 01, 01);
        lac.User_Account__c='test123';
        lac.User_Contact__c='test';
        insert lac;
        test.starttest();
        delete lac;
        test.stoptest();
    }
    static testMethod void testOPNETActGrpDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
       string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        Opnet_Account_Group__c opActGrp=new Opnet_Account_Group__c(Account__c=cusAcc.id,Name='test');
        insert opActGrp;
        test.starttest();
        delete opActGrp;
        test.stoptest();
    }
     static testMethod void testOPNETGrpDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        
        Contact con=new Contact();
        con.AccountId=cusAcc.Id;
        con.FirstName='cus';
        con.LastName='Contact';
        con.Email='test@test.com';
        con.Support_Role__c='Support Access';
        insert con;
        
        Opnet_Account_Group__c opActGrp=new Opnet_Account_Group__c(Account__c=cusAcc.id,Name='test');
        insert opActGrp;
        
        Opnet_Group_ID__c opg=new Opnet_Group_ID__c(Account__c=cusAcc.id,Contact__c=con.id,Opnet_Account_Group__c=opActGrp.id,Opnet_Is_Maintenance_Contact__c=true, Opnet_PASSWORD_SALT__c='test',OPnet_Password_Hash__c='test123',OPnet_POC_ID__c='test1234',Opnet_POC_Type__c='test',Opnet_username__c='test',OPNET_SupportSiteLinkSent__c=true,OPNET_SendSupportSiteLink__c=true);
        
        insert opg;
        test.starttest();
        delete opg;
        test.stoptest();
     }
     
    static testMethod void testAssetDeleteService(){
       B2BIntegrationUtil.isTestMethod=true;
        string cRecType='012300000000NE0AAM';
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=cRecType;
        cusAcc.name='cusAccountTest';
        cusAcc.Industry='Education';
        cusAcc.Geographic_Coverage__c='EMEA';
        cusAcc.Region__c='Central Europe';
        insert cusAcc;
        
        Asset ast= new Asset(name='test',SerialNumber='test1',SKU__c='test1',accountid=cusAcc.id,Instance_Number__c='X7777777777');
        insert ast;
        
        test.starttest();
        delete ast;
        test.stoptest();
    }
}