/*Description: This class is used by InsertIntoSalesTeam Trigger,OpportunityInsertBefore Trigger.
*1.To add Sold to Partner,Tier2 Partner, Tier3 Partner and Influence Partner User as sales team Member 
*as record inserted into Visible_to_Partners__c.
*2.Need to retain the partner users that were added to the sales team via Add to Sales Team object 
*whenever Opportunity owner changed. 
*By prashant.singh@riverbed.com
*Date:11/22/2010 
*/
public class OpportunitySalesTeamHlp{
    public static final Set<Id> PROFILES = new Set<Id>{'00e30000000bqt9'};  
    private static string result;
    public static void deleteFromSalesTeam(Map<Id,Visible_to_Partners__c>oldMap){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> userIds = new Set<Id>();
        try{
            // In a before delete trigger, the trigger accesses the records that will be
            // deleted with the Trigger.old list.
            for (Visible_to_Partners__c vp : oldMap.values()){
                oppIds.add(vp.Opportunity_Name__c);
                userIds.add(vp.PartnerName__c);
            }
            //Query for correct OpportunityTeamMember to be deleted based on the current record 
            //to be deleted from Visible_to_Partners__c object.     
            OpportunityTeamMember[] tm = [select Id,TeamMemberRole,OpportunityAccessLevel,OpportunityId,UserId 
                                            from OpportunityTeamMember 
                                            where OpportunityId in :oppIds and UserId in :userIds]; 
            //Delete the actual identified record from OpportunityTeamMember
            delete tm;
        }catch(DMLException dme){
            system.debug('Delete_ST_Exception:'+dme.getDMLMessage(0));
            
        }
    }
    
    public static string inserIntoSalesTeam(List<Visible_to_Partners__c>newList){
        Set<Id> oppIds = new Set<Id>();
        Set<Id> partners = new Set<Id>();        
        for(Visible_to_Partners__c vp : newList){
            partners.add(vp.PartnerName__c);
            oppIds.add(vp.Opportunity_Name__c);
        }
        Map<Id,User> users = new Map<Id,User>([select id, Contact.AccountId, UserType from User where id in :partners]);
        Map<Id,Opportunity> oppMap = new Map<Id,Opportunity>([select Id,OwnerId,Sold_To_Partner__c, Tier2__c,Tier3_Partner__c, Influence_Partner__c, Ordering_Center__c
                             from Opportunity where Id in :oppIds]);
        List<OpportunityTeamMember> team = new List<OpportunityTeamMember>();       
        Map<Id, String> accessLevelMap = new Map<Id,String>();           
        for (Visible_to_Partners__c vp : newList){
            Opportunity oq = oppMap.get(vp.Opportunity_Name__c);
            if ((vp.PartnerName__c == NULL) || (oq.OwnerId == vp.PartnerName__c)){
                result='isOwner';
                //vp.PartnerName__c.addError('Opportunity Owner automatically recieves the visibility. Please select appropriate user');
                return result;
            }else if (users.get(vp.PartnerName__c).UserType.equalsIgnoreCase('PowerPartner') && 
                    (oq.Sold_To_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && 
                    oq.Tier2__c != users.get(vp.PartnerName__c).Contact.AccountId) && //added to allow Tier 2 Partner users to be added to sales team
                    oq.Tier3_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Tier 3 partner users to be added to sales team: Ankita 3/8/2010
                    oq.Influence_Partner__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Influence partner users to be added to sales team: Ankita 3/8/2010
                    oq.Ordering_Center__c != users.get(vp.PartnerName__c).Contact.AccountId && //added to allow Ordering center partner users to be added to sales team: Ankita 3/8/2010
                    !PROFILES.contains(UserInfo.getProfileId())){
                    result='isAllowed';
                    //vp.PartnerName__c.addError('Only Associated Partner Users can be added to the Sales Team');
                    return result;
            }else if(users.get(vp.PartnerName__c).UserType.equalsIgnoreCase('PowerPartner') && 
                    (oq.Tier2__c == users.get(vp.PartnerName__c).Contact.AccountId || oq.Tier3_Partner__c == users.get(vp.PartnerName__c).Contact.AccountId
                    || oq.Influence_Partner__c == users.get(vp.PartnerName__c).Contact.AccountId)//added by Ankita 3/8/2010 for allowing Tier3 and Influence partner users to be added only with Read access
                    && vp.Opportunity_Access__c.equalsIgnoreCase('Edit')){
                    result='isEdit';
                    //vp.PartnerName__c.addError('Tier2, Tier3 and Influence Partner Users can be added to the Sales Team only with Read access');
                    return result; 
            }
            else{
                //system.debug('vp.PartnerName__c:'+vp.PartnerName__c);
                OpportunityTeamMember otm = new OpportunityTeamMember(TeamMemberRole=vp.Role__c,OpportunityId=vp.Opportunity_Name__c,UserId =vp.PartnerName__c);
                team.add(otm);
                accessLevelMap.put(vp.partnerName__c, vp.Opportunity_Access__c);
                //System.debug('otm --> ' + otm); 
            } 
        }
        //Insert into Opportunity Team Member
        try{
            //system.debug('calling insert');
            insert team;
        }catch(DMLException dme){
            //system.debug('ST_Exception:'+dme.getDMLMessage(0));
            result=dme.getDMLMessage(0);
        }   
        //ankita : moved soql queries out of for loop to avoid running into limits
        List<OpportunityShare> osList = [select Id,OpportunityAccessLevel , UserOrGroupId
                                        from OpportunityShare 
                                        where OpportunityId in :oppIds 
                                        and UserOrGroupId in :partners and RowCause='Team'];
                                
        //Log the queried record 
        //System.debug('os --> ' + osList); 
        if(!osList.isEmpty()){
            for(OpportunityShare os : osList){
                os.OpportunityAccessLevel = accessLevelMap.get(os.UserOrGroupId);
                //System.debug('OpportunityAccessLevel After --> ' + os.OpportunityAccessLevel);
            }
        }                
        //Insert into Opportunity Share based on the user visibility selection 
        try{
            update osList;
            result='success';
        }catch(DMLException dme){
            //system.debug('Update_Access_Level_ST_Exception:'+dme.getDMLMessage(0));
            result=dme.getDMLMessage(0);        
        }
        return result;
    }
    
    /*Need to retain the partner users that were added to the sales team via Add to Sales Team object 
    whenever Opportunity owner changed.*/ 
    public static void updateOpportunitySalesTeam(List<Opportunity>newList,Map<Id,Opportunity>oldMap){
        Set<Id> oppIds = new Set<Id>();
        List<Visible_to_Partners__c> newVPList=new List<Visible_to_Partners__c>();
        for(Opportunity opp:newList){
            if(opp.OwnerId!=oldMap.get(opp.Id).OwnerId){
                oppIds.add(opp.Id);
            }           
        } 
        if(oppIds.size()>0){
            newVPList=[select Id,Role__c,Opportunity_Name__c,PartnerName__c,Opportunity_Access__c from Visible_to_Partners__c where Opportunity_Name__c In:oppIds];
            string insertResult=inserIntoSalesTeam(newVPList);
        }
    }//End of Method
    
    /*Need to retain the partner users that were added to the sales team via Add to Sales Team object 
    whenever mass account transfer.*/
    public static void updateOpportunitySalesTeamUponAccountOwnerChange(List<Account>newList,Map<Id,Account>oldMap){
    	Set<String> oppStage = new Set<String>{'6 - Order Accepted','7 - Closed (Not Won)'};
    	Set<Id> oppIds = new Set<Id>();
    	Set<Id> accIds = new Set<Id>();
    	Map<Id,Opportunity> oppMap;
    	List<Visible_to_Partners__c> newVPList=new List<Visible_to_Partners__c>();
    	for(Account acc:newList){
            if(acc.OwnerId!=oldMap.get(acc.Id).OwnerId){
                accIds.add(acc.Id);
            }           
        }
        if(accIds.size()>0){
            oppMap=new Map<Id,Opportunity>([select Id, Name,AccountId,stageName from Opportunity where AccountId In :accIds and  stageName Not In:oppStage]);
            if(oppMap.size()>0){
            	newVPList=[select Id,Role__c,Opportunity_Name__c,PartnerName__c,Opportunity_Access__c from Visible_to_Partners__c where Opportunity_Name__c In:oppMap.keySet()];
            	string insertResult=inserIntoSalesTeam(newVPList);            	
            }
        } 
    }//End of Method
}