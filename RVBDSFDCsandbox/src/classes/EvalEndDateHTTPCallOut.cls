/***Created By: Anil Madithati
**Date: 08/23/2014
**Purpose: To Retrieve the latest eval end date from Key Server on asset ((Part of SE::2.0)) 
*/  
public class EvalEndDateHTTPCallOut {
   
    private ApexPages.StandardController stdCtrl {get; set;} 
    public Asset ast { get; set; }
    public String sn { get; set; }
    //constructor to get the Asset record
    public EvalEndDateHTTPCallOut(ApexPages.StandardController controller){ 
        ast =  (Asset) controller.getRecord();
        sn = ast.Name;
        System.debug('The Asset record: ' + sn);
    }
    public PageReference parseJSONResponse() {        
        Http httpProtocol = new Http();
        String s=sn;
        Date eDate;
        Asset a = [Select Id, name,EvalEndDate__c from Asset where name=: sn];
        // Create HTTP request to send.
        HttpRequest request = new HttpRequest();
        String apiKey= '@_v3ry_$3cu4e_k3y_4_k3y$erv3r_@pi';
        request.setHeader('RVBD-API-Key', apiKey);
        // Set the endpoint URL.
        String ENDPOINT=getUrl('Key_Server');
        request.setEndpoint(endpoint+s);
        // Set the HTTP verb to GET. 
        request.setMethod('GET');
        request.setTimeout(60000);
        // Send the HTTP request and get the response,The response is in JSON format.
        HttpResponse response = httpProtocol.send(request);
        system.debug(LoggingLevel.INFO,'response:'+response);
        if(response!=null && response.getStatusCode()==200){
            Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
            system.debug(LoggingLevel.INFO,'response data:'+data);
            system.debug(LoggingLevel.INFO,'ErrorID:'+(String)data.get('error_id'));
            if(!string.IsBlank((String)data.get('evalDate'))){
            eDate = Date.valueOf((String)data.get('evalDate'));
            }
        }
        if (eDate!= null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
                a.EvalEndDate__c = eDate;
                update a;
        }
        system.debug('updated asset=' + a);
        return new PageReference('/'+a.id);
        }
         
    @future(callout=true)
    public static void parseJSONResponseFuture(Set<Id> aIds){        
        Http httpProtocol = new Http();
        
        List<Asset>asts=new List<Asset>([Select Id, name,EvalEndDate__c,SerialNumber from Asset where id in : aIds and IB_Status__c='Under Evaluation']);
        system.debug(LoggingLevel.INFO,'asts:'+asts);
        For( Asset a:asts){
        String s=a.name;
        HttpRequest request = new HttpRequest();
        String apiKey= '@_v3ry_$3cu4e_k3y_4_k3y$erv3r_@pi';
        request.setHeader('RVBD-API-Key', apiKey);
        String ENDPOINT=getUrl('Key_Server');
        request.setEndpoint(endpoint+s);
        request.setMethod('GET');
        request.setTimeout(60000);
        try{
            if(!Test.isRunningTest()){
            
                HttpResponse response = httpProtocol.send(request);
                system.debug(LoggingLevel.INFO,'response:'+response);
                Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(response.getBody());
                system.debug(LoggingLevel.INFO,'response data:'+data);
                Date eDate= Date.valueOf((String)data.get('evalDate'));
              
        if (eDate!= null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
                a.EvalEndDate__c = eDate;
                update a;
            }
             system.debug('updated asset****=' + a);  
            } 
        }
        catch (Exception e) {}
        }
        //update asts;
        system.debug('updated assets=' + asts);
    }
     private static string getUrl(string serviceType){
        SFDC_PWS_Integration_Settings__c iProp=SFDC_PWS_Integration_Settings__c.getValues('IntegrationEndPoints');
        if(iprop != null){
        if(serviceType.equalsIgnoreCase('Key_Server')){
            return iProp.Key_Server__c;}
        return ' ';
        }
        else {
            return ' ';
        }  
    }
}