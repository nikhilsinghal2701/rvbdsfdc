@isTest
private class DiscountApproverMatrixTest {

	public testMethod static void testDiscountApproverMatrix() {
		Base_Approval_Matrix__c baseMatrix=new Base_Approval_Matrix__c();
		baseMatrix.BM_Start_Pct__c=10;
		baseMatrix.BM_End_Pct__c=5;
		baseMatrix.BM_Geo__c='AMERICAS';
		baseMatrix.BM_Level__c=5;
		baseMatrix.BM_Role__c='RSM';
		insert baseMatrix;
		
		Quote__c q = DiscountApprovalTestHlp.createQuote(null);
		/*
		RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
	    Account stpAcc=new Account();
	    stpAcc.RecordTypeId=accPatRT.Id;
	    stpAcc.name='stpAccount';
	    stpAcc.Type='Distributor';
	    stpAcc.Industry='Education';
	    stpAcc.RASP_Status__c='Authorized';
	    stpAcc.Authorizations_Specializations__c='RASP;RVSP';
	    insert stpAcc;
	    
	    // Test inserting Opportunity with neither rule's conditions met 
	    Opportunity opp = new Opportunity();
	    opp.CloseDate = System.today();
	    opp.Name = 'Test Opp';
	    opp.StageName = 'Expired';
	    opp.Number_Eval_Products__c = 5;
	    opp.Registered_Deal__c = true;
	    opp.Channel__c='Reseller';
	    opp.Sold_To_Partner__c=stpAcc.Id;
	    insert opp;
	    
	    Profile pro=[Select p.Id, p.Name from Profile p where p.Name='RSM'];
		User usr=[select Id,Name,UserRoleId  from user where isActive = true and Name='Rick Geehan' and ProfileId=:pro.Id];
       	set<Id> rsmIds=new set<Id>();
       	rsmIds.add(usr.Id);
       	
       	Quote__c q = new Quote__c (Opportunity__c = opp.Id, Name = 'test', RSM__c = usr.Id, Discount_Code__c = 'Competitive Pricing', Discount_Reason__c = 'Competitive Pricing');
		insert q;
		*/
		//create approvers routing matrix
		//Start:Adding extra code
		//Profile pro=[Select p.Id, p.Name from Profile p where p.Name='RSM'];;//commented by psingh@riverbed.com on 9/4/2013, to reduce the SOQL system limit
		//User usr=[select Id,Name,UserRoleId  from user where isActive = true and Name='Kevin Balkam' and ProfileId=:pro.Id];;//commented by psingh@riverbed.com on 9/4/2013 for RFC#6178 deployment due to user account got deactivated
		User usr=[SELECT Id,Name,UserRoleId FROM User WHERE IsActive = true AND Profile.Name = 'RSM' AND User_Manager__c!=null AND Approver__c = true limit 1];//added by psingh@riverbed.com on 9/4/2013 for RFC#6178
		DiscountApproverMatrixHelper.createQueryFromFields(q.Id);
		DiscountApproverMatrixHelper.getUserRoleNameByUserId(q.RSM__c);
		DiscountApproverMatrixHelper.getUserIdByRoleId(usr.UserRoleId);
		DiscountApproverMatrixHelper.deleteOldMatrix(q.Id,QuoteApprovalHlp.STATUS_PENDING);
		//End:Adding extra code
		Approvers_Routing_Matrix__c aprvMatrix=new Approvers_Routing_Matrix__c();
		System.assert(q.Id != null);
		aprvMatrix.Quote__c=q.Id;
		aprvMatrix.Category__c='A';
		aprvMatrix.Reference_Role__c='RSM';
		//aprvMatrix.User__c=usr.Id;
		aprvMatrix.User__c=q.RSM__c;
		
		insert aprvMatrix;
		
		//Approvers_Routing_Matrix__c[] apmatxs = [select id from Approvers_Routing_Matrix__c where id='00530000000t2dU' and Quote__c='00670000009M1tQ'];
		Approvers_Routing_Matrix__c[] apmatxs = [select id from Approvers_Routing_Matrix__c where id=:q.RSM__c and Quote__c=:q.Id];
		delete apmatxs;
		
		Exception_Mapping__c exmapp = new Exception_Mapping__c();
		exmapp.Active__c=true;
		exmapp.Geo__c='AMERICAS';
		exmapp.Region__c='test';
		exmapp.Role__c='RSM';
		exmapp.Exception_User__c=UserInfo.getUserId();
		insert exmapp;
		
	
	}

}