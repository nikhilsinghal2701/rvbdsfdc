public with sharing class AccountUtil { 
/*
    private string cusRecType;
    private string parRecType;
    private string cusRecType_15;
    private string parRecType_15;
    private string X30_EMAIL_TEMPLATE_ID;
    private string X15_EMAIL_TEMPLATE_ID;
    private string Dis_EMAIL_TEMPLATE_ID;
    private string Enable_EMAIL_TEMPLATE_ID;
    private Boolean IS_SANDBOX;
    private String TEST_USERID;
    private Map<Id,Id> assetCountMap=new Map<Id,Id>();
    private List<Account> accList=new List<Account>();
    private List<Account> sendEnableMailList=new List<Account>();
    private List<Account> sendDisableMailList=new List<Account>();
    private List<Account> X30DayNotificationList=new List<Account>();
    private List<Account> X15DayNotificationList=new List<Account>();
    private RVBD_Support_Site_Properties__c eProp;
    public static Boolean isTest=false;
    public AccountUtil(){
        eProp=RVBD_Support_Site_Properties__c.getValues('Support Site Restriction');
        cusRecType=eProp.Customer_Account_RecordType__c;
        cusRecType_15=cusRecType.substring(0, 15);
        parRecType=eProp.Partner_Account_RecordType__c;
        parRecType_15=parRecType.substring(0, 15);
        X30_EMAIL_TEMPLATE_ID=eProp.X30_Email_Template_Id__c;
        X15_EMAIL_TEMPLATE_ID=eProp.X15_Email_Template_Id__c;
        Dis_EMAIL_TEMPLATE_ID=eProp.Disabled_Email_Template_Id__c;
        Enable_EMAIL_TEMPLATE_ID=eProp.Enable_Email_Template_Id__c;
        IS_SANDBOX = eProp.isSandbox__c;
        TEST_USERID = eProp.Test_UserId__c;
    }
    public void updateSupportAccessDisabled(List<Account> newTrigger, Map<Id,Account> oldMap){
        for (Account o : newTrigger) {
            if((o.RecordTypeId==cusRecType)||(o.RecordTypeId==cusRecType_15)||(o.RecordTypeId==parRecType)||(o.RecordTypeId==parRecType_15)){
                accList.add(o);
            }
        }
        if(accList.size()>0){
            for(Account acc:accList){
                String supportAccess = acc.Disable_Support_Method__c == null ? 'Automatic' : acc.Disable_Support_Method__c;
                if(supportAccess.equalsIgnoreCase('Automatic')){
                    acc.Reason__c=null;
                }
                if(acc.RecordTypeId==cusRecType||acc.RecordTypeId==cusRecType_15){
                    if((acc.Support_Site_Access_Expiry_Date__c==NULL && supportAccess.equalsIgnoreCase('Automatic'))||(acc.Support_Site_Access_Expiry_Date__c>system.today()&&(acc.Disable_Support_Method__c!=oldMap.get(acc.Id).Disable_Support_Method__c || acc.Support_Site_Access_Expiry_Date__c!=oldMap.get(acc.Id).Support_Site_Access_Expiry_Date__c) && supportAccess.equalsIgnoreCase('Automatic'))){
                        acc.X30_Day_Notification_Sent__c=False;
                        acc.X15_Day_Notification_Sent__c=False;
                        acc.Support_Access_Disabled__c=False;
                        if(acc.Support_Access_Disabled__c!=oldMap.get(acc.Id).Support_Access_Disabled__c){                          
                            sendEnableMailList.add(acc);
                        }// added after case fail in UAT
                        //sendEnableMailList.add(acc);// added after case fail in UAT
                    }else if(acc.Disable_Support_Method__c!=NULL && supportAccess.equalsIgnoreCase('Allowed')){
                        acc.Support_Access_Disabled__c=False;
                        sendEnableMailList.add(acc);
                    }else if((acc.Support_Site_Access_Expiry_Date__c==NULL && supportAccess.equalsIgnoreCase('Restricted')) || (acc.Disable_Support_Method__c!=NULL && supportAccess.equalsIgnoreCase('Restricted'))){
                        acc.Support_Access_Disabled__c=True;
                        sendDisableMailList.add(acc);
                    }else if(acc.Support_Site_Access_Expiry_Date__c!=NULL && (acc.Support_Site_Access_Expiry_Date__c<=system.today()
                    ||acc.Support_Site_Access_Expiry_Date__c==acc.createdDate.date())){
                        acc.Support_Access_Disabled__c=True;
                    }
                }else if(acc.RecordTypeId==parRecType||acc.RecordTypeId==parRecType_15){
                    if(acc.Partner_Status1__c!=Null && (acc.Partner_Status1__c.equalsIgnoreCase('Pending Contract') || acc.Partner_Status1__c.equalsIgnoreCase('Good Standing')||
                    acc.Partner_Status1__c.equalsIgnoreCase('Probation')||acc.Partner_Status1__c.equalsIgnoreCase('Pending Termination'))&& acc.Support_Site_Access_Expiry_Date__c!=oldMap.get(acc.Id).Support_Site_Access_Expiry_Date__c){                     
                        acc.Support_Site_Access_Expiry_Date__c=null;
                        acc.X30_Day_Notification_Sent__c=False;
                        acc.X15_Day_Notification_Sent__c=False;
                        acc.Support_Access_Disabled__c=False;
                        //sendEnableMailList.add(acc);
                    }else if((acc.Disable_Support_Method__c!=NULL && supportAccess=='Automatic')&& (acc.Partner_Status1__c!=Null && (acc.Partner_Status1__c.equalsIgnoreCase('Terminated') || acc.Partner_Status1__c.equalsIgnoreCase('Not in Program')))&& (acc.Support_Site_Access_Expiry_Date__c>system.today())
                        && (acc.Disable_Support_Method__c!=oldMap.get(acc.Id).Disable_Support_Method__c || acc.Support_Site_Access_Expiry_Date__c!=oldMap.get(acc.Id).Support_Site_Access_Expiry_Date__c)){
                        acc.X30_Day_Notification_Sent__c=False;
                        acc.X15_Day_Notification_Sent__c=False;
                        acc.Support_Access_Disabled__c=False;
                        if(acc.Support_Access_Disabled__c!=oldMap.get(acc.Id).Support_Access_Disabled__c){                          
                            sendEnableMailList.add(acc);
                        }// added after case fail in UAT
                        //sendEnableMailList.add(acc);
                    }else if(acc.Disable_Support_Method__c!=NULL && supportAccess.equalsIgnoreCase('Allowed')){
                        acc.Support_Access_Disabled__c=False;
                        sendEnableMailList.add(acc);
                    }else if(acc.Disable_Support_Method__c!=NULL && supportAccess.equalsIgnoreCase('Restricted')){
                        acc.Support_Access_Disabled__c=True;
                        sendDisableMailList.add(acc);
                    }else if(acc.Support_Site_Access_Expiry_Date__c!=NULL && (acc.Support_Site_Access_Expiry_Date__c<=system.today()
                    ||acc.Support_Site_Access_Expiry_Date__c==acc.createdDate.date())){
                        acc.Support_Access_Disabled__c=True;
                    }
                }//end else if
            }//end for loop
            if(sendDisableMailList.size()>0){
                List<Contact>disableEmailCon=getAllContacts(sendDisableMailList);
                if(disableEmailCon!=NULL && disableEmailCon.size()>0){
                    sendEmail(disableEmailCon,Dis_EMAIL_TEMPLATE_ID);
                }
            }
            if(sendEnableMailList.size()>0){
                List<Contact>enableEmailCon=getAllContacts(sendEnableMailList);
                if(enableEmailCon!=NULL && enableEmailCon.size()>0){
                    sendEmail(enableEmailCon,Enable_EMAIL_TEMPLATE_ID);
                }
            }
        }//end if condition     
    }//end updateSupportAccessDisabled method   
    
    public void sendEmailNotification(List<Account> accNotifyList){
        //system.debug('sendEmailNotification:'+accNotifyList);
        for(Account acc:accNotifyList){
            if(acc.X30_Day_Notification_Sent__c && acc.X15_Day_Notification_Sent__c && acc.Support_Access_Disabled__c
                && (acc.Support_Site_Access_Expiry_Date__c!=NULL && acc.Support_Site_Access_Expiry_Date__c==system.today())){
                sendDisableMailList.add(acc);
            }else if(acc.X30_Day_Notification_Sent__c && acc.X15_Day_Notification_Sent__c){
                X15DayNotificationList.add(acc);
            }else if(acc.X30_Day_Notification_Sent__c){
                X30DayNotificationList.add(acc);
            }
        }//end for
        if(sendDisableMailList.size()>0){
            System.debug('Disabling accounts');
            List<Contact>disableEmailCon=getAllContacts(sendDisableMailList);
            if(disableEmailCon!=NULL && disableEmailCon.size()>0){
                sendEmail(disableEmailCon,Dis_EMAIL_TEMPLATE_ID);
            }
        }
        if(X15DayNotificationList.size()>0){
            System.debug('Disabling accounts in 15 days');
            List<Contact>x15EmailCon=getAllContacts(X15DayNotificationList);
            if(x15EmailCon!=NULL && x15EmailCon.size()>0){
                sendEmail(x15EmailCon,X15_EMAIL_TEMPLATE_ID);
            }
        }
        if(X30DayNotificationList.size()>0){
            System.debug(LoggingLevel.INFO,'Disabling accounts in 30 days');
            List<Contact>x30EmailCon=getAllContacts(X30DayNotificationList);
            if(x30EmailCon!=NULL && x30EmailCon.size()>0){
                sendEmail(x30EmailCon,X30_EMAIL_TEMPLATE_ID);
            }
        }
    }//end sendEmailNotification method
    
    private List<Contact> getAllContacts(List<Account> tempAccList){
        set<Id> accIds=new set<Id>();
        List<Contact> emailList=new List<Contact>();
        for(Account acc:tempAccList){
            accIds.add(acc.Id);
        }//end for
        If(accIds.size()>0){
            List<Contact> conList=[select Id,Email,AccountId from contact where AccountId IN : accIds and support_role__c!=NULL and Email!=NULL and HasOptedOutOfEmail=False];
            if(conList.size()>0){
                for(Contact con:conList){
                    //emailList.add(con.Email);
                    emailList.add(con);
                }//end for
            }//end if
        }//end if
        return emailList;
    }//end getAllContacts method
    private void sendEmail(List<Contact> emailList, String EMAIL_TEMPLATE_ID){          
        List<Messaging.SingleEmailMessage>mails=new List<Messaging.SingleEmailMessage>();
        Messaging.SendEmailResult[]result;
        Messaging.SingleEmailMessage mail,errorMail;
        for(Contact con:emailList){
            mail = new Messaging.SingleEmailMessage();
            mail.setOrgWideEmailAddressId(eProp.Org_Wide_Default_Email_Id__c);
            mail.setTargetObjectId(con.Id);
            //mail.setToAddresses(new List<String> {'sfdc_testing@riverbed.com'});
            mail.setTemplateId(EMAIL_TEMPLATE_ID);
            mail.setSaveAsActivity(True); 
            mail.setWhatId(con.AccountId);
            System.debug(LoggingLevel.INFO,'Sending email to : ' + mail.getToAddresses());
            mails.add(mail);  
            system.debug(LoggingLevel.INFO,'Mail:'+mails);      
        }
        if(mails.size()>0 && isTest==false){
            try{
              // result = Messaging.sendEmail(mails);// - commented by Ankita on 9/2/2011 to disable sending emails
               system.debug(LoggingLevel.INFO,'Email Sent Result:'+result);
            }catch(Exception e){
                system.debug(LoggingLevel.INFO,'Email Exception:'+e.getMessage());
                errorMail = new Messaging.SingleEmailMessage();
                errorMail.setToAddresses(new List<String> {'sfdc_testing@riverbed.com'});
                errorMail.setSubject('Error During Email Notification Sent:');
                errorMail.setOrgWideEmailAddressId(eProp.Org_Wide_Default_Email_Id__c);
                errorMail.setPlainTextBody(LoggingLevel.INFO+''+'Mail:'+mails+''+e.getMessage());
               // Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{errorMail});//- commented by Ankita on 9/2/2011 to disable sending emails
            }
        }//end if
    }//end sendEmail method
    
    */ // commented by prashant.singh@riverbed.com--09/14/2012--Concierge deployment
    /*Batch Process Methods: The below methods getAllAssetByAccount and getAssets will be used in nightly batch process.
    Will set the value of 'Support Site Access Expiry Date' field.
    */
   /* public Map<Id,Date> getAllAssetByAccount(List<sObject> scope){
        set<Id> Ids=new set<Id>();
        Map<Id,Date> accMap=new Map<Id,Date>();
        Map<Id,List<Asset>> assetMap;
        Id aId;Date dt;
        for(sObject so:scope){
            Account a=(Account)so;
            if(a.RecordTypeId==cusRecType||a.RecordTypeId==cusRecType_15||a.RecordTypeId==parRecType||a.RecordTypeId==parRecType_15){
                Ids.add(a.Id);
            }
        }
        if(Ids.size()>0){
            assetMap=getAssets(Ids);
            AggregateResult[] groupedResults=[Select AccountId, max(Support_End_Date__c) support_end_date, 
                max(Renewed_End_Date__c) renewed_end_date from Asset where accountId IN : Ids and Calculated_Contract_Status__c!=null and Support_End_Date__c!=null GROUP BY AccountId];
            if(groupedResults.size()>0){
                for (AggregateResult ar : groupedResults) {
                    //System.debug('support_end_date:' + ar.get('support_end_date'));
                    //System.debug('renewed_end_date:' + ar.get('renewed_end_date'));
                    //System.debug('AccountId:' + ar.get('AccountId'));
                    if((Date)ar.get('renewed_end_date')==null || (Date)ar.get('support_end_date')>=(Date)ar.get('renewed_end_date')){
                        aId=(ID)ar.get('AccountId');
                        //system.debug('Id:'+aId);
                        dt=(Date)ar.get('support_end_date');
                        //system.debug('Date:'+dt);
                        accMap.put(aId,dt);
                    }else if((Date)ar.get('support_end_date')==null || (Date)ar.get('renewed_end_date')>=(Date)ar.get('support_end_date')){
                        aId=(ID)ar.get('AccountId');
                        dt=(Date)ar.get('renewed_end_date');
                        accMap.put(aId,dt);
                    }   
                }
            }
            //system.debug('accMap:'+accMap);
            //system.debug('assetMap:'+assetMap);
            if(assetMap.size()>0 && accMap.size()>0){
                for(Id i:assetMap.keySet()){
                    if(assetMap.get(i).size()>0){
                        if(accMap.containsKey(i)){
                            accMap.remove(i);
                            accMap.put(i,NULL);
                        }else accMap.put(i,NULL);
                    }
                }
            //}else if(assetMap.size()<=0){
                 //return accMap;
            }else if(accMap.size()<=0){
                for(Id i:assetMap.keySet()){
                    if(assetMap.get(i).size()>0){
                        if(accMap.containsKey(i)){
                            accMap.remove(i);
                            accMap.put(i,NULL);
                        }else accMap.put(i,NULL);
                    }
                }
            }
        }
        for(sObject so:scope){
            Account a=(Account)so;
            if(a.RecordTypeId==cusRecType||a.RecordTypeId==cusRecType_15||a.RecordTypeId==parRecType||a.RecordTypeId==parRecType_15){
                if(accMap.containsKey(a.Id)){                   
                }else{
                    dt=a.createdDate.date();
                    accMap.put(a.Id,dt);
                }
            }
        }
        //system.debug('AccountMapSize:'+accMap);
        return accMap;
    }//end method getAllAssetByAccount
    /**/
    /*
    private Map<Id,List<Asset>> getAssets(set<Id> accIds){
        List<Asset> assetLst, tempLst;
        Map<Id,List<Asset>> assetMap=new Map<Id,List<Asset>>();
        tempLst=[Select a.AccountId, a.Id, a.IB_Status__c, a.Calculated_Contract_Status__c, a.Status, a.Support_End_Date__c 
                from Asset a where a.accountId IN :accIds and 
                ((a.IB_Status__c LIKE '%demo%') OR (a.IB_Status__c LIKE 'transfer%') OR (a.IB_Status__c LIKE 'Under%'))];
        if((tempLst!=NULL)&&(tempLst.size()>0)){
            for(Asset temp : tempLst){
                if(assetMap.containsKey(temp.accountId)){
                    assetMap.get(temp.accountId).add(temp);
                }else{
                    assetLst=new List<Asset>();
                    assetLst.add(temp);
                    assetMap.put(temp.accountId,assetLst);
                }
            }
        }
        return  assetMap;
    }//end method getAssets */
}//end class