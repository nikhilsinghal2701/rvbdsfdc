public with sharing class RVBD_MassUpdateListController {
	
   private final ApexPages.StandardSetController cntr;
   private final List<SObject> objs;
   public Schema.sObjectType obType;
   private String sType;
   
   public RVBD_MassUpdateListController(ApexPages.StandardSetController controller) {
       cntr = (ApexPages.StandardSetController)controller;
       if (this.objs == null){
            this.objs = (List<SObject>)cntr.getSelected();
        sType= discoverSObjectType(objs.get(0)).getName(); 
       }   
   }

	public PageReference save(){
		processList(objs);
		string returl;
		system.debug('Object type::::::::'+sType);
		if(sType =='Lead'){
			returl = '/00Q?fcf='+ApexPages.currentPage().getParameters().get('pg'); 
		}
		if(sType=='Contact'){
			returl = '/003?fcf='+ApexPages.currentPage().getParameters().get('pg');
		}
		PageReference listPage = new PageReference(returl);   
	    listPage.setRedirect(true);           
	    return listPage;  
	}
	
	public DescribeSObjectResult discoverSObjectType(SObject s) {
        Map<String, Schema.SObjectType> des = Schema.getGlobalDescribe();
        for(Schema.SObjectType o:des.values()){
            if( s.getSObjectType()==o){
                return o.getDescribe();
            }     
        }
        return null;
    }
	
	public void processList(List<sObject> items){		
   		 List<cObject> resultList = new List<cObject>();
    	 for(sObject s : items){
			resultList.add(new cObject(s));	
   		 } 
   		 list<Sobject> updateList = new  list<Sobject>(); 
   		 for (cObject res : resultList){
    		res.obj.put('Concierge_Rep__c',UserInfo.getUserId());
    		updateList.add(res.obj);
   		 }
   		 system.debug('Update List:::::::::'+updateList);
   		 update updateList;
  	}

   public class cObject{
  		 sObject obj {get; set;}    
    	 public cObject(sObject obj){
     	 this.obj = obj; }
	}
}