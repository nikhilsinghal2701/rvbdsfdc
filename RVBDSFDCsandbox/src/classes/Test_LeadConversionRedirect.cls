@isTest(SeeAllData=true)
public with sharing class Test_LeadConversionRedirect {
    
    static testmethod void test_LeadRedirection(){
        RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        Id timeoutQueueId = ([select id from group where name ='Time Out' limit 1].Id);
        Id inactive = ([Select id from User where isactive= True limit 1].Id);
        List<Lead> uplead = new List<Lead>();
        User eloquaUser = [select id, name from User where name = 'Eloqua Administrator'];
        
        Lead newLead1  = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'Riverbed',ownerid = inactive,
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id );
        Lead newLead2  = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'Riverbed', ownerid = timeoutQueueId,
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id );
        Lead newLead3 = new Lead(firstname = 'Tony', lastname = 'Bruch', company = 'Tony Burch', 
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id );
        uplead.add(newLead1);
        uplead.add(newLead2);
        uplead.add(newLead3);
       
        insert uplead;
        
        ApexPages.StandardController cont = new ApexPages.StandardController(new Lead());       
        LeadConversionRedirectController lcr = new LeadConversionRedirectController(cont);
        
            Test.startTest();
            System.currentPageReference().getParameters().put('id',newLead1.id);
            lcr.onLoad();
            System.currentPageReference().getParameters().put('id',newLead2.id);
            lcr.onLoad(); 
            System.currentPageReference().getParameters().put('id',newLead3.id);
            lcr.onLoad(); 
            lcr.cancel();
            Test.stopTest();
        
    }
}