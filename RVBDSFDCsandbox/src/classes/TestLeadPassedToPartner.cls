@isTest
private class TestLeadPassedToPartner {

    static testMethod void myUnitTest() {  
        string PROGRESSBAR='/img/waiting_dots.gif';     
        RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        RecordType conRT=[select id,name from recordtype where SobjectType ='Contact'  and name='Standard Contact']; 
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc;
        
        Contact testCon=new Contact();
        testCon.FirstName='test1';
        testCon.LastName='14Nov';
        testCon.AccountId=stpAcc.Id;
        testCon.RecordTypeId=conRT.Id;
        testCon.PartnerRole__c='orders';
        testCon.User_Profile__c='Admin';
        insert testCon;
        
        List<string> nameStr=new List<string>{'Deal Registration - Distributor','Deal Registration'};
        RecordType[] leadRT=[select id,name from recordtype where SobjectType ='Lead' and name ='Lead Passed To Partner'];        
        Account distiAcc=[select Id,Name,RecordTypeId from Account where name='Zycko Ltd' and RecordTypeId=:accPatRT.Id and Type='Distributor'];
        Account tier2Acc=[select Id,Name,RecordTypeId from Account where name='Trace 3' and RecordTypeId=:accPatRT.Id and Type='VAR'];
        Contact[] tier2Con=[select Id,Name,RecordTypeId from contact where AccountId=:tier2Acc.Id limit 10];
        Contact[] distiCon=[select Id,Name,RecordTypeId from contact where AccountId=:distiAcc.Id limit 10];
            
        Distributor_Reseller__c dr=new Distributor_Reseller__c();
        dr.Account__c=distiAcc.Id;
        dr.Distributor_Reseller_Name__c=tier2Acc.Id;
        insert dr;
        dr.Distributor_Reseller_Name__c=distiAcc.Id;
        update dr;
        
        User usr=[select Id,Name from user where UserType='PowerPartner' AND IsActive=True AND Contact.Account.Id=:distiAcc.Id limit 1];
        system.runAs(usr){
            List<SelectOption> options=new List<SelectOption>();
            User lstUser=[Select u.ContactId, u.Contact.Account.Id, Contact.Account.Name, u.Contact.Id, u.Contact.Name, Contact.Account.Distributor_Only__c,
                        contact.Account.Type, u.Contact.Phone, u.Id, u.UserType__c from User u where u.Id=:usr.Id];
            for(Distributor_Reseller__c distiRes:[Select d.Id, d.Account__c, d.Distributor_Reseller_Name__c, d.Partner_Type__c, d.Distributor_Reseller_Name__r.Name 
                from Distributor_Reseller__c d where d.Account__c=:lstUser.Contact.Account.Id]){
                if (distiRes.Distributor_Reseller_Name__c != null && distiRes.Distributor_Reseller_Name__r.Name != null)            
                    options.add(new SelectOption(distiRes.Distributor_Reseller_Name__c,distiRes.Distributor_Reseller_Name__r.Name));
            }   
            
            Lead led=new Lead(AnnualRevenue = 1200000, Number_Of_Employees__c = 200, Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
                                Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', Secondary_Application__c = 'UDP');
            led.Status='Open';
            led.Sold_To_Partner__c=distiAcc.Id;
            led.Partner__c=options[0].getValue();
            led.Tier2_Partner_Contact__c=tier2Con[0].Id;
            led.FirstName='testLead1';
            led.LastName='14Nov';
            led.Company='Edit Corp';
            led.Title='Tester';
            led.Email='tester@editcorp.com';
            led.Project_Name__c='MultiTier';
            led.Project_Close_Date__c=system.today();
            led.Verified_Budget__c='Yes';
            led.Opportunity_Value__c=70000;
            led.Competitors__c='Cisco';
            led.Phone='4154154156';
            led.MobilePhone='4154154156';
            led.Fax='4154154156';
            led.Industry='Electronics Wholesale';
            led.Website='www.editcorp.com';
            led.Street='199 fremont street';
            led.City='San Francisco';
            led.State='CA';
            led.Country='US';
            led.RecordTypeId=leadRT[0].Id;
            insert led;
            List<SelectOption> con=new List<SelectOption>();
            con.add(new SelectOption(testCon.Id,testCon.Id));
            con.add(new SelectOption(testCon.Id,testCon.Id));       
            
            
            ApexPages.StandardController sc = New ApexPages.StandardController(led);        
            System.currentPageReference().getParameters().put('id',led.id);
            LeadPassedToPartner dealReg=new LeadPassedToPartner(sc);
            dealReg.setCheck(true);
            dealReg.getCheck();
            dealReg.setCompany('Edit Corp');
            dealReg.getCompany();
            dealReg.setShowPartner(false);
            dealReg.getShowPartner();
            dealReg.setContact(testCon.Id);
            dealReg.getContact();
            dealReg.setContacts(con);
            dealReg.getContacts();
            dealReg.setLastName(led.LastName);
            dealReg.getLastName();
            dealReg.getLeadRecType();
            dealReg.getOwner();
            dealReg.setPartner(dr.Id);
            dealReg.getPartner();
            dealReg.setPartners(options);
            dealReg.getPartners();
            dealReg.getShow();
            dealReg.getShowPartner();
            dealReg.isContact();
            dealReg.isVisible();
            dealReg.setProgressBar(PROGRESSBAR);
            dealReg.getProgressBar();
            try{
                dealReg.saverec();
            }catch(Exception e){
                
            }
                        
            //Reseller to Distributor
            Lead led1=new Lead();
            led1.Status='In Progress';
            led1.Sold_To_Partner__c=distiAcc.Id;
            led1.Partner__c=tier2Acc.Id;
            led1.Sold_to_Partner_Contact__c=distiCon[0].Id;
            led1.FirstName='testLead1';
            led1.LastName='14Nov';
            led1.Company='Edit Corp';
            led1.Title='Tester';
            led1.Email='tester@editcorp.com';
            led1.Project_Name__c='MultiTier';
            led1.Project_Close_Date__c=system.today();
            led1.Verified_Budget__c='Yes';
            led1.Opportunity_Value__c=70000;
            led1.Competitors__c='Cisco';
            led1.Phone='4154154156';
            led1.MobilePhone='4154154156';
            //led1.Fax='4154154156';
            led1.Industry='Electronics Wholesale';
            led1.Website='www.editcorp.com';
            led1.Street='199 fremont street';
            led1.City='San Francisco';
            led1.State='CA';
            led1.Country='US';
            led1.RecordTypeId=leadRT[0].Id;
            List<SelectOption> con1=new List<SelectOption>();
            con1.add(new SelectOption(testCon.Id,testCon.Id));
            con1.add(new SelectOption(testCon.Id,testCon.Id));       
            
            
            ApexPages.StandardController sc1 = New ApexPages.StandardController(led1);        
            //System.currentPageReference().getParameters().put('id',led1.id);
            LeadPassedToPartner dealReg1=new LeadPassedToPartner(sc1);
            dealReg1.setCheck(true);
            dealReg1.getCheck();
            dealReg1.setCompany('Edit Corp');
            dealReg1.getCompany();
            dealReg1.setContact(testCon.Id);
            dealReg1.getContact();
            dealReg1.setContacts(con);
            dealReg1.getContacts();
            dealReg1.setLastName(led.LastName);
            dealReg1.getLastName();
            dealReg1.getLeadRecType();
            dealReg1.getOwner();
            dealReg1.setPartner(dr.Id);
            dealReg1.getPartner();
            dealReg1.setPartners(options);
            dealReg1.getPartners();
            dealReg1.getShow();
            dealReg1.getShowPartner();
            dealReg1.isContact();
            dealReg1.isVisible();
            try{
                dealReg1.saverec();
            }catch(Exception e){
                
            }        
        }           
    }         
}