public with sharing class AccountReverseBingoReport {
    public List<Case> accountCaseList{get; private set;}    
    public AccountReverseBingoReport(){
        Id aId = System.currentPageReference().getParameters().get('id');
        Id cId = System.currentPageReference().getParameters().get('cid');
        if(cId==null)
            //accountCaseList=[Select Id, AccountId, Contact.Support_Engineer__c,Account.Name, Additional_Comment__c, CaseNumber, Communications__c, ContactId, Contact.Name, Date_Time_Opened_2__c, Diagnostics_Data__c, IsClosed, OwnerId, Responsiveness__c, Status, c.Subject, Technical_Expertise__c   // commented Communications__c as its deleted now (Ticket# 192992)
            accountCaseList=[Select Id, AccountId, Contact.Support_Engineer__c,Account.Name, Additional_Comment__c, CaseNumber, ContactId, Contact.Name, Date_Time_Opened_2__c, Diagnostics_Data__c, IsClosed, OwnerId, Responsiveness__c, Status, c.Subject, Technical_Expertise__c 
                             from Case c where AccountId=:aId 
                             and (Technical_Expertise__c != null 
                             or Responsiveness__c != null 
                            // or Communications__c != null 
                             or Diagnostics_Data__c != null)];
        else
           // accountCaseList=[Select Id, AccountId, Contact.Support_Engineer__c, Account.Name, Additional_Comment__c, CaseNumber, Communications__c, ContactId, Contact.Name, Date_Time_Opened_2__c, Diagnostics_Data__c, IsClosed, OwnerId, Responsiveness__c, Status, c.Subject, Technical_Expertise__c 
            accountCaseList=[Select Id, AccountId, Contact.Support_Engineer__c, Account.Name, Additional_Comment__c, CaseNumber, ContactId, Contact.Name, Date_Time_Opened_2__c, Diagnostics_Data__c, IsClosed, OwnerId, Responsiveness__c, Status, c.Subject, Technical_Expertise__c 
                             from Case c where AccountId=:aId 
                             and ContactId=:cId
                             and (Technical_Expertise__c != null 
                             or Responsiveness__c != null 
                            // or Communications__c != null 
                             or Diagnostics_Data__c != null)];
    }
}