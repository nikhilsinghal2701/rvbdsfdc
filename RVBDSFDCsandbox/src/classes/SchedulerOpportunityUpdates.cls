global class SchedulerOpportunityUpdates implements Schedulable
{	
  global void execute(SchedulableContext SC) {
            	
     BatchOpportunityRollup bal = new BatchOpportunityRollup();
     Database.executeBatch(bal, 5);     
  }
}