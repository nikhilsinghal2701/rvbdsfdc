public class CreditApplicationExt {
    
    private List<Account_Reference__c> accountReferences;
    private List<Account_Reference__c> deleteAccountReferences {set;get;}
    
    private Credit_Application__c creditApp;
    private Boolean showNoDocumentCheckbox = false;
    private Credit_Application__c sCreditApp;
    private String userType {get; set;}
    public Boolean err {get; private set;}
    
    public CreditApplicationExt(ApexPages.StandardController controller) {
        this.creditApp = (Credit_Application__c) controller.getRecord();
        deleteAccountReferences = new List<Account_Reference__c>();
    }
    
    public List<Account_Reference__c> getAccountReferences() {
        if (accountReferences == null) {
            accountReferences = [Select Id,Company_Name__c, Contact_Name__c, Company_Address__c, City__c, State__c,
                Zip_Code__c, Phone__c, Email__c, Telephone__c, Credit_LIne__c
                from Account_Reference__c where Credit_Application__c = :this.creditApp.Id];
            if (accountReferences.isEmpty()) {
                accountReferences.add(new Account_Reference__c(Credit_Application__c = this.creditApp.Id));
            }
        }
        return accountReferences;
    }
    
    public PageReference saveAll() {
        try{

            if (!deleteAccountReferences.isEmpty()) {
                delete deleteAccountReferences; 
            } 
     
            if (! accountReferences.isEmpty()) {
                upsert accountReferences;   
            } 
            Decimal accRef = [select Total_Account_References__c from Credit_Application__c where id = :creditApp.Id].Total_Account_References__c;
            if(accRef < 1 ){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'At least 1 Account reference is required. You have entered only '+accref));
                return null;
            }else{
                update creditApp;
            }
        }catch(DMLException e){
            System.debug('Exception in saving Credit Application: ' + e.getDMLMessage(0));
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLMEssage(0)));
            //err = true;
            return null;
        }

        return new PageReference('/' + this.creditApp.Id);
    }
    
    public PageReference submitCreditApp() {
        err = false;
        System.debug('Start submitCreditApp()');
        //check if documents have been attached to the app
        sCreditApp = [Select a.Id, Total_Account_References__c, a.Accept_Terms__c, a.Document_Opt_Out__c, a.Partner_Application_Task__c, (select t.name from attachments t)  From Credit_Application__c a where Id = :this.creditApp.Id];
        System.debug('sCreditApp = ' + sCreditApp.attachments);
        if(sCreditApp.Total_Account_References__c == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all the required fields on the application form.'));
            err = true;
        }
        if (sCreditApp.attachments.size() == 0) {
            showNoDocumentCheckbox = true;
        }
        PageReference pageRef = Page.submitCreditApplication;
        return pageRef;
    }
    
    public PageReference cancelSubmitCreditApp() {
        PageReference pageRef = new pageReference('/' + this.creditApp.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference backToTask() {
        System.debug('this.creditApp = ' + this.creditApp);
        try {
            Id partnerAppTask = [Select Partner_Application_Task__c from Credit_Application__c where Id = :this.creditApp.Id].Partner_Application_Task__c;
            PageReference pageRef = new pageReference('/' + partnerAppTask);
            pageRef.setRedirect(true);
            return pageRef;
        } catch(Exception e) {
            System.debug('e = ' + e.getMessage());
            return null;
        }
    }
    
    public PageReference authorizeSubmit() {
        Boolean err = false;
        if (! sCreditApp.Accept_Terms__c) {
            ApexPages.Message errorMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please accept the Terms and Conditions.');
            ApexPages.addMessage(errorMsg1);
            err = true;
        }
        if (showNoDocumentCheckbox && (!sCreditApp.Document_Opt_Out__c)) {
            ApexPages.Message errorMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Either attach the document or accept to Opt out.');
            ApexPages.addMessage(errorMsg2);
            err = true;
        }
        if (err) {
            return null;
        }
        Approval.ProcessResult result = submitForApproval(this.sCreditApp.Partner_Application_Task__c,'Credit Application Complete' );
        if (! result.isSuccess()) {
            String message = 'Failed to Submit Credit Application for approval: ' + result.getErrors()[0].getMessage();
            ApexPages.Message errorMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR, message);
            ApexPages.addMessage(errorMsg2);
            err = true;
            return null;
        }
        sCreditApp.Submitted__c = true;
        update sCreditApp;
        
/*      Application_Task__c appTask = new Application_Task__c(Id = this.sCreditApp.Partner_Application_Task__c,
            Status__c = 'Complete');
        update appTask;*/
        PageReference pageRef = new PageReference('/' + this.sCreditApp.Partner_Application_Task__c);
        return pageRef;
    }
    
    public PageReference addAccountReference() {
        accountReferences.add(new Account_Reference__c(Credit_Application__c = this.creditApp.Id));
        return null;
    }
    
    public PageReference deleteAccountReference() {
        Integer size = accountReferences.size();
        if (size > 0) {
            Account_Reference__c accountReference = accountReferences.get(size - 1);
            if (accountReference.Id != null) {
                deleteAccountReferences.add(accountReference);  
            }
            accountReferences.remove(size - 1);//remove the last item in the list
        }
        return null;
    }
    
    public Boolean getShowNoDocumentCheckbox() {
        return this.showNoDocumentCheckbox; 
    }
    
    public Credit_Application__c getSCreditApp() {
        return this.sCreditApp; 
    }
    
    public Boolean getCreditAppEditable() {
        if (userType == null) {
            User u = [Select UserType from User where Id = :UserInfo.getUserId()];
            userType = u.UserType.toLowerCase().indexOf('partner') != -1 ? 'PARTNER' : 'INTERNAL';
        }
        
        return (!(this.CreditApp.Submitted__c && (userType.equals('PARTNER'))));    
    }
    
    public Boolean getCreditAppNotEditable() {
        return (! getCreditAppEditable());
    }
    
    public Boolean getPartnerUser() {
        return userType.equals('PARTNER');
    }
    
    /* Submits an object for approval*/
    public Approval.ProcessResult submitForApproval(Id objId, String message) {
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments(message);
        req.setObjectId(objId);
        Approval.ProcessResult result = Approval.process(req,false);
        return result;
    }
    
}