public class SupportProviderProfileController {
    
    public Id tempId {get; set;} 
    public Asset assetRecord {get; set;}
    public Account accRecord {get; set;}
    public Boolean displayCondition {get; set;}
     
    public SupportProviderProfileController (ApexPages.StandardController stdController) {
  
      this.tempId = stdController.getId(); 
      assetRecord = [Select Id,Support_Provider__c From Asset Where Id = :tempId ];
      accRecord = [Select Id, Name, RASP_Status__c,Support_Provider_Profile__c From Account Where Id = :assetRecord.Support_Provider__c];
      setDisplayCondition(accRecord);      
     }
     
     public void setDisplayCondition( Account acc){
     
        if(acc.RASP_Status__c == 'Authorised'){
           displayCondition = true ;
           }
     }
}