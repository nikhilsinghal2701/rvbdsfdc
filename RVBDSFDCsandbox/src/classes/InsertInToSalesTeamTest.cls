public class InsertInToSalesTeamTest 
{

	static testMethod void OpportunityInsertTest() 
	{
		//Opportunity o = new Opportunity (OwnerId='00570000000msXE',Name='Test Trigger 2',Registering_Partner__c='0015000000FC60Q',Registering_Partner_User__c='005T0000000nE8I',StageName='0 - Prospect',CloseDate=Date.newInstance(2008,06,30));
		//Visible_to_Partners__c vp = new Visible_to_Partners__c (OwnerId='00570000000mbxG',Name='Test Trigger 2',Registering_Partner__c='00130000000ZMdo',VAR__c='a0ST00000008yGy',Registering_Partner_User__c='00570000001ISUb',StageName='0 - Prospect',CloseDate=Date.newInstance(2008,06,30));
		

//			Visible_to_Partners__c [] vpd = [select Id,Role__c,PartnerName__c,Opportunity_Name__c,Opportunity_Access__c from Visible_to_Partners__c where Id = 'a0ET0000000n2u9'];
//			delete vpd;
		
//			OpportunityTeamMember[] tm = [select Id,TeamMemberRole,OpportunityAccessLevel,OpportunityId,UserId from OpportunityTeamMember where OpportunityId = '0067000000AtkLU' and UserId ='00530000000lXnZ']; 
	
			//Delete the actual identified record from OpportunityTeamMember
//			delete tm;
		List<Account> lstAcc=new List<Account>();
        Opportunity testOpp=new Opportunity();
		RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=accCusRT.Id;
        cusAcc.name='CustomeraAAccount';
        cusAcc.Type='Customer';
        cusAcc.Industry='Education';
        lstAcc.add(cusAcc);
        
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        lstAcc.add(stpAcc);
        
        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId=accRT.Id;
        tier2Acc.name='stpAccount';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Authorizations_Specializations__c='RASP';
        lstAcc.add(tier2Acc);
        
        insert lstAcc;
        
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' AND IsActive=True limit 2];
        
        testOpp.AccountId=cusAcc.Id;
        testOpp.Channel__c='Reseler';
        testOpp.Sold_To_Partner__c=stpAcc.Id;
        testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp.Primary_ISR__c=primaryISR.Id;
        testOpp.Name='TestOpportunity';
        testOpp.Type='New';
        testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp.CloseDate=System.today();
        testOpp.StageName='0-Prospect';
        testOpp.Forecast_Category__c='PipeLine';
        testOpp.Competitors__c='cisco';
        testOpp.LeadSource='Email';
        //testOpp.Support_Provider__c=stpAcc.Id;
        //testOpp.Support_Provided__c='Reseller';
        insert testOpp;
			
			
			//Visible_to_Partners__c vp = new Visible_to_Partners__c(Role__c='Partner',PartnerName__c='00530000000ejWT',Opportunity_Name__c='0067000000AtkLU',Opportunity_Access__c='Edit');
			Visible_to_Partners__c vp = new Visible_to_Partners__c(Role__c='Partner',PartnerName__c=testOpp.Sold_To_Partner_User__c,Opportunity_Name__c=testOpp.Id,Opportunity_Access__c='Edit');
			insert vp;
			Visible_to_Partners__c vp1 = new Visible_to_Partners__c(Role__c='Partner',PartnerName__c=testOpp.Sold_To_Partner_User__c,Opportunity_Name__c=testOpp.Id,Opportunity_Access__c='Edit');
			insert vp1;
			
			delete vp1;
		
			//OpportunityTeamMember otm = new OpportunityTeamMember(TeamMemberRole='Partner',OpportunityId='0067000000AtkLU',UserId ='00530000000ejWT');
			OpportunityTeamMember otm = new OpportunityTeamMember(TeamMemberRole='Partner',OpportunityId=testOpp.Id,UserId =testOpp.Sold_To_Partner_User__c);
			insert otm;
			
			//OpportunityShare os = [select Id,OpportunityAccessLevel from OpportunityShare where OpportunityId='0067000000AtkLU' and UserOrGroupId='00530000000ejWT' and RowCause='Team'];
			OpportunityShare os = [select Id,OpportunityAccessLevel from OpportunityShare where OpportunityId=:testOpp.Id and UserOrGroupId=:testOpp.Sold_To_Partner_User__c and RowCause='Team'];
				//Log the queried record 
				
				if (os.OpportunityAccessLevel != 'All'){
					os.OpportunityAccessLevel= vp.Opportunity_Access__c;
					update os;	 
				}
	}
	
}