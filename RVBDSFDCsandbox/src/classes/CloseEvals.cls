public with sharing class CloseEvals {
    public List<Asset> CEvalList {get;set;}
    public List<opportunity> opptylList {get;set;}
     public String opptyId;
     public boolean showContinueButton {get;set;}
     Id actId;
     public Opportunity o {get; set;}
     public Boolean closeOpp {get; set;}
     public String message {get; set;}
     private User u;
     
     public CloseEvals(ApexPages.StandardController controller)
     {
        
        showContinueButton = false;
        closeOpp = false;
        u = [Select id, Profile.Name from user where id=:UserInfo.getUserId()];
        opptyId = ApexPages.currentpage().getParameters().get('id');
        CEvalList = [Select id,SerialNumber,IB_Status__c,SKU__c,asset.Opportunity__r.ID,InstallDate, 
                            Asset.Opportunity__c, Asset.Opportunity__r.Close_Eval__c, Asset.Opportunity__r.StageName, 
                            Asset.Opportunity__r.Reason_Lost__c, Asset.Opportunity__r.Select_Product_Line__c, 
                            Asset.Opportunity__r.Probability,Asset.Opportunity__r.Deal_Winner_in_a_Lost_Deal__c
                    from asset where IB_Status__c='Under Evaluation' 
                    and Asset.Opportunity__c =:opptyId limit 1];
        //if(CEvalList.size() > 0)
        o = [select id,StageName,Deal_Winner_in_a_Lost_Deal__c,Other_competitor__c,Probability,Close_Eval__c,Why_did_we_win_lose__c,Select_Product_Line__c,Reason_Lost__c from opportunity where id=:opptyId];
        
               
    }
    public pagereference validateOppty()
    {   
        System.Debug('Oppty : '+o);
        if(u.Profile.Name != 'System Administrator' && ( o.StageName == '7 - Closed (Not Won)' || o.StageName =='6 - Order Accepted') && o!=null)
        {
            message = 'The Opportunity is already closed';
            return   null;
        }
        else
        {
            If(CEvalList.size() > 0)
            {
                showContinueButton = true;
                message = 'This Opportunity has open Eval Assets, Click on continue to close this opportunity.';
                return   null;
            }
            else
           //message = 'There is no evals agianst this Opportunity.';
            return  new pagereference('/'+O.id);
        }
    }
     
    public PageReference doContinue()
    {  
        closeOpp = true;
        showContinueButton = false;
        System.Debug('closeOpp : '+closeOpp);
        return null ;
    }
    
    public PageReference doSave(){
        try
        {
            if(o.StageName != '7 - Closed (Not Won)' || !o.Close_Eval__c)
            {
                o.StageName = '7 - Closed (Not Won)';
                o.Close_Eval__c = true;
                o.CloseDate=Date.today();
                update o;
            }
        }
        catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getMessage());
            Apexpages.addMessage(msg);
            return null;
        }
        return(new Apexpages.Standardcontroller(o).view());
    }
    
    /*public PageReference cancel()
    {
        return(new Apexpages.Standardcontroller(o).view());
    }*/
}