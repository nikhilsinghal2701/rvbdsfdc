/**
* Test trigger handler class to handle the logic of
* Account field values are copied to the related opportunities
* Fields: 1. Partner Level 2. Number of Competencies 3. List of Competencies
* Fired Conditions: 1. Account is 'Sold to Parnter' or 'Tier 2 Partner'
*                   2. Opportunity is Open
*
* @author Remy.Chen
* @date 10/08/2013
*/
@isTest
public class TestAccountTrigger {

																/**
																* Test method to test single opportunity update
																* when the threes fields in account is changed
																*  1. Partner Level 2. Number of Competencies 3. List of Competencies
																*/
																/*static testMethod void singleOpportunityUpdateTest() {
																																																List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,
																new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>2, 'Type__c'=>'Regular'});

																								List<Account> accList = TestingUtilPP.createAccounts(3, true,
																																new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'Acceleration', 'Type' => 'VAR'});
																								List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
																								List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(1, true,
																								new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
																								List<Opportunity> sOppList = TestingUtilPP.createOpportunities(1, true,
																																new Map<String, Object>{'Sold_to_Partner__c'=>accList[0].Id, 'AccountId'=>accList[1].Id, 'CloseDate'=>Date.today(), 'StageName'=>'0 - Prospect'});
																								List<Opportunity> tOppList = TestingUtilPP.createOpportunities(1, true,
																																new Map<String, Object>{'Sold_to_Partner__c'=>accList[0].Id,'Tier2__c'=>accList[1].Id, 'AccountId'=>accList[1].Id, 'CloseDate'=>Date.today(), 'StageName'=>'0 - Pre-Qualification'});
																								List<Account> accListt = TestingUtilPP.createAccounts(3, true,
																																new Map<String, Object>{'Partner_Level__c'=>'Gold', 'Authorizations_Specializations__c'=>'Acceleration', 'Type' => 'Cloud'});
																																																Test.startTest();

																								accList[0].Partner_Level__c = 'Gold';
																								accList[1].Partner_Level__c = 'Platinum';
																								update accList;
																								update accListt;
																								Test.stopTest();

																								List<Opportunity> result = [select Id, Sold_To_Partner_Level__c , Sold_To_Partner_Authorizations__c, STP_Number_of_Competencies__c
																																								FROM Opportunity WHERE Sold_to_Partner__c = : accList[0].Id ];
																								System.assertEquals('Gold', result[0].Sold_To_Partner_Level__c);
																								//System.assertEquals('Acceleration', result[0].Sold_To_Partner_Authorizations__c);
																								//System.assertEquals(1, result[0].STP_Number_of_Competencies__c);

																								List<Opportunity> result2 = [SELECT Id,Tier2__c,Tier2_Level__c, Tier2_Authorizations__c, Tier_2_Number_of_Competencies__c
																																								FROM Opportunity WHERE Tier2__c = : accList[1].Id ];

																								System.assertEquals('Platinum', result2[0].Tier2_Level__c);
																								System.assertEquals('Acceleration', result2[0].Tier2_Authorizations__c);
																}*/




/**
* Test method to test single opportunity doesn't update
* when the three fields in account are not changed
* 1. Partner Level 2. Number of Competencies 3. List of Competencies
*
*/
static testMethod void unupdatedOpportunyTest() {
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<Account> accList = TestingUtilPP.createAccounts(2, true,new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
	List<Opportunity> sOppList = TestingUtilPP.createOpportunities(1, true,new Map<String, Object>{'Sold_to_Partner__c'=>accList[0].Id, 'AccountId'=>accList[1].Id, 'CloseDate'=>Date.today(), 'StageName'=>'0 - Prospect'});
	Test.startTest();
	accList[0].name = 'helloworld';
	update accList;
	Test.stopTest();
	List<Opportunity> result = [select Id, Sold_To_Partner_Level__c , Sold_To_Partner_Authorizations__c, STP_Number_of_Competencies__c 
									FROM Opportunity WHERE Sold_to_Partner__c = : accList[0].Id ];
	System.assertEquals('Silver', result[0].Sold_To_Partner_Level__c);
}

/**
* Test method to test batch opportunities update
* when the three fields in account are changed
* 1. Partner Level 2. Number of Competencies 3. List of Competencies
*
**/
static testMethod void batchOpportuniesUpdateTest() {
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<Account> accList = TestingUtilPP.createAccounts(15, true,new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Authorizations_Specializations__c'=>'RASP'});
	List<Opportunity> sOppList = TestingUtilPP.createOpportunities(15, true,new Map<String, Object>{'Sold_to_Partner__c'=>accList[0].Id, 'AccountId'=>accList[1].Id, 'CloseDate'=>Date.today(), 'StageName'=>'0 - Prospect'});
	Test.startTest();
	accList[0].Partner_Level__c = 'Gold';
	update accList;
	Test.stopTest();
	List<Opportunity> result = [select Id, Sold_To_Partner_Level__c , Sold_To_Partner_Authorizations__c, STP_Number_of_Competencies__c
																	FROM Opportunity WHERE Sold_to_Partner__c = : accList[0].Id ];
	System.assertEquals('Gold', result[0].Sold_To_Partner_Level__c);
	System.assertEquals('RASP', result[0].Sold_To_Partner_Authorizations__c);
}

																/**
																* Test method to test logic of number of competencies increment
								* Partner Level will be updated and
								* Probation Ending Date will be cleaned
																*

																static testMethod void singleNumOfCompetenciesIncrementCloud() {
																								List<PartnerLevelConfig__c> plcList = TestingUtil.createPartnerLevelConfigs(1, true,
																new Map<String, Object>{'Automate_Promotion__c'=>true,'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Cloud'});

																								List<Account> accList = TestingUtil.createAccounts(1, true,
																																																new Map<String, Object>{'Partner_Level__c'=>'Silver','Partner_Status1__c'=>'Good Standing', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Cloud Partner'});
																								List<Authorizations_Master__c> amList = TestingUtil.createAuthMaster(1, true, new Map<String, Object>());
																								Test.startTest();
																								List<Account_Authorizations__c> accAuthList = TestingUtil.createAccAuths(2, true,
																								new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
																								update acclist;
																								Test.stopTest();

																List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
																From Account WHERE ID = : accList[0].Id];
																//System.assertEquals(null, result[0].Probation_Ending_Date__c);
																//System.assertEquals('Gold', result[0].Partner_Level__c);
																}**/

/**
* Test method to test logic of number of competencies increment
* Partner Level will be updated and
* Probation Ending Date will be cleaned
*
**/
static testMethod void singleNumOfCompetenciesIncrementRegular() {
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true,  'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Regular'});
		List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Silver','Partner_Status1__c'=>'Good Standing', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Authorization'});
		List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
		Test.startTest();
		List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,
		new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
		Test.stopTest();
		List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id,Compliance_Level__c
		From Account WHERE ID = : accList[0].Id];
		//System.assertEquals(null, result[0].Probation_Ending_Date__c);
		//System.assertEquals('Gold', result[0].Partner_Level__c);
										//System.assertEquals('Gold', result[0].Compliance_Level__c);
}

/*** Test method to test logic of number of competencies increment
* Partner Level will not be updated When the Do_not_Auto_Level__c is true
***/
static testMethod void singleNumOfCompetenciesPreventIncrementRegular() {
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true,  'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Regular'});
	Date d = Date.today();
	List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Silver','Compliance_Level__c'=>'none','Partner_Status1__c'=>'Good Standing', 'Do_not_Auto_Level__c'=>true, 'Probation_Ending_Date__c'=>d, 'Authorizations_Specializations__c'=>'Authorization'});
	List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
	Test.startTest();
	List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
	Test.stopTest();
	List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id From Account WHERE ID = : accList[0].Id];
	//System.assertEquals(null, result[0].Probation_Ending_Date__c);
	System.assertEquals('Silver', result[0].Partner_Level__c);
}
/*** Test method to test logic of number of competencies increment
* Partner Level will be updated and
* Probation Ending Date will be cleaned
***/
static testMethod void singleNumOfCompetenciesIncrementFederal() {
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Federal'});
	List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Silver','Partner_Status1__c'=>'Good Standing', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});
	List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
	Test.startTest();
	List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
	Test.stopTest();
	List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id From Account WHERE ID = : accList[0].Id];
	//System.assertEquals(null, result[0].Probation_Ending_Date__c);
	//System.assertEquals('Gold', result[0].Partner_Level__c);
	}

/*** Test method to test logic of number of competencies decrement
* Probation Ending Date will not be cleaned***/
static testMethod void singleNumOfCompetenciesDecrementFederal() {
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Federal'});
	Test.startTest();
	List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Silver','Partner_Status1__c'=>'Good Standing', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});
	List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
	List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
	Update accAuthList[0];
	list<id> aid = new list<id>();
	aid.add(accAuthList[0].id);
	if(accAuthList!=null && accAuthList.size()>0){
		AccountAuthorizationTriggerHandler.sendCompetencyNotificationEmail(accAuthList);
		AccountAuthorizationTriggerHandler.updateAuthorizationSpecializationFieldOnAccount(accAuthList);
	}
	AccountAuthorizationTriggerHandler.getPartnerLevelConfigs();
	AccountAuthorizationTriggerHandler.updateCompetencyProbationDate(aid);
	Test.stopTest();
	Date d = Date.today();
	List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id From Account WHERE ID = : accList[0].Id];
	System.assertEquals(d, result[0].Probation_Ending_Date__c);	
}
	/*** Test method to test logic of number of competencies decrement
	* Probation Ending Date will not be cleaned***/
	static testMethod void singleNumOfCompetenciesUndecrementFederal() {
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Federal'});
		List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Silver', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});
		List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
		List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Probation','Create_Account_Authorization__c'=>true});
		Test.startTest();
			delete accAuthList[0];
		Test.stopTest();
		Date d = Date.today();
		List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);
	}
static testMethod void singleNumOfCompetenciesUndecrementCloud() {
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Cloud'});
		List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Authorized', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Cloud'});
		List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
		List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Probation','Create_Account_Authorization__c'=>true});
		Test.startTest();
			delete accAuthList[0];
		Test.stopTest();
		Date d = Date.today();
		List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);
}

/**
* Test method to test logic of number of competencies decrement
* Probation Ending Date will not be cleaned
*
**/
static testMethod void singleNumOfCompetenciesUndecrementFederalType() {
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Federal'});
		List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Authorized', 'Type' =>'VAR','Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});
		Test.startTest();
		List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
		List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(2, true,
		new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
		delete accAuthList[0];
		Test.stopTest();
		Date d = Date.today();
		List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);
}
/**
* Test method to test logic of number of competencies decrement
* Probation Ending Date will not be cleaned
*
**/
static testMethod void singleNumOfCompetenciesCheckForCertificationCounts() {
		/*RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
		Account cusAcc=new Account();
		cusAcc.RecordTypeId=accRt.Id;
		cusAcc.name='partneracc';
		cusAcc.Type='partner';
		cusAcc.Industry='Education';
		insert cusacc;
		Account cusacc=[select id,RecordTypeId,name,Type,Industry from Account where RecordTypeId='012300000000NE5AAM' limit 1];*/
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Check_For_Certification_Counts__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Federal'});
		Test.startTest();
		List<Account> accList = TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Authorized', 'Type' =>'VAR','Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Federal (US)'});
		List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
		List<Account_Authorizations__c> accAuthList = TestingUtilPP.createAccAuths(1, true,
		new Map<String, Object>{'Account__c'=>accList[0].Id, 'Authorization_Master__c'=>amList[0].Id, 'Type__c'=>'Competency','Status__c'=>'Active','Create_Account_Authorization__c'=>true});
		List<Acct_Cert_Summary__c> accCertSumList = TestingUtilPP.createCertSummary(1, true,
		new Map<String, Object>{'RSS__c'=>1,'RTSS__c'=>2, 'RCSP__c'=>2,'RCSA_RCSP__c'=>2,'Account__c'=>accList[0].Id});
		accCertSumList[0].Status_Tap__c='yes';
		update accCertSumList;
		CertificationSummaryTriggerHandler.clearTAPReqSatisfiedDate(accCertSumList);
		CertificationSummaryTriggerHandler.getAuthorizedPartnerLevelConfigs();
		DmlException ex;
		accCertSumList[0].Status_Tap__c='no';
		update accCertSumList;
		Date d = Date.today();
		/*List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);*/
		plcList[0].Type__c = 'Regular';
		update plcList[0];
		accList[0].Authorizations_Specializations__c ='Authorization';
		update accList[0];
		/*result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);*/
		plcList[0].Type__c = 'Cloud';
		update plcList[0];
		accList[0].Authorizations_Specializations__c ='Cloud Partner';
		update accList[0];
	/*result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);*/
		Test.stopTest();
		List<Account> result = [SELECT Partner_Level__c, Probation_Ending_Date__c, Id
		From Account WHERE ID = : accList[0].Id];
		System.assertEquals(d, result[0].Probation_Ending_Date__c);
}

/* Test for method updateAccountPartnerLevelOnCompetencyChange*/
static testMethod void testupdateAccountPartnerLevelOnCompetencyChange(){
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<PartnerLevelConfig__c> plcListFederal = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Federal'});
	List<PartnerLevelConfig__c> plcListCloud = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Cloud'});
	List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>2, 'Type__c'=>'Regular'});
	List<Authorizations_Master__c> amList = TestingUtilPP.createAuthMaster(1, true, new Map<String, Object>());
	Account oldAcc = new Account(Partner_Level__c = 'Authorized', Type = 'VAR', 
									Probation_Ending_Date__c = Date.today(), Authorizations_Specializations__c = 'Federal (US)');
	List<Account> newAccList = new List<Account>{new Account (Partner_Level__c = 'Authorized', Type = 'Cloud Service Provider', Probation_Ending_Date__c =Date.today(),
									Authorizations_Specializations__c = 'Comp-W')};
	Map<Id,Account> oldAccMap = new Map<Id,Account>();
	Map<Id,Account> competencyIncrease = new Map<Id,Account>();
	Map<Id,Account> competencyDecrease = new Map<Id,Account>();    
	for(Account acct : [SELECT Id, Type,Authorizations_Specializations__c FROM Account WHERE Id =:oldAcc.Id]){
		oldAccMap.put(acct.Id,acct);
	}
	for(Account acct: TestingUtilPP.createAccounts(1, true,new Map<String, Object>{'Partner_Level__c'=>'Authorized', 'Probation_Ending_Date__c'=>Date.today(), 'Authorizations_Specializations__c'=>'Cloud'}))
	{	
		competencyDecrease.put(acct.Id,acct);
	}
	//AccountTriggerHandler.updateCertSummariesOnChangeOfAccountTypeORAuthSpecFields(oldAccMap,newAccList);
	AccountTriggerHandler.updateAccountPartnerLevelOnCompetencyChange(oldAccMap, competencyDecrease);
}
/* Test for Probation method probationEmailNotificationSending*/
static testMethod void testprobationEmailNotificationSending(){
	TestDataFactory tdf = new TestDataFactory();
	tdf.createPartnerEmailNotification();
	List<PartnerLevelConfig__c> plcListFederal = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Type__c'=>'Federal'});
	List<PartnerLevelConfig__c> plcListCloud = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Demotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>1, 'Probation_Period_days__c'=>2, 'Type__c'=>'Cloud'});
	List<PartnerLevelConfig__c> plcList = TestingUtilPP.createPartnerLevelConfigs(1, true,new Map<String, Object>{'Automate_Promotion__c'=>true, 'Name'=>'Gold', 'Number_Of_Competencies__c'=>2, 'Type__c'=>'Regular'});
	Map<Id,Account> oldAccMap = new Map<Id,Account>();
	Map<Id,Account> newAccMap = new Map<Id,Account>();
	Account oldAcc = new Account(Name = 'oldAcc',Partner_Level__c = 'Authorized', Type = 'VAR', probation_ending_days__c = 1,
									Probation_Ending_Date__c = Date.today(), Authorizations_Specializations__c = 'Federal (US)');
	insert oldAcc;
	Account newAcc = [SELECT Id, Type,Authorizations_Specializations__c FROM Account WHERE Id =:oldAcc.Id];
	oldAccMap.put(newAcc.Id,newAcc);
	newAcc.probation_ending_days__c = 1;
	newAccMap.put(newAcc.Id,newAcc);
	//Account newAcc = new Account (Partner_Level__c = 'Authorized', Type = 'Cloud Service Provider', Probation_Ending_Date__c =Date.today(),probation_ending_days__c = 2,
	//								Authorizations_Specializations__c = 'Comp-W');
	//Account newAcc = new Account (Partner_Level__c = 'Authorized', Type = 'Cloud Service Provider', Probation_Ending_Date__c =Date.today(),probation_ending_days__c = 0,
	//								Authorizations_Specializations__c = 'Comp-W');
	RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
    insert rvbdEmail;
	Contact contact1 = tdf.createContact(oldAcc.Id, 'oldAcct@email.com', 'oldAccountcnt');
	Contact contact2 = tdf.createContact(newAcc.Id, 'newAcc@email.com', 'newAccountcnt');
	List<Contact> contactList = new List<Contact>();
	contactList.add(contact1);
	contactList.add(contact2);
	insert contactList;
	AccountTriggerHandler.probationEmailNotificationSending(oldAccMap,newAccMap);
}
/*
	Test for checkForCertificationCounts method

static testMethod void testCheckForCertificationCounts(){
		TestDataFactory tdf = new TestDataFactory();
		Constant.disablePartnerCompetencyTrigger = true;
		// Create Partner level config records
    	PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,
    									Name = 'Authorized',Number_Of_Competencies__c = 0,Type__c = 'VAR',
    								Partner_Level__c = 'Authorized',Is_ProductLine__c = true,Priority__c = 1,Check_For_Certification_Counts__c = true);
		insert plc;
    	Account parentAcct = tdf.createAccount();
    	ParentAcct.Do_not_rollup_Certs__c = true;
    	insert parentAcct;
    	Account acct = tdf.createAccount();
    	acct.ParentId = parentAcct.Id;
    	insert acct;
    	List<Contact> contactList = new List<Contact>();
    	for(Integer i=0 ; i<5 ; i++){
    		String cntLname = 'cnt_email_00'+i;
    		String emailStr = 'cntemail00'+i+'@gmail.com';
    		contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
    	}
    	if(!contactList.isEmpty()){
    		insert contactList;
    	}
    	List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
    	List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
    	for(String str: certList){
    		listOfCertMaster.add(tdf.createMasterCerts(str));
    	}
    	insert listOfCertMaster;
    	List<Certificate__c> certs = new List<Certificate__c>(); 
    	List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
    	List<Acct_Cert_Summary__c> certsSumList = new List<Acct_Cert_Summary__c>();
    	for(Acct_Cert_Summary__c pc: listOfAcctCerts){
    		pc.RSS__c = 2;
    		pc.RTSS__c = 2;
    		pc.RCSA_RCSP__c = 2;
    		pc.Status_Tap__c = 'yes';
    		certsSumList.add(pc);
    	}
    	Constant.disablePartnerCompetencyTrigger = false;// this is to avoid the partner competency trigger execution.
    	update certsSumList;
		Map<String, List<PartnerLevelConfig__c>> plcMap = new Map<String, List<PartnerLevelConfig__c>>();
		List<PartnerLevelConfig__c> plcList = new List<PartnerLevelConfig__c>{plc};
		plcMap.put('VAR',plcList);
		Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
		insert certs;
		List<Acct_Cert_Summary__c> certSummaries = tdf.retriveCertSummary(acct.Id);
		Map<Id,List<Acct_Cert_Summary__c>> certSummMap = new Map<Id,List<Acct_Cert_Summary__c>>();
		certSummMap.put(acct.Id,certSummaries);
		test.startTest();
			//AccountTriggerHandler.
			AccountTriggerHandler.updateComplianceOrPartnerLevelOnAccount(acct, plc,plcMap, certSummMap);
			
		test.stopTest();
}
/* Test updateComplianceLevelAndEndingDate*/
static testMethod void updateComplianceLevelAndEndingDate(){
		TestDataFactory tdf = new TestDataFactory();
		tdf.createPartnerEmailNotification();
		RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
		// Create Partner level config records
    	PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,RCSA_RCSP__c = 0,
    									Name = 'Authorized',Number_Of_Competencies__c = 0,Type__c = 'VAR',RTSS__c =0,
    								Partner_Level__c = 'Authorized',Is_ProductLine__c = false,Priority__c = 1,Check_For_Certification_Counts__c = true);
    	PartnerLevelConfig__c plcregular = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Cloud - Authorized - AD',Number_Of_Competencies__c = 0,
    								Type__c = AccountTriggerHandler.REGULAR,Priority__c = 1,RCSA_RCSP__c =0,RTSS__c =0,
    								Partner_Level__c = 'Authorized',Check_For_Certification_Counts__c = true);
    	PartnerLevelConfig__c cloudConfig = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Gold',Number_Of_Competencies__c = 0,
    								Type__c = AccountTriggerHandler.CLOUD,Partner_Level__c = 'Authorized',Is_ProductLine__c = false,RTSS__c=0,
	    							Priority__c = 1,Check_For_Certification_Counts__c = true,
	    							RCSA_RCSP__c = 0);
    	List<PartnerLevelConfig__c> plcList = new List<PartnerLevelConfig__c>{plc,cloudConfig,plcregular};
		insert plcList;
		List<Account> childAccounts = new List<Account>();
		//Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
		Account parentAcct = tdf.createAccount();
		//parentAcct.Type = 'Cloud Service Provider';
    	ParentAcct.Do_not_rollup_Certs__c = true;
    	insert parentAcct;
    	Account acct = tdf.createAccount();
    	acct.ParentId = parentAcct.Id;
    	childAccounts.add(acct);
    	Account acctCloud = tdf.createAccount();
    	acctCloud.ParentId = parentAcct.Id;
    	acctCloud.Type = AccountTriggerHandler.CLOUD;
    	acctCloud.Authorizations_Specializations__c = 'Federal';
    	childAccounts.add(acctCloud);
    	insert childAccounts;
    	List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
    	List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
    	for(String str: certList){
    		listOfCertMaster.add(tdf.createMasterCerts(str));
    	}
    	insert listOfCertMaster;
    	List<Certificate__c> certs = new List<Certificate__c>(); 
    	List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(childAccounts[0].Id);
    	listOfAcctCerts.addAll(tdf.retriveCertSummary(childAccounts[1].Id));
    	List<Acct_Cert_Summary__c> certsSumList = new List<Acct_Cert_Summary__c>();
    	List<Contact> contactList = new List<Contact>();
    	for(Integer i=0 ; i<5 ; i++){
    		String cntLname = 'cnt_email_00'+i;
    		String emailStr = 'cntemail00'+i;
    		contactList.add(tdf.createContact(childAccounts[0].Id, emailStr+'@gmail.com' ,cntLname+'g'));
    		contactList.add(tdf.createContact(childAccounts[1].Id, emailStr+'@yahoo.com' ,cntLname+'y'));
    	}
    	if(!contactList.isEmpty()){
    		insert contactList;
    	}
    	for(Acct_Cert_Summary__c pc: listOfAcctCerts){
    		pc.RSS__c = 2;
    		pc.RTSS__c = 2;
    		pc.RCSP__c = 2;
    		pc.RCSA_RCSP__c = 2;
    		pc.Status_Tap__c = 'yes';
    		certsSumList.add(pc);
    	}
    	Constant.disablePartnerCompetencyTrigger = false;// this is to avoid the partner competency trigger execution.
    	update certsSumList;
    	Map<String, List<PartnerLevelConfig__c>> plcMap = new Map<String, List<PartnerLevelConfig__c>>();
		plcMap.put('VAR',plcList);
		plcMap.put('Cloud',plcList);
		insert certs;
		List<Acct_Cert_Summary__c> certSummaries = tdf.retriveCertSummary(acct.Id);
		Map<Id,List<Acct_Cert_Summary__c>> certSummMap = new Map<Id,List<Acct_Cert_Summary__c>>();
		certSummMap.put(acct.Id,certSummaries);
		update acct;
		List<String> authMaster = new List<String>{'Comp-AD','Comp-W','Comp-SD','Comp-PM'};
		List<Authorizations_Master__c> list_of_AuthMaster = tdf.createAuthMaster(authMaster);
        insert list_of_AuthMaster;
		test.startTest();
			AccountTriggerHandler.updateComplianceLevelAndEndingDate(childAccounts[0], plc,plcMap, certSummMap);// for regular account
			AccountTriggerHandler.updateComplianceLevelAndEndingDate(childAccounts[1], plc,plcMap, certSummMap);// for cloud account
			AccountTriggerHandler.updateComplianceOrPartnerLevelOnAccount(childAccounts[0], plc,plcMap, certSummMap);
			AccountTriggerHandler.updateComplianceOrPartnerLevelOnAccount(childAccounts[1], plc,plcMap, certSummMap);
			
			// create old acc map 
			Account oldChild = childAccounts[0];
			oldChild.probation_ending_days__c = 2;
			Account newChild = childAccounts[0];
			newChild.probation_ending_days__c = 1;
			newChild.Has_Probation_Ended__c = true;
			Map<Id,Account> oldAccountMap = new Map<Id,Account>{childAccounts[0].Id=>oldChild};
			Map<Id,Account> newAccountMap = new Map<Id,Account>{childAccounts[0].Id=>newChild};
			AccountTriggerHandler.probationEmailNotificationSending(oldAccountMap,newAccountMap);
			//AccountTriggerHandler.checkForCertificationCounts(childAccounts[0], cloudConfig, plcMap, certCountCheck);
			//AccountTriggerHandler.checkForCertificationCounts(childAccounts[1], cloudConfig, plcMap, certsSumList);
			Map<Id,List<Contact>> accCntMap = new Map<Id,List<Contact>>();
			List<Contact> cList = new List<Contact>();
			cList.addAll(contactList);
			accCntMap.put(childAccounts[0].Id,cList);
			Map<Id, Account_Authorizations__c> accAuthMap = new Map<Id,Account_Authorizations__c>();
			List<Acct_Cert_Summary__c> acctCertList = tdf.retriveCertSummary(childAccounts[0].Id); 
			String ccEmails = 'test@gmail.com;test1@gmail.com';
			EmailUtil.sendEmailNotification('Riverbed_Partner_Competency_Achieved', accCntMap, accAuthMap, ccEmails);
			List<Account_Authorizations__c> accAuthList = new List<Account_Authorizations__c>();
			accAuthList.add(new Account_Authorizations__c(Authorization_Master__c = list_of_AuthMaster[0].Id,
							Account__c = childAccounts[0].Id, Status__c = 'Active'));
			accAuthList.add(new Account_Authorizations__c(Authorization_Master__c = list_of_AuthMaster[1].Id,
							Account__c = childAccounts[1].Id, Status__c = 'Probation',Competency_Probation_Date__c=Date.today()));
			accAuthList.add(new Account_Authorizations__c(Authorization_Master__c = list_of_AuthMaster[1].Id,
							Account__c = childAccounts[1].Id, Status__c = 'Inactive',Type__c = 'Competency'));
			AccountAuthorizationTriggerHandler.sendCompetencyNotificationEmail(accAuthList);
			Set<Id> accIds = new Set<Id>{childAccounts[0].Id,childAccounts[1].Id};
			Set<Id> masterIds = new Set<Id>{list_of_AuthMaster[0].Id,list_of_AuthMaster[1].Id,list_of_AuthMaster[2].Id,list_of_AuthMaster[3].Id};
			AccountAuthorizationTriggerHandler.syncUpdateAuthorizationSpecializationFieldOnAccount(accAuthList,masterIds,accIds);
			Map<String,List<PartnerLevelConfig__c>> typeToPartnerLevelConfigMap = new Map<String,List<PartnerLevelConfig__c>>();
			typeToPartnerLevelConfigMap.put(plcList[0].Type__c,plcList);
			Id acctId = childAccounts[0].Id;
			Account cAcc = [SELECT Id,Partner_Status1__c,Partner_Level__c,Number_Of_Active_Competencies__c FROM Account WHERE Id =: acctId];
    		PartnerLevelConfig__c plcRec = [SELECT Id,Number_Of_Competencies__c,Check_For_Certification_Counts__c,Type__c,
                                            RCSA_RCSP__c,RSS__c,RTSS__c,RCSP__c
                                         FROM PartnerLevelConfig__c WHERE Id=:plcList[0].Id];
			AccountTriggerHandler.checkForCertificationCounts(cAcc, plcRec, typeToPartnerLevelConfigMap,tdf.retriveCertSummary(childAccounts[0].Id));
		test.stopTest();
		
}
}