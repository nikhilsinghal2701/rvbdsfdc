public with sharing class CampignMemberFcrm {
	public static void updateLeadsForReassignment(Set<Id> leadIds){
		list<id> LeadList = new list<id>();
		list<id> NoResponseLeadList = new list<id>();
		map<id,CampaignMember> LeadCampignMem = new map<id,CampaignMember>();
		List<CampaignMember> CampignMemToUpdate = new List<CampaignMember>();
		
		for(id lid: leadIds){
			LeadList.add(lid);
			if(FCRM.FCR_SupportAPI.GetActiveResponses(LeadList).isEmpty()){// Find Response,if found then do nothing but if not found then Add to list
				
				NoResponseLeadList.add(lid);
			}
			LeadList.clear();
		}
		
		if(!NoResponseLeadList.isEmpty()){ // Check for campaign member where no response found for Lead
			system.debug('CMLIST1****'+NoResponseLeadList);
			for(CampaignMember Cmid : [Select id,leadid,HasResponded,FirstRespondedDate,
										Campaign.FCRM__FCR_Campaign_Sourced_By__c,FCRM__FCR_Response_Status__c
										from CampaignMember  Where HasResponded=true and  
										(Campaign.FCRM__FCR_Campaign_Sourced_By__c IN ('Marketing','Sales','Deal Reg','OPEX')) and
										(FCRM__FCR_Response_Status__c IN ('Resolved - No Action Required','Resolved - Already Engaged' )) and 
										LeadId in : NoResponseLeadList order by FirstRespondedDate Desc ]){
				
				LeadCampignMem.put(Cmid.id,Cmid);
			}
			
		}
		
		if(!LeadCampignMem.isEmpty()){ // Set Values for Campignemember
			system.debug('CMLIST2****'+LeadCampignMem);
			for(id Cmidd : LeadCampignMem.keyset()){
				if(LeadCampignMem.get(Cmidd).Campaign.FCRM__FCR_Campaign_Sourced_By__c == 'Marketing'){
					
				CampignMemToUpdate.add(new CampaignMember(Id =Cmidd,FCRM__FCR_Admin_Response_Control__c=1,FCRM__FCR_Response_Status__c='Open'
															,FCRM__FCR_QR__c=true,FCRM__FCR_QR_Date__c=Date.today() ));
					
				
				}	else {			
			
				CampignMemToUpdate.add(new CampaignMember(Id =Cmidd,FCRM__FCR_Admin_Response_Control__c=1,FCRM__FCR_Response_Status__c='Open'));
			
				}
			
			}
				system.debug('CMLISTuPdate****'+CampignMemToUpdate);
		}
		
		if(!CampignMemToUpdate.isEmpty()){ //update CampaignMember
			Update CampignMemToUpdate;
			}
		
	}

}