/**
 * Test class for SFDC_OutageMessageController.
 * By: prashant.singh@riverbed.com
 * date: 12/11/2012 
 */
@isTest
private class TestSFDC_OutageMessageController {

    static testMethod void SFDC_OutageMessageController() {
        Opportunity opp = DiscountApprovalTestHlp.createOpp();
        ApexPages.StandardController oppMsg = New ApexPages.StandardController(opp);        
        System.currentPageReference().getParameters().put('id',opp.id);
        SFDC_OutageMessageController oppCon=new SFDC_OutageMessageController (oppMsg );
        oppCon.onLoad();
        
        Quote__c qt = DiscountApprovalTestHlp.createQuote(opp.Id);
        ApexPages.StandardController qtMsg = New ApexPages.StandardController(qt);        
        System.currentPageReference().getParameters().put('id',qt.id);
        SFDC_OutageMessageController qtCon=new SFDC_OutageMessageController (qtMsg);
        qtCon.onLoad();
        qt.NSD_Override__c=userInfo.getUserId();
        update qt;
        ApexPages.StandardController qtMsgManual = New ApexPages.StandardController(qt);        
        System.currentPageReference().getParameters().put('id',qt.id);
        SFDC_OutageMessageController qtConManual=new SFDC_OutageMessageController (qtMsgManual);
        qtConManual.onLoad();
    }
}