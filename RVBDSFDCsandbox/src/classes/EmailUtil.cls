/**
* Static class to send email
* Modified by Jaya ( Perficient )
  Adding Method: getDistyContactByAccountId
  Modifing purpose: adding functionality to retrive primary contact of the accounts related disty
  Modifing version Name:  Rev:1
*/
public with sharing class EmailUtil {
    //Fetch cc email list
     final static string PROBATION = 'Probation';
     final static string GOODSTANDING = 'Good Standing';
     final static string DISTRIBUTOR = 'Distributor';
    public static String getPartnerCompetencyCCEmailAddresses(List<Id> accId) {
        String emailAddresses = '';
        Partner_Email_Notification__c csmEmail = Partner_Email_Notification__c.getValues('CSM'); // email notification cusotm setting for csm
        if(!csmEmail.Do_Not_Send_Emails__c){// Rev:1 if condation for csm
        	List<Account> accList = [SELECT Channel_Account_Manager__r.Email, Channel_Marketing_Manager__r.Email, Id,
            	                                            Do_not_Auto_Level__c FROM Account WHERE Id = : accId ];
        	for(Account acc:accList){
            	if(!acc.Do_not_Auto_Level__c){
                	emailAddresses += ';'+acc.Channel_Account_Manager__r.Email;// +';'+acc.Channel_Marketing_Manager__r.Email Rev:1 maraketing manager is no longer required
            	}
        	}// do not need marketing manager control account manager using custom settings
        }// end of if
        return emailAddresses;
    }

        /**
        * Email send out method
        *
        */
        /*commented by prashant.singh@riverbed.com on 2/3/2014 to overcome send email limit exception
        public static void sendEmailNotification(String emailTemplateDevName,
                        Map<Id, List<Contact>> accCntMap,
                        Map<Id, Account_Authorizations__c> accAuthMap,
                        String ccEmails) {
                        String EmailFailure = null;
                        List<Messaging.SendEmailResult> mailResult=new List<Messaging.SendEmailResult>{};
                        List<String> toEmailAddress = ccEmails.split(';');
                                        List<String> emails = new List<String>();
                                        for( String emailStr: toEmailAddress){
                                                if( String.isNotEmpty(emailStr) && emailStr.contains('@')){
                                                                emails.add(emailStr);
                                                }
                                        }

                        if( String.isNotEmpty(emailTemplateDevName)) {
                                EmailTemplate templt = [SELECT DeveloperName,Id FROM EmailTemplate WHERE DeveloperName = : emailTemplateDevName limit 1];
                                Account_Authorizations__c accAuth;

                                        for(Id accId: accCntMap.keySet()){
                                                if(accAuthMap != null && !accAuthMap.isEmpty()) {
                                                        accAuth = accAuthMap.get(accId);
                                                }
                                                for(Contact cnt: accCntMap.get(accId)){
                                                        Messaging.SingleEmailmessage message = new Messaging.SingleEmailMessage();
                                                                        message.setTargetObjectId(cnt.Id);
                                                                        if( !emails.isEmpty()){
                                                                                message.setCcAddresses(emails);
                                                                        }
                                                                        message.setTemplateId(templt.Id);
                                                                        if( accAuth != null){
                                                                                message.setWhatId(accAuth.Id);
                                                                        }
                                                                        //***Skumar 12/08/13
                                                                        for(OrgWideEmailAddress owa : [select id, Address, DisplayName from OrgWideEmailAddress
                                                                                                                                    WHERE Address = 'partners@riverbed.com'])
                                                                        {
                                                                                if(owa.Address.contains('partners@riverbed.com'))
                                                                                message.setOrgWideEmailAddressId(owa.id);
                                                                        }
                                                                        //***
                                                                        message.setSaveAsActivity(false);
                                                                        try{
                                                                                mailResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { message });
                                                                                if(!mailResult[0].isSuccess()) {
                                                                                        EmailFailure = 'Email Failed: ' + mailResult[0].getErrors()[0].getMessage();
                                                                                }
                                                                        }catch(System.EmailException e){
                                                                                system.debug(LoggingLevel.ERROR,EmailFailure+':'+e);
                                                                        }
                                                }
                                }
                        }

                }*/
        /*
            Rev:1 modified emailnotification method to send emails to disty primary contact too.
        */
        public static void sendEmailNotification(String emailTemplateDevName,Map<Id, List<Contact>> accCntMap,
                                                Map<Id, Account_Authorizations__c> accAuthMap,String ccEmails){            
            Messaging.SingleEmailmessage message;
            List<Messaging.SingleEmailmessage> messages=new List<Messaging.SingleEmailMessage>();
            System.debug('ccEmails:'+ccEmails);
            List<String> toEmailAddress = ccEmails.split(';');
            List<String> emails = new List<String>();
            for( String emailStr: toEmailAddress){
                if( String.isNotEmpty(emailStr) && emailStr.contains('@')){
                        emails.add(emailStr);
                }
            }
            if( String.isNotEmpty(emailTemplateDevName)) {
                OrgWideEmailAddress owa = [select id, Address, DisplayName from OrgWideEmailAddress WHERE Address = 'partners@riverbed.com' limit 1];
                EmailTemplate templt = [SELECT DeveloperName,Id FROM EmailTemplate WHERE DeveloperName = : emailTemplateDevName limit 1];
                Account_Authorizations__c accAuth;    
                System.debug('accCntMap:'+accCntMap);
                for(Id accId: accCntMap.keySet()){
                    if(accAuthMap != null && !accAuthMap.isEmpty()) {
                        accAuth = accAuthMap.get(accId);
                    }
                    System.debug('accCntMap.get(accId):'+accCntMap.get(accId));
                    for(Contact cnt: accCntMap.get(accId)){
                    	System.debug('cnt:'+cnt);
                        message = new Messaging.SingleEmailMessage();
                        message.setTargetObjectId(cnt.Id);
                        if( !emails.isEmpty()){
                            message.setCcAddresses(emails);
                        }                                    
                        message.setTemplateId(templt.Id);
                        if( accAuth != null){
                            message.setWhatId(accAuth.Id);
                        }                        
                        message.setOrgWideEmailAddressId(owa.id);
                        message.setSaveAsActivity(false);
                        messages.add(message);          
                        System.debug('messages:'+messages);              
                    }
                } 
            }
            if(messages.size()>0){
                sendMessages(messages);
            }
        }
                
                
        //actually send all email notifications of certification achieved or lost.
        public static void sendMessages(List<Messaging.SingleEmailmessage> allMessages){
            String EmailFailure = null;
            List<Messaging.SendEmailResult> mailResult=new List<Messaging.SendEmailResult>{};
            try{
                if(allMessages.size()>0){
                    mailResult = Messaging.sendEmail(allMessages);
                    if(!mailResult[0].isSuccess()) {
                        EmailFailure = 'Email Failed: ' + mailResult[0].getErrors()[0].getMessage();
                    }
                }
            }catch(System.EmailException e){
                system.debug(LoggingLevel.ERROR,EmailFailure+':'+e);
            }   
        }

        /**
        * Get map of account Id with list of primary contact
        */
        public static Map<Id, List<Contact>> getContactListByAccountId(List<Id> accIds) {
        		// Rev:1 using cusotm settings to control the email sending process 
        		Partner_Email_Notification__c partnerEmail = Partner_Email_Notification__c.getValues('Partners');
				Partner_Email_Notification__c distiEmail = Partner_Email_Notification__c.getValues('Distributor');
				// Rev:1 
                Map<Id, List<Contact>> contactMap = new Map<Id, List<Contact>>();
                if(!partnerEmail.Do_Not_Send_Emails__c){// Rev:1 custom settings flag check
	                List<Contact> cntList = [SELECT Name, AccountId,Account.Do_not_Auto_Level__c,Account.Partner_Status1__c,Account.type,Id FROM Contact
	                                                                WHERE AccountId = : accIds
	                                                                AND Primary_Contact__c = true
	                                                                AND LeftCompany__c = false];
	                for(Id i: accIds){
	                        List<Contact> newList = new List<Contact>();
	                        for( Contact cnt: cntList) {
	                                if( cnt.AccountId == i && !cnt.Account.Do_not_Auto_Level__c && (cnt.Account.Partner_Status1__c=='Good Standing'||cnt.Account.Partner_Status1__c=='Probation') && cnt.Account.Type!='Distributor') {
	                                        newList.add(cnt);
	                                }
	                        }
	                        contactMap.put(i, newList);
	                }
	                System.debug('contactMap:'+contactMap);
                }
                //Rev:1 for every account retrive the primary contact on the related disty/reseller account and merge them with the contactMap above
                //Map<Id,List<Contact>> partnerAccToDistyContact = new Map<Id,List<Contact>>();
                // Rev:1 adding custom settings for disti email control
                if(!distiEmail.Do_Not_Send_Emails__c){
	                Map<Id,Id> accToDistyMap = new Map<Id,Id>();
	                for(Distributor_Reseller__c dr: [SELECT Id, Name,Account__c,Distributor_Reseller_Name__c,Primary_Distributor__c 
	                                                    FROM  Distributor_Reseller__c WHERE Primary_Distributor__c = true AND Account__c IN: accIds])
	                {
	                    accToDistyMap.put(dr.Distributor_Reseller_Name__c,dr.Account__c);
	                }
	                // disty account for all the partner accounts[accIds]
	                String query = 'SELECT Id,(SELECT Id, Email, Primary_Contact__c,LeftCompany__c,AccountId FROM Contacts)' 
	                                +'FROM Account WHERE Do_not_Auto_Level__c = FALSE AND Type !=: DISTRIBUTOR' 
	                                +'AND(Partner_Status1__c = GOODSTANDING || Partner_Status1__c = PROBATION)'
	                                +'Id IN: accToDistyMap.keySet()';
	                for(Account acct: [SELECT Id,(SELECT Id, Email, Primary_Contact__c,LeftCompany__c,AccountId FROM Contacts) 
	                                FROM Account WHERE Do_not_Auto_Level__c = FALSE AND Type !=: DISTRIBUTOR 
	                                AND(Partner_Status1__c= :GOODSTANDING OR Partner_Status1__c= :PROBATION)
	                                AND Id IN: accToDistyMap.keySet()])
	                {
	                    for(Contact c: acct.Contacts)
	                    {
	                        System.debug('in primary c:'+c);
	                        List<Contact> contactList = new List<Contact>();
	                        if(c.Primary_Contact__c && accToDistyMap.containsKey(c.AccountId) && !c.LeftCompany__c)
	                        {
	                            Id partnerAcctId = accToDistyMap.get(c.AccountId);
	                            if(contactMap.containsKey(partnerAcctId))
	                            {
	                                contactList = contactMap.get(partnerAcctId);
	                            }
	                            contactList.add(c);
	                            contactMap.put(partnerAcctId,contactList);
	                        }
	                    }
	                }// End Of Rev:1
	                System.debug('contactMap:'+contactMap);
                }
                return contactMap;
        }
}