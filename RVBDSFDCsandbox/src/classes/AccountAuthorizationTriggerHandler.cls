/*
  Class Name : AccountAuthorizationTriggerHandler
  Modified by Jaya ( Perficient )
  Modifing Method: updateAccAuthorizationSpecializationFieldOnAuthDelete
  Modifing purpose: adding functionality to update Authorizations/Specializations and Allowed Product Families fields on account
            when a account authoriziation record in deleted.
  Modifing version Name:  Rev:1
  Note: Trigger handlers should always be on without sharing. added on Rev:1 version 
*/
public without sharing class AccountAuthorizationTriggerHandler {

    final static string IN_PROBATION = 'Probation';
    final static string ACTIVE = 'Active';
    final static string INACTIVE = 'Inactive';
    final static string RASA = 'RASA';
    final static string RASP = 'RASP';
    final static string AUTH_MASTER_TYPE_VOLUME = 'Volume';
    final static string FEDERAL = 'Federal';
    final static string CLOUD = 'Cloud';
    final static string Regular = 'Regular';
  public static Boolean EMAILAUTHOR = false;
  public static Integer emailCounter = 0;

    public static void autoPopulateAuthMasterFieldsOnAccAuthorizations( List<Account_Authorizations__c> accAuthsInserted ){
        Set<Id> authMasterIds = new Set<Id>();
        for( Account_Authorizations__c accAuth : accAuthsInserted ){
            if( accAuth.Authorization_Master__c != null ){
                authMasterIds.add( accAuth.Authorization_Master__c );
            }
        }
        system.debug( authMasterIds );
        if( !authMasterIds.isEmpty() ){
            Map<Id, Authorizations_Master__c> authMastersMap = new Map<Id, Authorizations_Master__c>( [ SELECT Id, Name, Type__c
                                                                                                        FROM Authorizations_Master__c
                                                                                                        WHERE Id IN :authMasterIds ] );
            if( !authMastersMap.isEmpty() ){
                for( Account_Authorizations__c accAuth : accAuthsInserted ){
                    if( accAuth.Authorization_Master__c != null ){
                        accAuth.Type__c = authMastersMap.get( accAuth.Authorization_Master__c ).Type__c;
                  }
                }
            }
        }
    }

   
    public static void sendEmailNotification(List<Account_Authorizations__c>  newAccAuthList,
        List<Account_Authorizations__c>  oldAccAuthList,String action) {
       // if(Label.Do_Not_Send_Email_To_Partners.equalsIgnoreCase('False') || test.isRunningTest()){
          List<Account_Authorizations__c> tmpList = new  List<Account_Authorizations__c>();
          if(action == 'new' && !CertificationSummaryTriggerHandler.EMAILAUTHOR) {
              //CertificationSummaryTriggerHandler.EMAILAUTHOR = true;
                emailCounter += 1;
              System.debug('emailCounter new:'+emailCounter);
              sendCompetencyNotificationEmail(newAccAuthList);
          } else {
              for(Account_Authorizations__c newAuth: newAccAuthList){
                  for(Account_Authorizations__c oldAuth: oldAccAuthList){
                      if(newAuth.Id == oldAuth.Id && newAuth.Competency_Probation_Status_Remain_Days__c != 0 
                          && (newAuth.Competency_Probation_Status_Remain_Days__c != oldAuth.Competency_Probation_Status_Remain_Days__c ||
                            (newAuth.Status__c == ACTIVE) && oldAuth.Status__c != newAuth.Status__c)) {
                          tmpList.add(newAuth);
                      }
                  }
              }
              if(!CertificationSummaryTriggerHandler.EMAILAUTHOR){
                sendCompetencyNotificationEmail(tmpList);
                //CertificationSummaryTriggerHandler.EMAILAUTHOR = true;
                emailCounter += 1;
                System.debug('emailCounter:'+emailCounter);
              } 
          }
        //}
    }
    /**
    * Method to send out notification email when the competency status is changed
    * @param list of Account Authorization
    */
    public static void sendCompetencyNotificationEmail(List<Account_Authorizations__c>  accAuthList){
        List<Id> activeAccIds = new List<Id>();
        List<Id> probationAccIds = new List<Id>();
        List<Id> inactiveAccIds = new List<Id>();
        Map<Id, Account_Authorizations__c> activeMap = new Map<Id, Account_Authorizations__c>();
        Map<Id, Account_Authorizations__c> probationMap = new Map<Id, Account_Authorizations__c>();
        Map<Id, Account_Authorizations__c> inactiveMap = new Map<Id, Account_Authorizations__c>();
    System.debug('accAuthList:'+accAuthList);
        for(Account_Authorizations__c accAuth: accAuthList) {
            if( accAuth.Status__c == ACTIVE && accAuth.Type__c == 'Competency'){
                //activeAccIds.add(accAuth.Account__c);
                activeMap.put(accAuth.Account__c, accAuth);
            }

            if(accAuth.Status__c == IN_PROBATION && accAuth.Competency_Probation_Date__c != null &&
            accAuth.Type__c == 'Competency'){
                //probationAccIds.add(accAuth.Account__c);
                probationMap.put(accAuth.Account__c, accAuth);
            }

            if(accAuth.Status__c == INACTIVE && accAuth.Type__c == 'Competency'){
                //inactiveAccIds.add(accAuth.Account__c);
                inactiveMap.put(accAuth.Account__c, accAuth);
            }
        }
        System.debug('activeMap:'+activeMap);
        List<Id> ids = new List<Id>();
        if( activeMap != null && !activeMap.isEmpty()){
            ids.addAll(activeMap.keySet());
            Map<Id, List<Contact>> accCntMap = EmailUtil.getContactListByAccountId(ids);
            String ccEmailAddresses = EmailUtil.getPartnerCompetencyCCEmailAddresses(ids);
            if(!accCntMap.isEmpty()){
        EmailUtil.sendEmailNotification('Riverbed_Partner_Competency_Achieved', accCntMap, activeMap, ccEmailAddresses);
            }
        }
    System.debug('probationMap:'+probationMap);
        if( probationMap != null && !probationMap.isEmpty()){
            ids.addAll(probationMap.keySet());
            Map<Id, List<Contact>> accCntMap = EmailUtil.getContactListByAccountId(ids);
            System.debug('accCntMap:'+accCntMap);
            String ccEmailAddresses = EmailUtil.getPartnerCompetencyCCEmailAddresses(ids);
            if(!accCntMap.isEmpty()){
        EmailUtil.sendEmailNotification('Partner_Competency_in_probation', accCntMap, probationMap, ccEmailAddresses);
            }
        }
    System.debug('inactiveMap:'+inactiveMap);
        if( inactiveMap != null && !inactiveMap.isEmpty()){
            ids.addAll(inactiveMap.keySet());
            Map<Id, List<Contact>> accCntMap = EmailUtil.getContactListByAccountId(ids);
            String ccEmailAddresses = EmailUtil.getPartnerCompetencyCCEmailAddresses(ids);
            if(!accCntMap.isEmpty()){
        EmailUtil.sendEmailNotification('Partner_Competency_Removal', accCntMap, inactiveMap, ccEmailAddresses);
            }
        }
    }

    //Deciding synchronous or asynchronous call of methods
    public static void updateAuthorizationSpecializationFieldOnAccount(List<Account_Authorizations__c> accAuthorizations){
        set<Id> accAuthIds=new set<Id>();
        Set<Id> masterIds = new Set<Id>();
        set<Id>accIds=new set<Id>();
        for(Account_Authorizations__c aAuth:accAuthorizations){
          accAuthIds.add(aAuth.Id);
          accIds.add(aAuth.Account__c);
          masterIds.add( aAuth.Authorization_Master__c );
        }
        if(!system.isFuture() && !system.isBatch() && !accAuthIds.isEmpty() && accAuthIds.size()>0){
          asyncUpdateAuthorizationSpecializationFieldOnAccount(accAuthIds,masterIds,accIds);
        }else{
            syncUpdateAuthorizationSpecializationFieldOnAccount(accAuthorizations,masterIds,accIds);
        }
    }

    /*
    This helper method updates the account field, Authorizations_Specializations__c when a Certification
    Summary's field TAP_Requirement_Satisfied__c equals 'yes.' The account field is a multi-select
    picklist. This update ignores the update if the TAP type is 'General.'
    When the Account authorization status is Inactive delete them.
    Synchronous call.
    */
    public static void syncUpdateAuthorizationSpecializationFieldOnAccount( List<Account_Authorizations__c> accAuthorizations,set<Id> masterIds,set<Id> accIds){
        //set<Id> masterIds = new set<Id>();
        set<Id> accountAuthDelete=new set<Id>();
        string authSpe,allowedProductFamily;
        //set<Id>accIds=new set<Id>();
        List<Account> updateAccountList=new List<Account>();
        Account updAccount;
        List<Account_Authorizations__c> authList = new List<Account_Authorizations__c>();
        List<Product_Family_Authorizations__c> pfaList = new List<Product_Family_Authorizations__c>();
        Map<Id,List<Account_Authorizations__c>> accountAuthorizationsMap=new Map<Id,List<Account_Authorizations__c>>();
        Map<Id,List<Product_Family_Authorizations__c>> productFamilyAuthorizationMap=new Map<Id,List<Product_Family_Authorizations__c>>();
        Account acct;
        String qual='';
        //string allowedProductFamily = '';
        string val = '';
        Map<Id, Account> acctsMap = new Map<Id ,Account>();
        List<Account_Authorizations__c> accAuthsToDelete = new List<Account_Authorizations__c>();
              List<ID> objIDs=new List<ID>(); 
        
        if(accIds!=null && !accIds.isEmpty()){
          for( Account_Authorizations__c aAuth : [SELECT a.id,a.Authorization_Master__c, a.Authorization_Master__r.Name, a.Account__r.Partner_Level__c,a.Account__c,
                                                              a.Account__r.Authorizations_Specializations__c, a.Account__r.Type,Status__c,a.Account__r.TAPs__c
                                                              FROM Account_Authorizations__c a WHERE a.Account__c IN :accIds] ){
                    if(aAuth.Status__c == INACTIVE){
              accAuthsToDelete.add(aAuth);
            }else if(aAuth.Status__c!=INACTIVE){              
              if( !accountAuthorizationsMap.containsKey( aAuth.Account__c) ){
                          accountAuthorizationsMap.put( aAuth.Account__c, new List<Account_Authorizations__c> { aAuth } );
                      }else{
                          accountAuthorizationsMap.get( aAuth.Account__c ).add( aAuth );
                      }
                      masterIds.add(aAuth.Authorization_Master__c);
                    }                    
                }//end of for
                for(Product_Family_Authorizations__c pfa:[SELECT Product_Family__c,Authorization_Master__c
                            FROM Product_Family_Authorizations__c where Authorization_Master__c IN:masterIds]){
                    if( !productFamilyAuthorizationMap.containsKey( pfa.Authorization_Master__c) ){
                        productFamilyAuthorizationMap.put( pfa.Authorization_Master__c, new List<Product_Family_Authorizations__c> { pfa } );
                    }else{
                        productFamilyAuthorizationMap.get( pfa.Authorization_Master__c ).add( pfa );
                    }
                }
        }
        system.debug(LoggingLevel.Error,'accountAuthorizationsMap:'+accountAuthorizationsMap);
        if(accAuthsToDelete.size()>0){
                        for(Account_Authorizations__c accAuth:accAuthsToDelete){
                            //accountAuthDelete.add(accAuth.Id);
                            objIDs.add(accAuth.Id);
                        }
                        if(objIDs.size()>0){
                            B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs);
                        }
                    }
        if(accountAuthorizationsMap.size()>0){
          for(Id accAuth:accountAuthorizationsMap.keySet()){
            if(accountAuthorizationsMap.containsKey(accAuth)){
              authSpe=getAuthorizationSpecialization(accountAuthorizationsMap.get(accAuth));              
              allowedProductFamily=getAllowedProductFamily(accountAuthorizationsMap.get(accAuth),productFamilyAuthorizationMap);
              updAccount=new Account(Id=accAuth,Authorizations_Specializations__c=authSpe,Allowed_Product_Families__c=allowedProductFamily,TAPs__c=authSpe);
              updateAccountList.add(updAccount);
            }
          }
        }        
        system.savepoint sp;
        try{
          sp = Database.setSavepoint();
          if( !accAuthsToDelete.isEmpty() ){
            delete accAuthsToDelete;
          }          
          /*if( !acctsMap.isEmpty() ){
            system.debug('***Accounts Map:'+acctsMap);
            update acctsMap.values();
          }*/
          if(updateAccountList.size()>0)update updateAccountList;
        }catch( DmlException ex ){
          Database.rollback( sp );
        }
    }
    /*
    This helper method updates the account field, Authorizations_Specializations__c when a Certification
    Summary's field TAP_Requirement_Satisfied__c equals 'yes.' The account field is a multi-select
    picklist. This update ignores the update if the TAP type is 'General.'
    When the Account authorization status is Inactive delete them.
    Asynchronous call.
    */
    @future(callout=true)
    public static void asyncUpdateAuthorizationSpecializationFieldOnAccount( set<Id> accAuthorizationsIds,set<Id> masterIds,set<Id> accIds){        
        Account updAccount;
        Account acct;
        String qual='';
        //string allowedProductFamily = '';
        string authSpe,allowedProductFamily;
        string val = '';
        Map<Id, Account> acctsMap = new Map<Id ,Account>();
        List<Account_Authorizations__c> accAuthsToDelete = new List<Account_Authorizations__c>();
        set<Id> accountAuthDelete=new set<Id>();
        List<Account_Authorizations__c> authList = new List<Account_Authorizations__c>();
        List<Product_Family_Authorizations__c> pfaList = new List<Product_Family_Authorizations__c>();
        Map<Id,List<Account_Authorizations__c>> accountAuthorizationsMap=new Map<Id,List<Account_Authorizations__c>>();
        Map<Id,List<Product_Family_Authorizations__c>> productFamilyAuthorizationMap=new Map<Id,List<Product_Family_Authorizations__c>>();
        List<Account> updateAccountList=new List<Account>();
        List<ID> objIDs=new List<ID>();
        
        if(accIds!=null && !accIds.isEmpty()){
          for( Account_Authorizations__c aAuth : [SELECT a.id,a.Authorization_Master__c, a.Authorization_Master__r.Name, a.Account__r.Partner_Level__c,a.Account__c,
                                                              a.Account__r.Authorizations_Specializations__c, a.Account__r.Type,Status__c,a.Account__r.TAPs__c
                                                              FROM Account_Authorizations__c a WHERE a.Account__c IN :accIds] ){
                    if(aAuth.Status__c == INACTIVE){
              accAuthsToDelete.add(aAuth);
            }else if(aAuth.Status__c!=INACTIVE){              
              if( !accountAuthorizationsMap.containsKey( aAuth.Account__c) ){
                          accountAuthorizationsMap.put( aAuth.Account__c, new List<Account_Authorizations__c> { aAuth } );
                      }else{
                          accountAuthorizationsMap.get( aAuth.Account__c ).add( aAuth );
                      }
                      masterIds.add(aAuth.Authorization_Master__c);
                    }                    
                }//end of for
                for(Product_Family_Authorizations__c pfa:[SELECT Product_Family__c,Authorization_Master__c
                            FROM Product_Family_Authorizations__c where Authorization_Master__c IN:masterIds]){
                    if( !productFamilyAuthorizationMap.containsKey( pfa.Authorization_Master__c) ){
                        productFamilyAuthorizationMap.put( pfa.Authorization_Master__c, new List<Product_Family_Authorizations__c> { pfa } );
                    }else{
                        productFamilyAuthorizationMap.get( pfa.Authorization_Master__c ).add( pfa );
                    }
                }
        }
        system.debug(LoggingLevel.Error,'***accountAuthorizationsMap:'+accountAuthorizationsMap);        
        if(accAuthsToDelete.size()>0){
                    for(Account_Authorizations__c accAuth:accAuthsToDelete){
                        //accountAuthDelete.add(accAuth.Id);
                        objIDs.add(accAuth.Id);
                    }
                    if(objIDs.size()>0){
                        B2BIntegrationUtil.objectDeleteServiceForTIBCO(objIDs);
                    }
                }
        if(accountAuthorizationsMap.size()>0){
          for(Id accAuth:accountAuthorizationsMap.keySet()){
            if(accountAuthorizationsMap.containsKey(accAuth)){
              authSpe=getAuthorizationSpecialization(accountAuthorizationsMap.get(accAuth));
              allowedProductFamily=getAllowedProductFamily(accountAuthorizationsMap.get(accAuth),productFamilyAuthorizationMap);
              updAccount=new Account(Id=accAuth,Authorizations_Specializations__c=authSpe,Allowed_Product_Families__c=allowedProductFamily,TAPs__c=authSpe);
              updateAccountList.add(updAccount);
            }
          }
        }
        system.savepoint sp;
        try{
          sp = Database.setSavepoint();
          if(!accAuthsToDelete.isEmpty()){
            delete accAuthsToDelete;
          }
          /*if( !acctsMap.isEmpty() ){
            system.debug('***Accounts Map:'+acctsMap);
            update acctsMap.values();
          }*/
          if(updateAccountList.size()>0)update updateAccountList;
        }catch( DmlException ex ){
          Database.rollback( sp );
        }
    }


    public static void updateAccAuthorizationSpecializationFieldOnAuthUpdate( Map<Id, Account_Authorizations__c> oldAuthsMap, List<Account_Authorizations__c> newAuths ){
        Account_Authorizations__c oldAuth;
        List<Account_Authorizations__c> changedAuths = new List<Account_Authorizations__c>();
        for( Account_Authorizations__c auth : newAuths ){
            oldAuth = oldAuthsMap.get( auth.Id );
            if( oldAuth.Status__c != auth.Status__c && !auth.Manual_Override__c && auth.Status__c != IN_PROBATION ){
                changedAuths.add( auth );
            }
        }
        if( !changedAuths.isEmpty() ){
          updateAuthorizationSpecializationFieldOnAccount( changedAuths );
        }
    }

    public static void updateCompetencyProbationDateOnAuthStatusChange( Map<Id, Account_Authorizations__c> oldAuthsMap, List<Account_Authorizations__c> newAuths ){
        Account_Authorizations__c oldAuth;
        List<Account_Authorizations__c> changedAuths = new List<Account_Authorizations__c>();
        List<Id> authIds = new List<Id>();
        for( Account_Authorizations__c auth : newAuths ){
            oldAuth = oldAuthsMap.get( auth.Id );
            if( oldAuth.Status__c != auth.Status__c && !auth.Manual_Override__c && auth.Status__c == IN_PROBATION ){
                authIds.add(auth.Id);
            }
        }
        List<Account_Authorizations__c> authorizationsInProbation = new  List<Account_Authorizations__c>();
        if( !authIds.isEmpty() ){
            authorizationsInProbation = updateCompetencyProbationDate( authIds );
        }
        for( Account_Authorizations__c newAuth : newAuths ) {
            for( Account_Authorizations__c auth : authorizationsInProbation ) {
                if( auth.Id == newAuth.Id ) {
                    newAuth.Competency_Probation_Date__c = auth.Competency_Probation_Date__c;
                }
            }
        }
    }


    /**
    * helper method to get all the partner level configuratiosn for each type
    *
    *
    * @return the map of partner level config custom setting
    */
    public static Map<String, List<PartnerLevelConfig__c>> getPartnerLevelConfigs(){
        Map<String, List<PartnerLevelConfig__c>> typeToPartnerLevelConfigMap = new Map<String, List<PartnerLevelConfig__c>>();
        for( PartnerLevelConfig__c config : [ SELECT Automate_Promotion__c, Automate_Demotion__c, Id, IsDeleted, Partner_Level__c,
                                                    Number_Of_Competencies__c, Probation_Period_days__c, Type__c,
                                                    Check_For_Certification_Counts__c, Is_ProductLine__c, RCSA_RCSP__c,
                                                    RCSP__c, RSS__c, RTSS__c, RSA__c, Priority__c
                                              FROM PartnerLevelConfig__c
                                              WHERE IsDeleted = false
                                              ORDER BY Priority__c ASC ] ){
            if( !typeToPartnerLevelConfigMap.containsKey( config.Type__c) ){
                typeToPartnerLevelConfigMap.put( config.Type__c, new List<PartnerLevelConfig__c> { config } );
            }else{
                typeToPartnerLevelConfigMap.get( config.Type__c ).add( config );
            }
        }
        return typeToPartnerLevelConfigMap;
    }

    public static List<Account_Authorizations__c> updateCompetencyProbationDate( List<Id> authIds  ){
        // if the account authorization is in probation, get the custom setting based on the account types and partner level
      List<Account_Authorizations__c> authorizationsInProbation = [ SELECT a.id, a.Authorization_Master__r.Name, a.Account__r.Partner_Level__c,a.Competency_Probation_Date__c,
                                                                    a.Status__c,a.Account__r.Authorizations_Specializations__c, Account__r.Type
                                                                    FROM Account_Authorizations__c a
                                                                    WHERE Id IN :authIds ] ;
        PartnerLevelConfig__c plc;
        Map<String, List<PartnerLevelConfig__c>> configs = getPartnerLevelConfigs();
        for( Account_Authorizations__c accAuth : authorizationsInProbation ){
            if( accAuth.Account__r.Authorizations_Specializations__c!=null && accAuth.Account__r.Authorizations_Specializations__c.contains( FEDERAL ) ){
                for( PartnerLevelConfig__c config : configs.get( FEDERAL )){
                    if( accAuth.Account__r.Partner_Level__c == config.Partner_Level__c ){
                        System.debug(LoggingLevel.INFO,'** FEDERAL :'+config.Partner_Level__c);
                        plc = config;
                    }
                }
            }else if( accAuth.Account__r.Type != null && accAuth.Account__r.Type.contains( CLOUD ) ){
                for( PartnerLevelConfig__c config : configs.get( CLOUD )){
                    if( accAuth.Account__r.Partner_Level__c == config.Partner_Level__c ){
                        System.debug(LoggingLevel.INFO,'** CLOUD :'+config.Partner_Level__c);
                        plc = config;
                    }
                }
            }else{                
                for( PartnerLevelConfig__c config : configs.get( REGULAR )){
                    System.debug('partnerlevel---'+config.Partner_Level__c+'--'+accAuth.Account__r.Partner_Level__c );
                    if( accAuth.Account__r.Partner_Level__c == config.Partner_Level__c ){
                        System.debug(LoggingLevel.INFO,'** REGULAR :'+config.Partner_Level__c);
                        plc = config;
                    }
                }
            }
            if( plc != null && accAuth.Status__c==ACTIVE){
                if(  plc.Probation_Period_days__c == 0) {
                    accAuth.Competency_Probation_Date__c = Date.today();
                } else {
                    accAuth.Competency_Probation_Date__c = Date.today().addDays( Integer.valueof( plc.Probation_Period_days__c ) );
                }
            }
        }
        return authorizationsInProbation;
    }
    /*
    This method is used for updating the Authorization Specialization and Allowed product family fields upon
    Account Authorization record deletion.
    Date:11/16/2013
    By:psingh@riverbed.com
    */
    public static void updateAccAuthorizationSpecializationFieldOnAuthDelete(Map<Id, Account_Authorizations__c> oldAuthsMap, List<Account_Authorizations__c> newAuths){
        string authSpe,allowedProductFamily;
        set<Id>accIds=new set<Id>();
        Set<Id> masterIds = new Set<Id>();
        List<Account> updateAccountList=new List<Account>();
        Account updAccount;
        List<Account_Authorizations__c> authList = new List<Account_Authorizations__c>();
        List<Product_Family_Authorizations__c> pfaList = new List<Product_Family_Authorizations__c>();
        Map<Id,List<Account_Authorizations__c>> accountAuthorizationsMap=new Map<Id,List<Account_Authorizations__c>>();
        Map<Id,List<Product_Family_Authorizations__c>> productFamilyAuthorizationMap=new Map<Id,List<Product_Family_Authorizations__c>>();
        
        // List of Account Authorizations records under the same account as that of the delete record.
        Set<Id> existingAuth = new Set<Id>();
        Set<Id> deleteAuth = new Set<Id>();
        for(Account_Authorizations__c accAuth:oldAuthsMap.values()){
            accIds.add(accAuth.Account__c);
        }
        system.debug('***AccountId:' +accIds);
        if(accIds!=null && !accIds.isEmpty()){
            for( Account_Authorizations__c aAuth : [SELECT a.id,a.Authorization_Master__c, a.Authorization_Master__r.Name, a.Account__r.Partner_Level__c,a.Account__c,
                                                            a.Account__r.Authorizations_Specializations__c, a.Account__r.Type,Status__c,a.Account__r.TAPs__c
                                                            FROM Account_Authorizations__c a WHERE a.Account__c IN :accIds] ){
                if(!oldAuthsMap.containsKey(aAuth.Id)){
                  existingAuth.add(aAuth.Id);
                }else{
                  deleteAuth.add(aAuth.Id);
                }
                
            }//end of for
            
            /*Rev:1 calling async method for sending data to Tibco and deleting records.*/
                 if(system.isFuture()||system.isBatch()){
                     syncDeleteRecords(existingAuth, deleteAuth, accIds);
                 }else{
                     asyncDeleteRecords(existingAuth, deleteAuth, accIds);
                 }
           }
    }
    /* Rev:1 method to call for B2BIntegrationUtil.accountAuthDeleteService. this sends the delete record to tibco and deletes the record.
      getAuthorizationSpecialization and getAllowedProductFamily considers only those records that are not inactive. 
      as there is a possablity of deleting records without changing the status to Inactive this method was added.
    */
    
  @future(callout=true)
  public static void asyncDeleteRecords(Set<Id> existingAuth, Set<Id> deleteAuth, Set<Id> aAuthAcctIds)
  {
    Map<Id,List<Account_Authorizations__c>> accountAuthorizationsMap=new Map<Id,List<Account_Authorizations__c>>();
    List<Account_Authorizations__c> existingAuthRec = new List<Account_Authorizations__c>();
    List<Account_Authorizations__c> allAccAuthRecs = new List<Account_Authorizations__c>();
    Map<Id,List<Product_Family_Authorizations__c>> productFamilyAuthorizationMap=new Map<Id,List<Product_Family_Authorizations__c>>();
    Set<Id> masterIds = new Set<Id>();
    Set<Account> updateAccountSet=new Set<Account>();
    Set<Account_Authorizations__c> accAuthsToDelete = new Set<Account_Authorizations__c>();
    String authSpe,allowedProductFamily;
    
    if(!deleteAuth.isEmpty())
    {
      //B2BIntegrationUtil.accountAuthDeleteService(deleteAuth);
    }
    // to update the Authorization and allowed product families field on related Account record.
    if(aAuthAcctIds.size()>0)
    {
      for(Account_Authorizations__c aAuth:[SELECT Id,Authorization_Master__c, Authorization_Master__r.Name, 
                          Account__r.Partner_Level__c,Account__c,
                                                    Account__r.Authorizations_Specializations__c, 
                                                    Account__r.Type,Status__c,Account__r.TAPs__c
                                                    FROM Account_Authorizations__c 
                                                    WHERE Id IN: existingAuth OR Id IN: deleteAuth ALL ROWS])
             {
               List<Account_Authorizations__c> aAuthList = new List<Account_Authorizations__c>();
        if(accountAuthorizationsMap.containsKey(aAuth.Account__c))
        {
          aAuthList = accountAuthorizationsMap.get(aAuth.Account__c);
        }
        aAuthList.add(aAuth);
        accountAuthorizationsMap.put(aAuth.Account__c,aAuthList);
        if(existingAuth.contains(aAuth.Id))
        {
          existingAuthRec.add(aAuth);
          masterIds.add(aAuth.Authorization_Master__c);
        }
        else if(deleteAuth.contains(aAuth.Id))
        {
          aAuth.Status__c = INACTIVE;
          accAuthsToDelete.add(aAuth);
        }
        allAccAuthRecs.add(aAuth);
      }
      /* retrives product family Authorizations*/
      for(Product_Family_Authorizations__c pfa:[SELECT Product_Family__c,Authorization_Master__c
                                        FROM Product_Family_Authorizations__c 
                                        WHERE Authorization_Master__c IN:masterIds]){
                if( !productFamilyAuthorizationMap.containsKey( pfa.Authorization_Master__c) ){
                    productFamilyAuthorizationMap.put( pfa.Authorization_Master__c, new List<Product_Family_Authorizations__c> { pfa } );
                }else{
                    productFamilyAuthorizationMap.get( pfa.Authorization_Master__c ).add( pfa );
                }
            }
            for(Account_Authorizations__c aAuth: allAccAuthRecs)
      {  Account updAccount;
        if(accountAuthorizationsMap.containsKey(aAuth.Account__c)){
          authSpe=getAuthorizationSpecialization(accountAuthorizationsMap.get(aAuth.Account__c));
          allowedProductFamily=getAllowedProductFamily(accountAuthorizationsMap.get(aAuth.Account__c),productFamilyAuthorizationMap);
          updAccount=new Account(Id=aAuth.Account__c,Authorizations_Specializations__c=authSpe,Allowed_Product_Families__c=allowedProductFamily,TAPs__c=authSpe);
          updateAccountSet.add(updAccount);
        }
      }
      
    }
    system.savepoint sp;
    try{
      sp = Database.setSavepoint();
      if(updateAccountSet.size()>0)
      {List<Account> updateAccountList = new List<Account>(updateAccountSet);
        update updateAccountList;
      }
    }catch( DmlException ex ){
      Database.rollback( sp );
    }
    
  }
  
  public static void syncDeleteRecords(Set<Id> existingAuth, Set<Id> deleteAuth, Set<Id> aAuthAcctIds)
  {
    Map<Id,List<Account_Authorizations__c>> accountAuthorizationsMap=new Map<Id,List<Account_Authorizations__c>>();
    List<Account_Authorizations__c> existingAuthRec = new List<Account_Authorizations__c>();
    List<Account_Authorizations__c> allAccAuthRecs = new List<Account_Authorizations__c>();
    Map<Id,List<Product_Family_Authorizations__c>> productFamilyAuthorizationMap=new Map<Id,List<Product_Family_Authorizations__c>>();
    Set<Id> masterIds = new Set<Id>();
    Set<Account> updateAccountSet=new Set<Account>();
    Set<Account_Authorizations__c> accAuthsToDelete = new Set<Account_Authorizations__c>();
    String authSpe,allowedProductFamily;
    
    if(!deleteAuth.isEmpty())
    {
      //B2BIntegrationUtil.accountAuthDeleteService(deleteAuth);
    }
    // to update the Authorization and allowed product families field on related Account record.
    if(aAuthAcctIds.size()>0)
    {
      for(Account_Authorizations__c aAuth:[SELECT Id,Authorization_Master__c, Authorization_Master__r.Name, 
                          Account__r.Partner_Level__c,Account__c,
                                                    Account__r.Authorizations_Specializations__c, 
                                                    Account__r.Type,Status__c,Account__r.TAPs__c
                                                    FROM Account_Authorizations__c 
                                                    WHERE Id IN: existingAuth OR Id IN: deleteAuth ALL ROWS])
             {
               List<Account_Authorizations__c> aAuthList = new List<Account_Authorizations__c>();
        if(accountAuthorizationsMap.containsKey(aAuth.Account__c))
        {
          aAuthList = accountAuthorizationsMap.get(aAuth.Account__c);
        }
        aAuthList.add(aAuth);
        accountAuthorizationsMap.put(aAuth.Account__c,aAuthList);
        if(existingAuth.contains(aAuth.Id))
        {
          existingAuthRec.add(aAuth);
          masterIds.add(aAuth.Authorization_Master__c);
        }
        else if(deleteAuth.contains(aAuth.Id))
        {
          aAuth.Status__c = INACTIVE;
          accAuthsToDelete.add(aAuth);
        }
        allAccAuthRecs.add(aAuth);
      }
      /* retrives product family Authorizations*/
      for(Product_Family_Authorizations__c pfa:[SELECT Product_Family__c,Authorization_Master__c
                                        FROM Product_Family_Authorizations__c 
                                        WHERE Authorization_Master__c IN:masterIds]){
                if( !productFamilyAuthorizationMap.containsKey( pfa.Authorization_Master__c) ){
                    productFamilyAuthorizationMap.put( pfa.Authorization_Master__c, new List<Product_Family_Authorizations__c> { pfa } );
                }else{
                    productFamilyAuthorizationMap.get( pfa.Authorization_Master__c ).add( pfa );
                }
            }
            for(Account_Authorizations__c aAuth: allAccAuthRecs)
      {  Account updAccount;
        if(accountAuthorizationsMap.containsKey(aAuth.Account__c)){
          authSpe=getAuthorizationSpecialization(accountAuthorizationsMap.get(aAuth.Account__c));
          allowedProductFamily=getAllowedProductFamily(accountAuthorizationsMap.get(aAuth.Account__c),productFamilyAuthorizationMap);
          updAccount=new Account(Id=aAuth.Account__c,Authorizations_Specializations__c=authSpe,Allowed_Product_Families__c=allowedProductFamily,TAPs__c=authSpe);
          updateAccountSet.add(updAccount);
        }
      }
      
    }
    system.savepoint sp;
    try{
      sp = Database.setSavepoint();
      if(updateAccountSet.size()>0)
      {List<Account> updateAccountList = new List<Account>(updateAccountSet);
        update updateAccountList;
      }
    }catch( DmlException ex ){
      Database.rollback( sp );
    }    
  }
    private static String getAuthorizationSpecialization(List<Account_Authorizations__c> accAuth){
        String finalStr='';
        for(Account_Authorizations__c temp:accAuth){
            if(temp.Status__c!=INACTIVE && !finalStr.contains(temp.Authorization_Master__r.Name))
                finalStr+=temp.Authorization_Master__r.Name+';';
        }
        if(finalStr=='')return finalStr;
        else if(finalStr.trim().endsWith(';')){
            return finalStr.trim().substring(0,finalStr.trim().length()-1);
        }else return finalStr.trim();
    }
    private static String getAllowedProductFamily(List<Account_Authorizations__c> accAuth,Map<Id,List<Product_Family_Authorizations__c>>productFamilyAuth){
        string finalAlloedProductFamily='';
        string str;
        for(Account_Authorizations__c tempAuth:accAuth){
            if(tempAuth.Status__c!=INACTIVE && productFamilyAuth.containsKey(tempAuth.Authorization_Master__c) ){
                for(Product_Family_Authorizations__c tempPFA:productFamilyAuth.get(tempAuth.Authorization_Master__c)){
                    str=String.valueOf(tempPFA.Product_Family__c);
                    str=str.substring(0,str.length()-3);
                    if(!finalAlloedProductFamily.contains(str))
                        finalAlloedProductFamily+=str+',';
                }
            }
        }
        if(finalAlloedProductFamily=='')return finalAlloedProductFamily;
        else{
            finalAlloedProductFamily=finalAlloedProductFamily.trim().substring(0, finalAlloedProductFamily.trim().length()-1);
            return finalAlloedProductFamily;
        }
    }
}