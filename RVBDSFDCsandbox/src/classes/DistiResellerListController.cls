/*
Modified by Jaya ( Perficient ) 
Modify Version: Rev:1
Purpose: for adding Sorting per column and export functionality to the results displayed with pagination
Modified Date: 7/2/2014
*/
public with sharing class DistiResellerListController {
	
	//Rev:1
	public final static String SORT_ASC = 'ASC';
    public final static String SORT_DESC = 'DESC';
    public final static Integer PAGESIZE = 1000; 
    public final static String EXPORT_FILE_AS_XLS = 'application/vnd.ms-excel#';
    public final static String EXPORT_FILE_AS_CSV = 'text/csv#';
    public final static String STANDARD_USER = 'Standard';
    
	public String sortString	{get;set;}
	public String exportFileType	{get;set;}
	public String prevSortString {get;set;}
    public SortUtil sortObj 	{get;set;}
    public String exportFileName {get;set;}
    public Boolean recordFlag	{get;set;} 
    
    public List<selectOption> getExportType() {
        List<selectOption> options = new List<selectOption>(); 
        options.add(new selectOption('application/vnd.ms-excel;.xls', 'Excel Format .xls')); 
        options.add(new selectOption('application/vnd.ms-excel;.csv', 'Comma Delimited .csv'));
        return options;
    }
    
	public String acctId;
    List<Distributor_Reseller__c> distiResellerList;
    private List<Id> tempList;
    public String queryStr;
    public List<Account> exportList {get;set;}
    public DistiResellerListController(){
    	tempList = new List<Id>();
    	acctId = '';
    	recordFlag = true;
    	sortObj = new SortUtil('Name',SORT_ASC);
        getExportType();
    }
    
    /*
    page action retrives related distibutor resellers of logged in user. 
    */
    public void init() 
    {
        exportFileName = System.Label.Disti_Export_File_Name;
        getExportType();
        if(UserInfo.getUserType().equals(STANDARD_USER))
        {
        	acctId = ApexPages.currentPage().getParameters().get('id');
        }
       	if(acctId ==  null  || acctId == ''){
        	acctId=[select Contact.accountId from User where id = :UserInfo.getUserId()].Contact.AccountId;
        }
        if(acctId != null && acctId != ''){
	        for(Distributor_Reseller__c temp:[SELECT Id, Account__c, Distributor_Reseller_Name__c 
	        					FROM Distributor_Reseller__c WHERE Account__c=:acctId limit 1000]){
	            tempList.add(temp.Distributor_Reseller_Name__c);
	        }
	        System.debug('tempList:'+tempList);
	        if(!tempList.isEmpty()){
	        	queryStr = getQuery();
	        }else{
	        	recordFlag = false;
        		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,DistiResellerList__c.getValues('disti').Message__c));
	        }
        }
        else{
        	recordFlag = false;
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,DistiResellerList__c.getValues('acctId').Message__c));
        }
    } 
    /*
    	builds query baed on the sort column. Invoked for every click on the pageblock table. 
    */
   public String getQuery()
   {
       String str = 'SELECT Id, Name, Partner_Level__c,Partner_Status1__c, Probation_Reason__c, '
    					+' BillingCountry, TAPs_formula__c, Partner_Classification__c ' 
                		+' FROM Account WHERE Id IN : tempList ';
        
        if(sortObj != null){
        	str += ' ORDER BY '+sortObj.sortString+' '+sortObj.sortDirection+' NULLS LAST ';
        }
        else{
        	str += ' ORDER BY Name NULLS LAST ';
        }
    	return str;   
   }
    public ApexPages.StandardSetController distiResAccountList{
    	get{
	    	 if(tempList.size()>0){
	            	if(distiResAccountList == null)
            		{	distiResAccountList = new ApexPages.StandardSetController(Database.getQueryLocator(getQuery()));
	            		distiResAccountList.setPageSize(PAGESIZE);
	            	}            	
	            }
	         return distiResAccountList;
    	}set;
    } 
    /*sortList getter*/
    public List<Account> getSortList()
    {
    	List<Account> acctList = new List<Account>();
    	if(tempList != null){
    		acctList = Database.query(getQuery());
    	}else{
    		recordFlag = false;
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,DistiResellerList__c.getValues('acctId').Message__c));
    	}
    	return acctList;
    }
  
    //Rev:1 export data as excel file
    public PageReference exportData()
    {
    	String exportContent = exportFileType.substring(0,exportFileType.indexOf(';'));
		String exporttype = exportFileType.substring(exportFileType.indexOf(';')+1,exportFileType.length());
    	exportFileName = exportContent+'#'+exportFileName+exporttype;
    	PageReference pref = exporttype.equalsIgnoreCase('.xls')?Page.DistiResellerExportList:exporttype.equalsIgnoreCase('.csv')?Page.DistiResellerExportCSV:Page.DistiResellerExportList;
    	exportList = Database.query(getQuery());
    	//pref.setRedirect(false);
    	return pref;
    } 
    // sort the result based on the column the user selected and in Asc/Desc   
    public void doSort()
    {
    	 if(sortObj != null && prevSortString != null && prevSortString != '' && prevSortString.equals(sortString)){
	    		sortObj.sortDirection = sortObj.sortDirection.equals(SORT_ASC) ? SORT_DESC : SORT_ASC;
    	}
    	else{
    		sortObj = new SortUtil(sortString,SORT_DESC);
    	}
    	exportList = Database.query(getQuery());
		prevSortString = sortObj.sortString;
    }
    
    public class SortUtil
    {
    	public String sortString {get;set;}
    	public String sortDirection {get;set;}
    	public SortUtil(String sortString, String sortDirection){
    		this.sortString = sortString;
    		this.sortDirection = sortDirection;
    	}
    }
    // End Of Rev:1
    //Test Method for DistiResellerListController Controller 
    /*static testMethod void testDistiResellerListController(){        
        DistiResellerListController temp;
		pref = temp.exportData();
		temp.sortString = 'Name';
		temp.doSort();
        User usr=[select Id,Name,usertype from user where name='Alex Hobson' and usertype='PowerPartner' and Isactive=true];
        system.runAs(usr){
            temp=new DistiResellerListController();
            temp.getDistiResellerList();
            temp.getDistiResAccountList();
        }
    }*/
}