public class CreateBlanket{
    
private ApexPages.StandardController controller {get; set;}
private Quote__c qt {get;set;}
private Opportunity op {get;set;}
private Discount_Schedule__c ds {get;set;}
private DiscountCategory__c dt {get;set;} 
public String recordId {get;set;}
public ID newRecordId {get;set;}

public CreateBlanket(ApexPages.StandardController controller){
       
       this.controller = controller;
        // load the current record
        qt = (Quote__c)controller.getRecord();
       
       if(recordId == null){
          recordId = ApexPages.currentPage().getParameters().get('id');
          }
       }
public PageReference autoBlanketCreate(){
      
      
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        DescribeSObjectResult result = gd.get('Discount_Schedule__c').getDescribe();
        Map<String,Schema.RecordTypeInfo>recordTypeInfo = result.getRecordTypeInfosByName();
        system.debug('RECORD TYPES:' + recordTypeInfo);
        String dsrecordTypeId = recordTypeInfo.get('Blanket').getRecordTypeId();
        
       Savepoint sp = Database.setSavepoint();
       //Discount_Schedule__c newds;
       List<Discount_Detail__c> ddList = new List<Discount_Detail__c>();
       Map<String,Discount_Detail__c>oppDdetailMap=new Map<String,Discount_Detail__c>();
       qt=[SELECT Blanket__c,opportunity__r.Primary_ISR__c, Deal_Desk_Status__c, Discount_Status__c, Discount_Schedule_Created__c, Name,Discount_Reason__c, Opportunity__c, PWS_Quote_Id__c,End_Date__c, Start_Date__c FROM Quote__c where id = :qt.id];   
       op=[SELECT AccountId,Account.name, Name, Sold_to_Partner__c, Tier2__c,Tier2__r.name, Tier3_Partner__c, Support_Provided__c,Primary_ISR__c FROM Opportunity where id = :qt.Opportunity__c];
       ddList=[SELECT Description__c,Opportunity__c, Name, IsOpp__c, Id, Full_Discount__c, Discount__c,Special_Discount__c,Quote__c,Uplift__c,Disti_Special_Discount__c,Category_Master__c,Category_Master__r.Name,Category_Master__r.Group__c
           //FROM Discount_Detail__c WHERE Opportunity__c =:qt.Opportunity__c and Quote__c=:qt.id and IsOpp__c=True];
           FROM Discount_Detail__c WHERE Opportunity__c =:qt.Opportunity__c];
       DiscountCategory__c dsTable= new DiscountCategory__c();
       
       dsTable.Name=op.Account.name+' '+ '-'+' '+op.Tier2__r.name;
       
       for(Discount_Detail__c dd:ddList){
           oppDdetailMap.put(dd.Category_Master__c,dd);
       }
       system.debug(LoggingLevel.INFO,'oppDdetailMap'+oppDdetailMap);
      try{
           insert dsTable;
           system.debug(LoggingLevel.INFO,'dsTable'+ dsTable);
           
           Discount_Schedule__c newds=new Discount_Schedule__c() ;
           
           newds.Active__c=True;
           newds.Name=op.Account.name+' '+ '-'+' '+op.Tier2__r.name;
           newds.Discount_Type__c='Blanket';
           newds.recordtypeid=dsrecordTypeId;
           newds.Discount_Category__c=dsTable.id;
           newds.quote__c=qt.id;
           newds.Reason_Text__c=qt.Discount_Reason__c;
           newds.Start_Date__c=qt.Start_Date__c;
           newds.End_Date__c=qt.End_Date__c;
           newds.ApplyUplift__c=True;
           newds.ISR__c=qt.opportunity__r.Primary_ISR__c;
           insert newds;
           
           newRecordId = newds.id;
           system.debug(LoggingLevel.INFO,'ds***:'+newds);
           
           List<Discount_Category_Detail__c> dstDetails = new List<Discount_Category_Detail__c>();
           List<Discount_Category_Detail__c> updstDetails = new List<Discount_Category_Detail__c>();
           dstDetails=[SELECT Active__c, Category__c,Description__c, Discount_Category__c, Discount_Table__c, Name, Id, Registered_Discount__c, Standard_Discount__c FROM Discount_Category_Detail__c where  Discount_Table__c =:dsTable.id];
           system.debug(LoggingLevel.INFO,'dstDetails***:'+dstDetails); 
           
           for(Discount_Category_Detail__c dtDetails:dstDetails){
               system.debug(LoggingLevel.INFO,'dtDetails.Category__c:'+ dtDetails.Category__c);
               dtDetails.Standard_Discount__c=oppDdetailMap.get(dtDetails.Category__c)!=null?oppDdetailMap.get(dtDetails.Category__c).Discount__c:null;
               dtDetails.Registered_Discount__c=oppDdetailMap.get(dtDetails.Category__c)!=null?oppDdetailMap.get(dtDetails.Category__c).Special_Discount__c:null;
               updstDetails.add(dtDetails);
           }
           Update updstDetails;
       }
       catch(Exception ex){
            
            Database.rollback(sp);
            ApexPages.addMessages(ex);
            return null;
            }
       return new PageReference ('/' + newRecordId);
   }
}