/****
*This batch class is used to query all Opportunities that are not in a deleted state and trial project is not populated and does not have a trial install date
*and the Id has to be related to an Asset where the IB Status is in Under Evaluation or Under Right of Return
*Next, queries the Asset related to the Opportunity where the IB Status is in Under Evaluation or Under Right of Return
*and updates the Opportunity with Assets IB Status and Install Date
*
*change - Trial Asset Count this class can be modified to handle
*/
global without sharing class BatchOpportunityRollup implements Database.Batchable<sObject> {

	global String Query;

	global BatchOpportunityRollup(){
   		Query = 'Select Id, Trial_Program__c, Trial_Asset_Count__c, Trial_Install_Date__c,(SELECT Id,IB_Status__c,InstallDate From Assets__r '+
   		'where IB_Status__c in (\'Under Evaluation\',\'Under Right of Return\') ORDER BY InstallDate DESC LIMIT 1) '+
		'FROM Opportunity WHERE (IsDeleted = False) AND (Trial_Program__c = null) AND (Trial_Install_Date__c = null) '+
		'AND (Id in (SELECT Opportunity__c FROM Asset WHERE (IB_Status__c in (\'Under Evaluation\',\'Under Right of Return\'))))';
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	   	   		
   		List<Opportunity> ToUpdate = new List<Opportunity>();
   		
   		for(sObject so : scope)
   		{
   			Opportunity opp = (Opportunity)so;   			
   			for(Asset a:opp.Assets__r)
   			{
   				if(a.IB_Status__c!=null)
   					opp.Trial_Program__c = a.IB_Status__c;
   				if(a.InstallDate!=null)
   					opp.Trial_Install_Date__c = a.InstallDate;
   			}
   			ToUpdate.add(opp);   			
   		}
   		
   		//if (!ToUpdate.IsEmpty()) update ToUpdate;
   		if (ToUpdate.size() > 0) 
   			update ToUpdate; 
								
   }
   
   global void finish(Database.BatchableContext BC){

   }
	
}