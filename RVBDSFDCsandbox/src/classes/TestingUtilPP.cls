/* Rivision History
Sunil Kumar [PRFT] 10/01/2013: Added Generic method to create bulk Records for sobjects - Account,
Contact, Certificate Master, Certificate, Certificate Summary, Authorizations_Master
*******************************************************************/
public with sharing class TestingUtilPP {
  public String opId;
  public String scsId;
  public String scsecId;
  public String scsecItemId;
  public String actId;

  public TestingUtilPP(){
    /*opId = makeTestableOp();
    scsId = makeSalesCoachStage();
    scsecId = makeSalesCoachSections();
    scsecItemId = makeSecItem();
    actId = makeActivities();*/
    ApexPages.currentPage().getParameters().put('id', this.opId);
  }
/* Start: commented by Prashant on 01/04/2014
  private String makeTestableOp(){
    Opportunity op = new Opportunity();
    op.Name='testOpName';
    op.CloseDate = Date.today();
    op.StageName='testStage';
    insert op;
    return op.Id;
  }

  private String makeSalesCoachStage(){
    Sales_Coach_Stage__c scs = new Sales_Coach_Stage__c();
    scs.Name='testStage';
    scs.Buyer_Stage__c='testBuyStage';
    scs.Active__c=true;
    insert scs;
    return scs.Id;
  }

  private String makeSalesCoachSections(){
    Sales_Coach_Section__c scsec =  new Sales_Coach_Section__c();
    scsec.Section_Ordering__c =1.0;
    scsec.Section_Name__c='testSection';
    scsec.Sales_Coach_Stage__c=scsId;
    scsec.Section_Width__c=33.0;
    insert scsec;
    return scsec.Id;
  }

private String makeSecItem(){
    Sales_Coach_Section_Item__c secItem = new Sales_Coach_Section_Item__c();
    secItem.Section_Ordering__c=1.0;
    secItem.Sales_Coach_Section__c=scsecId;
    secItem.Link_to_Content__c='http://www.google.com';
    secItem.Item_Name__c='testItem';
    secItem.Display__c=true;
    secItem.Products__c='';
    insert secItem;
    return secItem.Id;
}

private String makeActivities(){
  Sales_Coach_Activity__c sca = new Sales_Coach_Activity__c();
  sca.Sales_Coach_Stage__c=scsId;
  sca.Ordering_Number__c=1.0;
  sca.Activity_Details__c='testActivity';
  insert sca;
  return sca.Id;
}*///End: commented by Prashant 01/04/2014

    public static List<Account> createAccounts( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
       // createADMConfig();
        List<Account> accountsToInsert = new List<Account>();
        for( Integer i=0; i< numToInsert; i++ ){
            Account acc = new Account();
            acc.Name = 'Test Account' + i;
            accountsToInsert.add( acc );
        }
        return createRecords( accountsToInsert, doInsert, nameValueMap );
    }

    public static List<Contact> createContacts( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Contact> contactsToInsert = new List<Contact>();
        for( Integer i=0; i< numToInsert; i++ ){
            Contact a = new Contact();
            a.FirstName = 'Con FirstName' + i;
            a.LastName = 'Con LastName' + i;
            contactsToInsert.add( a );
        }
        return createRecords( contactsToInsert, doInsert, nameValueMap );
    }

    public static List<Certificate_Master__c> createCertMasterRecords( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Certificate_Master__c> certMastersToInsert = new List<Certificate_Master__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Certificate_Master__c a = new Certificate_Master__c();
            a.Classification__c = 'Certification';
            a.Group__c = 'Sales/Technical';
            certMastersToInsert.add( a );
        }
        return createRecords( certMastersToInsert, doInsert, nameValueMap );
    }

    public static List<Certificate__c> createCertificates( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Certificate__c> certificatesToInsert = new List<Certificate__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Certificate__c a = new Certificate__c();
            a.ClassName__c = 'Description' + i;
            a.Score__c = 'Complete';
            a.Status__c = 'Complete';
            a.IssuedOn__c = Date.Today() - 7;
            a.Exam_Date__c = Date.Today() + 30;
            certificatesToInsert.add( a );
        }
        return createRecords( certificatesToInsert, doInsert, nameValueMap );
    }

    public static List<Acct_Cert_Summary__c> createCertSummary( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Acct_Cert_Summary__c> certSummaryToInsert = new List<Acct_Cert_Summary__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Acct_Cert_Summary__c a = new Acct_Cert_Summary__c();
            certSummaryToInsert.add( a );
        }
        return createRecords( certSummaryToInsert, doInsert, nameValueMap );
    }

    public static List<Opportunity> createOpportunities( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Opportunity> Opportunities = new List<Opportunity>();
        for( Integer i=0; i< numToInsert; i++ ){
            Opportunity opp = new Opportunity();
            opp.Name = 'Opportunity Test' + i;
            Opportunities.add( opp );
        }
        return createRecords( Opportunities, doInsert, nameValueMap );
    }

    public static List<PartnerLevelConfig__c> createPartnerLevelConfigs( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<PartnerLevelConfig__c> plcs = new List<PartnerLevelConfig__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            PartnerLevelConfig__c plc = new PartnerLevelConfig__c();
            plcs.add( plc );
        }
        return createRecords( plcs, doInsert, nameValueMap );
    }

    public static List<Account_Authorizations__c> createAccAuths( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
    List<Account_Authorizations__c> accAuths = new List<Account_Authorizations__c>();
    for( Integer i=0; i< numToInsert; i++ ){
        Account_Authorizations__c accAuth = new Account_Authorizations__c();
        accAuths.add( accAuth );
    }
    return createRecords( accAuths, doInsert, nameValueMap );
    }


    public static List<Authorizations_Master__c> createAuthMaster( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Authorizations_Master__c> authMasterToInsert = new List<Authorizations_Master__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Authorizations_Master__c a = new Authorizations_Master__c();
            a.Name = 'Authorization Master' + i;
            a.Type__c='Competency';
            authMasterToInsert.add( a );
        }
        return createRecords( authMasterToInsert, doInsert, nameValueMap );
    }

    public static List<Opportunty_Discount_Schedule__c> createOppDiscountSchedule( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Opportunty_Discount_Schedule__c> oppDiscountSchedules = new List<Opportunty_Discount_Schedule__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Opportunty_Discount_Schedule__c a = new Opportunty_Discount_Schedule__c();
            a.Name = 'A ' + i;
            oppDiscountSchedules.add( a );
        }
        return createRecords( oppDiscountSchedules, doInsert, nameValueMap );
    }

    public static List<Category_Master__c> createCategoryMasters( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Category_Master__c> categoryMasters = new List<Category_Master__c>();
        for( Integer i=0; i< numToInsert; i++ ){
            Category_Master__c a = new Category_Master__c();
            a.Name = 'A ' + i;
            categoryMasters.add( a );
        }
        return createRecords( categoryMasters, doInsert, nameValueMap );
    }

    public static List<Discount_Detail__c> createDiscountDetails( List<Category_Master__c> cmList, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Discount_Detail__c> discountDetails = new List<Discount_Detail__c>();
        for( Integer i=0; i< cmList.size(); i++ ){
            Discount_Detail__c a = new Discount_Detail__c();
            a.Category_Master__c = cmList[i].Id;
            discountDetails.add( a );
        }
        return createRecords( discountDetails, doInsert, nameValueMap );
    }
    
        public static List<Opportunity_Development__c> createOD( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
       // createADMConfig();
        List<Opportunity_Development__c> odToInsert = new List<Opportunity_Development__c>();
        for( Integer i=0; i< numToInsert; i++ ){
         Opportunity_Development__c od = new Opportunity_Development__c(Customer_intiative_Area__c = 'Test od ' + i);
            odToInsert.add( od );
        }
        return createRecords( odToInsert, doInsert, nameValueMap );
    }
    
        public static List<Customer_Partner__c> createcp( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
       // createADMConfig();
        List<Customer_Partner__c> cpToInsert = new List<Customer_Partner__c>();
        for( Integer i=0; i< numToInsert; i++ ){
         Customer_Partner__c cp = new Customer_Partner__c(Customer_Partner__c = 'Test cp ' + i);
            cpToInsert.add( cp );
        }
        return createRecords( cpToInsert, doInsert, nameValueMap );
    }
    
      public static List<Implementation_Customer__c> createic( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Implementation_Customer__c> icToInsert = new List<Implementation_Customer__c>();
        for( Integer i=0; i< numToInsert; i++ ){
         Implementation_Customer__c ic = new Implementation_Customer__c(Units__c ='2'+ i);
            icToInsert.add( ic );
        }
        return createRecords( icToInsert, doInsert, nameValueMap );
    }
    
          public static List<Competitive_Landscape__c> createcl( Integer numToInsert, Boolean doInsert, Map<String, Object> nameValueMap ){
        List<Competitive_Landscape__c> clToInsert = new List<Competitive_Landscape__c>();
        for( Integer i=0; i< numToInsert; i++ ){
         Competitive_Landscape__c cl = new Competitive_Landscape__c(Units__c ='2'+ i);
           clToInsert.add( cl );
        }
        return createRecords( clToInsert, doInsert, nameValueMap );
    }

 /*  public static void createADMConfig(){
        jsADM__ADMConfig__c config = new jsADM__ADMConfig__c();
        config.Name = 'MasterSettings';
        config.jsADM__ADMAccountsTriggerEnabled__c = false;    // uninstalling with 'Jigsaw Advanced Data Management (Managed)'
        insert config;
    }*/

    public static List<SObject> createRecords( List<SObject> records, Boolean doInsert, Map<String, Object> attributes ){

        Integer i = 0;
        if( attributes != null ){
          for ( Integer j =0; j < records.size(); j ++ ) {
            Sobject record = records[j];
            for (String key : attributes.keySet()) {
                Object value = attributes.get(key);
                if (value instanceof List<Object>) {
                  object obj =  ((List<Object>) value).get(i);
                  if( obj instanceof Sobject ){
                    Id sObjectId = ((SObject) obj).Id;
                    record.put( key,sObjectId );
                  }
                  else {
                    record.put(key,obj);
                  }
                } else {
                  record.put(key, value);
                }
              //}
            }
            i++;
          }
        }

        if (doInsert) {
          insert records;
        }
        return records;
      }


}