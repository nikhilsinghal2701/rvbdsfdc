@isTest(SeeAllData = true)
private class LeadPartnerUserTest {

    static testMethod void myUnitTest() {        
        //List<string> nameStr=new List<string>{'Deal Registration - Distributor','Deal Registration'};//commented by prashant on 08/28/2012
        List<string> nameStr=new List<string>{'Deal Registration'};//Added by prashant on 08/28/2012
        RecordType[] leadRT=[select id,name from recordtype where SobjectType ='Lead'  and name in:(nameStr)];        
        Account distiAcc=[select Id,Name,RecordTypeId from Account where name='Zycko Ltd'and Type='Distributor'];
        Account tier2Acc=[select Id,Name,RecordTypeId from Account where name='Trace 3'and Type='VAR'];
        Contact[] tier2Con=[select Id,Name,RecordTypeId,User_Profile__c from contact where AccountId=:tier2Acc.Id and User_Profile__c in ('Orders','Admin')];
        Contact[] distiCon=[select Id,Name,RecordTypeId,User_Profile__c from contact where AccountId=:distiAcc.Id and User_Profile__c in ('Orders','Admin')];
        Lead led=new Lead(AnnualRevenue = 1200000, Number_Of_Employees__c = 200, Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
                            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', Secondary_Application__c = 'UDP');
        led.Status='Open';
        led.Sold_To_Partner__c=distiAcc.Id;
        led.Tier2__c=tier2Acc.Id;
        led.Tier2_Partner_Contact__c=tier2Con[0].Id;
        led.FirstName='testLead1';
        led.LastName='14Nov';
        led.Company='Edit Corp';
        led.Title='Tester';
        led.Email='tester@editcorp.com';
        led.Project_Name__c='MultiTier';
        led.Project_Close_Date__c=system.today();
        led.Verified_Budget__c='Yes';
        led.Opportunity_Value__c=70000;
        led.Competitors__c='Cisco';
        led.Phone='4154154156';
        led.MobilePhone='4154154156';
        led.Fax='4154154156';
        led.Industry='Education';
        led.Website='www.editcorp.com';
        led.Street='199 fremont street';
        led.City='San Francisco';
        led.State='CA';
        led.Country='US';
        led.RecordTypeId=leadRT[0].Id;
        insert led;
        if (tier2Con.size()>1) {
            led.Tier2_Partner_Contact__c=tier2Con[1].Id;
            led.FirstName='testLead2';
            update led;   
        } 
        
        Lead led1=new Lead(AnnualRevenue = 1200000, Number_Of_Employees__c = 200, Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
                            Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', Secondary_Application__c = 'UDP');
        led1.Status='Open';
        led1.Sold_To_Partner__c=distiAcc.Id;
        led1.Tier2__c=tier2Acc.Id;
        led1.Sold_to_Partner_Contact__c=distiCon[0].Id;
        led1.FirstName='testLead3';
        led1.LastName='14Nov09';
        led1.Company='Edit Corp';
        led1.Title='Tester';
        led1.Email='tester@editcorp.com';
        led1.Project_Name__c='MultiTier';
        led1.Project_Close_Date__c=system.today();
        led1.Verified_Budget__c='Yes';
        led1.Opportunity_Value__c=70000;
        led1.Competitors__c='Cisco';
        led1.Phone='4154154156';
        led1.MobilePhone='4154154156';
        led1.Fax='4154154156';
        led1.Industry='Education';
        led1.Website='www.editcorp.com';
        led1.Street='199 fremont street';
        led1.City='San Francisco';
        led1.State='CA';
        led1.Country='US';
        led1.RecordTypeId=leadRT[0].Id;
        insert led1;
        if (distiCon.size()>1) {
            led1.Sold_to_Partner_Contact__c=distiCon[0].Id;
            led1.FirstName='testLead4';
            update led1;
        }
    }
}