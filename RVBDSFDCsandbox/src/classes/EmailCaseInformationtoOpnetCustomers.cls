/****************
Class: EmailCaseInformationtoOpnetCustomers
Description: This will be used to send emails to all customers with their case information
    and Salesforce case number
    
Author: Clear Task (Rucha) - 4/2/2012

Rev1: Rucha 6/20/2013 - To set fields on Contacts to whom emails were sent successfully
*****************/
public with sharing class EmailCaseInformationtoOpnetCustomers implements Database.Batchable<sObject>{
    private Id fromEmailId;
    private String headerImgUrl;
    public EmailCaseInformationtoOpnetCustomers(){
        fromEmailId = RVBD_Support_Site_Properties__c.getInstance('Support Site Restriction').Org_Wide_Default_Email_Id__c;
        headerImgUrl = RVBD_Email_Properties__c.getInstance('rvbd_header_image_url').Server_URL__c;
    }
    
    public Database.Querylocator start(Database.BatchableContext bc){       
        Id opnetRecTypeId = [Select Id from RecordType where sobjectType='Case' and DeveloperName='OPNET'].get(0).Id;       
        String query = 'Select Id,Legacy_Case_Number__c,CaseNumber,ContactId,Contact.Email,OPNET_CaseInformationSent__c ' + 
                        'From Case ' + 
                        'Where RecordTypeId=\'' + opnetRecTypeId + '\' ' +
                            'and (IsClosed=false OR ClosedDate=LAST_N_DAYS:30) ' +                           
                            'and Legacy_Case_Number__c != null';                           
        if(Test.isRunningTest()){
                  query = query + ' limit 5';
        }                                           
        return Database.getQueryLocator(query);
    }   
    
    public void execute(Database.BatchableContext bc, List<sObject> caseList){
    	Map<Id,Case> conToCaseMap = new Map<Id,Case>();
    	List<Case> conEmailsSentTo = new List<Case>();
    	for(sObject obj : caseList){
            Case c = (Case)obj;
            Case caseToUpdate = conToCaseMap.get(c.ContactId);
            
            if(caseToUpdate == null && c.OPNET_CaseInformationSent__c == false){
            	caseToUpdate = new Case(Id=c.Id,OPNET_SendCaseInformation__c=true);
            	conToCaseMap.put(c.ContactId,caseToUpdate);
            }
            
            if(c.OPNET_CaseInformationSent__c == true){
            	conEmailsSentTo.add(c);
            }
    	}
    	
    	for(Case c : conEmailsSentTo){
    		if(conToCaseMap.get(c.ContactId)!=null){
    			conToCaseMap.remove(c.ContactId);
    		}
    	}
    	
    	if(conToCaseMap.size() > 0){
    		update conToCaseMap.values();
    	}
    	
        //Collect all case information for each contact 
        /*Map<Id,Messaging.SingleEmailMessage> contactToEmailMap = new Map<Id,Messaging.SingleEmailMessage>();              
        String htmlBody = '';              
        for(sObject obj : caseList){
            Case c = (Case)obj;             
            Messaging.SingleEmailMessage email = contactToEmailMap.get(c.ContactId);
            if(email == null){
                email = new Messaging.SingleEmailMessage();
                email.setToAddresses(new List<String>{c.Contact.Email});
                if(!(Label.Email_to_Testers_Switch.equalsIgnoreCase('none'))){
                    email.setCcAddresses(Label.Email_to_Testers_Switch.split(';'));
                }
                email.setSubject('OPNET to Riverbed Support Case Transition Information');
                
                htmlBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1.0"/><title>Email Template #6</title><style type="text/css">#outlook a {padding:0;}body{width:600px !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:auto; padding:10px 0;} .ExternalClass {width:100%;} .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}table td {border-collapse: collapse;}</style></head>' +
                            '<body><table width="600" cellpadding="0" cellspacing="0" border="0" id="backgroundTable"><tr><td><!--header table--><table border="0" cellpadding="0" cellspacing="0" ><tr><td width="100%"><img style="outline:none; text-decoration:none; text-align:left;" src="' + headerImgUrl + '" alt="Riverbed - Your Case Information" border=0 width=600 height=60" ></td></tr></table></td></tr><tr><td><!--email body table--><table width="100%" border="0" cellspacing="0" cellpadding="0" ><tr><td width="400" style="background-color:#ffffff;font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;padding:10px; " ><!--left column email body-->' + 
                            '<p>Dear Customer,</p>' +
                            '<p>We have migrated all open and prior OPNET cases to Riverbed’s case management system, with new Riverbed case numbers.  ' + 
                            'Below is a table of your open cases, and cases closed within the last 30 days, and their respective new Riverbed case numbers. </p>' +
                            '<p>You may search for your cases on the Riverbed Support Site using either the OPNET case number or the Riverbed case number (My Riverbed > Cases and RMA).</p>' +
                            '<p>Please reference the Riverbed case number in future correspondences with Riverbed Support.</p>' +
                            '<table><tr style="font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;"><td width="50%"><span style="text-decoration: underline">OPNET Case Number</span></td><td width="50%"><span style="text-decoration: underline">Riverbed Case Number</span></td><tr>';   
                email.setHtmlBody(htmlBody);
                
                email.setOrgWideEmailAddressId(fromEmailId);        
            }
            htmlBody = email.getHtmlBody();
            htmlBody += '<tr style="font-family:Arial, sans-serif;text-align:left;font-size:11px;line-height:1.3em;"><td valign="top" width="50%">' + c.Legacy_Case_Number__c + '</td><td valign="top" width="50%">' + c.CaseNumber + '</td></tr>';
            email.setHtmlBody(htmlBody);
            contactToEmailMap.put(c.ContactId,email);
        }
        
        //Send emails
        List<Messaging.SingleEmailMessage> emailsToSendList = new List<Messaging.SingleEmailMessage>();     
        String endOfMessage = '</table><p>Sincerely,<br/>Riverbed Technical Support<br/><a href="https://support.riverbed.com" style="color:#e55603;">Support.Riverbed.com</a></p></td>' +
                                '<td valign="top"><!--right column table--><table width="180" border="0" cellspacing="0" cellpadding="0" style="font-family:Arial, Helvetica, sans-serif;font-weight:bold;color:#ffffff;text-align:left;font-size:12px;table-layout:fixed;"><tr><td height="1" > </td></tr>' +
                                '<!--orange button--><tr><td style="border-color:#da7c0c; border-style:solid; border-width:1px;background: #ff6600;background: -webkit-gradient(linear, left top, left bottom, from(#faa51a), to(#ff6600));background: -moz-linear-gradient(top, #faa51a, #f47a20);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#faa51a\', endColorstr=\'#ff6600\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;overflow: hidden;" ><a href="https://login.riverbed.com/login_support.htm" target ="_blank" title="View Your Account" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;">View Your Account</a></td></tr><tr><td height="1"></td></tr>' +
                                '<!--black button #1--><tr><td style="border-color:#000; border-style:solid; border-width:1px;background: #333;background: -webkit-gradient(linear, left top, left bottom, from(#666), to(#000));background: -moz-linear-gradient(top, #666, #000);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#666666\', endColorstr=\'#000000\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;"><a href="https://support.riverbed.com/index.htm" target ="_blank" title="Visit Support" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;">Visit Support</a></td></tr>' +
                                '<tr><td height="1"></td></tr><!--black button #2--><tr><td  style="border-color:#000; border-style:solid; border-width:1px; background: #333;background: -webkit-gradient(linear, left top, left bottom, from(#666), to(#000));background: -moz-linear-gradient(top, #666, #000);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=\'#666666\', endColorstr=\'#000000\'); padding:10px 0px 10px 20px;-webkit-border-radius: .5em;-moz-border-radius: .5em;border-radius: .5em;"><a href="mailto:support@riverbed.com"  target ="_blank" title="Email Support" style="color:#ffffff; text-decoration:none; font-family:Arial, Helvetica, sans-serif;font-weight:bold;text-align:left;font-size:12px;" >Email Support</a></td></tr>' +
                                '</table></td><!--end of right column table--></tr></table></td><!--end email body table--></tr>' +
                                '<tr><td style=" font-family:Arial, sans-serif;text-align:left;font-size:9px;line-height:1.3em;padding:8px;border-color:#e55603;border-top-style:solid;border-top-width:medium; ">' +
                                '<!-- footer-->' +
                                'Copyright &copy;2012 Riverbed Technology, Inc. All rights reserved.<br /> ' +
                                '<a href="https://support.riverbed.com/contact/index.htm" style="color:#e55603;">Contact Support </a> | ' +
                                '<a href="http://www.riverbed.com/us/contact/" style="color:#e55603;">Contact Riverbed</a> | ' +
                                '<a href="http://www.riverbed.com/us/privacy_policy.php" style="color:#e55603;">Privacy Policy</a> | <a href="http://www.riverbed.com/us/legal_notices.php"style="color:#e55603;">Legal Notices </a> | <a href="http://www.riverbed.com/us/sitemap.php" style="color:#e55603;">Site Map </a></td></tr></table></body></html>';
                                
        for(Messaging.SingleEmailMessage email : contactToEmailMap.values()){           
            email.setHtmlBody(email.getHtmlBody() + endOfMessage);
            emailsToSendList.add(email);
        }
        
        List<Messaging.SendEmailResult> smeResult = Messaging.sendEmail(emailsToSendList,false);//Modified by Rucha on 6/20/2013 to capture sentEmail results
        
        //Added by Rucha on 6/20/2013 to mark contact to whom emails were sent successfully
        Integer i = 0;
        List<Contact> emailsSentSuccessfully = new List<Contact>();
        for(Messaging.SendEmailResult sme : smeResult){
        	if(sme.isSuccess()){
        		String sentEmailBody = emailsToSendList.get(i).getHtmlBody();
        		Id conId;
        		for(Id contactId : contactToEmailMap.keySet()){ 
        			Messaging.SingleEmailMessage email = contactToEmailMap.get(contactId);
        			if(email.getHtmlBody().equals(sentEmailBody)){
        				emailsSentSuccessfully.add(new Contact(Id=contactId,OPNET_CaseInformationEmailSent__c=true));
        				break;
        			}
        		}        		
        	}        	
        	i++;
        }
        
        if(!emailsSentSuccessfully.isEmpty()){
        	update emailsSentSuccessfully;
        }
        //End added by Rucha on 6/20/2013*/
    }
    
    public void finish(Database.BatchableContext bc){       
    }
    
    @isTest(SeeAllData=true)
    static void testEmailCaseInformationtoOpnetCustomers(){
        Id opnetRecTypeId, defaultRecTypeId;
        for(RecordType rt : [Select Id,DeveloperName from RecordType where sobjectType='Case' and DeveloperName in('OPNET','Default')]){
            if(rt.DeveloperName.equals('Default')){
                defaultRecTypeId = rt.Id;
            }
            else{
                opnetRecTypeId = rt.Id;
            }
        }       
        
        Account acc = new Account(Name='ECIO Test Account');
        insert acc;
        
        Contact c = new Contact(LastName='ECIO Test Con 1',Contact_Sticky_Notes__c ='Test Notes',AccountId=acc.Id,Email='test@test.com');
        insert c;
        
        List<Case> caseList = new List<Case>();
        //Create cases that exactly match all criteria
        Case c1 = new Case(RecordTypeId=opnetRecTypeId,ContactId=c.Id,Status='New',Legacy_Case_Number__c='123');
        caseList.add(c1);
        
        Case c2 = new Case(RecordTypeId=opnetRecTypeId,ContactId=c.Id,Status='Closed - Resolved',Legacy_Case_Number__c='123');
        caseList.add(c2);
        
        //Create cases that match the criteria but no contact
        Case c3 = new Case(RecordTypeId=opnetRecTypeId,Status='New',Legacy_Case_Number__c='123');
        caseList.add(c3);
        
        //Create cases that do not match recordtypeid criteria
        Case c4 = new Case(RecordTypeId=defaultRecTypeId,Status='New',ContactId=c.Id,Legacy_Case_Number__c='123');
        caseList.add(c4);       
        
        insert caseList;
        
        EmailCaseInformationtoOpnetCustomers ec = new EmailCaseInformationtoOpnetCustomers();
        ID batchprocessid;
        Test.startTest();
        Database.executeBatch(ec);
        Test.stopTest();        
    }
}