public class AddToSalesTeam {
    public static final Set<Id> PROFILES = new Set<Id>{'00e30000000bqt9'};
    private static AddToSalesTeam m_instance = null;    
    //private static Map <Id, Lead> m_convertedLeads;    
    //private static Map <Id,Lead> m_leadMap = null;    
    private static OpportunityTeamMember [] m_oppMembers = new List<OpportunityTeamMember> ();    
    private static OpportunityShare [] m_oShares = new List<OpportunityShare> ();    
    private static Integer m_executionCount = 0;    
    private static String DEFAULT_ACCESS = 'Edit';    
    private static String READ_ACCESS = 'Read';
    private static String DEFAULT_ROLE = 'Partner OC';    
    private static OpportunityShare oShare;    
    private static Map<Id,Lead> m_oldMap;    
    private Set<Id> ownerIds;            
    private static Id partner, reseller; 
    private AddToSalesTeam(){}        
    public static Boolean run = true;
    
    public static Boolean runOnce() { 
        if (run) {
            run = false;
            return true;
        } else {
            return false;
        }
    }
    
    public AddToSalesTeam (Map<Id,Lead> newMap) {
        //m_oldMap = oldMap;
        //m_leadMap = newMap;        
        m_instance = this;        
        //this.ownerIds = new Set<Id>();        
        //m_convertedLeads = newMap;                
        //System.debug('m_convertedLeads :' + m_convertedLeads.values());                
   /*   for (Lead l: m_leadMap.values()) {            
            if (l.ConvertedOpportunityId != null ){                   
                m_convertedLeads.put(l.Id, l);                
            //  this.ownerIds.add(oldMap.get(l.Id).OwnerId);            
            }       
        }*/           
        //System.debug('m_partnerUsers' + this.ownerIds);    
    }        
    
    public static AddToSalesTeam getInstance(){        
        return m_instance;    
    }        
    
    @future
    public static void addPartnerToSalesTeam(Set<Id> leadIds){
        List<Lead> m_convertedLeads = [select Id, Sold_To_Partner_User__c, Tier2_User__c, ConvertedOpportunityId from Lead where id in :leadIds];        
        System.debug('m_convertedLeads.size() ' + m_convertedLeads.size());        
        for (Lead l: m_convertedLeads) {            
            //Id partner = oldLead.OwnerId;
            partner = l.Sold_To_Partner_User__c;
            //Ankita 1/29/10: Added to add tier 2 partner(reseller) user in read only mode to Sales Team
            reseller = l.Tier2_User__c;
            //if (this.ownerIds.contains(partner)){                
                String role = DEFAULT_ROLE;                
                String accessLevel = DEFAULT_ACCESS;                
                List<OpportunityShare> oppShares = [SELECT Id, OpportunityAccessLevel, RowCause FROM OpportunityShare 
                                                    WHERE OpportunityId =: l.ConvertedOpportunityId 
                                                    AND (UserOrGroupId =: partner or  UserOrGroupId = :reseller)
                                                    AND RowCause!='Owner'];                
                OpportunityTeamMember tmSTP = new OpportunityTeamMember(UserId = partner,                                                                        
                                                                    OpportunityId = l.ConvertedOpportunityId,                                                                        
                                                                    TeamMemberRole = role); 
                OpportunityTeamMember tmTTP = new OpportunityTeamMember(UserId = reseller,  
                                                                    OpportunityId = l.ConvertedOpportunityId,
                                                                    TeamMemberRole = role); 
                if( oppShares.size() == 0 ) {                    
                    System.debug('No OpportunityShare object, so need to create new one');                    
                    OpportunityShare oppShareSTP = new OpportunityShare( UserOrGroupId = partner, OpportunityAccessLevel = DEFAULT_ACCESS,
                                                                    OpportunityId = l.ConvertedOpportunityId);                    
                    OpportunityShare oppShareTTP = new OpportunityShare( UserOrGroupId = reseller, OpportunityAccessLevel = READ_ACCESS,
                                                                    OpportunityId = l.ConvertedOpportunityId);                    
                    oppShares.add(oppShareSTP);                                                    
                    oppShares.add(oppShareTTP);                                                    
                }                
                for(OpportunityShare os : oppShares) {                    
                //  os.OpportunityAccessLevel = accessLevel;                    
                    System.debug(os);                
                }                
                m_oShares.addAll(oppShares);                                
                System.debug('Access level:' + accessLevel);                        
                System.debug('OppShares:' + oppShares);                             
                System.debug('Created Opportunity Team Member:' + tmSTP);                
                m_oppMembers.add(tmSTP);                                
                m_oppMembers.add(tmTTP);                                
            //}
            flush();           
        }                                                            
    }        

//  @future
    public static void flush(){                
        //String accessLevel = DEFAULT_ACCESS;        
        m_executionCount++;        
        System.debug('m_executionCount ' + m_executionCount);
        //      system.debug('m_oppMembers: ' + m_oppMembers);   
        //      system.debug('m_oShares:' + m_oShares);        
        System.debug('OppMembersSize:' + m_oppMembers.size());        
        if (m_oppMembers.size() > 0){            
            //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg
            database.saveresult[] sr = database.insert(m_oppMembers, false);            
            System.debug(sr);        
        }        
        if (m_oShares.size() > 0){            
            database.Upsertresult[] usr = database.upsert(m_oShares, false);        
            for (Database.UpsertResult u_s_r : usr ){           
                system.debug('SAVERESULT : ' + u_s_r);           
                List<OpportunityShare> toUpdateShares = new List<OpportunityShare>();                  
                for (OpportunityShare op_s : [SELECT Id, UserOrGroupId, OpportunityId, RowCause, OpportunityAccessLevel FROM OpportunityShare WHERE Id=:u_s_r.Id]){
                    if(op_s.UserOrGroupId == partner){
                        op_s.OpportunityAccessLevel = DEFAULT_ACCESS;
                    }else if(op_s.UserOrGroupId == reseller){
                        op_s.OpportunityAccessLevel = READ_ACCESS;
                    }  
                    toUpdateShares.add( op_s );           
                }           
                if ( !toUpdateShares.isEmpty() ) {                
                    //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg
                    system.debug('toUpdateShares is:' + toUpdateShares.size());                
                    update toUpdateShares;           
                } else {                
                    system.debug('toUpdateShares is empty');           
                }        
            }
            }            
        }        
        
        
        public static void reset(){        
            m_oppMembers = new List<OpportunityTeamMember> ();        
            m_executionCount = 0;        
        }    
        
        
        testMethod static void unit() {        
            Test.startTest();
            try {                    
                 User p = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 1];        
                 System.debug('power user:' + p);        
                 User crmUser = [SELECT Id FROM User WHERE IsActive = TRUE LIMIT 1];        
                 //User crmUser = [SELECT Id FROM User WHERE IsActive = TRUE AND ContactId = null LIMIT 1];        
                 System.debug('crm-user:' + crmUser);                
                 System.assert(p != null);                
                 // Test insert        
                 Lead l = DealRegApprovalActionTestUtil.createLeadDR();             
                //Lead ld = [select Id, LastName from Lead where Email = 'lead@test.com' limit 1];                
                //System.debug('new lead:' + ld);                
                //System.assertEquals(ld.lastname, 'last');        
                AddToSalesTeam.reset();                
                LeadStatus convertStatus = [Select Id, MasterLabel                                     
                                            from LeadStatus                                     
                                            where IsConverted=true                                     
                                            limit 1];                                    
                System.debug('convertStatus:' + convertStatus);                
                Database.LeadConvert lc = new Database.LeadConvert();        
                lc.setLeadId(l.id);        
                lc.setConvertedStatus(convertStatus.MasterLabel);        
                lc.setOwnerId(crmUser.Id);                
                System.debug(lc);                
                Database.LeadConvertResult lcr = Database.convertLead(lc);        
                System.debug('opportunityId:' + lcr.getOpportunityId());        
                System.assert(lcr.isSuccess());        
                List<OpportunityShare> sh = [SELECT Id, OpportunityId, UserorGroupId FROM OpportunityShare where opportunityid = :lcr.getOpportunityId()];        
                System.debug('shares count:' + sh.size());        
                for(OpportunityShare s : sh) {            
                    System.debug(s);            
                }                 
                List<OpportunityTeamMember> members = [SELECT Id, UserId, OpportunityId FROM OpportunityTeamMember where opportunityid = :lcr.getOpportunityId()];        
                System.debug('members count:' + members.size());        
                for(OpportunityTeamMember m : members) {            
                    System.debug(m);            
                }                 
                
                final Opportunity opportunityObtained = [Select Id from Opportunity                             
                                                        where Id = :lcr.getOpportunityId()];        
                System.assert(opportunityObtained != null);                
                Integer count = [Select count() from OpportunityTeamMember where UserId = :p.Id and OpportunityId = :lcr.getOpportunityId()];                                    
                System.debug('count of OpportunityTeamMember:' + count);        
                System.debug('count of OpportunityTeamMember:' + p.Id);        
                System.debug('count of OpportunityTeamMember:' + lcr.getOpportunityId());                
                count = [Select count() from OpportunityShare where UserorGroupId = :p.Id and OpportunityId = :lcr.getOpportunityId()];                                       
                System.debug('2-count:' + count);                
                try {          
                    Set<Id> leads = new Set<Id> ();
                    leads.add(l.Id);            
                    //AddToSalesTeam pVis = new AddToSalesTeam(leads, leads);            
                    //m_convertedLeads.put(l.Id, l);            
                    //m_oldMap.put(l.Id, l);            
                    //pVis.ownerIds.add(l.OwnerId);            
                    addPartnerToSalesTeam(leads);            
                    //pVis.flush();        
                } catch (Exception e1) {          
                    System.debug( e1 );        
                }                
            } catch (Exception e) {            
                System.debug( e );        
            }        
            Test.stopTest();    
        }
    }