public class ZeusUtility {
    public static Boolean isTest=false;
    //static Map<AccountNumber,Account>accMap;
/*---------------------------------- START:For changeStatusOnEscalation -------------------------------------*/     
//Start:For changeStatusOnEscalation trigger bulk handling. by prashant.singh@riverbed.com on 10/15/2011
    public static void addComment(Map<Id,String> caseMsg){
        CaseComment cc;
        List<CaseComment> ccList=new List<CaseComment>();
        for(Id tempId: caseMsg.keySet()){
            cc = new CaseComment();
            cc.ParentId = tempId;
            cc.CommentBody = caseMsg.get(tempId);
            cc.IsPublished = false;
            ccList.add(cc);
        }       
        if(ccList.size()>0){
            insert ccList;  
        }
    }//End:For changeStatusOnEscalation trigger
/*---------------------------------- END:For changeStatusOnEscalation -------------------------------------*/    
    
/*---------------------------------- START:For handleEscalationMessage -------------------------------------*/    
//  Start:For handleEscalationMessage trigger bulk handling. by prashant.singh@riverbed.com on 10/16/2011    
    public static void handleEscalationMessage(List<Case> newCaseTrigger, Map<Id,Case> oldCaseTrigger){
        List<Case>caseList=new List<Case>();
        set<Id> Ids=new set<Id>();
        List<CaseComment>ccList=new List<CaseComment>();
        Messaging.SingleEmailMessage mail;
        List<Messaging.SingleEmailMessage>mails=new List<Messaging.SingleEmailMessage>();
        CaseComment cc;
        String recipient='',message='',urls = '',accName;
        for(Case temp:newCaseTrigger){
            if(temp.EscalationMessage__c == null && 
            ((oldCaseTrigger.get(temp.Id).Status != 'Development' && temp.Status == 'Development') || (oldCaseTrigger.get(temp.Id).Status != '3rd Line' && temp.Status == '3rd Line'))
            ){
                caseList.add(temp);
                Ids.add(temp.AccountId);
           }
        }
        if(caseList.size()>0){ 
            Map<Id,Account> accountMap=new Map<Id,Account>([SELECT Id,Name FROM Account WHERE Id IN :Ids]);
            for(Case newCase: caseList){
                cc = new CaseComment();
                cc.ParentId = newCase.Id;
                cc.CommentBody = '*** Escalation message ***\n\n' + newCase.EscalationMessage__c;
                cc.IsPublished = false;
                ccList.add(cc);
                system.debug('***CaseComment:'+cc);
                accname=accountMap.get(newCase.AccountId).Name;
                urls = 'Cached version: http://devpage-sf-cache.support.cam.zeus.com/dev_page.php?mode=detail&id=' + newCase.CaseNumber + '\n' +
                      'Slow version: http://dev-support.cam.zeus.com/dev_page.php?mode=detail&id=' + newCase.CaseNumber + '\n\n';
                if (newCase.Status == 'Development') {
                    recipient = 'dev@zeus.com';                     
                    message=createMessage(newCase,urls,accname);
                    mail=createEmail(newCase,message,recipient);
                    mails.add(mail);
                    //newCase.EscalationMessage__c = '';
                } else if (newCase.Status == '3rd Line') {
                    recipient = 'support-3rd@zeus.com';
                    urls = 'SF cached: http://emea-sf-cache.support.cam.zeus.com/' + newCase.Id + '\n' +
                        'SF direct: https://emea.salesforce.com/' + newCase.Id + '\n\n' + urls;
                    message=createMessage(newCase,urls,accname);
                    mail=createEmail(newCase,message,recipient);
                    mails.add(mail); 
                    //newCase.EscalationMessage__c = '';
                }
            }
            if(ccList!=NULL && ccList.size()>0){
                system.debug('***CaseCommentList:'+ccList);
                upsert ccList;
            }
            if(mails.size()>0){
                sendEmail(mails);
            }       
        }
        
    }//End: handleEscalationMessage trigger
    //Create Reference: createRef
    private static String createRef(Case newCase){
        String myId = newCase.Id;
        String ref = 'ref:00D2NO4.' + myId.substring(0, 4) + myId.substring(10, 15) + ':ref';  // LIVE SYSTEM ONLY!
        return ref;
    }
    //Create Email Notification Message: createMessage
    private static String createMessage(Case newCase,String urls,String accName){
        String msg = 'Ticket: ' + newCase.CaseNumber + '\n' +
        'Subject: ' + newCase.Subject + '\n' +
        'Account: ' + accName + '\n' +
        'Product: ' + newCase.Product__c + '\n' +
        'Platform: ' + newCase.Platform__c + '\n' +
        'Version: ' + newCase.Version__c + '\n\n' +
        newCase.EscalationMessage__c + '\n\n\n' +
        urls +
        createRef(newCase);
        return msg;
    }//End Email Notification Message
    //Create Email Notification: createEmail
    private static Messaging.SingleEmailMessage createEmail(Case newCase,String message,String recipient){
        Messaging.SingleEmailMessage email=new Messaging.SingleEmailMessage();
        email.setReplyTo('support@zeus.com');
        email.setSaveAsActivity(false);
        email.setToAddresses(new List<String> {recipient});
        email.setSubject('Support escalation: ' + newCase.CaseNumber + ' [' + newCase.Subject + '] ');
        email.setPlainTextBody(message);
        return email;
    }//End Create Email Notification
/*---------------------------------- END:For handleEscalationMessage -------------------------------------*/    
    
/*---------------------------------- START:Common for email send -----------------------------------------*/    
    //Send Email Notification: sendEmail
    private static void sendEmail(List<Messaging.SingleEmailMessage> mails){
        Messaging.SendEmailResult[]result;
        system.debug(LoggingLevel.INFO,'Mail:'+mails);
        if(mails.size()>0 && isTest==false){
            try{
                if(!isTest){
                   result = Messaging.sendEmail(mails);
                   system.debug(LoggingLevel.INFO,'Email Sent Result:'+result);
                }
            }catch(Exception e){
                system.debug(LoggingLevel.INFO,'Email Exception:'+e.getMessage());
            }
        }//end if
    }//End sendEmail method
/*---------------------------------- END:Common for email send -------------------------------------*/
    
/*---------------------------------- START:For leadEmailNotification -------------------------------------*/   
   //START:For leadEmailNotifications trigger bulk handling. by prashant.singh@riverbed.com on 10/16/2011
    public static void leadEmailNotifications(List<Lead> newLeadTrigger,Map<Id,Lead> oldMapTrigger){
        Lead oldLead;
        UserRole partnerRole;
        //User accMan;
        List<Lead> leadList=new List<Lead>();
        set<Id> ownerIds=new set<Id>();
        set<Id> userRoleIds=new set<Id>();
        set<Id> acctOwnerIds=new set<Id>();
        Map<Id,User> userMap=new Map<Id,User>();
        Messaging.SingleEmailMessage mail;
        List<Messaging.SingleEmailMessage>mails=new List<Messaging.SingleEmailMessage>();
        try{
            /*
            User partnerUser = [ SELECT Name, Contact.Account.Name, Contact.Account.OwnerId, UserRoleId
                             FROM User 
                             WHERE Id = :newLead.OwnerId 
                           ];       
            UserRole partnerRole = [ SELECT Name FROM UserRole WHERE Id = :partnerUser.UserRoleId ];
            User accMan = [ SELECT Email FROM User WHERE Id = :partnerUser.Contact.Account.OwnerId ];
            */
            RecordType dealReg = [ SELECT Id FROM RecordType WHERE Name = 'Deal Registration' ];
            for(Lead temp:newLeadTrigger){
                ownerIds.add(temp.OwnerId);
            }
            List<User> partnerUser = [ SELECT Name, Contact.Account.Name, Contact.Account.OwnerId, UserRoleId
                             FROM User WHERE Id IN :ownerIds];
            for(User usr:partnerUser){
                userRoleIds.add(usr.UserRoleId);
                acctOwnerIds.add(usr.Contact.Account.OwnerId);
                userMap.put(usr.Id,usr);
            }
            Map<Id,UserRole> userRoleMap=new Map<Id,UserRole>([SELECT Name FROM UserRole WHERE Id IN :UserRoleIds]);           
            Map<Id,User> accManMap=new Map<Id,User>([SELECT Email FROM User WHERE Id IN :acctOwnerIds]);
            for(Lead newLead:newLeadTrigger){
                partnerRole=userRoleMap.get(userMap.get(newLead.OwnerId).UserRoleId); 
                //accMan=accManMap.get((userMap.get(newLead.OwnerId).Contact.Account.OwnerId));
                //system.debug('***AccMan:'+accMan);  
                if((newLead.RecordTypeId == dealReg.Id)&&(partnerRole.Name.contains('Partner'))){                           
                    if(oldMapTrigger==null){            
                        //String msg = getMessage('DealRegCreated', newLead, partnerUser);
                        String msg = getMessage('DealRegCreated', newLead, userMap.get(newLead.OwnerId));
                        String subj = getSubject('DealRegCreated', newLead);                          
                        //sendEmail(msg, subj, accMan.Email);
                        mail=createEmail(msg, subj, accManMap.get(userMap.get(newLead.OwnerId).Contact.Account.OwnerId).Email);             
                        if(newLead.Status == 'Qualified Lead'){
                            //msg = getMessage('LeadQualified', newLead, partnerUser);
                            msg = getMessage('LeadQualified', newLead, userMap.get(newLead.OwnerId));
                            subj = getSubject('LeadQualified', newLead);                    
                            //sendEmail(msg, subj, accMan.Email);
                            mail=createEmail(msg, subj, accManMap.get(userMap.get(newLead.OwnerId).Contact.Account.OwnerId).Email);
                        }
                        mails.add(mail);
                    }else if(oldMapTrigger!=null){
                        oldLead = oldMapTrigger.get(newLead.Id);                
                        if(oldLead!=null && (oldLead.Status != 'Qualified Lead')&& (newLead.Status == 'Qualified Lead')){
                            //String msg = getMessage('LeadQualified', newLead, partnerUser);
                            String msg = getMessage('LeadQualified', newLead, userMap.get(newLead.OwnerId));
                            String subj = getSubject('LeadQualified', newLead); 
                            //sendEmail(msg, subj, accMan.Email);
                            mail=createEmail(msg, subj,accManMap.get(userMap.get(newLead.OwnerId).Contact.Account.OwnerId).Email);
                            mails.add(mail);
                        }
                    }
                }
            }
            if(mails.size()>0){
                system.debug('***Mail Size:'+mails.size());
                sendEmail(mails);
            }      
        }catch(Exception e){
        }
    }//END: leadEmailNotifications
    
    //START: createEmail for Lead Email Notification. by prashant.singh@riverbed.com on 10/16/2011
    private static Messaging.SingleEmailMessage createEmail(String msg, String subj, String toAddress)
    // PASS: String (body text for email), String (Subject for email), String (recipient address for email)
    // DOES: Sends the email
    {
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] rcpt = new String[] { toAddress };
         
        email.setReplyTo('noreply@salesforce.com');
        email.setSaveAsActivity(false);
        email.setToAddresses(rcpt);
        email.setSubject(subj);
        email.setPlainTextBody(msg);
        return email;
       // Messaging.SendEmailResult[] sendResult = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { email });
    }
    
     // PASS: String (Type of message), Lead (The current lead record)
    // RETURNS: String (Text for email subject)
    private static String getSubject(String msgType, Lead l){
        if(msgType == 'DealRegCreated'){
            return 'New deal registration created - ' + l.Company;
        }else if(msgType == 'LeadQualified'){
            return 'Partner lead qualified - ' + l.Company + ' - convert required';
        }        
        return '';
    }
    
    // PASS: String (Type of message), Lead (The current lead record), User (Partner user who filed the lead)
    // RETURNS: String (Body text for email message)
    private static String getMessage(String msgType, Lead l, User u){
        String msg = '';
        String partnerAction = '';
        String accManAction = '';        
        if(msgType == 'DealRegCreated'){
            partnerAction = 'submitted a deal registration with the following details to be reviewed';
            accManAction = 'review lead registration';
        }else if(msgType == 'LeadQualified'){
            partnerAction = 'qualified a lead with the following details';
            accManAction = 'review the lead and convert it for the partner';
        }        
        String title = (l.Salutation == null) ? '<title unknown>' : l.Salutation;
        String firstName = (l.FirstName == null) ? '<first name unknown>' : l.FirstName;        
        msg += u.Name + ' of ' + u.Contact.Account.Name + ' has ' + partnerAction + '.\n\n';
        msg += 'Company: ' + l.Company + '\n';
        msg += 'Contact: ' + title + ' ' + firstName + ' ' + l.LastName + '\n';
        msg += 'Created: ' + l.CreatedDate + '\n\n';
        msg += 'Please ' + accManAction + '.\n\n';
        msg += 'You can view the lead at https://emea.salesforce.com/' + l.Id;        
        return msg;
    }
/*------------------------------------ END: leadEmailNotification -------------------------------------*/

/*------------------------------------ START: emailLoginDetails ---------------------------------------*/
//  Start:For emailLoginDetails trigger bulk handling. by prashant.singh@riverbed.com on 10/17/2011 
    public static void sendEmailLoginDetails(List<Developer__c> devLoginDetails){
        Messaging.SingleEmailMessage mail;
        List<Messaging.SingleEmailMessage> mails=new List<Messaging.SingleEmailMessage>();
        for(Developer__c d:devLoginDetails){
            mail=createLoginMail(d);
            if(mail!=null){
                mails.add(mail);
            }
        }
        if(mails.size()>0){
            sendEmail(mails);
        }
        
    }//END
    
    private static Messaging.SingleEmailMessage createLoginMail(Developer__c dev){
        String subj = 'Your Stingray developer account';
        String msg = 'Hi ' + dev.FirstName__c + ',\n\n';
        msg += 'Your Stingray developer account has been created.\n\n';
        msg += '\tUsername: ' + dev.Email__c + '\n';
        msg += '\tPassword: ' + dev.Password__c + '\n';
        msg += '\tExpiry date: ' + dev.Expires__c.format() + '\n\n';
        msg += 'You can download the software binaries and license keys from http://www.zeus.com/downloads/developers/downloads.php.\n\n';
        msg += 'Developer licenses are not supported.  If you require the assistance of experienced Stingray consultants ';
        msg += 'as you evaluate Riverbed solutions, please commence a managed evaluation at http://forms.riverbed.com/forms/StingrayTrafficManagerEvaluationRegistration\n\n';
        msg += 'Regards,\n\nRiverbed Technology\n';
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] rcpt = new String[] { dev.Email__c };
             
        email.setReplyTo('noreply@riverbed.com');
        email.setSenderDisplayName('Riverbed Technology');
        email.setSaveAsActivity(false);
        email.setToAddresses(rcpt);
        email.setSubject(subj);
        email.setPlainTextBody(msg);
        return email;
    }
/*------------------------------------ END: emailLoginDetails -----------------------------------------*/

/*---------------------------------- START:For amazonNewProductEmailConfirmations -------------------------------------*/
    public static void amazonNewProductEmailConfirmations(List<AmazonAccount__c> newTrigger, Map<Id,AmazonAccount__c> oldTrigger){
        String msg,subj,rcpt,accNo='';
        Integer preSupportLength,postSupportLength,preProductsLength,postProductsLength;
        boolean emailRequired = false;
        set<ID> accIds=new set<ID>();
        Map<Id,Account> accMap;
        AmazonAccount__c pre;
        Messaging.SingleEmailMessage mail;
        List<Messaging.SingleEmailMessage> mails=new List<Messaging.SingleEmailMessage>();
        for(AmazonAccount__c temp:newTrigger){
            if(temp.Account__c!=null){
                accIds.add(temp.Account__c);
            }
        }
        if(accIds.size()>0){
            accMap=new Map<Id,Account>([SELECT AccountNumber FROM Account WHERE Id IN :accIds]);
        }
        for(AmazonAccount__c post:newTrigger ){
            rcpt = post.ContactEmail__c;
            pre=oldTrigger.containsKey(post.Id)? oldTrigger.get(post.Id):null;
            preSupportLength = (pre.Support__c == null) ? 0 : pre.Support__c.length();
            postSupportLength = (post.Support__c == null) ? 0 : post.Support__c.length();
            preProductsLength = (pre.Products__c == null) ? 0 : pre.Products__c.length();
            postProductsLength = (post.Products__c == null) ? 0 : post.Products__c.length();
            if(accMap!=null && accMap.size()>0){
                accNo=accMap.containsKey(post.Account__c)?accMap.get(post.Account__c).AccountNumber:'';
            }
            if(preSupportLength < postSupportLength){
                emailRequired = true;
                subj = 'Welcome to Riverbed Support';
                msg = 'Welcome to Riverbed Support. In the following email you will find\n';
                msg += 'information relating to the Support services, their operation, how you\n';
                msg += 'contact support, and procedural details.\n';                              
                msg += '\n';                                                                      
                msg += 'To enable the Riverbed Support Team to deal with your requests in a timely\n';
                msg += 'and efficient manner, it is essential that the processes are adhered\n';  
                msg += 'to, any deviation may cause delays to your request.\n';                   
                msg += '\n'; 
                msg += 'Whenever you contact Riverbed Support, please quote your account number which is ' + accNo + '.\n';                                                                     
                msg += '\n';                                                                      
                msg += 'Riverbed Community\n';                                                         
                msg += '==================\n';                                                      
                msg += 'A wealth of constantly reviewed and revised information on Product\n';    
                msg += 'Usage, common scenarios, and Feature articles is available at:\n';        
                msg += '\n';                                                                      
                msg += 'http://community.riverbed.com/\n';                                         
                msg += '\n';                                                                      
                msg += 'Contract Types\n';                                                        
                msg += '==============\n';                                                        
                msg += 'You will have either a Standard or Premium support contract, please see\n';
                msg += '\n';                                                                       
                msg += 'http://www.zeus.com/support/packages\n';                                   
                msg += '\n';                                                                       
                msg += 'which details full explanations of service type, requirements and\n';      
                msg += 'variations associated with contract levels.\n';                            
                msg += '\n';                                                                       
                msg += 'Technical Services\n';                                                     
                msg += '==================\n';                                                     
                msg += 'Details of Support procedures including escalation and Out of Hour\n';     
                msg += 'processes are covered in:\n';                                              
                msg += '\n';                                                                       
                msg += 'http://www.riverbed.com/us/support/available_plans\n';
                msg += '\n';                                                                       
                msg += '\n';                                                                       
                msg += 'Contacting Support: UK & Rest of World\n';                                 
                msg += '==========================================\n';                             
                msg += 'Standard Contract: 08:00 to 23:59 hours GMT (Monday - Friday excluding\n'; 
                msg += 'UK Bank Holidays)\n';
                msg += 'Non-critial queries: email support@riverbed.com\n';
                msg += '\n';
                msg += 'Critical queries - within support hours: email support@riverbed.com - call\n';
                msg += '+44 1223 720 463 or 1-888-ZEUS-123 (USA)\n';
                msg += '\n';
                msg += 'Premium Contract: 08:00 TO 23:59 hours GMT (Monday - Friday excluding\n';
                msg += 'UK Bank Holidays)\n';
                msg += 'Non-critical queries: email support@riverbed.com [Include your customer\n';
                msg += 'number in the subject header of the email]\n';
                msg += '\n';
                msg += 'Critical queries - within support hours: email support@riverbed.com   - or\n';
                msg += 'call +44 1223 720 463 or 1-888-ZEUS-123 (USA)\n';
                msg += '\n';
                msg += 'Critical queries : Outside of support hours: call +44 1223 472 190 or\n';
                msg += '1-888-ZEUS-123 (USA) - quoting your customer number.\n';
                msg += '\n';
                msg += 'Contacting Support: US Customers\n';
                msg += '================================\n';
                msg += 'Standard Contract: 08:00 to 23:59 hrs UTC (Monday - Friday excluding UK\n';
                msg += 'Bank Holidays)\n';
                msg += 'Non-critial queries: email support@riverbed.com [Include your customer\n';
                msg += 'number in the subject header of the email]\n';
                msg += '\n';
                msg += 'Critical queries - within support hours: email support@riverbed.com - call\n';
                msg += '+1 650 965 4627 or 1-888-ZEUS-123 (USA)\n';
                msg += 'Premium Contract: 08:00 TO 23:59 hours UTC (Monday - Friday excluding\n';
                msg += 'UK Bank Holidays)\n';
                msg += 'Non-critical queries: email support@riverbed.com\n';
                msg += '\n';
                msg += 'Critical queries - within support hours: email support@riverbed.com   - or\n';
                msg += 'call +1 650 965 4627 or 1-888-ZEUS-123 (USA)\n';
                msg += '\n';
                msg += 'Critical queries : Outside of support hours: call +1 650 965 4627 or\n';
                msg += '1-888-ZEUS-123 (USA)\n';
                msg += '\n';
                msg += 'Customer Technical Team\n';
                msg += '======================\n';
                msg += 'Please provide up to 2 named individuals for Standard contracts, and up\n';
                msg += 'to 6 named individuals for Premium contracts, who will be authorised\n';
                msg += 'from your organisation to contact Riverbed Support.\n';
                msg += '\n';
            }else if((preProductsLength < postProductsLength)||(pre.ContactEmail__c == null && post.ContactEmail__c != null)){
                emailRequired = true;
                subj = 'Thank you for purchasing a Riverbed product on Amazon EC2';
                msg = 'Thank you for signing up to use Riverbed software on Amazon EC2.\n\n'; 
                msg += 'What next?\n';
                msg += 'Refer to the EC2 Getting Started Guide at https://support.riverbed.com/docs/stingray/trafficmanager.htm '; 
                msg += 'to learn how to launch your software on Amazon EC2.\n\n';
                msg += 'Contact us:\n';
                msg += 'This subscription has been assigned to the Riverbed customer account ' + accNo + '.  '; 
                msg += 'All Stingray products on EC2 come with free Standard technical support; to contact our support team please email support@riverbed.com ';
                msg += 'and include your account number in the subject line.  ';
                msg += 'If you would like to check your support status, or purchase Premium support, please go to http://www.zeus.com/stingray-traffic-manager-ec2/choose_support.php\n\n';
                msg += 'Regards,\n\n';
                msg += 'Riverbed Technology\n';
            }
            if(emailRequired && rcpt != null){        
                mail=sendEmail(subj, msg, rcpt);
                mails.add(mail); 
            }
        }
        if(mails.size()>0){
            sendEmail(mails);
        } 
    }
    private static Messaging.SingleEmailMessage sendEmail(String subj, String msg, String rcpt){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] rcptArr = new String[] { rcpt };
        email.setSenderDisplayName('Riverbed Technology');         
        email.setReplyTo('noreply@riverbed.com');
        email.setSaveAsActivity(false);
        email.setToAddresses(rcptArr);
        email.setSubject(subj);
        email.setPlainTextBody(msg);
        return email;
    }
/*---------------------------------- END:For amazonNewProductEmailConfirmations -------------------------------------*/   
/*---------------------------------- START: setAccountNumber --------------------------------------------------------*/
public static void setAccountNumber(List<Account> newTrigger){
    /*List<Account> accList=[SELECT Id,AccountNumber,Type FROM Account WHERE Type = 'Customer EC2' AND AccountNumber != null];
    for(Account a:accList){
        accMap.put(a.AccountNumber,a);
    }*/
    for(Account a: newTrigger){             
        if(a.Type!=Null && a.Type.equalsIgnoreCase('Customer EC2')){
            String aNumber = Generator.getNewCustomerNumber('A'); // 'A' for Amazon
            a.AccountNumber = aNumber;
            a.Zeus_Account_Number__c = aNumber;
        }else if(a.AccountNumber == null){
            String aNumber = Generator.getNewCustomerNumber('S'); // 'S' for SPLA
            a.AccountNumber = aNumber;
            a.Zeus_Account_Number__c = aNumber;
        }
    }
}
public static Boolean checkCustomerNumber(String customerNumber){
    Boolean recCount=false;
    /*if(accMap!=null){
        recCount=accMap.containsKey(customerNumber);
    }*/
    return recCount;    
}
/*---------------------------------- END: setAccountNumber ----------------------------------------------------------*/

}