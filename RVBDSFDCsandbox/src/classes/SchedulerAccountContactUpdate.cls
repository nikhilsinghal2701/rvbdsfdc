global class SchedulerAccountContactUpdate implements Schedulable
{
  global void execute(SchedulableContext SC) {
     BatchAccountRollups bar = new BatchAccountRollups() ;
     Database.executeBatch(bar,2);
  }
}