@isTest
public class TestCaseEscalation {
    
    private static testmethod void testCaseEsc() {
        
        Account acc = new Account(Name = 'Test');
        insert acc;
        Contact con = new Contact(LastName = 'Test');
        insert con;
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', cc_Contact_Email__c = con.id, ContactId = con.id,
                Escalation_Status__c = null);
        insert cObj;
        
        CaseComment cmnt = new CaseComment(CommentBody = 'Test', IsPublished = false, ParentId = cObj.id);
        insert cmnt;
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test';
        cObj.Environment_Details__c = 'Test';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test';
        cObj.Issue_First_Seen__c = 'Test';
        cObj.Problem_Definition__c = 'Test';
        cObj.Problem_Details__c = 'Test';
        cObj.Recent_Changes__c = 'Test';
        cObj.Relevant_Data__c = 'Test';
        cObj.Reproducibility__c = 'Test';
        cObj.Version__c = 'Test';
        cObj.Staff_Engineer__c = 'Chris Geary';
        update cObj;
        Test.startTest();
        cObj.Escalation_Status__c = 'Closed';
        cObj.Staff_Engineer__c = 'Danny Yow';
        update cObj;
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Re_escalation_Reason__c = 'Test';
        cObj.Current_Status__c = 'Test';
        
        update cObj;
        
        Test.stopTest();
        
    }
    
    private static testmethod void testCaseCloseEsc() {
        
        Case cObj = new Case(Status = 'New', Description = 'Desc', Escalation_Comment__c = 'Test',
                Escalation_Status__c = null, Staff_Engineer__c = 'Danny Yow');
        insert cObj;
        
        
        cObj.Escalation_Status__c = 'Open';
        cObj.Escalation_Comment__c = 'Test';
        cObj.Environment_Details__c = 'Test';
        cObj.Escalation_Priority__c = 'High';
        cObj.Escalation_Severity__c = 'P1';
        cObj.Frequency__c = 'Test';
        cObj.Installation_Status__c = 'Test';
        cObj.Issue_First_Seen__c = 'Test';
        cObj.Problem_Definition__c = 'Test';
        cObj.Problem_Details__c = 'Test';
        cObj.Recent_Changes__c = 'Test';
        cObj.Relevant_Data__c = 'Test';
        cObj.Reproducibility__c = 'Test';
        cObj.Version__c = 'Test';
        cObj.Staff_Engineer__c = 'Chris Geary';
        update cObj;
        
        Test.startTest();
        cObj.Status = 'Closed';
        update cObj;
        
        Test.stopTest();
        
    }
}