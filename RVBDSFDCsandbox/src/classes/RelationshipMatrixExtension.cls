public with sharing class RelationshipMatrixExtension{
	Relationship_Matrix__c rm{get;set;}
	Account_Plan__c ap=null;
	public RelationshipMatrixExtension(ApexPages.StandardController controller){
			this.rm = (Relationship_Matrix__c)controller.getRecord();
			ap=[select id, name,AccountId__c from Account_Plan__c where id=:rm.Account_Plan__c];
	}
	//custome save and redirect to account plan page
	public pageReference relationshipMatrixSave(){
		if(rm!=null){    	
	    	try{
	    		upsert rm;
	    	}catch(Exception e){
	    		system.debug('Error:+e.getMessage()');
	    	}
	    	PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        	accPage.setRedirect(true);
        	return accPage;
		}
		return null;    	
    }
    //custome cancel and redirect to account plan page
    public pageReference relationshipMatrixCancel(){
    	PageReference accPage = new PageReference ('/apex/AccountPlanPage?Id='+ap.Id+'&aId='+ap.AccountId__c+'&tabFocus=name1'+'&mode=edit');
        accPage.setRedirect(true);
        return accPage;
    }
}