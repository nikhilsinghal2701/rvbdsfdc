@isTest
public with sharing class Test_INQ_CaseLinkBugField_Trigger {
    static testMethod void test_INQ_CaseLinkBugField_Trigger() {
        List<Case> testCases = new List<Case>();
        testCases.add(new Case(Status = 'Needs Attention', Priority = 'P3 - Disruptive', Origin = 'Web', Products__c = 'Hardware', Subject = 'AUTOPOPULATECASEBUGFILED TestMethod Case 1'));
        testCases.add(new Case(Status = 'Working', Priority = 'P4 - Support Request', Origin = 'Web', Products__c = 'CMC', Subject = 'AUTOPOPULATECASEBUGFILED TestMethod Case 2'));
        insert testCases;
        
        testCases = [SELECT Id, Bug_No__c FROM Case WHERE Subject LIKE :'AUTOPOPULATECASEBUGFILED TestMethod%'];
        
        List<InQuira_Article_Info__c> testArts = new List<InQuira_Article_Info__c>();
        testArts.add(new InQuira_Article_Info__c(Display_URL__c = 'http://supportkb.riverbed.com/support/index?page=content&bugzilla=external&id=12345', Title__c = 'AUTOPOPULATECASEBUGFILED Test Solution 12345', Document_ID__c = '12345', Document_GUID__c = 'aefaefaefaefaefaef', Article_Type__c = 'External'));
        testArts.add(new InQuira_Article_Info__c(Display_URL__c = 'http://supportkb.riverbed.com/support/index?page=content&bugzilla=external&id=67890', Title__c = 'AUTOPOPULATECASEBUGFILED Test Solution 67890', Document_ID__c = '67890', Document_GUID__c = 'guidguidguidguidgu', Article_Type__c = 'External'));
        testArts.add(new InQuira_Article_Info__c(Display_URL__c = 'http://supportkb.riverbed.com/support/index?page=content&id=75215&bugzilla=external', Title__c = 'AUTOPOPULATECASEBUGFILED Test Solution 75215', Document_ID__c = '75215', Document_GUID__c = 'nknlknlkncsknkcsks', Article_Type__c = 'External'));
        insert testArts;
        
        testArts = [SELECT Id, Display_URL__c FROM InQuira_Article_Info__c WHERE Title__c LIKE :'AUTOPOPULATECASEBUGFILED Test Solution%'];
        
        List<InQuira_Case_Info__c> testLinks = new List<InQuira_Case_Info__c>();
        for (Case c : testCases) {
            for (InQuira_Article_Info__c iai : testArts) {
                testLinks.add(new InQuira_Case_Info__c(Article_Version__c = 1.0, Related_Case__c = c.Id, Related_InQuira_Article__c = iai.Id));
            }
        }
        insert testLinks;
        
        testLinks = [SELECT Id FROM InQuira_Case_Info__c WHERE Related_Case__c = :testCases.get(0).Id OR Related_Case__c = :testCases.get(1).Id];
        
        testCases = [SELECT Id, Bug_No__c FROM Case WHERE Subject LIKE :'AUTOPOPULATECASEBUGFILED TestMethod%'];
        testArts = [SELECT Id, Display_URL__c FROM InQuira_Article_Info__c WHERE Title__c LIKE :'AUTOPOPULATECASEBUGFILED Test Solution%'];
        for (Case c : testCases) {
            for (InQuira_Article_Info__c iai : testArts) {
                System.assert(c.Bug_No__c.contains((iai.Display_URL__c.split('id=')[1]).split('&')[0]));
            }
        }
        try{
	        Test.startTest();
	        delete testLinks.get(1);
	        delete testLinks.get(0);
	        delete testLinks.get(2);
	        delete testLinks.get(3);
	        delete testLinks.get(4);
	        delete testLinks.get(5);
	        Test.stopTest();
        }catch(Exception e){
        	
        }
        testCases = [SELECT Id, Bug_No__c FROM Case WHERE Subject LIKE :'AUTOPOPULATECASEBUGFILED TestMethod%'];
        for (Case c : testCases) {
            System.assert(c.Bug_No__c == null || c.Bug_No__c == '');
        }
    }
}