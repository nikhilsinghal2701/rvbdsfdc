@isTest
public class TestEvalEndDateHTTPCallOut {
    public static testmethod void testEVAlEndDateCallout() {
        Test.setMock(HttpCalloutMock.class, new TestMockHttpResponseGenerator());
        set<ID> ast=new Set<ID>();
        Account a = new Account();
        a.Name = 'test account';
        a.Product_Subscriptions__c='test;test:';
        insert a;
        Asset a1 = new Asset(Name='testAsset', RVBD_Product_Family__c= 'test',IB_Status__c='Under Evaluation',Instance_Number__c='77777777XX',AccountID=a.id);
        insert a1;
        ast.add(a1.id);
        
        Contact foo = new Contact(FirstName = 'Foo', LastName='Bar');
        insert foo;
        Case c = new Case(Subject = 'Test case', ContactId=foo.id,AssetID=a1.id,AccountID=a.id);
        Insert c;
        
        PageReference pageRef = Page.RefreshEvalEndDate;
        Test.setCurrentPage(pageRef);
        Apexpages.StandardController stdController = new Apexpages.StandardController(a1);
        Apexpages.currentPage().getParameters().put('id',a1.Id);
        Test.startTest();
        EvalEndDateHTTPCallOut controller = new EvalEndDateHTTPCallOut(StdController);
        controller.parseJSONResponse();
        EvalEndDateHTTPCallOut.parseJSONResponseFuture(ast);
        Test.stopTest();
    }
}