public with sharing class PortletController {
	
	public String tabName { get; set; }
	private List<Portlet_Left_Navigation__c> leftNavs;
	private List<Portlet_Headline__c> headlines;
	public Id selectedNav { get; set;}
	private Set<String> groupMemberList;

	public PortletController() {
		tabName = ApexPages.currentPage().getParameters().get('tabName');
/*		//get user group
		groupMemberList = new Set<String>(); 
		for (GroupMember gm : [Select g.Group.Id, g.Group.Name From GroupMember g where g.UserOrGroupId = :Userinfo.getUserId()]) {
			System.debug('gm = ' + gm.Group);
			groupMemberList.add(gm.group.Name);
		}
*/		
		/*leftNavs = [Select p.Name, p.Id, 
			(Select Group__c, Document_Name__c, Document_Url__c, Open_In_New_Window__c From Portlet_Right_Navigations__r
				where Active__c = true and (Group__c In :groupMemberList OR Group__c = null)) 
			From Portlet_Left_Navigation__c p where Active__c = true 
				AND Portal_Tab__r.Tab__c = :tabName];
		leftNavs = [Select p.Name, p.Id from Portlet_Left_Navigation__c p];
		if (leftNavs.size() > 0) {
			selectedNav = leftNavs.get(0).Id;
		}*/
	}
	
	public List<Portlet_Right_Navigation__c> getRightNavs() {
		if (selectedNav == null && leftNavs.size() > 0) {
			selectedNav = leftNavs.get(0).Id;
		}
		for (Portlet_Left_Navigation__c leftNav : leftNavs) {
			if (leftNav.Id == selectedNav) {
				List<Portlet_Right_Navigation__c> rightNavs = leftNav.Portlet_Right_Navigations__r;
				return  rightNavs;
			}
		}
		return new List<Portlet_Right_Navigation__c>();
	}
	
	public List<Portlet_Headline__c> getHeadlines() {
		if (headlines == null) {
/*			headlines = [Select p.Name, p.Id, p.Description__c,
				(Select Name, Group__c, Document_Name__c, Document_Url__c, Open_In_New_Window__c From Portlet_Right_Navigations__r
				where Active__c = true and (Group__c In :groupMemberList OR Group__c = null)) 
				From Portlet_Headline__c p where Active__c = true 
				AND Portal_Tab__r.Tab__c = :tabName];*/
			headlines = [Select p.Name, p.Id, p.Description__c, p.Sort__c, p.Image__c,
				(Select Name, Document_Name__c, Document_Url__c, Open_In_New_Window__c From Portlet_Right_Navigations__r
				where Active__c = true) 
				From Portlet_Headline__c p where Active__c = true 
				AND Portal_Tab__r.Tab__c = :tabName AND Portal_Tab__r.Active__c = true order by p.Sort__c asc];
		}
		System.debug('headlines = ' + headlines.size());
		return headlines;
	}

	public PageReference selectLeftNav() {
		selectedNav = ApexPages.currentPage().getParameters().get('leftNavId');
		return null;
	}
	
	public List<Portlet_Left_Navigation__c> getLeftNavs() {
		if (leftNavs == null) {
/*			leftNavs = [Select p.Name, p.Id, 
				(Select Group__c, Document_Name__c, Document_Url__c, Open_In_New_Window__c From Portlet_Right_Navigations__r
					where Active__c = true and (Group__c In :groupMemberList OR Group__c = null)) 
				From Portlet_Left_Navigation__c p where Active__c = true 
					AND Portal_Tab__r.Tab__c = :tabName];*/

			leftNavs = [Select p.Name, p.Id, 
				(Select Document_Name__c, Document_Url__c, Open_In_New_Window__c From Portlet_Right_Navigations__r
					where Active__c = true) 
				From Portlet_Left_Navigation__c p where Active__c = true 
					AND Portal_Tab__r.Tab__c = :tabName AND Portal_Tab__r.Active__c = true Order By Name desc];
					
		}
		return leftNavs;
	}
	
	public Boolean getTabActive() {
		List<Portal_Tab__c> portalTabs = [Select Active__c from Portal_Tab__c where Tab__c = :tabName limit 1];
		if (portalTabs.size() > 0) {
			return portalTabs.get(0).Active__c;
		}
		return false;
	}
	
	public String getDealRegUrl() {
		String dealRegUrl = '';
		Id profileId = [Select ProfileId from User where Id = :UserInfo.getUserId()].ProfileId;
		if (profileId == '00e70000000vPmr' || profileId == '00e70000000vPn1' || profileId == '00e70000000vPms' || 
			profileId == '00e70000000wXnJ' || profileId == '00e70000000wXnO') 
		{//apac & indirect Var
			dealRegUrl = '/00Q/e?retURL=%2F00Q%2Fo&RecordType=0127000000057yd';
		} else if (profileId == '00e70000000wXho' || profileId == '00e70000000wXmp' || profileId == '00e70000000wXmu'
		 || profileId == '00eA0000000MJVx' || profileId == '00eA0000000MJW2' || profileId == '00eA0000000MJW7') {//disti
			dealRegUrl = '/apex/Deal_Registration';
		} else {
			dealRegUrl = '/apex/Deal_Registration';
		}
		return dealRegUrl;
	}

}