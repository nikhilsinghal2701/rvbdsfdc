public with sharing class CaseHttp {
    @Future(callout=true)
    public static void getTheCase(Set<Id> Cids){
        for(Id i : Cids){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        if(RVBD_Email_Properties__c.getValues('SupportSite') != null){
            RVBD_Email_Properties__c CMD = RVBD_Email_Properties__c.getValues('SupportSite');
            if(CMD.Server_URL__c != null){
                req.setEndpoint(CMD.Server_URL__c+i);
                req.setMethod('GET');
                req.setTimeout(60000);
                String result;
                if(!Test.isRunningTest()){
                    HttpResponse res = h.send(req);
                    result = res.getBody();
                }
                system.debug(result);                
            }
        }        
    }
    }  
}