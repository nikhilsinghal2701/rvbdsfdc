/*
*Description:On Lead - Have a button which creates a task with End date based on a number input in days.
*Created By:Prashant.Singh@riverbed.com
*Created Date:Sep 25,2010
*/
global class CreateTaskWS {
	webService static String createTask(String days,String lId, String lOwner) {
		String result;
		Task newTask = new Task(); 
		newTask.Status ='Not started'; 
		newTask.Subject='Follow Up Call /Email'; 		 
		newTask.OwnerId=lOwner; 
		newTask.whoId=lId;		
		DateTime d = System.now();
		date myDate = date.newInstance(d.year(),d.month(), d.day());
		date newDate = mydate.addDays(integer.valueOf(days));
		newTask.ActivityDate=newDate;
		try{
			insert newTask;
			result='true';
		}catch(Exception e){
			result='false';
		}
		return result;
	}
}