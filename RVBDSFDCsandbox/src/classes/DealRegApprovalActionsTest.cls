@isTest
/* 
	This class covers tests for following actions:
		1. Request More info
		2. Reply with More info
		3. Reassign Request
		4. Reject with Reason
		5. Request Opportunity on Rejected Lead
		
	Author: Ankita Goel
	Date: 1/8/2010
*/
private class DealRegApprovalActionsTest {

	static testmethod void testRequestMoreInfo(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDR();
		//submit for approval
		DealRegApprovalActionTestUtil.submitForApproval(l);
		//approver requests more info
        PageReference pageRef = Page.ApproverActions;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
        System.currentPageReference().getParameters().put('mode','info');
		DealRegApprovalActions con = new DealRegApprovalActions(sc);
		//l.Rejection_Reason__c = 'Already registered to another partner';
		l.Pending_Reason__c = 'Pending Meeting';
		l.Comments__c = 'Need to set up Meeting';
		con.saveAction();
		Lead lead = [select Status from Lead where id = :l.Id];
		System.assertEquals('Pending - More Info',lead.Status);

		//Request Opportunity
        PageReference pageRef1 = Page.RequestOpportunity;
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController sc1 = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
		DealRegApprovalActions cont = new DealRegApprovalActions(sc1);
		cont.request();
		Lead lead1 = [select Opportunity_Requested_On_Rejected_Lead__c from Lead where id = :lead.Id];
		System.assertEquals(false,lead1.Opportunity_Requested_On_Rejected_Lead__c);
	}
	
	static testmethod void testReply(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDRD();
		//submit for approval
		DealRegApprovalActionTestUtil.submitForApproval(l);
		//approver requests more info
        PageReference pageRef = Page.ApproverActions;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
        System.currentPageReference().getParameters().put('mode','info');
		DealRegApprovalActions con = new DealRegApprovalActions(sc);
		//l.Rejection_Reason__c = 'Already registered to another partner';
		l.Pending_Reason__c = 'Pending Meeting';
		l.Comments__c = 'Need to set up Meeting';
		con.saveAction();
		Lead lead = [select Status from Lead where id = :l.Id];
		System.assertEquals('Pending - More Info',lead.Status);
		//Partner replies with More Info
        PageReference pref = Page.ReplyMoreInfo;
        Test.setCurrentPage(pref);
        ApexPages.StandardController sc1 = New ApexPages.StandardController(lead);        
        System.currentPageReference().getParameters().put('id',lead.id);
		DealRegApprovalActions cont = new DealRegApprovalActions(sc1);
		cont.reply();
		lead.Comments__c = 'Replying with more info as Partner user';
		cont.submitReply();
		Lead l1 = [select Status from Lead where id = :lead.Id];
		System.assertEquals('Pending - Resubmitted', l1.status);
	}
	
	static testmethod void testReassign(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDR();
		//select new approver
		Id ProfileId = [select Id from Profile where name = :DealRegApprovalActionTestUtil.ISRPROFILE].Id;
		User u = [select Id from User where ProfileId = :ProfileId limit 1];
		l.Assign_Approver__c = u.id;
		update l;
		//submit for approval
		DealRegApprovalActionTestUtil.submitForApproval(l);
		//Reassign Request
        PageReference pageRef = Page.Reassign;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
        System.currentPageReference().getParameters().put('mode','reassign');
		DealRegApprovalActions con = new DealRegApprovalActions(sc);
		con.reassign();
		List<ProcessInstance> pInstance = [Select p.TargetObjectId, p.Status, p.Id, 
					(Select Id, ActorId, Actor.Name, IsDeleted From Workitems order by SystemModStamp limit 1) 
					From ProcessInstance p 
					where targetobjectid = :l.Id and
					status = 'Pending'];
		System.debug('ProcessInstance  = '+ pInstance );	
		Id actor = pInstance.get(0).WorkItems[0].ActorId;
		//System.assertEquals(u.Id,actor);
	}
	
	static testmethod void testReject(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDRD();
		//submit for approval
		DealRegApprovalActionTestUtil.submitForApproval(l);
		//Reject lead
        PageReference pageRef = Page.ApproverActions;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
        System.currentPageReference().getParameters().put('mode','reject');
		l.Rejection_Reason__c = 'Already registered to another partner';
		update l;
		DealRegApprovalActions con = new DealRegApprovalActions(sc);
		l.Comments__c = 'Rejecting Lead';
		con.saveAction();
		Lead lead = [select Status from Lead where id = :l.Id];
		System.assertEquals('Rejected',lead.Status);
	}
	
	static testmethod void testRequestOpp(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDR();
		//submit for approval
		DealRegApprovalActionTestUtil.submitForApproval(l);
		//Reject lead
        PageReference pageRef = Page.ApproverActions;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
        System.currentPageReference().getParameters().put('mode','reject');
		l.Rejection_Reason__c = 'Already registered to another partner';
		update l;
		DealRegApprovalActions con = new DealRegApprovalActions(sc);
		l.Comments__c = 'Rejecting Lead';
		con.saveAction();
		Lead lead = [select Status from Lead where id = :l.Id];
		System.assertEquals('Rejected',lead.Status);
		//Request Opportunity
        PageReference pageRef1 = Page.RequestOpportunity;
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController sc1 = New ApexPages.StandardController(l);        
        System.currentPageReference().getParameters().put('id',l.id);
		DealRegApprovalActions cont = new DealRegApprovalActions(sc1);
		cont.request();
		Lead lead1 = [select Opportunity_Requested_On_Rejected_Lead__c from Lead where id = :lead.Id];
		System.assertEquals(true,lead1.Opportunity_Requested_On_Rejected_Lead__c);
	}
}