/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
public class ObjectHistoryUtil {

public static boolean firstRun = true; 

public static void AfterUpdateAssetHistory(List<Asset> asts,Map<id,Asset>oldMap){
    
List<Schema.FieldSetMember> trackedFields = SObjectType.Asset.FieldSets.Asset_History_Tracking_Fields.getFields();
system.debug('trackedFields*******'+trackedFields);
system.debug('inputs*******'+oldMap+'########'+asts);
if (trackedFields.isEmpty()) return;

 List<AssetHistory__c> fieldChanges = new List<AssetHistory__c>();
 List<AssetHistory__c> astDelete = new List<AssetHistory__c>();
 List<string> apiNameList = new List<string>();        
 system.debug('Delete*******'+Trigger.isDelete);
    
        for (Asset aNew : asts) {
            system.debug('InLoopT*******');
            Asset aOld = oldmap.get(aNew.Id); 
            
            for (Schema.FieldSetMember fsm : trackedFields) { 
                     
                    String fieldName  = fsm.getFieldPath();
                    String fieldLabel = fsm.getLabel();
                    if (aNew.get(fieldName) != aOld.get(fieldName)) {
                    String oldValue = String.valueOf(aOld.get(fieldName));
                    String newValue = String.valueOf(aNew.get(fieldName));
                    if (oldValue != null && oldValue.length()>255) oldValue = oldValue.substring(0,255);
                    if (newValue != null && newValue.length()>255) newValue = newValue.substring(0,255); 
                    
                    AssetHistory__c aht = new AssetHistory__c();
                    aht.AssetID__c=aNew.Id; 
                    aht.Asset__c=aNew.ID;
                    aht.Field__c   = fieldName;
                    aht.SerialNumber__c=aNew.Name;
                    aht.Name=aNew.Name;
                    aht.OldValue__c  = oldValue;
                    aht.NewValue__c  = newValue;
                    
                    apiNameList.add(aht.Field__c);
                    system.debug('AHT*******'+aht);
                    fieldChanges.add(aht);
                }        
            }
    
         if (!fieldChanges.isEmpty() && ObjectHistoryUtil.firstRun==True) {
         insert fieldChanges;
         ObjectHistoryUtil.firstRun=false;
         }
    }
}

public static void AfterInsertAssetHistory(List<Asset> asts){
 
  List<AssetHistory__c> fieldChanges = new List<AssetHistory__c>();
    for(Asset aNew : asts){
        AssetHistory__c aht = new AssetHistory__c();
        aht.AssetID__c=aNew.Id;
        aht.Asset__c=aNew.ID;
        aht.Asset_CreatedDate__c= system.now();
        fieldChanges.add(aht);  
        }
    if (!fieldChanges.isEmpty() && ObjectHistoryUtil.firstRun==True) {
         insert fieldChanges;
         ObjectHistoryUtil.firstRun=false;
         }
    }
   public static void BeforeDeleteAssetHistory(List<Asset> asts){ 
     List<AssetHistory__c> astDelete = [select id from AssetHistory__c where Asset__c in :asts and IsDeleted__c=false];
     List<AssetHistory__c> fieldChanges = new List<AssetHistory__c>();
            if(astDelete.size()>0){
            delete astDelete;}
         for(Asset aOld : asts){
             system.debug('inside*******');
                AssetHistory__c aht = new AssetHistory__c();
                aht.AssetID__c=aOld.Id; 
                aht.IsDeleted__c=True;
                aht.Deleted_On__c=system.now();
                fieldChanges.add(aht);  
            }
        
         if (!fieldChanges.isEmpty() && ObjectHistoryUtil.firstRun==True) {
             system.debug('inside fieldChanges insert *******'+ fieldChanges);
         insert fieldChanges;
         ObjectHistoryUtil.firstRun=false;
         }  
    }
}