public with sharing class AssetCasesCtrl {
    public List<Case> caseList {get;set;}
    public AssetCasesCtrl(){
        String ssno = ApexPages.currentPage().getParameters().get('sn') ;
        caseList = new List<Case> ();
        DateTime dt = System.now().addYears(-1);
        if(ssno != null){
            if(!test.isRunningTest()){
                caseList = [Select c.Status, c.Priority, c.CaseNumber, c.Asset.Name, c.AssetId, c.CreatedDate 
                             From Case c where c.Asset.Name = :ssno And CreatedDate >=  :dt Order By c.Asset.Name, CaseNumber DESC];//
            }else{
                caseList = [Select c.Status, c.Priority, c.CaseNumber, c.Asset.Name, c.AssetId, c.CreatedDate 
                             From Case c where c.Asset.Name = :ssno  Order By c.Asset.Name, CaseNumber DESC];
            }
        }
        
    }
}