global with sharing class EmailSupportSiteLinkController {	
	public Opnet_Group_Id__c opId{get;set;}
	
	public List<Opnet_Group_Id__c> getOgidList(){
		List<Opnet_Group_Id__c> ogidList = new List<Opnet_Group_Id__c>();		
		
		opId = [Select Contact__c, Id from Opnet_Group_Id__c where Id=:opId.Id].get(0);
		
		for(Opnet_Group_Id__c ogid : [Select Id,Name, Opnet_username__c,Contact__c,Opnet_Account_Group_Name__c From Opnet_Group_ID__c where Contact__c= :opId.Contact__c]){
            ogidList.add(ogid);
		}
		return ogidList;
		
	}
	
	public String getConUsername(){
		opId = [Select Contact__c, Id from Opnet_Group_Id__c where Id=:opId.Id].get(0);
		return [Select Email from Contact Where Id = :opId.Contact__c].get(0).Email;
	}
}