public Class GenerateCSV {
    public List<Opnet_Group_ID__c> getContacts() {
        Set<Id> conIds = new Set<Id>();
        List<Opnet_Group_ID__c> recList = new List<Opnet_Group_ID__c>();
        for(Opnet_Group_ID__c o : [Select  o.Contact__r.Renewal_Contact__c, o.Contact__r.FCRM__FCR_Status__c, o.Contact__r.Account.OracleCustomerId__c,
                                      o.Contact__r.User_Profile__c, o.Contact__r.Status__c, o.Contact__r.PartnerRole__c, 
                                      o.Contact__r.Department, o.Contact__r.Title, o.Contact__r.Email, o.Contact__r.MobilePhone, 
                                      o.Contact__r.Fax, o.Contact__r.Phone, o.Contact__r.Salutation, o.Contact__r.FirstName, 
                                      o.Contact__r.LastName, o.Contact__r.AccountId, o.Contact__r.Id, o.Contact__c 
                                      From Opnet_Group_ID__c o where Opnet_Is_Maintenance_Contact__c = true
                                      AND Contact__r.User_Profile__c != 'Deactivated' 
                                      AND o.Contact__r.Renewal_Contact__c  != true]){
              if(!conIds.contains(o.Contact__c)){
                  recList.add(o);
                  conIds.add(o.Contact__c);     
              }
          }
          return recList;
    }
}