/**
 * This class contains unit tests for validating the behavior of Apex triggers.
 * 1.CaseSupportHandlingNotesOnCreate
 * 2.CaseSupportHandlingNotesPopulate	
 */
@isTest
private class TestSupportHandlingNotesTriggers {

    static testMethod void testCaseSupportHandlingNotesPopulate() {
        // // create new account record
        //RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        //RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        String accPatRT='012300000000NE0AAM';
        String accCusRT='012300000000NE5AAM';
        List<Account> lstAcc=new List<Account>();
        
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=accCusRT;
        cusAcc.name='CustomerAccount';
        cusAcc.Type='Customer';
        cusAcc.Industry='Education';
        cusAcc.Support_Handling_Notes_del__c='customer account testing';
        lstAcc.add(cusAcc);
        
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        stpAcc.Support_Handling_Notes_del__c='stp account testing';
        insert stpAcc; 
        
        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId=accPatRT;
        tier2Acc.name='Trace 3';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Authorizations_Specializations__c='RASP';
        tier2Acc.Support_Handling_Notes_del__c='tier2 account testing';
        lstAcc.add(tier2Acc);
        
        Account tier3Acc=new Account();
        tier3Acc.RecordTypeId=accPatRT;
        tier3Acc.name='stpAccount';
        tier3Acc.Type='VAR';
        tier3Acc.Industry='Education';
        tier3Acc.RASP_Status__c='Not Reviewed';
        tier3Acc.Support_Handling_Notes_del__c='tier3 account testing';
        lstAcc.add(tier3Acc);
        
        Account influAcc=new Account();
        influAcc.RecordTypeId=accPatRT;
        influAcc.name='stpAccount';
        influAcc.Type='VAR';
        influAcc.Industry='Education';
        influAcc.RASP_Status__c='Not Reviewed';
        influAcc.Support_Handling_Notes_del__c='influence account testing';
        lstAcc.add(influAcc);
        
        insert lstAcc;
        
        Contact newCon=new Contact();
        newCon.AccountId=cusAcc.Id;
        newCon.FirstName='test';
        newCon.LastName='user';
        newCon.Support_Role__c='Support Admin';
        insert newCon;
        
        Case newCase=new Case();
        newCase.AccountId=cusAcc.Id;
        newCase.Status='New';
        newCase.Priority='P4 - Support Request';
        newCase.Origin='Email';
        newCase.ContactId=newCon.Id;
        insert newCase;
        
        cusAcc.Support_Handling_Notes_del__c='Updating support handling notes';
        update cusAcc;        
    }
}