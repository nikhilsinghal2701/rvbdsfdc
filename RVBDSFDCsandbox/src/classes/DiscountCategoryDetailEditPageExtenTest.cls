/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the test class for the class DiscountCategoryDetailEditPageExtension
Created Date : Oct 2011
*******************************************************************************************************/

@isTest
private class DiscountCategoryDetailEditPageExtenTest {

    static testMethod void DiscountCategoryDetailEditPageExtenTest() // Test Method for the class DiscountCategoryDetailEditPageExtension
     {
        test.starttest();
        Discount_Category_Detail__c Obj = new Discount_Category_Detail__c ();
        String detailListID;
        List<Category_Master__c> catList=DiscountApprovalTestHlp.insertCategoryMasterData();
        DiscountCategory__c dis = new DiscountCategory__c(Name='Discount 1');
        insert(dis);
        Discount_Category_Detail__c det;
       // Tests for mass edit from discount table record. 
        PageReference VFpage = Page.DiscountCategoryDetailEditPage;
        VFpage.getParameters().put('retUrl','/'+dis.id);
        test.setCurrentPage(VFpage);
         ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        DiscountCategoryDetailEditPageExtension CLS = new DiscountCategoryDetailEditPageExtension(VFpage_Extn);
        if(!CLS.detailList.isempty())
         {
            detailListID = CLS.detailList[0].Id;
            CLS.detailList[0].Standard_Discount__c = 10;
            CLS.detailList[0].Registered_Discount__c=15;
         }
         
       
        cls.save();
        if(detailListID!=null)
        det = [select Id,Standard_Discount__c,Registered_Discount__c from Discount_Category_Detail__c where Id=:detailListID];
        System.assertEquals(det.Standard_Discount__c,10); // Verifies that record is updated or not
        System.assertEquals(det.Registered_Discount__c,15);// Verifies that record is updated or not.
        cls.Cancel();
     // Tests mass edit for single record click scenario.   
         VFpage = Page.DiscountCategoryDetailEditPage;
        VFpage.getParameters().put('id','/'+det.id);
        test.setCurrentPage(VFpage);
        VFpage_Extn = new ApexPages.StandardController(obj);
         CLS = new DiscountCategoryDetailEditPageExtension(VFpage_Extn);
        if(!CLS.detailList.isempty())
         {
            detailListID = CLS.detailList[0].Id;
            CLS.detailList[0].Standard_Discount__c = 10;
            CLS.detailList[0].Registered_Discount__c=15;
         }
         
       
        cls.save();
        if(detailListID!=null)
        det = [select Id,Standard_Discount__c,Registered_Discount__c from Discount_Category_Detail__c where Id=:detailListID];
        System.assertEquals(det.Standard_Discount__c,10); // Verifies that record is updated or not
        System.assertEquals(det.Registered_Discount__c,15);// Verifies that record is updated or not.
        cls.Cancel();
        test.stopTest();
    }
}