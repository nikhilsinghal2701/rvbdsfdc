public with sharing class EmailCaseInfoToOpnetCustCtrl {
	public Case caseInContext{get;set;}
	
	public List<Case> getCaseList(){
		caseInContext = [Select ContactId from Case Where Id = :caseInContext.Id];
		
		List<Case> caseList = new List<Case>();
		for(Case c : [Select Legacy_Case_Number__c,CaseNumber from Case Where ContactId=:caseInContext.ContactId]){
			caseList.add(c);
		}
		
		return caseList;
	}
}