public with sharing class AccountShareHelper {
	List<AccountShare> newAccountShares = new List<AccountShare>();
    AccountShare thisAccountShare;
    private set<Id> accountIds=new set<Id>();
    private Map<Id,UserRole> userRoleMap = new Map<Id,UserRole>();
    private Map<Id,Group> groupMap = new Map<Id,Group>();
	// Added for Contact Share on Contact Update
	List<ContactShare> newContactShares = new List<ContactShare>();
    ContactShare thisContactShare;
   
    private Map<Id,UserRole> userRoleMapCon = new Map<Id,UserRole>();
    private Map<Id,Group> groupMapCon = new Map<Id,Group>();
    	
	public void insertIntoAccountShare(List<Distributor_Reseller__c> newTrigger){		
		accountIds = getAllAccountId(newTrigger);	
		userRoleMap = getAllUserRoles(accountIds);
		groupMap = getAllGroups(userRoleMap);			   			 	
		for(Distributor_Reseller__c theAccount : newTrigger){		
			thisAccountShare = new AccountShare();
			thisAccountShare.userorgroupid = groupMap.get((userRoleMap.get(theAccount.Account__c).Id)).Id;		
			thisAccountShare.accountid = theAccount.Distributor_Reseller_Name__c;		
			thisAccountShare.accountaccesslevel = 'Read';		
			thisAccountShare.OpportunityAccessLevel = 'None';		
			thisAccountShare.CaseAccessLevel = 'None';		
			thisAccountShare.ContactAccessLevel = 'Read';				
			newAccountShares.add(thisAccountShare);					
		}	
		try{
			insert newAccountShares;
		}catch(DMLException dme){
			system.debug('Exception:'+dme.getMessage());
		}	
	}
	
	/*
	Added this to handle account share rule upon account owner change, so that distributor/reseller
	will get list of reseller/disti respectively during deal reg.
	Added on: 5/3/2011
	Added by: prashant.singh@riverbed.com
	*/ //Start
	public void insertIntoAccountShare(set<ID> accountIds){
		Map<ID,List<ID>> drMap=new Map<ID,List<ID>>();
		List<ID> idList;
		List<Distributor_Reseller__c> drList=[select Id,Account__c,Distributor_Reseller_Name__c from
		Distributor_Reseller__c where Distributor_Reseller_Name__c IN:accountIds];
		if(drList!=null){
			for(Distributor_Reseller__c temp: drList){
				if(drMap.containsKey(temp.Distributor_Reseller_Name__c)){
					drMap.get(temp.Distributor_Reseller_Name__c).add(temp.Account__c);		
				}else{
					idList=new List<ID>();
					idList.add(temp.Account__c);
					drMap.put(temp.Distributor_Reseller_Name__c,idList);
				}
			}
		}		
		if(drMap.size()>0){
			accountIds = getAllAccountId(drList);
			userRoleMap = getAllUserRoles(accountIds);
			groupMap = getAllGroups(userRoleMap);
			for(ID theAccount : drMap.keySet()){
				for(Id accId:drMap.get(theAccount)){
					thisAccountShare = new AccountShare();
					thisAccountShare.userorgroupid = groupMap.get((userRoleMap.get(accId).Id)).Id;
					thisAccountShare.accountid = theAccount;
					thisAccountShare.accountaccesslevel = 'Read';		
					thisAccountShare.OpportunityAccessLevel = 'None';		
					thisAccountShare.CaseAccessLevel = 'None';		
					thisAccountShare.ContactAccessLevel = 'Read';				
					newAccountShares.add(thisAccountShare);
				}		
			}		
		}		
		try{
			if(newAccountShares.size()>0){
				insert newAccountShares;	
			}
		}catch(DMLException dme){
			system.debug('Exception:'+dme.getMessage());
		}	
	}
	//End
	
	
  	/*
	Added this to handle contact share rule upon contact owner change upon account owner update to give accessibility 
	to partner admin user, so that Partner user will have the accessibility of their company contact records.
	Added on: 9/14/2012
	Added by: raj.kumar@riverbed.com
	*/ //Start
	
	public void insertIntoContactShare(Map<Id,List<Contact>> accountContactMap){
		for(Id id:accountContactMap.keyset()){ 
			accountIds.add(id);
		}
		userRoleMapCon = getAllUserRoles(accountIds);
		groupMapCon = getAllGroups(userRoleMapCon); 	 	
		for( Id acctId : accountContactMap.keyset()){
			for(Contact con : accountContactMap.get(acctId)){						  
				thisContactShare = new ContactShare();			
				String tempRole = userRoleMapCon.get(acctId).name;
				If(tempRole.contains('Partner Executive')){
					thisContactShare.userorgroupid = groupMapCon.get((userRoleMapCon.get(acctId).Id)).Id;		
					thisContactShare.ContactId = con.Id;				
					thisContactShare.ContactAccessLevel = 'Edit';				
					newContactShares.add(thisContactShare);					
				}else if(tempRole.contains('Partner User')){
					thisContactShare.userorgroupid = groupMapCon.get((userRoleMapCon.get(acctId).Id)).Id;		
					thisContactShare.ContactId = con.Id;				
					thisContactShare.ContactAccessLevel = 'Read';				
					newContactShares.add(thisContactShare);					
		        }
		    }
		
		}
		try{
			insert newContactShares;	
		}catch(DMLException dme){
			system.debug('Exception:'+dme.getMessage());
		}	
	}	
	private set<Id> getAllAccountId(List<Distributor_Reseller__c> lstDistiRes){
		set<Id> accId=new set<Id>();
		for(Distributor_Reseller__c dr:lstDistiRes){
			//accId.add(dr.Distributor_Reseller_Name__c);
			accId.add(dr.Account__c);
		}
		return accId;
	}
	private Map<Id,UserRole> getAllUserRoles(set<Id> accountIds){
		Map<Id,UserRole> tempUserRoleMap=new Map<Id,UserRole>();
		List<UserRole> lstUserRole = [Select u.Id, u.Name, u.ParentRoleId, u.PortalAccountId, 
			u.PortalType from UserRole u where u.portalAccountId in :accountIds 
			and u.Name like '%Partner Executive'];
		for(UserRole uRole:lstUserRole){
			tempUserRoleMap.put(uRole.PortalAccountId,uRole);
		}
		return 	tempUserRoleMap;		
	}
	private Map<Id,Group> getAllGroups(Map<Id,UserRole> userRoleMap){
		Map<Id,Group> tempGroupMap=new Map<Id,Group>();
		set<Id> uRoleIds=new set<Id>();
		for(UserRole uRole:userRoleMap.values()){
			uRoleIds.add(uRole.Id);
		}
		for(Group groups:[Select g.Type, g.SystemModstamp, g.RelatedId, g.OwnerId, g.Name, 
						g.LastModifiedDate, g.LastModifiedById, g.Id, g.Email, g.DoesSendEmailToMembers, 
						g.CreatedDate, g.CreatedById From Group g where g.RelatedId in :uRoleIds 
						and g.Type='RoleAndSubordinates']){
			tempGroupMap.put(groups.RelatedId,groups);			
		}
		return tempGroupMap;		
	}
	/*
	*To get all the roles of user associated to that contact's account.
	
	private Map<Id,List<UserRole>> getAllUserRolesCon(set<Id> accountIds){
		Map<Id,List<UserRole>> tempUserRoleMapCon=new Map<Id,List<UserRole>>();
		List<UserRole> templist;
		List<UserRole> lstUserRole = [Select u.Id, u.Name, u.ParentRoleId, u.PortalAccountId, 
			u.PortalType from UserRole u where u.portalAccountId in :accountIds 
			and ( u.Name like '%Partner Executive' or u.Name like '%Partner User') ];
			for(UserRole uRole:lstUserRole){
				if(tempUserRoleMapCon.containsKey(uRole.PortalAccountId)){
                    tempUserRoleMapCon.get(uRole.PortalAccountId).add(uRole);
                }else{
                    templist=new List<UserRole>();
                    templist.add(uRole);
                    tempUserRoleMapCon.put(uRole.PortalAccountId,templist);
                }
			}
		System.debug('tempUserRoleMapCon::::::::::::::'+tempUserRoleMapCon);
		return 	tempUserRoleMapCon;		
	}
	*/
	/*
	*To get all the groups of user roles associated to that contact's account.
	
	private Map<Id,Group> getAllGroupsCon(Map<Id,List<UserRole>> userRoleMap){
		Map<Id,Group> tempGroupMap=new Map<Id,Group>();
		List<UserRole> tempList=new List<UserRole>();
		set<Id> uRoleIds=new set<Id>();
		for(Id temp:userRoleMap.keySet()){
			for(UserRole tempRole:userRoleMap.get(temp)){
				templist.add(tempRole);
			}			
		}
		if(tempList.size()>0){
			for(UserRole uRole:templist){
				uRoleIds.add(uRole.Id);
			}
		}		
		for(Group groups:[Select g.Type, g.SystemModstamp, g.RelatedId, g.OwnerId, g.Name, 
						g.LastModifiedDate, g.LastModifiedById, g.Id, g.Email, g.DoesSendEmailToMembers, 
						g.CreatedDate, g.CreatedById From Group g where g.RelatedId in :uRoleIds 
						and g.Type='RoleAndSubordinates']){
			tempGroupMap.put(groups.RelatedId,groups);			
		}
		return tempGroupMap;		
	}*/
	/**
	*	Code coverate test method > 90%
	*	@return void
	*/
	static  testMethod void coverage(){
		
		RecordType rType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
		Account distiAcc=[select Id,OwnerId from account where name='ZYCKO LTD' and RecordTypeId=:rType.Id];
        Account reAcc=[select Id,OwnerId from account where name='EssentialNet Ltd.' and RecordTypeId=:rType.Id];
        Distributor_Reseller__c disRes=new Distributor_Reseller__c();
        disRes.Account__c=distiAcc.Id;
        disRes.Distributor_Reseller_Name__c=reAcc.Id;
        insert disRes;
        reAcc.OwnerId=userInfo.getUserId();
        update reAcc;
	}   

}