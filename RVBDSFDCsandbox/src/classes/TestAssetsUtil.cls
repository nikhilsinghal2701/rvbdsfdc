/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest(SeeAllData=true)
public class TestAssetsUtil {
    static testMethod void AssetProductFamily_ChassisTypeUpdateTest() {
        RecordType pRecType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
        Account patAcc=new Account();
        patAcc.RecordTypeId=pRecType.Id;
        patAcc.name='tier2Account';
        patAcc.Partner_Status1__c='Good Standing';
        patAcc.Type='VAR';
        patAcc.Industry='Education';
        patAcc.Geographic_Coverage__c='Americas';
        patAcc.Region__c='Latin America';
        patAcc.BillingCountry='Argentina';
        insert patAcc;
        
        Product2 prod=[Select p.Id, p.Name, p.ProductCode from Product2 p where p.ProductCode='SHA-05010'];
        
        Asset newAsset=new Asset();
        newAsset.AccountId=patAcc.Id;
        newAsset.SerialNumber='SD123456789';
        newAsset.Product2Id=prod.Id;
        newAsset.Name='TestSteelhead 5010';
        newAsset.SKU__c='CAS-VE';
        newAsset.IB_Status__c='Sold';
        newAsset.Base_sku__c='SH-12345';
        newAsset.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset.Support_End_Date__c=Date.newInstance(2013, 4, 10);
        insert newAsset;
        
        newAsset.SKU__c='CSP-12345';
        update newAsset;
        
        
        AssetHistory__c aht = new AssetHistory__c();
                    aht.AssetID__c=newAsset.Id; 
                    aht.Asset__c=newAsset.ID;
                    aht.Field__c   = 'fieldName';
                    aht.SerialNumber__c=newAsset.SerialNumber;
                    aht.Name=newAsset.SerialNumber;
                    aht.OldValue__c  = 'oldValue';
                    aht.NewValue__c  = 'newVal';
                    
                    Insert aht;
                    
                    delete newAsset;
        
    }
}