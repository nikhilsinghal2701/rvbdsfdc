public class extSendToPartner{

    //fields
    private final Case myCase;
    
    //constructor
    public extSendToPartner(ApexPages.StandardController stdController) {
        this.myCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateCase() {
        //get case
        Case c = [select Id,Status,ContactId,Contact.Email,CaseNumber from Case where Id = :myCase.id limit 1];

        //update case
        c.Status = 'Working with Technology Partner';
        //add comment
        //CaseComment cc = new CaseComment(ParentId = c.Id, CommentBody = 'Template comment goes here.', IsPublished = false);

        //commit changes
        Update c;
        //Insert cc;

        //build new email page reference
        Contact con = [select Id,Email from Contact where Email like 'ProCurve_ONE_Call_Transfer@lists.hp.com%' limit 1]; //Get HP contact ID
        PageReference pageRef = new PageReference('/email/author/emailauthor.jsp'); //base url for Send Email
        pageRef.getParameters().put('retURL', '/' + c.Id); //set return URL
        pageRef.getParameters().put('p3_lkid', c.Id); //set related case
        pageRef.getParameters().put('rtype', '003'); //set To type to Contact (would be 00Q for a Lead)
        pageRef.getParameters().put('p2_lkid',con.Id); //set To field
        //pageRef.getParameters().put('p24', 'ProCurve_ONE_Call_Transfer@lists.hp.com'); //set the Additional To field
        pageRef.getParameters().put('p4', c.Contact.Email + '; ' + c.CaseNumber + '@support.riverbed.com'); //set the CC field
        pageRef.getParameters().put('p5', ''); //blank the BCC field
        pageRef.getParameters().put('template_id', [Select id from EmailTemplate where developername = 'Send_to_Partner'][0].id); //set template
        //pageRef.getParameters().put('p6', 'subject goes here'); //set the Subject field
        //pageRef.getParameters().put('p7', 'body goes here'); //set the Body field

        pageRef.setRedirect(true);
        return pageRef;
      
    }
    
    static testMethod void extSendToPartnerTest() {
    
        //create objects
        /* if template doesnt already exist  
        Insert new EmailTemplate(
            name = 'Send_to_Partner',
            developername = 'Send_to_Partner',
            templatetype = 'text',
            FolderId = [select id from folder where type = 'Email' limit 1][0].id); 
        */
        
        Contact con = [Select id, email from contact where email != null limit 1];
        
        Case cs = new Case(Subject = 'Test', ContactId = con.id);
        insert cs;
        
        //instantiate class and run method
        extSendToPartner eSTP = new extSendToPartner(new ApexPages.StandardController(cs));
        PageReference pg = eSTP.updateCase();
        
        //check results
        cs = [Select Id, CaseNumber, Contact.Email, Status From Case where id =: cs.id][0];
        
        System.assert(cs.status == 'Working with Technology Partner');
        
        System.assert(pg.getParameters().get('p3_lkid') == string.valueOf(cs.id));
        System.assert(pg.getParameters().get('rtype') == '003');
        //System.assert(pg.getParameters().get('p2') == 'ProCurve_ONE_Call_Transfer@lists.hp.com');
        //System.assert(pg.getParameters().get('p24') == 'ProCurve_ONE_Call_Transfer@lists.hp.com');
        System.assert(pg.getParameters().get('p4') == cs.Contact.Email + '; ' + cs.CaseNumber + '@support.riverbed.com');
        System.assert(pg.getParameters().get('p5') == '');
        System.assert(pg.getParameters().get('template_id') != null);
    }
    
    
    
}