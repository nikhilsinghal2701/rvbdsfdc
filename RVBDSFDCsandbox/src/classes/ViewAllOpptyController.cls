public without sharing class ViewAllOpptyController 
{
    public Contact c1 = new Contact();
    public Contact c2 = new Contact();
    public Opportunity opp = new Opportunity();
    public List<opportunity> liOpp = new List<Opportunity>();
    public Boolean display{get;set;}
    public String accId;
    public String opptyId;
    public Contact getc1()
    { System.debug('C1=========='+c1);
        return c1;
        
    }   
    
    public Contact getc2()
    {
        return c2;
    }   
    
    public Opportunity getopp()
    {
        return opp;
    }
    
    public List<Opportunity> getliOpp()
    {
        return liOpp;
    }

    public ViewAllOpptyController(ApexPages.StandardController controller)
    {  //Tctdate= new contact();
         accId = ApexPages.currentpage().getParameters().get('id');
          //Birthdate = ApexPages.currentpage().getParameters().get('Birthdate');
       
        System.debug('AcountID=========='+accId);
        System.debug('Date C1=========='+ c1.Birthdate);
        System.debug('Date C2=========='+c2.Birthdate);
        if(accId!=null)
        {
            for(Opportunity o : [select id, AccountId, Name, OwnerId, StageName,closeDate, createdDate from Opportunity WHERE AccountId =: accId and createdDate >=: c1.Birthdate and createdDate <=: c2.BirthDate ORDER BY name])
            {
                liOpp.add(o);
            }
            if(liOpp.size()>0)
                display = true;
            else
            {    
                display = false;
                ApexPages.Message msg = new ApexPages.message(ApexPages.Severity.INFO, 'No opportunities to be displayed for this criteria');
                ApexPages.addMessage(msg);
                
            }   
        }
        else
        {
            ApexPages.Message msg = new ApexPages.message(ApexPages.Severity.INFO, 'Invalid Account');
            ApexPages.addMessage(msg);
        }     
        
    } 
    
    Public list<selectOption> getStages(){
       List<selectOption> cStage = new List<selectOption>();  
       Schema.DescribeFieldResult opportunityStageNameResult = Opportunity.StageName.getDescribe();
       List<Schema.PicklistEntry> stageValues = opportunityStageNameResult.getPicklistValues();
       cStage.add(new SelectOption('','None'));
            
        for(Schema.PicklistEntry stageValue : stageValues)
        {
          cStage.add(new SelectOption(stageValue.getValue(), stageValue.getLabel()));
        }  
         
        return cStage;
    }
    public PageReference Search()
    {
        liOpp.clear();
        
        System.debug('AcountID ==========>>> '+accId);
        System.debug('Date C1 ==========>>> '+ c1.Birthdate);
        System.debug('Date C2 ==========>>> '+c2.Birthdate);
        System.debug('opp.StageName ==========>>> '+opp.StageName);
        
        if(opp.StageName != null && opp.StageName != '')
        {
            for(Opportunity o : [select id, AccountId, Name, OwnerId, StageName,closeDate, createdDate from Opportunity WHERE AccountId =: accId and createdDate >=: c1.Birthdate and  createdDate <=: c2.BirthDate and StageName =: opp.StageName ORDER BY name])
            {
                liOpp.add(o);
            }   
        }
        else
        {
            for(Opportunity o : [select id, AccountId, Name, OwnerId, StageName,closeDate, createdDate from Opportunity WHERE AccountId =: accId and createdDate >=: c1.Birthdate and  createdDate <=: c2.BirthDate ORDER BY name])
            {
                liOpp.add(o);
            }
        }
        
        System.debug('liOpp size ==========>>> '+liOpp.size());
        System.debug('liOpp  ==========>>> '+liOpp);
         
        if(liOpp.size()>0)
            display = true;
        else
        {    
            display = false;
            ApexPages.Message msg = new ApexPages.message(ApexPages.Severity.INFO, 'No opportunities to be displayed for this criteria');
            ApexPages.addMessage(msg);
        }        
        return null;
    }
  public Opportunity opty {get; set;}
  public List<opportunityLineItem> optyLineItem {get; set;}
  Public List<OpenActivity> openActivityList {get; set;} 
  Public List<ActivityHistory> activityHistoryList {get; set;} 
  Public List<Partner> partnerList {get; set;} 
  Public List<OpportunityContactRole> opportunityContactList {get; set;} 
  public void getOpptyInfo()
  {
 	opptyId = ApexPages.currentpage().getParameters().get('id');
 	opty = [Select id, name,AccountId,Account.Name, Account.Type, Campaign.Name, OwnerName__c,closeDate,StageName,ForecastCategoryName,NextStep,Deal_Winner_in_a_Lost_Deal__c,Probability,Reason_Lost__c,RegistrationExpirationDate__c,SO__c,NoofSites__c,Segment__c,Primary_App__c,SecondaryApplication__c,Competitors__c,Bake_Off__c,Key_Applications_by_name__c,Why_did_we_win_lose__c,RTA_partner_influence__c,Where_did_you_hear_about_us__c,LeadSource,SourceDetail__c,CampaignId,Eval_Try_and_Buy_Installed__c,ActivityLevel__c,Decision_Maker_Name__c,Decision_Maker_Title__c,Decision_Maker_Email__c,Decision_Maker_Phone__c,Technical_Lead__c,Open_Tech_Issue__c,Key_Influencer_Name__c,Key_Influencer_Title__c,Key_Influencer_Email__c,Key_Influencer_Phone__c,lastModifiedBy.Name, lastModifieddate,createdBy.Name,createdDate, (Select id, Quantity,Pricebookentry.Product2.Name from OpportunityLineItems),(Select Id,Subject,ActivityDate,Status,Priority,WhoId,Owner.Name from OpenActivities), (Select Id,Subject,WhoId,ActivityDate,Status,Priority,Owner.Name  from ActivityHistories),(Select id,AccountfromId,Role,IsPrimary,AccountToId from Partners where AccountToId not in (Select AccountId from Opportunity where id =: opptyId)), (select ContactId, Contact.Name, Contact.Email, Contact.Phone, Role, IsPrimary From OpportunityContactRoles ) from Opportunity where id=:opptyId ];
    optyLineItem = opty.opportunityLineItems;
    openActivityList = opty.OpenActivities;
    activityHistoryList = opty.ActivityHistories;
    partnerList = opty.Partners;
    opportunityContactList = opty.OpportunityContactRoles;
    //return opty;
  }
 
}