@isTest(seeAllData=true)
public with sharing class Test_RVBD_LeadViewController {
 
   
 
 static  testmethod void test_leadviewController(){
 	
 	User usr = [Select ProfileId, LastName, Id From User u where ProfileId ='00e50000000uVEZ' and isactive = true limit 1];
 	User partusr = [Select ProfileId, LastName, Id From User u where UserType = 'PowerPartner' and isactive = true limit 1];
 	
 	RecordType accRecType = [select id from RecordType where name ='Customer Account' and sObjectType='Account'];
 	//Account probAcc = new Account( Name ='Salesforce',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
    //insert probAcc;
    Account probAcc = [select name,WW_HQ__c from Account where name ='NETAPP' limit 1];
     WW_HQ__c rCompany = [select id, name from WW_HQ__c limit 1];
    Contact con = new Contact(firstname = 'Steve', lastname = 'Jobs',email='Test@Riverbed.com', accountId = probAcc.Id );
    insert con;
 	Lead selectedlead = new Lead(firstname = 'Tony', lastname = 'Montana', 
 	     status = 'Open',
         Title = '',
         ownerid = usr.Id,
         Email = 'riverbed@riverbed.com',
         company = 'RVBD',
         Website ='WWW.riverbed.com',
         Role_in_IT_Decision__c = 'Authorize purchases',
         Phone = '234 567 7890',
         Probable_Account__c = probAcc.Id,
         street = '199 Fremont',
         state = 'CA',
         city = 'San francisco',
         Country = 'US',
         postalcode = '92105',
         Associated_Contact__c = con.id);
         insert selectedlead;
         
 		Test.starttest();
 		//system.runAs(usr){
 	    RVBD_LeadViewController lvc = new  RVBD_LeadViewController();
 	    RVBD_LeadViewController.mergeWrapper mw = new RVBD_LeadViewController.mergeWrapper(selectedLead,true,true);
 	    lvc.mergeWrapList.add(mw);
 	    lvc.leadSets();
 	    lvc.selId = selectedlead.Id;
 	    lvc.sellead = [select Id,Owner.name,Role_in_IT_Decision__c,ownerid,Auto_Convert__c,Street,City,State,Country,postalcode,Name,Status,lastname,Title,LeadSource,Company,SourceDetail__c,Website,Lead_Score__c,Decision_Role__c,Phone,Probable_Account__c,Email,Associated_Contact__c,WW_HQ__c,ProbAcctDemo__c,CompFamInsight__c,ProbAcctInsight__c  from Lead where id=:selectedlead.id];
 	    lvc.copylead = [select Id,Owner.name,Role_in_IT_Decision__c,ownerid,Auto_Convert__c,Street,City,State,Country,postalcode,Name,Status,lastname,Title,LeadSource,Company,SourceDetail__c,Website,Lead_Score__c,Decision_Role__c,Phone,Probable_Account__c,Email,Associated_Contact__c,WW_HQ__c,ProbAcctDemo__c,CompFamInsight__c,ProbAcctInsight__c  from Lead where id=:selectedlead.id];
 	    lvc.converted =false;
 	    PageReference pageRef = Page.RVBD_LeadView;
        pageRef.getParameters().put('selId',selectedlead.Id);
        Test.setCurrentPage(pageRef);
 	    lvc.searchSimilarLeads();
 	    lvc.strCompany = 'RVBD';
 	    lvc.strName =  'TONY';
 	    lvc.strScDet= 'Deal Reg Request';
 	    lvc.strScore = 'A';
 	    lvc.strStatus = 'Open';
 	    lvc.searchLeads();
 	    lvc.clearSearch();
 	    lvc.sellead.email ='Riverbed2@riverbed.com';	    
 	    lvc.cancelEdited();
 	    
 	    lvc.sellead.Role_in_IT_Decision__c ='Select vendors'; 	
 	    lvc.sellead.ownerId = partusr.id;    
 	    lvc.checkPassToPartner();
 	    lvc.cancelEdited();
 	    lvc.sellead = [select Id,Owner.name,Role_in_IT_Decision__c,ownerid,Auto_Convert__c,Street,City,State,Country,postalcode,Name,Status,lastname,Title,LeadSource,Company,SourceDetail__c,Website,Lead_Score__c,Decision_Role__c,Phone,Probable_Account__c,Email,Associated_Contact__c,WW_HQ__c,ProbAcctDemo__c,CompFamInsight__c,ProbAcctInsight__c  from Lead where id=:selectedlead.id];
 	    lvc.sellead.Company = 'Yahoo';
 	    lvc.saveEdited();
 	    lvc.refreshPanels();
 	    lvc.clearLists();
 	    
 	    lvc.closePopup();
 	    lvc.closeSearchDupPopup();
 	    lvc.duplicateLeadActivities();
 	    lvc.editLead();
 	    lvc.selId = selectedlead.Id;
 	    Lead ld = [select id,name from Lead limit 1];
 	    lvc.mergeWrapList.add(new RVBD_LeadViewController.mergeWrapper(ld,true,true));
 	    lvc.mergeLeads();
 	    lvc.flipSortOrder();
 	    lvc.showPopup();
 	    lvc.showPopupCon();
 	    lvc.getLeadsSet();
 	    lvc.getQueryString(); 		
 		Test.StopTest();		 		
 	}
 	
 	
 	static  testmethod void test_leadviewController2(){
 	
 	User usr = [Select ProfileId, LastName, Id From User u where ProfileId ='00e50000000uVEZ' and isactive = true limit 1];
 	User partusr = [Select ProfileId, LastName, Id From User u where UserType = 'PowerPartner' and isactive = true limit 1];
 	
 	RecordType accRecType = [select id from RecordType where name ='Customer Account' and sObjectType='Account'];
 	//Account probAcc = new Account( Name ='Salesforce',Industry= 'Software',OS_City__c='San Francisco',OS_State__c='California' ,D_B_Country__c='U.S',recordTypeid= accRecType.Id );
    //insert probAcc;
    Account probAcc = [select name from Account where name ='NETAPP' limit 1];
    WW_HQ__c rCompany = [select id, name from WW_HQ__c limit 1];
    Contact con = new Contact(firstname = 'Steve', lastname = 'Jobs',email='Test@Riverbed.com', accountId = probAcc.Id );
    insert con;
 	Lead selectedlead = new Lead(firstname = 'Tony', lastname = 'Montana', 
 	     status = 'Open',
         Title = '',
         ownerid = usr.Id,
         Email = 'riverbed@riverbed.com',
         company = 'RVBD',
         Website ='WWW.riverbed.com',
         Role_in_IT_Decision__c = 'Authorize purchases',
         Phone = '234 567 7890',
         Probable_Account__c = probAcc.Id,
         street = '199 Fremont',
         state = 'CA',
         city = 'San francisco',
         Country = 'US',
         WW_HQ__c = rCompany.Id,
         postalcode = '92105',
         Associated_Contact__c = con.id);
         insert selectedlead;
         
 		Test.starttest();
 		//system.runAs(usr){
 	    RVBD_LeadViewController lvc = new  RVBD_LeadViewController();
 	    RVBD_LeadViewController.mergeWrapper mw = new RVBD_LeadViewController.mergeWrapper(selectedLead,true,true);
 	    lvc.mergeWrapList.add(mw);
 	    
 	    lvc.selectedtab='Unworked Leads';
 	    lvc.getLeadsSet();
 	    lvc.searchSimilarLeads();
 	    lvc.next();
 	    lvc.previous();
 	    lvc.last();
 	    lvc.first();
 	    Integer np= lvc.leadPageNo;
 	    Integer rec =lvc.totalLeadRecords;
 	    integer pages= lvc.noOfPages;
 	    Boolean hasrecord = lvc.hasNext;
 	    hasrecord =lvc.hasPrevious;
 	    
 	    
 	    lvc.selectedtab='Work in Progress Leads';
 	    lvc.getLeadsSet();
 	    lvc.searchSimilarLeads();
 	    lvc.next();
 	    lvc.previous();
 	    lvc.last();
 	    lvc.first();
 	    np=lvc.leadPageNo;
 	    rec= lvc.totalLeadRecords;
 	    pages=lvc.noOfPages;
 	    hasrecord = lvc.hasNext;
 	    hasrecord =lvc.hasPrevious;
 	    
 	    
 	    lvc.selectedtab='Disqualified Leads';
 	    lvc.getLeadsSet();
 	    lvc.searchSimilarLeads();
 	    lvc.next();
 	    lvc.previous();
 	    lvc.last();
 	    lvc.first();
 	    np=lvc.leadPageNo;
 	    rec=lvc.totalLeadRecords;
 	    pages=lvc.noOfPages;
 	    hasrecord = lvc.hasNext;
 	    hasrecord =lvc.hasPrevious;
 	    
 	    
 	    lvc.selectedtab='Expired Leads';
 	    lvc.getLeadsSet();
 	    lvc.searchSimilarLeads();
 	    lvc.next();
 	    lvc.previous();
 	    lvc.last();
 	    lvc.first();
 	    np=lvc.leadPageNo;
 	    rec=lvc.totalLeadRecords;
 	    pages=lvc.noOfPages;
 	    hasrecord = lvc.hasNext;
 	    hasrecord =lvc.hasPrevious;
 	   
 	    
 	    lvc.selconCompany =true;
 	    lvc.selconEmail =true;
 	    lvc.selconlName =true;
 	    //lvc.searchDuplicateContacts();
 	    lvc.selEmail = true;
 	    lvc.selCompany = true;
 	    lvc.sellName =true;
 	    lvc.selName =true;
 	    lvc.selPhone =true;
 	    //lvc.searchDuplicateLeads();
 	   
 	    List<Lead> ldlst = [select id,name from Lead limit 2];
 	    lvc.mergeWrapList.add(new RVBD_LeadViewController.mergeWrapper(ldlst[0],true,true));
 	    lvc.mergeWrapList.add(new RVBD_LeadViewController.mergeWrapper(ldlst[1],true,true));
 	    lvc.duplicateLeadActivities();
 	    lvc.setSelectedLead();
 	    
 	    
 	    
 	    
 	    //lvc.sortList(unsortlist, sortField, order);
 	    //lvc.dateString(d);
 		//}
 		
 		Test.StopTest();		 		
 	}
}