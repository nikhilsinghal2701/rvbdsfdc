@isTest (seealldata=true)
public class TestSendEmail {

		private static testmethod void unitTest() {
			List<id> cid = new list<id>();
				Contact con = new Contact(Partner_Win_Story_Contact__c = true, Email = 'test@test.com', LastName = 'Test');
				//insert con;
				List<Contact> conList = [select Email from Contact where Email != null limit 1];

				for(Contact c :conList) {
						c.Partner_Win_Story_Contact__c = true;
				}
				update conlist;
				cid.add(conlist[0].id);

				Partner_Win_Story__c pws = new Partner_Win_Story__c(Sent_to_Distribution__c = false);
				insert pws;

				SendEmailNotifications.sendEmails(cid);
				ApexPages.StandardController ctrl = new ApexPages.StandardController(pws);
				SendEmailExtension sme = new SendEmailExtension(ctrl);
				//sme.isEligible = false;
				sme.selectedEmail = new List<String>{'test@test.com'};
				sme.confirm();
				sme.backtoDistList();
				sme.sendToDistribution();
				pws = [select Sent_to_Distribution__c, Date_Sent__c from Partner_Win_Story__c where id = :pws.id];
				System.assertEquals(true, pws.Sent_to_Distribution__c);
				System.assertNotEquals(null, pws.Date_Sent__c);
		}
}