/*
 * Class Name : ChangeForecastCategoryUtil
 * Description: Util class which has common methods that will be used for 
 *              ChangeForecastCategoryListViewController and ChangeForecastCategoryController. 
 * Authour    : Sorna (Perficient)
 * Version    : 1.0
 * Date       : 08/15/2013
 */
 
 public class ChangeForecastCategoryUtil
 {
        
    /* getForecastCategoryValues:
     * gets the picklist values of standard forecast category field
     */
    public static List<SelectOption> getForecastCategoryValues()
    {
        List<SelectOption> categories = new List<SelectOption>();
        Schema.DescribeFieldResult F = Opportunity.ForecastCategoryName.getDescribe();
        
        categories.add(new SelectOption('','--None--'));
        for(Schema.PicklistEntry p : F.getPicklistValues())
        {
            if(p.getValue() != 'Closed')
                categories.add(new SelectOption(p.getValue(),p.getLabel()));
        }
        
        return categories;
    }
    
    /* validateCategory:
     * validate the inputs before saving the opty
     */
    public static Boolean validateCategory(string category)
    {
        if(category == null || category == '')
        {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Forecast Category: you must enter value'));
            return false;
        }
        else
        {
            return true;
        }
    }
 }