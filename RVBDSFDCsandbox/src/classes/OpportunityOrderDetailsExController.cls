public without sharing class OpportunityOrderDetailsExController {
    public Opportunity opportunity {get;set;}
    public List<Discount_Detail__c> ddList{get;set;}
    Id oppId1;
    public OpportunityOrderDetailsExController(ApexPages.StandardController controller){
        id oppId = controller.getId();
        opportunity=(Opportunity)controller.getRecord();
        oppId1 = System.currentPageReference().getParameters().get('id'); // Added by Mahasvi Samineni for Tkt #207923
        
        if(oppId!=null){
            ddList=[select id,Name,Category_Master__c,Discount_Detail__c.Category_Master__r.Name,Customer_Discount__c,Var_Discount__c,Opportunity__c,IsOpp__c,Description__c from Discount_Detail__c where Opportunity__c=:oppId and 
                    IsOpp__c=true and Discount_Detail__c.Category_Master__r.Name NOT IN('B-2','B-3','B-4','K-2','K-3','K-4','L-24','L-36')order By Discount_Detail__c.Category_Master__r.Name];
            system.debug('***ddList size:'+ddList.size());          
        }
    }
    public void newSave(){      
        if(ddList!=null && !ddList.isEmpty()){
            update ddList;
        }
        Opportunity oppUpd=new Opportunity();
        oppUpd.Id=opportunity.Id;
        if(opportunity.Support_Renewal_Rate_RVBD__c!=null)oppUpd.Support_Renewal_Rate_RVBD__c=opportunity.Support_Renewal_Rate_RVBD__c;
        if(opportunity.Support_Renewal_Rate_RASP__c!=null)oppUpd.Support_Renewal_Rate_RASP__c=opportunity.Support_Renewal_Rate_RASP__c;
        update oppUpd;
    }
    public pageReference cancel(){
    //PageReference ddetailPage = new PageReference ('/apex/OpportunityOrderDetailsEx?id='+ddList[0].Opportunity__c);
    PageReference ddetailPage = new PageReference ('/apex/OpportunityOrderDetailsEx?id='+oppId1);
    ddetailPage.setRedirect(true);
    return ddetailPage;
     }// Lines 26 to 31 added by Mahasvi Samineni for Tkt #207923

}