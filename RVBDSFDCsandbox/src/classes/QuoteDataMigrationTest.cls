/*************************************************************************************************************
 Name:QuoteDataMigrationTest
 Author: Santoshi Mishra
 Purpose: This is test class for the class QuoteDataMigration.
 Created Date : Nov 2011
*************************************************************************************************************/

@isTest(seeAllData=true)
private class QuoteDataMigrationTest {
    
        static testMethod void QuoteDataMigrationTest() {
        // TO DO: implement unit test
      
        Test.StartTest();
        Quote__c q=DiscountApprovalTestHlp.createQuote(null);
        System.debug('inserted detail---------');
        List<Category_Master__c> catList= new List<Category_Master__c>();
        Category_Master__c cat = new Category_Master__c(Name='B1' , Group__c='Appliance');
        Category_Master__c cat1 = new Category_Master__c(Name='B2' , Group__c='Software');
        catList.add(cat);
        catList.add(cat1);
        insert(catList);
        Map<Id,Category_Master__c> catMap= new Map<Id,Category_Master__c>(catList);
        QuoteDataMigration cls1 = new QuoteDataMigration();
        QuoteDataMigration cls = new QuoteDataMigration(true); 
        Database.executeBatch(cls);
        Test.StopTest();
        List<Discount_Detail__c> childList = [select Id,Quote__c,category_master__C,category_master__r.Name,Discount__c,Uplift__c,Special_Discount__c
             from Discount_Detail__c where Quote__c =: q.Id and category_master__r.Name =:'A' limit 1 ];
                
        //System.assertequals(q.Discount_Product__c,childlist[0].Discount__c);
        //System.assertequals(q.Uplift_Prodcuts__c,childlist[0].Uplift__c);
        //System.assertequals(q.Special_Discount_A__c,childlist[0].Special_Discount__c);
        
    
    }

}