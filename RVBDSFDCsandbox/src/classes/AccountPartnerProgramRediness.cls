public without sharing class AccountPartnerProgramRediness {
	static set<Id> accId=new set<Id>();
	static List<Acct_Cert_Summary__c> accSumList;
	static Map<Id,List<Acct_Cert_Summary__c>> accMap=new Map<Id,List<Acct_Cert_Summary__c>>();
	static List<Acct_Cert_Summary__c> accCertSumList;
	static List<Account> newAccountSumCert;
	static List<string> tapStr=null;
	public static Boolean isTrigger=false;
	public static void createAccountSummaryCert(List<Account> newTrigger){
		for(Account acc:newTrigger){
			accId.add(acc.Id);
		}
		if(accId.size()>0){
			tapStr=getTapList(getTap());
			accCertSumList=[select Id,Name,Account__c,TAP__c from Acct_Cert_Summary__c
							where Account__c IN:accId and TAP__c IN:tapStr];
		}
		if(accCertSumList!=null && accCertSumList.size()>0){
			for(Acct_Cert_Summary__c temp : accCertSumList){
                if(accMap.containsKey(temp.Account__c)){
                    accMap.get(temp.Account__c).add(temp);
                }else{
                    accSumList=new List<Acct_Cert_Summary__c>();
                    accSumList.add(temp);
                    accMap.put(temp.Account__c,accSumList);
                }
            }
		}else{
			newAccountSummaryCert(newTrigger);
		}
		if(accMap.size()>0){
			for(Account acc:newTrigger){
				if(accMap.containsKey(acc.Id)){					
				}else{
					if(newAccountSumCert==null)newAccountSumCert=new List<Account>();
					newAccountSumCert.add(acc);
				}
			}
		}
		if(newAccountSumCert!=null && newAccountSumCert.size()>0){
			newAccountSummaryCert(newAccountSumCert);
		}
	}
	private static string getTAP(){
		string tempStr=null;
		Schema.DescribeFieldResult F = Acct_Cert_Summary__c.TAP__c.getDescribe();
		List<Schema.PicklistEntry> P = F.getPicklistValues();
		for(Schema.Picklistentry pe:p){			
			if(tempStr==null)tempStr=pe.getValue();
			else tempStr=tempStr+','+pe.getValue();
		}
		if(tempStr.length()>0)return tempStr;
		return null;
	}
	
	private static void newAccountSummaryCert(List<Account> newAccSumCertList){
		Acct_Cert_Summary__c aCert;
		List<Acct_Cert_Summary__c> newList=new List<Acct_Cert_Summary__c>();
		List<String> tapList=getTapList(getTAP());
		for(Account acc:newAccSumCertList){
			for(string str:tapList){
				acert=new Acct_Cert_Summary__c();
				acert.Account__c=acc.Id;
				acert.TAP__c=str;
				newList.add(acert);
			}
		}
		if(newList.size()>0){
			try{
				insert newList;
			}catch(Exception ex){
				system.debug('Exception during record insert:'+ex.getMessage());
			}
		}
	}
	
	private static List<String> getTapList(String tempStr){
		List<String> tapList=new List<String>();
		if(tempStr!=null && tempStr.length()>0){
			tapList=tempStr.split(',');
		}
		return tapList;
	}
	/*1.Trigger on account owner change: update all contacts of this account to have the same owner as the account.
	** ticket#109263
	*/
	public static void updateContactOwner(List<Account> newTrigger , Map<Id,Account>oldTrigger){
		Map<Id,List<Contact>> accountContactMap=new Map<Id,List<Contact>>();
		List<Account> tempAccList = new List<Account>();
		List<Contact> conList;
		Set<Id> ids = new Set<Id>();
		List<Contact> updateList = new List<Contact>();
		for(Account acc:newTrigger){
			if(acc.OwnerId!=oldTrigger.get(acc.Id).OwnerId){
				tempAccList.add(acc);
				ids.add(acc.Id);
			}
		}
		if(tempAccList.size()>0 && ids.size()>0){
			List<Contact> tempConList=[select Id,OwnerId,AccountId from Contact where AccountId IN:ids ];
			if(tempConList!=null && tempConList.size()>0){
				for(Contact con:tempConList){
					if(accountContactMap.containsKey(con.AccountId)){
						accountContactMap.get(con.AccountId).add(con);
					}else{
						conList=new List<Contact>();
						conList.add(con);
						accountContactMap.put(con.AccountId,conList);
					}
				}
			}
			if(accountContactMap.size()>0){
				for(Account acc:tempAccList){
					if(accountContactMap.containsKey(acc.Id)){
						for(Contact temp:accountContactMap.get(acc.Id)){
							temp.OwnerId=acc.OwnerId;
							updateList.add(temp);
						}
					}
				}
			}
			if(!updateList.isEmpty()){
				try{
					update updateList;
				}catch(Exception e){
					system.debug('Exception:'+e.getMessage());
				}
			}
		}
		/*
		* Added for contact share with partner admin user as per accounts.
		*/
		if(accountContactMap.keySet().size()>0){
			AccountShareHelper asHelper = new AccountShareHelper();
			asHelper.insertIntoContactShare(accountContactMap);
		}
	} 
}