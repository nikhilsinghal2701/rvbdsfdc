@isTest
private class TestOpportunityService {
    
    static testmethod void testInsertOppLines(){
            Opportunity testOp1 = new Opportunity(Name='Test',StageName='4 - Selected',CloseDate=Date.newInstance(2011, 01, 01),ownerid=userInfo.getUserId());
            testOp1.Support_Renewal_Rate_RASP__c = 20;
            testOp1.Support_Renewal_Rate_RVBD__c = 25;
            
            insert testOp1; 
            Category_Master__c cat = new Category_Master__c(Name  ='K');
            insert cat;
            Discount_Detail__c testdd = new Discount_Detail__c(Name='Test', Category_Master__c = cat.Id, Opportunity__c = testOp1.id, Special_Discount__c = 5, Uplift__c =4, Discount__c = 10 );
            insert testdd; 
            List<String> oppIDs = new List<Id>();
            oppIDs.add(testOp1.Id);
            String response = OpportunityService.getOpportunityDiscounts(oppIDs);
            System.debug('response :::'+response );

    }
}