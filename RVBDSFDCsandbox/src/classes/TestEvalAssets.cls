@isTest(SeeAllData=true)
public class TestEvalAssets{
    /*private static User SystemAdministrator() 
    {
    Profile p = [select Id, Name from Profile where Name = 'System Administrator'];
        User u = new User(
            UserName = 'test-user@test-company.com',
            FirstName = 'Test-First-Name',
            LastName = 'Test-Last-Name',
            Alias = 'test',
            Email = 'test-user@test-company.com',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocalesIdKey = 'en_US',
            TimezonesIdKey = 'America/Los_Angeles',
            ProfileId = p.Id
            );
        insert u;
        //List<User> u= [select Id from User where Profile = 'System Administrator' and isActive=True limit 100];
        return u;
        }*/
        
static testMethod void testCloseEvals()
{
      //System.runAs(SystemAdministrator()) {  
    
        //RecordType pRecType=[Select r.Id, r.Name, r.SobjectType from RecordType r where SobjectType='Account' and Name='Partner Account'];
              
     // List<Account> acc =[select id,name from Account where isdeleted=false limit 100];
      //system.debug('*** acc:'+acc);
      Account acc = new Account();
      acc.Name = 'Test';
      acc.Type='VAR';
      acc.Industry='Electronics';
      acc.Geographic_Coverage__c='Americas';
      acc.Region__c='Latin America';
      acc.BillingCountry='Argentina';
      insert acc ;
        
        Opportunity opty = new Opportunity();
        opty.name = 'testopp';
        opty.accountId = acc.id;
        //opty.accountId='00130000002NcZE';
        opty.Primary_ISR__c = UserInfo.getUserId();
        opty.Type = 'New';
        opty.Channel__c = 'Direct';
        opty.LeadSource = 'Email';
        opty.CloseDate = System.today()+5;
        opty.StageName = '7 - Closed (Not Won)';
        opty.Select_Product_Line__c='Cascade';
        opty.Forecast_Category__c = 'Commit';
        opty.Competitors__c = 'Citrix';
        opty.Why_did_we_win_lose__c = 'Competition';
        insert opty;
              
        Product2 prod=[Select p.Id, p.Name, p.ProductCode from Product2 p where p.ProductCode='SHA-05010'];
        
        Asset newAsset=new Asset();
        newAsset.AccountId=acc.id;
        //newAsset.AccountId= '00130000002NcZE';
        newAsset.SerialNumber='SD123456789';
        newAsset.Opportunity__c=opty.id;
        newAsset.Product2Id=prod.Id;
       // newAsset.Product2Id='01t30000000Bi0Z';
        newAsset.Name='TestSteelhead 5010';
        newAsset.SKU__c='CAS-VE';
        newAsset.IB_Status__c='Under Evaluation';
        newAsset.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset.Support_End_Date__c=Date.newInstance(2013, 4, 10);
        insert newAsset;
         ApexPages.StandardController std1 = new ApexPages.StandardController(newAsset);
         Apexpages.currentPage().getParameters().put('id',opty.Id);
         CloseEvals controller1 = new CloseEvals(std1); 
         controller1.validateOppty();
         controller1.doContinue();
         controller1.doSave();
        
        
  }
      //}
static testMethod void testOutstandingEvals() 
{
      Account acc = new Account();
      acc.Name = 'Test';
      acc.Type='VAR';
      acc.Industry='Electronics';
      acc.Geographic_Coverage__c='Americas';
      acc.Region__c='Latin America';
      acc.BillingCountry='Argentina';
      insert acc ;
        
        Opportunity opty = new Opportunity();
        opty.name = 'testopp';
        opty.accountId = acc.id;
        //opty.accountId='00130000002NcZE';
        opty.Primary_ISR__c = UserInfo.getUserId();
        opty.Type = 'New';
        opty.Channel__c = 'Direct';
        opty.LeadSource = 'Email';
        opty.CloseDate = System.today()+5;
        opty.StageName = '7 - Closed (Not Won)';
        opty.Select_Product_Line__c='Cascade';
        opty.Forecast_Category__c = 'Commit';
        opty.Competitors__c = 'Citrix';
        opty.Why_did_we_win_lose__c = 'Competition';
        insert opty;
              
        Product2 prod=[Select p.Id, p.Name, p.ProductCode from Product2 p where p.ProductCode='SHA-05010'];
        
        Asset newAsset=new Asset();
        newAsset.AccountId=acc.id;
        //newAsset.AccountId= '00130000002NcZE';
        newAsset.SerialNumber='SD123456789';
        newAsset.Opportunity__c=opty.id;
        newAsset.Product2Id=prod.Id;
       // newAsset.Product2Id='01t30000000Bi0Z';
        newAsset.Name='TestSteelhead 5010';
        newAsset.SKU__c='CAS-VE';
        newAsset.IB_Status__c='Under Evaluation';
        newAsset.SupportStartDate__c=Date.newInstance(2012, 4, 11);
        newAsset.Support_End_Date__c=Date.newInstance(2013, 4, 10);
        insert newAsset;
         ApexPages.StandardController std1 = new ApexPages.StandardController(newAsset);
         Apexpages.currentPage().getParameters().put('id',opty.Id);
         OutstandingEvals controller1 = new OutstandingEvals(std1); 
         controller1.ValidateOpty();
         
}
static testMethod void testUnderEvalAssets() 
{
}
}