@isTest(SeeAllData=true)
private class ForcastTestClass {

static testMethod void Testmyclass() {

       /* Profile p = [select id from profile where name='System Administrator'];
        //modified user name by Santoshi on Jan-2012 to eliminate duplicate username error.
              User u = new User(alias = 'std133', email = 'stduser12333@test.com', emailencodingkey='UTF-8', lastname='Testin12333', languagelocalekey='en_US', localesidkey='en_US', profileid=p.Id, timezonesidkey='America/Los_Angeles', username='stduser12333@test.com');
                  insert u;   
       */
       	     
        //List <Quote_Line_Item__c> qtiList= new List <Quote_Line_Item__c>();
        //List <Opportunity> oppList= new List <Opportunity>();
        //List <Quote__c> qtList= new List <Quote__c>();
        Opportunity op = new Opportunity(Name='Test',StageName='Testing',CloseDate=Date.newInstance(2011, 01, 01),ownerid=userinfo.getUserId());
        insert op;
        /*
        oppList.add(op);
        Opportunity op1 = new Opportunity(Name='Test3',StageName='6 - Order Accepted',CloseDate=Date.newInstance(2011, 01, 01),ownerid=u.id);
        //insert op1;
        oppList.add(op1);
        Opportunity op2 = new Opportunity(Name='Test2',StageName='4 - Influencing',CloseDate=Date.newInstance(2011, 34, 01));
        //insert op2;
        oppList.add(op2);
        insert oppList;*/
        Quote__c o=new Quote__c(Name='Test',Opportunity__c=op.id,Forecasted_Quote__c = true,ownerid=userinfo.getUserId());
        insert o;
        /*
        qtList.add(o);
        Quote__c o1=new Quote__c(Name='Test1',Opportunity__c=op1.id,Forecasted_Quote__c = true,ownerid=u.id);
        //insert o1;
        qtList.add(o1);
        Quote__c o12=new Quote__c(Name='Test12',Opportunity__c=op2.id,Forecasted_Quote__c = true,ownerid=u.id);
        //insert o12;
        qtList.add(o12);
        Quote__c o2=new Quote__c(Name='Test3',Opportunity__c=op1.id,Forecasted_Quote__c = true);
        //insert o2;
        qtList.add(o2);
        insert qtList;*/
        Product2 prod = [SELECT Name, ProductCode FROM Product2 WHERE ProductCode = 'SVC-PSD-00002'];//Added by prashant on 1/17/2013 for test code failure
		Quote_Line_Item__c ql = new Quote_Line_Item__c(Quote__c=o.Id, Non_Standard_Discount__c=30,Qty_Ordered__c=1, Category__c='E', Product_Code__c=prod.ProductCode, Product2__c=prod.Id, CurrencyIsoCode=UserInfo.getDefaultCurrency()); //Added by prashant on 1/17/2013 for test code failure
        //Quote_Line_Item__c ql= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o.id); //Commented by prashant on 1/17/2013 for test code failure       
        try{
        	insert ql;
        }catch(Exception e){
    		system.debug('Exception:'+e);
    	}
        /*Quote_Line_Item__c ql= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o.id);
        insert ql;
        qtiList.add(ql);
        Quote_Line_Item__c ql1= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o1.id);
        //insert ql1;
        qtiList.add(ql1);
        insert qtiList;*/
        ApexPages.StandardController sc = new ApexPages.StandardController(o);
        product2quote cn = new product2quote(sc);
        pagereference pg=new pagereference('/apex/product2quote?id='+o.ID); 
        system.test.setCurrentpage(pg); 
        pagereference pg1= cn.product2quotes();
        cn.Setproduct2quotes(pg.getparameters().get('id')); 
        String st=cn.Getproduct2quotes();       
        
        
       /* List <Quote__c> lsqc= new List <Quote__c>();
        
        Quote_Line_Item__c ql1= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o1.id);
        insert ql1;
        Quote__c o2=new Quote__c(Name='Test3',Opportunity__c=op1.id,Forecasted_Quote__c = true);
        insert o2;
        //Quote_Line_Item__c ql2= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o2.id);
       //insert ql2;
  */
 /*     ApexPages.StandardController sc1 = new ApexPages.StandardController(o1);
        product2quote cn1 = new product2quote(sc1);
        pagereference pg3=new pagereference('/apex/product2quote?id='+o1.ID); 
        system.test.setCurrentpage(pg3); 
           pagereference pg2= cn1.product2quotes();
            cn1.Setproduct2quotes(pg2.getparameters().get('id')); 
        String st1=cn1.Getproduct2quotes(); 
 
  
  test.starttest();
  try
  {
   
                  
        //List <Quote__c> lsqc1= new List <Quote__c>();
        
        OpportunityLineItem   p3= new OpportunityLineItem(OpportunityId=o12.Opportunity__c);
        
           p3.TotalPrice=2230;
           p3.Quantity=1;
                p3.Discount_Percentage__c=12;
                 Product2 product3 = new Product2(Name = 'Test Stack Product 600x1000');
                                product3.CanUseRevenueSchedule = true;
                          product3.IsActive = true;   
                               product3.RevenueScheduleType = 'Repeat'; 
                                   product3.RevenueInstallmentPeriod = 'Monthly'; 
                                         product3.NumberOfRevenueInstallments = 5; 
                                                insert product3;
                                                  Quote_Line_Item__c ql23= new Quote_Line_Item__c(Unit_List_Price__c=2,Qty_Ordered__c=3,Quote__c=o12.id,Product2__c=product3.id);
        insert ql23;
                                                Pricebook2 pricebook3 = [select Id from Pricebook2 where isStandard = true]; 
                                               
                PricebookEntry pricebookEntry1 = new PriceBookEntry( 
                  UnitPrice = 200,  Product2Id = ql23.Product2__c, Pricebook2Id = ql23.Product2__c,  IsActive = true);   
                                                                           insert pricebookEntry1;
                   
                 p3.PricebookEntryId = pricebookEntry1.Id;
               p3.TotalPrice=2230;
                p3.Discount_Percentage__c=12;
                 
               System.assertEquals(pricebookEntry1.Id, p3.PricebookEntryId); 
           insert p3; 
           }  
             catch(exception e)
             {
            System.debug(e);
             }   
             
        //SalesPrice trigger test class by anil
        OpportunityLineItem pl= new OpportunityLineItem(OpportunityId=op1.id);
       Quote__c q1=new Quote__c(Name='Test3',Forecasted_Quote__c = true,Opportunity__c=op1.id);
       insert q1;
       
        pl.OpportunityId=op.id;
        pl.Quantity=1;
        Product2 product = new Product2(Name = 'Test Stack Product 600x1000');
                                product.CanUseRevenueSchedule = true;
                          product.IsActive = true;   
                               product.RevenueScheduleType = 'Repeat'; 
                                   product.RevenueInstallmentPeriod = 'Monthly'; 
                                         product.NumberOfRevenueInstallments = 5; 
                                                insert product; 
                                                
                                                Product2 product1 = new Product2(Name = 'Test Stack Product 600x1000');
                                product1.CanUseRevenueSchedule = true;
                          product1.IsActive = true;   
                               product1.RevenueScheduleType = 'Repeat'; 
                                   product1.RevenueInstallmentPeriod = 'Monthly'; 
                                         product1.NumberOfRevenueInstallments = 5; 
                                                insert product1;
                                                
                                                                                 Pricebook2 pricebook = [select Id from Pricebook2 where isStandard = true]; 
                                                            PricebookEntry pricebookEntry = new PriceBookEntry( 
                                                                        UnitPrice = 200,  Product2Id = product.Id, Pricebook2Id = pricebook.Id,  IsActive = true);   
                                                                           insert pricebookEntry;
                                                                            
        
       pl.PricebookEntryId = pricebookEntry.Id;
       pl.TotalPrice=2230;
                pl.Discount_Percentage__c=12;
        try{
            insert pl;
       update pl;
       Delete pl;
        }catch(Exception e){
            
        } 
        test.stoptest();
       */ 
     }
}