public class DiscountApproverCache {
	private Map<Id, QuoteApprovers> approversByQuoteId;

	public DiscountApproverCache() {
		approversByQuoteId = new Map<Id,QuoteApprovers>();
	}

	public void loadByQuoteIds(Set<Id> quoteIds) {
		if (approversByQuoteId.isEmpty()) {
			for (Discount_Approver__c approver : [SELECT Id, Approver__c, Quote__c FROM Discount_Approver__c WHERE Quote__c IN :quoteIds]) {
				addApprover(approver);
			}
		}
	}
	
	public QuoteApprovers getOrCreateApprovers(Id quoteId) {
		QuoteApprovers approvers = approversByQuoteId.get(quoteId);
		if (approvers == null) {
			approvers = new QuoteApprovers(quoteId);
			approversByQuoteId.put(quoteId, approvers);
		}
		return approvers;
	}
	
    /* private static Discount_Approver__c getPastDiscountApprover(Id quoteId, Id userId) {
    	for (Discount_Approver__c approver : pastApproversCache) {
    		if ((approver.Quote__c == quoteId) && (approver.Approver__c == userId)) {
    			return approver;
    		}
    	}
    	return null;
    } */

	public void addApprover(Discount_Approver__c approver) {
		if (approver == null) {
			return;
		}
		
		System.assert(approver.Quote__c != null, 'Approver must be assigned to a Quote');
		
		QuoteApprovers qapprovers = approversByQuoteId.get(approver.Quote__c);
		if (qapprovers == null) {
			qapprovers = new QuoteApprovers(approver.Quote__c);
			approversByQuoteId.put(approver.Quote__c, qapprovers);
		}
		qapprovers.add(approver);
		if (approver.Id == null) {
			// TODO: May need to batch and flush these
			insert approver;
		}
	}


	public void deleteApprovers(Set<Id> quoteIds){
		List<Discount_Approver__c> batch = new List<Discount_Approver__c>();
		for (Id quoteId : quoteIds) {
			QuoteApprovers qapprovers = approversByQuoteId.get(quoteId);
			if (qapprovers != null) {
				batch.addAll(qapprovers.approvers);
				approversByQuoteId.remove(quoteId);
			}
		}

		if (!batch.isEmpty()) {
			delete batch;   
		}
	}
	
	/**
	 * Flushes pending inserts/updates to Discount_Approver__c records
	 */
	public void flush() {
		List<Discount_Approver__c> batch = new List<Discount_Approver__c>();
		for (QuoteApprovers approvers : approversByQuoteId.values()) {
			batch.addAll(approvers.approversByUserId.values());
		}
		upsert batch;
	}	
	
	public class QuoteApprovers {
		private Map<Id,Discount_Approver__c> approversByUserId;
		
		public Id quoteId {get; private set;}
		public List<Discount_Approver__c> approvers {get; private set;}
		
		public QuoteApprovers(Id quoteId) {
			this.quoteId = quoteId;
			approversByUserId = new Map<Id,Discount_Approver__c>();
			approvers = new List<Discount_Approver__c>();
		}
		
		public void add(Discount_Approver__c approver) {
			approvers.add(approver);
			approversByUserId.put(approver.Approver__c, approver);
		}
		
		/**
	     * Tests if user identified by specified ID has approved the quote
	     * identified by specified ID. This method tests against the internal
	     * Discont Approver cache -- it does not issue SOQL query.
	     * 
	     * @return <code>true</code> if approved; <code>false<code> otherwise
	     */
	    public Boolean hasUserApproved(Id userId) {
	    	return approversByUserId.containsKey(userId);
	    }
	    
	    public Discount_Approver__c getApproverByUserId(Id userId) {
	    	return approversByUserId.get(userId);
	    }
	    
	    public Discount_Approver__c getApproverByUserIdRequired(Id userId) {
	    	System.assert(approversByUserId.containsKey(userId), 'Missing Discount Approver for user: ' + userId + '; Cache: ' + approversByUserId);
	    	return approversByUserId.get(userId);
	    }
	    
	    /**
	     * Convenience method that returns size of approvers array.
	     */
	    public Integer size() {
	    	return approvers.size();
	    }
		
		/**
		 * Returns last Discount Approver who approved the quote given specified Approval Chain.
		 */
		public Discount_Approver__c getLastApprover(List<Approvers_Routing_Matrix__c> approvalChain) {
			if ((approvalChain == null) || approvalChain.isEmpty()) {
				return null;
			}
			
			Discount_Approver__c lastApprover = null;
			for (Approvers_Routing_Matrix__c approver : approvalChain) {
				if (!approversByUserId.containsKey(approver.User__c)) {
					return lastApprover;
				}
				lastApprover = approversByUserId.get(approver.User__c);
			}
			return null;
		}
		
		/**
		 * Creates Discount Approver for next approver in specified Approval Chain.
		 */
		public Discount_Approver__c createNextApprover(List<Approvers_Routing_Matrix__c> approvalChain) {
			if ((approvalChain == null) || approvalChain.isEmpty()) {
				return null;
			}
			
			for (Approvers_Routing_Matrix__c approver : approvalChain) {
				if (!approversByUserId.containsKey(approver.User__c)) {
					return new Discount_Approver__c(Quote__c=quoteId,Approver__c=approver.User__c);
				}
			}
			return null;
		}
	}
}