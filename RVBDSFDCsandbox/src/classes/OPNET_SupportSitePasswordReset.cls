/*********************
Class: OPNET_SupportSitePasswordReset
Description: Class to reset and email passwords to Opnet customers
Author: Rucha -  6/21/2013

**********************/
public with sharing class OPNET_SupportSitePasswordReset implements Database.Batchable<sObject>{
	 private Id fromEmailId;
	 private EmailTemplate et;   
     private String query;
     
	public OPNET_SupportSitePasswordReset(String q){
        //Get from address
        fromEmailId = RVBD_Support_Site_Properties__c.getInstance('Support Site Restriction').Org_Wide_Default_Email_Id__c;
        
        //Get template id       
        et = [Select Id,Body,HtmlValue from EmailTemplate Where DeveloperName='ResetPassword' and Folder.DeveloperName='SupportAlertTemplates'];          
        query = q;          
    }  
    
    public Database.Querylocator start(Database.BatchableContext bc){                            
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<sObject> contactList){    	
    	//List<Messaging.SingleEmailMessage> emailsToSendList = new List<Messaging.SingleEmailMessage>();
    	//Map<Id,Messaging.SingleEmailMessage> contactToEmailMap = new Map<Id,Messaging.SingleEmailMessage>();
    	List<Contact> conToUpdateList = new List<Contact>();
    	for(sObject obj : contactList){
    		Contact c = (Contact)obj;
    		if(c.Opnet_Group_IDs__r.size() > 0){
	    		String pwd = String.valueOf(Math.abs(Crypto.getRandomInteger()));
	    		Integer i=0;
	    		while(pwd.length() < 8 && i <5){//If password length is less than 8, regenerate the random number, attempt 5 times
	    			pwd = String.valueOf(Math.abs(Crypto.getRandomInteger()));
	    			i++;
	    		}
	    		
	    		if(i<5){
	    			String passWd = pwd.substring(0,8);
		    		//c.PasswordHash__c = EncodingUtil.Base64Encode(Crypto.generateDigest('MD5',Blob.valueOf(passWd)));
		    		c.PasswordHash__c = EncodingUtil.convertToHex(Crypto.generateDigest('MD5',Blob.valueOf(passWd)));
		    		c.PasswordChange__c = true;
		    		c.OPNET_SendPasswordResetEmail__c = true;
		    		
		    		c.OPNET_PasswordReset__c = 'https://login.riverbed.com/auth_submit.htm?username=' + EncodingUtil.urlEncode(c.Email,'UTF-8') + '&token=' + passWd;
		    		conToUpdateList.add(c);	
		    		
		    		/*Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();                     
		            email.setOrgWideEmailAddressId(fromEmailId);                                 
		           
		            email.setToAddresses(new List<String>{c.Email});	            
		           
		            String resetUrl = 'https://login.riverbed.com/auth_submit.htm?username=' + c.Email + '&riverbed.com&token=' + pwd;
		            email.setPlainTextBody(et.Body.replace('{!Password.ResetLink}',resetUrl));         
		          	email.setHtmlBody(et.HtmlValue.replace('{!Password.ResetLink}','<a href="' + resetUrl + '">' + resetUrl + '</a>'));
	            
		            if(!(Label.Email_to_Testers_Switch.equalsIgnoreCase('none'))){
		            	email.setCcAddresses(Label.Email_to_Testers_Switch.split(';'));
		            }    		
	                
	            	emailsToSendList.add(email);  
	            
	            	contactToEmailMap.put(c.Id,email);*/
	    		}
    		}       
    	}
    	system.debug(LoggingLevel.ERROR, 'Num of eligible contacts: ' + conToUpdateList.size());
    	update conToUpdateList;
    	
    	/*List<Messaging.SendEmailResult> smeResult = Messaging.sendEmail(emailsToSendList,false);
    	
        List<Contact> emailsSentSuccessfully = new List<Contact>();        
        Integer i = 0;
        for(Messaging.SendEmailResult sme : smeResult){        	
        	if(sme.isSuccess()){
        		Messaging.SingleEmailMessage email = emailsToSendList.get(i);       		
        		for(Id contactId : contactToEmailMap.keySet()){        			
        			if(email == contactToEmailMap.get(contactId)){
        				emailsSentSuccessfully.add(new Contact(Id=contactId,OPNET_PasswordResetEmailSent__c=true));
        				break;
        			}        			
        		}        		
        	}
        	i++;	
        }
        
        if(!emailsSentSuccessfully.isEmpty()){
        	update emailsSentSuccessfully;
        }*/
    }
    
     public void finish(Database.BatchableContext bc){
        
    }
    
     @isTest(SeeAllData=true)
    static void testEmailSupportSiteLink(){ 
        String query = 'Select Id,Email From Contact Where Support_Role__c=\'Support Access\' and PasswordHash__c=null limit 5';
		OPNET_SupportSitePasswordReset es = new OPNET_SupportSitePasswordReset(query);
		Database.executeBatch(es);
    }
}