@isTest
private class TestRenewalContactPermissionAss {
    
    static testmethod void unitTest(){
            Account acc = new Account(Name = 'Test acc', Region__c = 'AMER - Canada Region');
            insert acc;
            Contact con = new Contact(
                FirstName = 'Test',
                LastName = 'User', 
                Region__c = 'AMER - Canada Region',
                Email = 'test@ctm.com',
                AccountId = acc.Id,
                Account_Region__c = 'AMER - Canada Region'
            );
            insert con;
            con.Renewal_Contact__c = true;
            update con;
    }
}