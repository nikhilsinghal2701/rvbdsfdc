global class AccountOwnerEmailService {
    webservice static List<AccountFieldWrapper> getAccountOwnerEmail(List<Id> accIds){
        List<AccountFieldWrapper> wrapListMap = new List<AccountFieldWrapper>();
        for(Account a : [Select id, Owner.Email from Account Where Id in :accIds]){
            System.debug('REcord:::'+a);
            wrapListMap.add(new AccountFieldWrapper (String.valueof(a.Id), a.Owner.Email));
        }
        System.debug('wrapListMap:::'+wrapListMap);
        return wrapListMap ;
    }
    
    global class AccountFieldWrapper {
        webservice String accId {get;set;}
        webservice String email {get;set;}
        global AccountFieldWrapper (String accId, String email){
            this.accId = accId;
            this.email = email;
        
        }
    }
    
}