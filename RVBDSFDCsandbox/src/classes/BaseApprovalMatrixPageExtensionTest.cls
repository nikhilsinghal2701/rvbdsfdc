/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the test class for the class BaseApprovalMatrixPageExtension
Created Date : Oct 2011
*******************************************************************************************************/

@isTest
private class BaseApprovalMatrixPageExtensionTest {
    

    static testMethod void BaseApprovalMatrixPageExtensionTest() {
        test.starttest();
        Base_Approval_Matrix__c Obj = new Base_Approval_Matrix__c (BM_Active__C=true);
        List<Category_Master__c> catList=DiscountApprovalTestHlp.insertCategoryMasterData();
        PageReference VFpage = Page.BaseApprovalMatrixPage;
        test.setCurrentPage(VFpage);
        ApexPages.StandardController VFpage_Extn = new ApexPages.StandardController(obj);
        BaseApprovalMatrixPageExtension CLS = new BaseApprovalMatrixPageExtension(VFpage_Extn);
        CLS.getCategories();
        for(Category_Master__c cat : catList)
        {
            CLS.selectedCats.add(cat.Name);
            
        }
        
        CLS.Save();
        System.assertequals(CLS.selectedCats.size(),CLS.detailList.size());// Verifies that inserted number of child records is same as number of categories selected.
        CLS.selectedCats.clear();
        VFpage = Page.BaseApprovalMatrixPage;
        VFpage.getParameters().put('id',obj.id);
        test.setCurrentPage(VFpage);
        VFpage_Extn = new ApexPages.StandardController(obj);
        CLS = new BaseApprovalMatrixPageExtension(VFpage_Extn);
        Category_Master__c cat = new Category_Master__c(Name ='TT3');
        CLS.getCategories();
        String removedStr=  CLS.selectedCats.remove(CLS.selectedCats.size()-1);
        CLS.selectedCats.add(cat.Name);
        CLS.Save();
        Base_Approval_Matrix_Detail__c b = [select Id, Category_Master__r.Name, Active__C from Base_Approval_Matrix_Detail__c where Category_Master__r.Name = :removedStr order by active__C  limit 1];
       System.assertequals(b.Active__c,false); // Verifies that the active flag is reset for the deselected records.
      
       obj.BM_Active__c=false;
       update obj;
       CLS.selectedCats.clear();
        VFpage = Page.BaseApprovalMatrixPage;
        VFpage.getParameters().put('id',obj.id);
        test.setCurrentPage(VFpage);
        VFpage_Extn = new ApexPages.StandardController(obj);
        CLS = new BaseApprovalMatrixPageExtension(VFpage_Extn);
        CLS.save();
        test.stoptest();  

    }
}