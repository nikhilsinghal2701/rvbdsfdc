@isTest
private class DiscountApprovalTest {
    /**
    *   Test methods for DiscountApproval
    *
    *   Author megar@apprivo.com
    */  
        
    testMethod static void testRunRules() {
    
        //Rohit - 01082010
        DiscountApproval.isTest = true;
        
        inactivateExistingRules();
        
        system.test.startTest();
        
      //  Discount_Schedule__c rule = setupTestRule();
        
        // Test Opportunity with inactive rule
     //   Opportunity opp = DiscountApprovalTestHlp.createOpp();
         Opportunity opp= new Opportunity(Discount_Schedule__c=null);
         List<Account> accList= [select Id,Name from Account where Name in ('Arrow Electronics','Arrow Enterprise Computing Systems','Avnet Technology Solutions')];
      for(Account ac : accList)
      {
        if(ac.Name=='Avnet Technology Solutions')
        opp.Tier2__c=ac.Id;
        else if(ac.Name=='Arrow Electronics')
        opp.AccountId=ac.ID;
        else if(ac.Name=='Arrow Enterprise Computing Systems')
        opp.Sold_to_Partner__c=ac.Id;
      }
       opp.Sold_to_Partner_Level__c='Distributor';
       opp.Tier2_Level__c='Distributor';
       opp.CloseDate = System.today();
        opp.Name = 'Test Opp1';
        opp.StageName = '0 - Prospect';
        opp.Number_Eval_Products__c = 5;
        opp.Registered_Deal__c = false;
        opp.Channel__c='Direct';
       insert(opp);
       opp.NSD_Calc__c=true;
       DiscountApproval.isTest = true;
       update(opp); 
        opp.NSD_Calc__c=true;
       DiscountApproval.isTest = true;
       update(opp);
        
       System.debug(LoggingLevel.INFO,'ttttttttttttestttttttttt--'+opp.Discount_Schedule__c);
       //Quote__c q = DiscountApprovalTestHlp.createQuote(opp.Id);
       //Map<Id,Quote__c> quoteMap = new Map<Id,Quote__c>();
       //quoteMap.put(q.Id,q);
       //DiscountManagementHelper.insertMarginSummaryRecords(quoteMap,true); 
        system.test.stopTest();    
    }
    
    
  testMethod public static void testMultipleRules() {
        
        //inactivateExistingRules();
        system.test.startTest();
        DiscountCategory__c dc1 = createDiscountCategory();
        Discount_Schedule__c rule1 = setupTestRule();
        rule1.Discount_Category__c = dc1.id; 
        rule1.Active__c = true;
        update rule1;
        
        
        Discount_Schedule__c rule2 = new Discount_Schedule__c();
        
        rule2.Active__c = true;
        DiscountCategory__c dc2 = createDiscountCategory();
        rule2.Discount_Category__c = dc2.id;        
        insert rule2;
        
        
        Discount_Condition__c c1 = new Discount_Condition__c();
        c1.Discount_Schedule__c = rule2.Id;
        c1.Field__c = 'Opportunity.Number_Eval_Products_';
        c1.Operator__c = 'equals';
        c1.Value__c = '3';
        insert c1;
        
        RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc;
        
        // Test inserting Opportunity with neither rule's conditions met 
        Opportunity opp = new Opportunity();
        opp.CloseDate = System.today();
        opp.Name = 'Test Opp';
        opp.StageName = 'Expired';
        opp.Number_Eval_Products__c = 5;
        opp.Registered_Deal__c = true;
        opp.Channel__c='Reseller';
        opp.Sold_To_Partner__c=stpAcc.Id;
        insert opp;
        
        Id schId = getDiscountSchedule(opp.Id);
        system.assertEquals (null, schId );
       

        opp.Number_Eval_Products__c = 3;
        opp.NSD_Calc__c = true;
        opp.StageName = '0 - Prospect';
        update opp;
        
        Id schId2 = getDiscountSchedule(opp.Id);
        //commented skm to revisit phase2 
        //system.assertNotEquals (null, schId2);
        //system.assertEquals (schId2, rule2.Id);
        system.test.stopTest();
       
    }
    
    private static DiscountCategory__c createDiscountCategory(){
        
        DiscountCategory__c dc = new DiscountCategory__c();
        dc.DiscountProducts__c = 15;
        dc.DiscountSupportBand0__c = 15;
        dc.DiscountSupportBand1__c = 15;
        dc.DiscountSupportBand2__c = 15;
        dc.DiscountDemos__c = 15;
        dc.DiscountNone__c = 15;
        dc.DiscountSpare__c = 15;
        dc.DiscountServices__c = 15;
        insert dc;
        
        return dc;  
    }
    
    private static DiscountCategory__c getDiscountCategory(Id oppId){
        
        DiscountCategory__c dc = null;
        
        Id dcId = [select Discount_Category__c from Opportunity where Id = : oppId].Discount_Category__c;
        
        if (dcId != null){
             dc = [select Id,
                    DiscountProducts__c,
                    DiscountSupportBand0__c,
                    DiscountSupportBand1__c,
                    DiscountSupportBand2__c,
                    DiscountDemos__c,
                    DiscountNone__c,
                    DiscountSpare__c,
                    DiscountServices__c 
                    from DiscountCategory__c 
                    where Id = : dcId];
        }
        return dc;  
    }   
    
    private static Id getDiscountSchedule(Id oppId){
        
        Id dcId = [select Discount_Schedule__c from Opportunity where Id = : oppId].Discount_Schedule__c;

        return dcId;    
    }   
    private static void inactivateExistingRules() {
        // Turn off all existing rules
        List<Discount_Schedule__c> rules = new List<Discount_Schedule__c>();
        for (Discount_Schedule__c rule : [SELECT Id,Active__c FROM Discount_Schedule__c limit 200]) {
            rule.Active__c = false;
            rules.add(rule);
        }
        update rules;
    }
    
   private static Discount_Schedule__c setupTestRule() {
        // Setup test objects
        DiscountCategory__c dc = createDiscountCategory(); 
        
        Discount_Schedule__c rule = new Discount_Schedule__c();
        rule.Discount_Category__c = dc.id;
        rule.Active__c = true;
        rule.ApplyUplift__c=false;
        insert rule;
        
        rule = [select Discount_Type__c, RecordTypeId from Discount_Schedule__c where id = : rule.Id];
        system.debug('rule: ' + rule);
        Discount_Condition__c c1 = new Discount_Condition__c();
        c1.Discount_Schedule__c = rule.Id;
        c1.Field__c = 'Opportunity.Registered_Deal_';
        c1.Operator__c = 'equals';
        c1.Value__c = 'true';
        //system.debug('*************************' +  c1.Value__c);
        insert c1;
        
        
        Discount_Condition__c c2 = new Discount_Condition__c();
        c2.Discount_Schedule__c = rule.Id;
        c2.Field__c = 'Opportunity.StageName';
        c2.Operator__c = 'equals';
        c2.Value__c = '0 - Prospect';
        insert c2;
        
        Discount_Condition__c c3 = new Discount_Condition__c();
        c3.Discount_Schedule__c = rule.Id;
        c3.Field__c = 'Opportunity.Number_Eval_Products_';
        c3.Operator__c = DiscountApproval.OPERATOR_EQUALS;
        c3.Value__c = '5';
        insert c3;

        return rule;
    }
    
    private static Discount_Schedule__c setupDynSoqlTestRule() {
        // Setup test objects
        DiscountCategory__c dc = createDiscountCategory(); 
        
        Discount_Schedule__c rule = new Discount_Schedule__c();
        rule.Discount_Category__c = dc.id;
        rule.Active__c = true;
        insert rule;
        
        Discount_Condition__c c1 = new Discount_Condition__c();
        c1.Discount_Schedule__c = rule.Id;
        c1.Field__c = DiscountApproval.END_USER_ACCOUNT + '.Name';
        c1.Operator__c = 'equals';
        c1.Value__c = 'IBM';

        insert c1;

        return rule;
    }    
    
    static testMethod void testDynSOQL() {
        
        //Rohit - 01082010
        DiscountApproval.isTest = true;
        
        //inactivateExistingRules();
        system.test.startTest();
        Discount_Schedule__c rule = setupDynSoqlTestRule(); 
        
        Account a = new account (name = 'no match',BillingState='Birmingham',BillingCountry='AF');
        
        insert a;
        
        RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc;
        
        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId=accPatRT.Id;
        tier2Acc.name='stpAccount';
        tier2Acc.Type='Distributor';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Authorizations_Specializations__c='RASP;RVSP';
        insert tier2Acc;
          
        // Test Opportunity with inactive rule
        Opportunity opp = new Opportunity();
        opp.CloseDate = System.today();
        opp.Name = 'Test Opp';  
        opp.StageName = 'Expired';        
        opp.accountId =  a.Id; 
        opp.Channel__c='Dstributor';
        opp.Sold_To_Partner__c=stpAcc.Id;
        opp.Tier2__c= tier2Acc.Id;
        insert opp;
                    
        Id schId = getDiscountSchedule(opp.Id);
        system.assertEquals (null, schId); 
               
        // Test updating Opportunity with active rule and matching conditions
        a.name = 'IBM';
        update a;
        
        opp.StageName = '0 - Prospect';
        opp.NSD_Calc__c = true;
        update opp;
       
    
        Id schId2 = getDiscountSchedule(opp.Id);
        //commented assert and need to revisit on phase2
        //system.assertEquals (rule.id, schId2); 

         system.test.stopTest();
    
    }
    
     public static testMethod void testRuleConditions() {
        
        inactivateExistingRules();
        
         system.test.startTest();

        //test not equal
        Discount_Schedule__c rule1 = new Discount_Schedule__c();
        DiscountCategory__c dc1 = createDiscountCategory(); 
        rule1.Discount_Category__c = dc1.id;
        

        rule1.Active__c = true;
        insert rule1;

        Discount_Condition__c c1 = new Discount_Condition__c();
        c1.Discount_Schedule__c = rule1.Id;
        c1.Field__c = 'Opportunity.Number_Eval_Products_';
        c1.Operator__c = DiscountApproval.OPERATOR_NOT_EQUAL;
        c1.Value__c = '1';
        insert c1;

        Opportunity opp1 = new Opportunity();
        opp1.CloseDate = System.today();
        opp1.Name = 'Test Opp';
        opp1.StageName = 'Prospecting';

        //insert opp1;


        System.Debug('opp1='+opp1);
        System.Debug('test not equal');

        
        inactivateExistingRules();

        //test less than
        Discount_Schedule__c rule2 = new Discount_Schedule__c();
        DiscountCategory__c dc2 = createDiscountCategory(); 
        rule2.Discount_Category__c = dc2.id;
        rule2.Active__c = true;
        insert rule2;

        Discount_Condition__c c2 = new Discount_Condition__c();
        c2.Discount_Schedule__c = rule2.Id;
        c2.Field__c = 'Opportunity.Number_Eval_Products_';
        c2.Operator__c = DiscountApproval.OPERATOR_LESS_THAN;
        c2.Value__c = '5';
        insert c2;



        Opportunity opp2 = new Opportunity();
        opp2.CloseDate = System.today();
        opp2.Name = 'Test Opp';
        opp2.StageName = 'Prospecting';
        //insert opp2;

        System.Debug('opp2.Number_Eval_Products_='+opp2.Number_Eval_Products__c);
        System.Debug('test less than');

        
        inactivateExistingRules();

        //test less than or equal to
        Discount_Schedule__c rule3 = new Discount_Schedule__c();
        DiscountCategory__c dc3 = createDiscountCategory(); 
        rule3.Discount_Category__c = dc3.id;
        rule3.Active__c = true;
        insert rule3;

        Discount_Condition__c c3 = new Discount_Condition__c();
        c3.Discount_Schedule__c = rule3.Id;
        c3.Field__c = 'Opportunity.Number_Eval_Products__c';
        c3.Operator__c = DiscountApproval.OPERATOR_LESS_OR_EQUAL;
        c3.Value__c = '5';
        insert c3;

        Opportunity opp3 = new Opportunity();
        opp3.CloseDate = System.today();
        opp3.Name = 'Test Opp';
        opp3.StageName = 'Prospecting';
        //insert opp3;

        System.Debug('opp3.Number_Eval_Products__c='+opp3.Number_Eval_Products__c);
        System.Debug('test less than or equal to');

        
        inactivateExistingRules();

        //test greater than
        Discount_Schedule__c rule4 = new Discount_Schedule__c();
        DiscountCategory__c dc4 = createDiscountCategory(); 
        rule4.Discount_Category__c = dc4.id;
        rule4.Active__c = true;
        insert rule4;

        Discount_Condition__c c4 = new Discount_Condition__c();
        c4.Discount_Schedule__c = rule4.Id;
        c4.Field__c = 'Opportunity.Number_Eval_Products__c';
        c4.Operator__c = DiscountApproval.OPERATOR_GREATER_THAN;
        c4.Value__c = '5';
        insert c4;

        Opportunity opp4 = new Opportunity();
        opp4.CloseDate = System.today();
        opp4.Name = 'Test Opp';
        opp4.StageName = 'Prospecting';
        //insert opp4;

        System.Debug('opp4.Number_Eval_Products__c='+opp4.Number_Eval_Products__c);
        System.Debug('test greater than');

       
        inactivateExistingRules();

        //test greater than or equal to
        Discount_Schedule__c rule5 = new Discount_Schedule__c();
        DiscountCategory__c dc5 = createDiscountCategory(); 
        rule5.Discount_Category__c = dc5.id;
        rule5.Active__c = true;
        insert rule5;

        Discount_Condition__c c5 = new Discount_Condition__c();
        c5.Discount_Schedule__c = rule5.Id;
        c5.Field__c = 'Opportunity.Number_Eval_Products__c';
        c5.Operator__c = DiscountApproval.OPERATOR_GREATER_OR_EQUAL;
        c5.Value__c = '5';
        insert c5;

        Opportunity opp5 = new Opportunity();
        opp5.CloseDate = System.today();
        opp5.Name = 'Test Opp';
        opp5.StageName = 'Prospecting';
        //insert opp5;

        System.Debug('opp5.Number_Eval_Products__c='+opp5.Number_Eval_Products__c);
        System.Debug('test greater than or equal to');  
        
         system.test.stopTest();
         
  }
     
    testMethod public static void testMultipleRecords() {
        //inactivateExistingRules();
         system.test.startTest();
         List<String> values = new List<String>();
        values.add('1');
        values.add('15');
        values.add('30');
        
        List<Opportunity> opps = new List<Opportunity>();
        for (Integer i=0;i<3;i++) {
            Opportunity opp = new Opportunity();
            opp.Name = 'Test' + i;
            opp.StageName = 'Prospect';
            opp.CloseDate = System.today().addMonths(2);
            opp.Channel__c='Direct';
            opps.add(opp);
        }
        
        
        for (Integer i=0;i<3;i++) {
            Discount_Schedule__c rule = new Discount_Schedule__c();


            rule.Active__c = true;
            insert rule;
            
            Discount_Condition__c c1 = new Discount_Condition__c();
            c1.Discount_Schedule__c = rule.Id;
            c1.Field__c = 'Opportunity.Number_Eval_Products__c';
            c1.Operator__c = 'equals';
            c1.Value__c = values.get(i);
            insert c1;
            

        }
        
        insert opps;   
        //tbd assert        
        system.test.stopTest();
    }


    private static List<Opportunity> toList(Opportunity opp) {
        List<Opportunity> result = new List<Opportunity>();
        result.add(opp);
        return result;
        
    }
    
    testMethod public static void insertMarginSummaryTest() {
        Test.startTest();
         List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
       
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        Quote__C q = DiscountApprovalTestHlp.createQuote(opp.Id);
        //update(q);
         Schema.DescribeFieldResult F = Category_Master__c.Group__c.getDescribe();
            List<Schema.PicklistEntry> P = F.getPicklistValues();
            
            Set<String> catSet = new Set<String>();
             for(Schema.PicklistEntry entry : P)
            {
                catSet.add(entry.getValue());
            }
            
             List<Margin_Summary__c> marginList = [Select m.Total_Value__c, m.Quote__c,m.Margin__c, m.Id, m.Group_Name__c, m.Cost__c,m.Extended_List_Price__c,m.Extended_Discount__c From Margin_Summary__c m where m.Quote__C = :q.Id];
            System.assertequals(marginList.size(),catSet.size());
        test.stoptest();
    }
    
    
}