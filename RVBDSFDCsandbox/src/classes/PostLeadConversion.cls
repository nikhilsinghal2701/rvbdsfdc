/*
* This class handles all the events that need to occur when a lead of type 
* Partner recruitment is converted
*/
public class PostLeadConversion {
	
	public static Boolean runOnce = false;
	
	private static final String PARTNER_USER_EMAIL_SUFFIX = '.rvbd';
	private static final String PARTNER_USER_TIME_ZONE = 'America/Los_Angeles';
	private static final String PARTNER_USER_LOCALE = 'en_US';
	private static final String PARTNER_USER_EMAIL_ENCODING = 'ISO-8859-1';
	private static final String PARTNER_USER_LANGUAGE_LOCALE = 'en_US';
	
	
	//public static final String PARTNER_RECRUITMENT_RECORD_TYPE_ID = '012S00000008anC';
	//private static final String PARTNER_USER_DEFAULT_PROFILE = '00eS0000000I05w';
	
	private static final String PARTNER_APP_CATEGORY_DEFAULT_STATUS = 'In Progress';
	private static final String APP_TASK_DEFAULT_STATUS = 'Not Started';
	
	private static final String PARTNER_APP_NAME_SUFFIX = ' Partner Application';
	private static final String PARTNER_APP_DEFAULT_STATUS = 'Draft';
	
	//maybe query by name
	//Sandbox
	//private static final String PARTNER_APP_TASK_CREDIT_RECORD_TYPE_ID = '012S00000008an2';
	//private static final String PARTNER_APP_TASK_STND_RECORD_TYPE_ID = '012S00000008an3';

	//prod
	private static final String PARTNER_APP_TASK_CREDIT_RECORD_TYPE_ID = '012700000005d8y';
	private static final String PARTNER_APP_TASK_STND_RECORD_TYPE_ID = '012700000005d8z';

	/*
	*
	*/
	public void process(Lead l) {
		Id recordTypeId = l.RecordTypeId;
		System.debug('recordTypeId = ' + recordTypeId);
		Id partnerRecruitmentRecordTypeId = [Select r.Id From RecordType r where SobjectType = 'Lead' And Name = 'Partner Recruitment'].Id;
		if (recordTypeId != partnerRecruitmentRecordTypeId) {//only for partner recruitment record type
			return;	
		}
		Id oppId = l.ConvertedOpportunityId;
		Id accountId = l.ConvertedAccountId;
		Id contactId = l.ConvertedContactId;
		System.debug('opp id = ' + oppId);

        //get the task template based on the fields
        RuleEvaluator evaluator = new RuleEvaluator();
        Id taskTemplateId = evaluator.evaluate(l);

        Boolean isPartnerAccount = enablePartnerAccount(accountId);
        //TEST
        String ownerName = [Select Owner.name from Account where id = :accountId].Owner.name;
        System.debug('owner name = ' + ownerName);
        Id partnerUserId = null;
        if (isPartnerAccount) {
        	partnerUserId = enablePartnerUser(contactId);
	        //now create the next contact
	        createSalesContact(l, accountId);
        }
        
        //now create the partner application object
        Partner_Application__c partnerApp = new Partner_Application__c(name = l.Company + PARTNER_APP_NAME_SUFFIX,
        	Account__c = accountId, Application_Submit_Date__c = Date.today(), 
        	Application_Status__c = PARTNER_APP_DEFAULT_STATUS,
        	Expected_Riverbed_Annual_Revenue__c = l.Expected_Riverbed_Annual_Revenue__c,
        	Current_Opportunity_Value__c = l.Current_Opportunity_Value__c,
        	Email_RSM__c = l.Email_RSM__c, Email_ISR__c = l.Email_ISR__c, Email_Distributor__c = l.Email_Distributor__c,
        	Email_CAM_VP__c = l.Email_CAM_VP__c, Provides_Level_1_2_Support__c = l.Provides_Level_1_2_Support__c,
        	 Lead_Submitter__c = l.OwnerId, Primary_Contact__c = contactId);
        partnerApp.One_Time_User__c = (l.Partner_Level__c.equalsIgnoreCase('One-off'));
        //partnerApp.OwnerId = partnerUserId;
        insert partnerApp;
        
        //get the approval history over
        createPartnerAppApprovalHistory(l, partnerApp.Id);
        
        //get the task for the template selected on the lead
        List<Tasks__c> tasks = [Select Id, Name, Task_Category__c, Time__c, Timeframe__c, Time_Type__c, Time_Unit__c, Document_ID__c,
        	Instructions__c, Description__c
        	from Tasks__c where Task_Template__c = :taskTemplateId];
        	
        Map<String, Partner_Application_Category__c> applicationCategories = getApplicationCategories(tasks, partnerApp);
        if (! applicationCategories.isEmpty()) {
        	insert applicationCategories.values();
        }

		List<Application_Task__c> appTaskList = getAppTaskList(tasks, applicationCategories, partnerUserId, l.Company); 
       	if (! appTaskList.isEmpty()) {
        	insert appTaskList;
        }
        
        createCreditApplication(appTaskList, l, accountId);

        //give the partner access to his own account
        //CreateAccountShare.create(accountId, partnerUserId);
	}
	
	/*
	*
	*/
	private Boolean enablePartnerAccount(Id accountId) {
		Id recordTypeId = [Select r.Id From RecordType r where SobjectType='Account' and Name = 'Partner Account'].Id;
		Account acc = new Account(id = accountId, isPartner = true, recordTypeId = recordTypeId, Partner_Recruitment_Account__c = true);
		update acc;
		return true;
	}
	
	/*
	*
	*/
	private Boolean createPartnerAppApprovalHistory(Lead l, Id partnerAppId) {
		Lead obj = [Select (Select StepStatus, OriginalActorId, ActorId, Comments, CreatedDate From ProcessSteps) From Lead l where l.Id = :l.Id];
		List<Id> actorIds = new List<Id>();
		for (ProcessInstanceHistory processInstance : obj.ProcessSteps) {
			actorIds.add(processInstance.OriginalActorId);
		}
		//List<Sobject> actors = Database.query('Select name from sObject where Id In ' + actorIds);
		List<Partner_Application_Approval_History__c> partnerAppApprovalHistoryList = new List<Partner_Application_Approval_History__c>();
		for (ProcessInstanceHistory processInstance : obj.ProcessSteps) {
			if(!String.valueOf(processInstance.ActorId).startsWith('00G')){
				Partner_Application_Approval_History__c partnerAppApprovalHistory 
					= new Partner_Application_Approval_History__c(Status__c = processInstance.StepStatus, Comments__c = processInstance.Comments,
						Assigned_To__c = processInstance.OriginalActorId, Actual_Approver__c = processInstance.ActorId,
						Process_Date__c = processInstance.CreatedDate, Partner_Application__c = partnerAppId);
				partnerAppApprovalHistoryList.add(partnerAppApprovalHistory);
			}
		}
		try{
			insert partnerAppApprovalHistoryList;
			return true; 
		}catch(Exception e){
			System.debug('Exception in inserting approval history : '+ e.getMessage());
			return true;
		}
	}
	
	/*
	*
	*/
	private Id enablePartnerUser(Id contactId) {
		//get the contact
		Contact c = [Select Id, firstName, lastName, email, title, phone from Contact where Id = :contactId];
		System.debug('contact = ' + c);
		String userName = c.Email + PARTNER_USER_EMAIL_SUFFIX;
		System.debug('username = ' + userName);
		Id defaultUserProfile = [Select Id from profile where name = 'Partner User - Unregistered'].Id;
		System.debug('defaultUserProfile = ' + defaultUserProfile);
		try {
			User u = new User(firstName = c.FirstName, 
				LastName = c.lastName, 
				Alias = createAlias(c),
				email = c.Email, 
				Username = userName, 
				Title = c.title, 
				Phone = c.phone,
				ProfileId = defaultUserProfile,
				IsActive = true, 
				ContactId = c.Id, 
				TimeZoneSidKey = PARTNER_USER_TIME_ZONE, 
				LocaleSidKey = PARTNER_USER_LOCALE,
				EmailEncodingKey = PARTNER_USER_EMAIL_ENCODING, 
				LanguageLocaleKey = PARTNER_USER_LANGUAGE_LOCALE,
				Temporary_Password_Set__c = true);
				
				System.debug('Now Insert Partner');
				insert u;
				System.debug('enablePartnerUser userId = ' + u.Id);
				return u.Id;
		} catch(Exception e) {
			System.debug('error: ' + e.getMessage());
			throw e;
		}
	}
	
	private void createAccountShare(Id accountId, Id partnerUserId) {
		/*AccountShare accShare = new AccountShare(AccountId = accountId, UserOrGroupId = partnerUserId,
			AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'Read',
			RowCause = 'Manual Sharing');
		insert accShare;*/
	}
	
	/*
	*
	*/
	private String createAlias(Contact c) {
		String alias = (c.firstName != null) ? c.FirstName : c.LastName;
		if (alias.length() > 0) {
			alias.substring(0, 1);	
		}
		alias += c.lastName;
		return alias;
	}
	
	/*
	*
	*/
	private String createSalesContact(Lead l, Id accountId) {
		if (l.Sales_Contact_Last_Name__c != null && ( ! l.Sales_Contact_Last_Name__c.equals(''))) {
			Contact salesContact = new Contact(firstName = l.Sales_Contact_First_Name__c, 
				lastName = l.Sales_Contact_Last_Name__c, 
				title = l.Sales_Contact_Title__c, email = l.Sales_Contact_Email__c, 
				phone = l.Sales_Contact_Phone__c, Contact_Type__c = 'Primary Sales',
				AccountId = accountId);
			insert salesContact;
			return salesContact.Id;	
		}
		return null;
	}
	
	/*
	*
	*/
	private Map<String, Partner_Application_Category__c> getApplicationCategories(List<Tasks__c> tasks, Partner_Application__c partnerApp) {
        Map<String, Partner_Application_Category__c> applicationCategories = new Map<String, Partner_Application_Category__c>();
        
        for (Tasks__c t : tasks) {
        	String taskCategory = t.Task_Category__c;
        	Partner_Application_Category__c appCategory = applicationCategories.get(taskCategory);
        	if (appCategory == null) {
        		appCategory = new Partner_Application_Category__c(Name = t.Task_Category__c, Partner_Application__c = partnerApp.Id,
        			Status__c = PARTNER_APP_CATEGORY_DEFAULT_STATUS);
        		applicationCategories.put(taskCategory, appCategory);
        	}
        }
		return applicationCategories;	
	}
	
	/*
	*
	*/
	private List<Application_Task__c> getAppTaskList(List<Tasks__c> tasks, 
		Map<String, Partner_Application_Category__c> applicationCategories, Id partnerUserId, String accName) 
	{
		List<RecordType> recordTypes = [Select Id, Name from RecordType where SobjectType = 'Application_Task__c'];
		List<Application_Task__c> appTaskList = new List<Application_Task__c>(); 
        for (Tasks__c t : tasks) {
        	String taskCategory = t.Task_Category__c;
        	Partner_Application_Category__c appCategory = applicationCategories.get(taskCategory);
			Application_Task__c appTask = new Application_Task__c(Name = t.Name + accName,
				Application_Category__c = appCategory.Id,
				Assigned_to__c = partnerUserId, Status__c = APP_TASK_DEFAULT_STATUS, Time__c = t.Time__c,
				Timeframe__c = t.Timeframe__c, Time_Type__c = t.Time_Type__c,
				Time_Unit__c = t.Time_Unit__c,
				Instructions__c = t.Instructions__c,
				Description__c = t.Description__c,
				RecordTypeId = getRecordTypeForAppTask(taskCategory, recordTypes));
				if (t.Document_ID__c != null) appTask.Document__c = t.Document_ID__c; 
			Date dueDate = getDueDate(t);
			appTask.Due_Date__c = dueDate;
			appTaskList.add(appTask);
        }
		return appTaskList;
	}
	
	/*
	*
	*/
	private Id getRecordTypeForAppTask(String taskCategory, List<RecordType> recordTypes) {
		for (RecordType rt : recordTypes) {
			String name = rt.Name;
			if (taskCategory.equalsIgnoreCase('credit')) {
				if (name.equalsIgnoreCase('Credit Application')) {
					return rt.Id;
				}
			} else {
				if (name.equalsIgnoreCase('Standard Application Task')) {
					return rt.Id;
				}
			}
		}
		return PARTNER_APP_TASK_STND_RECORD_TYPE_ID;
	}
	
	/*
	*
	*/
	private void createCreditApplication(List<Application_Task__c> appTaskList, Lead l, Id accountId) 
	{
		System.debug('Start createCreditApplication() - ' + appTaskList.size());
		List<Credit_Application__c> creditAppList = new List<Credit_Application__c>();
		String accountName = [Select Name from Account where Id = :accountId].Name;
		for (Application_Task__c appTask : appTaskList) {
			String rt = appTask.RecordTypeId;
			System.debug('RecordTypeId - ' + rt);
			if (rt.startsWith(PARTNER_APP_TASK_CREDIT_RECORD_TYPE_ID)) {
				System.debug('Now create the credit app object');
				//create a credit app 
				Credit_Application__c creditApp = new Credit_Application__c(Partner_Application_Task__c = appTask.Id,
					Application_Date__c = Date.today(), City__c = l.city, State__c = l.State,
					Zip_Code__c = l.PostalCode, Country__c = l.Country, Telephone__c = l.Phone,
					Email__c = l.Email, Fax__c = l.Fax, Organization__c = accountName,
					Name = accountName + ' Credit Application');
				creditAppList.add(creditApp);
			}
				
		}
		
		System.debug('creditAppList = ' + creditAppList.size());
		if (! creditAppList.isEmpty()) {
			insert creditAppList;
		}
	}
	
	/*
	*
	*/
	private Date getDueDate(Tasks__c t) {
		Date dueDate = Date.today();

		Integer noOfDays = integer.valueOf(String.valueOf(t.Time__c));
		System.debug('noOfDays = ' + noOfDays);

		if (t.Time_Unit__c == 'Days') {
			dueDate = dueDate.addDays(noOfDays);
		} else if (t.Time_Unit__c == 'Weeks') {
			dueDate = dueDate.addDays(noOfDays * 7);
		}else if (t.Time_Unit__c == 'Months') {
			dueDate = dueDate.addMonths(noOfDays);
		}
		return dueDate;
	}
}


/*
TO get attachments - 
Select a.Name, (select t.name from attachments t)  From Application_Task__c a where Id='a0R80000000pvhG'

*/