/**
* Lookup class to control the logic of support provider lookup
* field 
*
* @auther Remy.Chen
* @date 10/24/2013
*/
public class LookupController{
    //Set search string and account list variables
    public string searchStr {get;set;}
    public List<Account> accList{get;set;}
                                              
    public LookupController(){
       Map<String,String> m = ApexPages.currentPage().getParameters();
       searchStr = m.get('qryText');
       accList = accountList();  
    }
    
    public PageReference search(){
        accList = accountList();
        return null;
        
    }
    
    /**
    * Method to get account list with specific 
    * search partten
    *
    * @return the list of account
    */
    public List<Account> accountList() {
        if(String.isNotEmpty(searchStr)){
            searchStr = searchStr.replace('*', '');
        }
        /*
        String qry = 'SELECT Id, Name,Type FROM Account'+ 
        ' WHERE Authorizations_Specializations__c IN (\'RASP\', \'RASA\', \'SUP\')'+
        ' AND Name like \'%'+searchStr+'%\'';*/
        String qry = 'SELECT Id, Name,Type,Authorizations_Specializations__c FROM Account'+ 
        ' WHERE Authorizations_Specializations__c !=null'+
        ' AND Name like \'%'+searchStr+'%\'';
        List<sObject> soList = Database.query(qry);
        List<Account>newList=new List<Account>();
        for(sObject temp:soList){
            Account acc=(Account)temp;
            if(acc.Authorizations_Specializations__c.contains('RASP')||acc.Authorizations_Specializations__c.contains('RASA')||acc.Authorizations_Specializations__c.contains('SUP')){
                newList.add(acc);
            }
        }
        //return (List<Account>)soList;
        return newList;
    }
    
}