/*
	Class: Constant
    Purpose: to declare constants and static flags
    Author: Jaya ( Perficient )
    Created Date: 7/31/2014
*/
public without sharing class Constant {
	public static Boolean disablePartnerCompetencyTrigger = false;
	public static String DISTY_ACCOUNT = 'Distributor';
	public static final String GENERAL_PARTNER_COMPETENCY = 'General';
	public static final String ACCOUNT_PARTNER = 'Partner Account';
	
	public static final String PARTNET_TYPE_VAR = 'VAR';
	public static final String PARTNET_LEVEL = 'Authorized';
	public static final String PARTNET_STATUS_PROBATION = 'Probation';
	public static final String PARTNET_STATUS_GSTAND = 'Good Standing';
	public static final String PRM_DISTI_ADMIN_PROFILE = 'PRM - Distributor - Admin';

}