public class testApprovalStep {
	
	public ProcessInstanceWorkItem piwi {get; set;}
	Id id;
    public testApprovalStep(ApexPages.StandardController controller) {
        id = ApexPages.currentPage().getParameters().get('id');
        if(id!=null){
        	piwi = [select id, ProcessInstance.TargetObject.Name from ProcessInstanceWorkItem where id=:id];
        }else{
        	this.piwi=(ProcessInstanceWorkItem)controller.getRecord();
        }
        if(piwi==null){
        	piwi=new ProcessInstanceWorkItem();
        }
    }  
	
	static testmethod void testApprovalComments(){
		//Lead l = new Lead(lastname='test', company = 'test company', street='srteet', city='city', state='CA', country = 'US', email = 'a@a.com');
		Lead l = DealRegApprovalActionTestUtil.createLeadDR();
		//insert l;
		
		// Create an approval request for the account 
		    
		Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
		req1.setComments('Submitting request for approval.');
		req1.setObjectId(l.id);
		
		// Submit the approval request for the account 
		    
		Approval.ProcessResult result = Approval.process(req1);
		
		// Verify the result 
//		System.debug('Request Comments = ' + result.get )    
		System.assert(result.isSuccess());
		
		System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
		
		// Approve the submitted request 
		    
		
		// First, get the ID of the newly created item 
		    
		List<Id> newWorkItemIds = result.getNewWorkitemIds();
		
		// Instantiate the new ProcessWorkitemRequest object and populate it 
		    
		Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
		req2.setComments('Approving request.');
		req2.setAction('Approve');
		req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
		
		// Use the ID from the newly created item to specify the item to be worked 
		    
		req2.setWorkitemId(newWorkItemIds.get(0));
		
		// Submit the request for approval 
		    
		Approval.ProcessResult result2 =  Approval.process(req2);
		
		// Verify the results 
		    
		System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
		
		System.assertEquals('Approved', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());
		
	}
}