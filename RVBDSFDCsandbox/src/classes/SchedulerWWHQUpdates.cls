global class SchedulerWWHQUpdates implements Schedulable 
{
  global void execute(SchedulableContext SC) {
       
  	 BatchWWHQStatistics bws = new BatchWWHQStatistics() ;
     Database.executeBatch(bws, 1);

     BatchWWHQUnions bwu = new BatchWWHQUnions() ;
     Database.executeBatch(bwu, 1);     
  }
}