/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to MarginSummaryPage
Created Date : Oct 2011
*******************************************************************************************************/

public with sharing class MarginSummaryPageExtension {

	Quote__C pageObj ;
	public List<Margin_Summary__c> marginList {get;set;}
	 decimal TotalCost;
	 decimal TotalQuote;
	 decimal TotalMargin;
	 public MarginSummaryPageExtension(Apexpages.StandardController controller)
    {
    	pageObj = (Quote__c)controller.getrecord();
 //   	marginList =[Select m.Total_Value__c, m.Quote__c,m.Margin__c, m.Id, m.Group_Name__c, m.Cost__c From Margin_Summary__c m where m.Quote__C = :pageObj.Id];
    	TotalCost=0;
    	TotalQuote=0;
    	Map<String,Margin_Summary__c> summaryMap = new Map<String,Margin_Summary__c>();
    	for(Margin_Summary__c m : [Select m.Total_Value__c, m.Quote__c,m.Margin__c, m.Id, m.Group_Name__c, m.Cost__c From Margin_Summary__c m where m.Quote__C = :pageObj.Id])
    	{
    		TotalCost = TotalCost+m.Cost__c;
    		TotalQuote=TotalQuote+m.Total_Value__c;
    		summaryMap.put(m.group_name__c, m);
    	}
    	if(TotalQuote!=0)
    	{
           TotalMargin=((TotalQuote-TotalCost)/TotalQuote)*100;
           TotalMargin=TotalMargin.setscale(2);
    	} 
    	orderElements(summaryMap);
       Margin_Summary__c m = new Margin_Summary__c(Group_Name__C ='Group Total' ,Cost__c= TotalCost, Total_Value__c=TotalQuote, Margin__c= TotalMargin);
       if(!marginList.isempty())    							
       marginList.add(m);
    }  

        //added by Ankita to sort the elements 
        public void orderElements(Map<String,Margin_Summary__c> marginSummaryInnerMap){
			  List<Margin_Summary__c> tempList = new List<Margin_Summary__c>();
			  tempList.add(marginSummaryInnerMap.get('Appliance'));
			  tempList.add(marginSummaryInnerMap.get('Support'));
			  tempList.add(marginSummaryInnerMap.get('Software'));
			  tempList.add(marginSummaryInnerMap.get('Services'));
			  //marginList.clear();
			  marginList = tempList;
        }

      
    
}