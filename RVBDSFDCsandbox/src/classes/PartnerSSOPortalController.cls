public without sharing class PartnerSSOPortalController {

    String role ='Partner';  
    String accessLevel ='Edit';  
    private static Visible_to_Partners__c[] m_oppMembers = new List<Visible_to_Partners__c> (); 
    List<Opportunity> oppList = new List<Opportunity>();
    Integer i=0;
    String userids=UserInfo.getUserId();
    //added by ankita for Search criteria
    public String acc {get;set;}
    public String opp {get;set;}
   // public Opportunity opp {get{opp = new Opportunity(); return opp;}set;}
    public String stage {get; set;}
    public List<SelectOption> stages;
    public Boolean error {get; private set;}
    public String errorMessage {get; private set;}
    public String severity {get; private set;}
    public String message {get; private set;}
    Id accId;

    public PartnerSSOPortalController(){
        accId = [select Contact.accountId from User where id = :UserInfo.getUserId()].Contact.AccountId;
        //system.debug('***Account of User:'+accId+':'+UserInfo.getUserId());
        message = 'If you cannot find the opportunity you are looking for below, please use the search form above.';
        oppList = [select Id, Name, stagename,CloseDate,ownerid, Account.name ,Account.BillingCountry
                    from Opportunity 
                    where sold_to_Partner__c = :accId
                    and Isclosed=false 
                    and Id not in (select OpportunityId from OpportunityTeamMember where userid=:userids ) 
                    order by Name limit 1000];
        showList();
    }

    public PageReference search(){
        
        String query = 'select Id, Name, stagename,CloseDate,ownerid, Account.name,Account.BillingCountry from Opportunity where Isclosed=false and Sold_To_Partner__c = \''+accId+'\' and Id not in (select OpportunityId from OpportunityTeamMember where userid=\''+userids+'\' ) ';
        
        if(acc != null && acc.length() >= 3)
        {
            String accName = '%'+acc+'%';
            query += ' and account.name like \''+accName+'\'';
        }
        if(stage != null && !stage.equals(''))
        {
            query += ' and stagename = \''+stage+'\'';
        }
        if(opp != null && opp.length() >= 3)
        {
            String oppName = '%'+opp+'%';
            query += ' and name like \''+oppName+'\'';
        }
        query += ' limit 1000';
        //system.debug('***Query:'+query);
        oppList = Database.Query(query);
        /*if(stage == null || stage.equals('')){
        oppList = [select Id, Name, stagename,CloseDate,ownerid, Account.name 
                    from Opportunity 
                    where sold_to_Partner__c = :accId
                    and account.name like :accName 
                    and Isclosed=false 
                    and Id not in (select OpportunityId from OpportunityTeamMember where userid=:userids ) 
                    order by Name limit 1000];
        }else{
        oppList = [select Id, Name, stagename,CloseDate,ownerid, Account.name 
                    from Opportunity 
                    where sold_to_Partner__c = :accId
                    and stagename = :stage
                    and account.name like :accName 
                    and Isclosed=false 
                    and Id not in (select OpportunityId from OpportunityTeamMember where userid=:userids ) 
                    order by Name limit 1000];
        }*/ 
        //if(oppList.size() > 0){           
            showList();         
        //}
        return null;   
            
    }
    
    public List<SelectOption> getStages(){
        List<SelectOption> stages = new List<SelectOption>();
        Schema.DescribeFieldResult F = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        stages.add(new SelectOption('','All'));
        for(Schema.PickListEntry ple : P){
            stages.add(new SelectOption(ple.getValue(), ple.getLabel()));
        }       
        return stages;
    }
    
    public List<OpportunityWrapper> ow {get;set;}

       public PageReference back() {
       List<OpportunityWrapper>  selectedopp = new List<OpportunityWrapper>();
       PageReference pr = System.Page.PartnerSSO_OpportunityList;
       pr.setRedirect(true);
        return pr;
   }
    

    public void showList() { 
        //get{
            error = false;
            errorMessage = '';
            selectedopp.clear();
            //oppList.clear();
            if(ow != null){
                ow.clear();
            }else{
                ow = new List<OpportunityWrapper>();
            }
          //  Id accId = [select Contact.accountId from User where id = :UserInfo.getUserId()].Contact.AccountId;
             //oppList = [select Id, Name, stagename,CloseDate,ownerid from Opportunity where sold_to_Partner__c = :accId and Isclosed=false and Id not in (select Opportunity_Name__c from Visible_to_Partners__c where PartnerName__c=:userids and role__c='Partner') order by Name]; 
        //  oppList = [select Id, Name, stagename,CloseDate,ownerid from Opportunity where sold_to_Partner__c = :accId and Isclosed=false and Id not in (select OpportunityId from OpportunityTeamMember where userid=:userids ) order by Name];
        if(!oppList.isEmpty()) {
          for(Opportunity c : oppList){
            OpportunityWrapper cw = new OpportunityWrapper(c);
            ow.add(cw);
          }
        }
        // return ow;
    //   } 
     //set; 
     }
         
       public PageReference Updateopp() {
        selectedopp.clear();
        List<Visible_to_Partners__c> vpc = new  List<Visible_to_Partners__c>();
        for (OpportunityWrapper cw : ow) {
            if (cw.checked){
                 try{
                    vpc = [select id from Visible_to_Partners__c where PartnerName__c=:userids and Opportunity_Name__c=:cw.ops.id];
                }catch(Exception e){
                System.debug('***********'+e);
                }     
                selectedopp.add(new OpportunityWrapper(cw.ops));
                Visible_to_Partners__c tmSTP = new Visible_to_Partners__c(PartnerName__c=userids,Opportunity_Name__c=cw.ops.id,role__c=role,Opportunity_Access__c=accessLevel,ownerid=cw.ops.ownerid);   
                m_oppMembers.add(tmSTP);
                System.debug('***********'+cw.ops.id);
              }
        }
        if (selectedopp.size() > 0 && selectedopp.size() < 6 ) {
            try{
                 if(vpc.size()>0){
                    delete vpc;
                 }
                 insert m_oppMembers;
            }catch(DMLException e){
                System.debug('Exception in adding user to sales team: ' + e.getMessage());
                error = true;
                errorMessage = e.getDMLMessage(0); //+ opp.Name ;
                if(errorMessage.equalsIgnoreCase('operation performed with inactive user')){
                    errorMessage = 'You cannot be added to the Sales team of one (or more) opportunity below. Please try updating one opportunity at a time. If the problem persists, please contact ISR.';
                }
                severity = 'error';
               selectedopp.clear();
               return null;
            }
         PageReference pr = new PageReference('/apex/PartnerSSOoppselected');
        return pr;
        }else {
/*           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one and maximum 5');
           ApexPages.addMessage(myMsg);*/
            error = true;
            errorMessage = 'Please select atleast one and maximum 5';
            severity = 'error';
           selectedopp.clear();
           return null;
        }    
        return null;
        }

        public List<OpportunityWrapper> selectedopp {
        get {
            if (selectedopp == null)
            {
             selectedopp = new List<OpportunityWrapper>();
            }
            return selectedopp ;
        }
        set;
    }
}