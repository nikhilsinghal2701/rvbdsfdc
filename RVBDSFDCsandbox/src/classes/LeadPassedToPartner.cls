public with sharing class LeadPassedToPartner {
    private Lead lead{get;set;}
    private string leadRecType;
    private string lastName;
    private string company;
    private Boolean check;
    private Boolean showPartner;
    //added by Anil
    private Boolean Federal_Deal ; 
    public String selectedState {get; set;}
    private Boolean show;
    private string partner;
    private string contact;
    private String progressBar;    
    private RecordType rType;
    private User lstUser;
    private Lead newLead;
    private string owner;
    private string status;
    private Id id;
    private List<SelectOption> options;
    private List<SelectOption> contacts=new List<SelectOption>();
    /* Constants Declaration */
    private static string OTHERS='None of the Above';
    private static string DISTI='Deal Registration - Distributor';
    private static string RESELLER='Deal Registration'; 
    private static string DISTRIBUTOR='Distributor';
    private static string VAR='VAR';
    private static string leadpassedtopartner='Lead Passed To Partner';
    private static string REDIRECT='/apex/LeadPassedToPartner';
    private static string PROGRESSBAR='/img/waiting_dots.gif';
    //added by Ankita 10/6/2010 for Enabling Deal Reg for SI/SP
    private static string SERVICEPARTNER='Service Provider';
    private static string SI='System Integrator';
    
    public LeadPassedToPartner(ApexPages.StandardController controller) {
        id = ApexPages.currentPage().getParameters().get('id');
        lstUser=[Select u.ContactId, u.Contact.Account.Id, Contact.Account.Name, u.Contact.Id, u.Contact.Name, Contact.Account.Distributor_Only__c,
                contact.Account.Type, u.Contact.Phone, u.Id, u.UserType__c from User u where u.Id=:userInfo.getUserId()];//00570000000oU12 geoffery '00570000000oTor'
        String accType = lstUser.contact.Account.Type == null ? '' : lstUser.contact.Account.Type;      
        if(id!=null){
            Lead tempLead=[select id,lastName,Company, email, phone, state, street, city, country, country__c, state__c, OS_Address1__c,
                            postalcode, Primary_App__c, Project_Close_Date__c,Status,Verified_Budget__c, Secondary_Application__c,OS_City__c, 
                            Primary_Application__c, Number_Of_Employees__c, Partner_Sales_Rep_Phone__c, recordTypeId, OS_Postal_Code__c,
                            Sold_To_partner__c, Tier2__c,Tier2_Partner_Contact__c, Sold_To_Partner_Contact__c, Riverbed_Products__c, Purchase_Driver__c
                            from Lead where id=:id];
            lastName=tempLead.lastName;
            company=tempLead.company;
            Status=tempLead.Status;
            //Added new code by Prashant on 1/14/2011 for New Deal Registration Process -- Feb'2011 release 
            if(accType.equalsIgnoreCase(DISTRIBUTOR)){
                partner = tempLead.Tier2__c;
                isContact();
                contact = tempLead.Tier2_Partner_Contact__c;
            }else{
            	partner = tempLead.Sold_To_Partner__c;
                isContact();
                contact = tempLead.Sold_To_Partner_Contact__c;
            }//End for New Deal Registration Process -- Feb'2011 release
        }
        this.lead=(Lead)controller.getRecord();
        if(lead==null){
            lead=new Lead();
            lead.OwnerId=userInfo.getName();
            lead.Status='Open';
            leadRecType=[select id,name from RecordType where SobjectType='lead' and name =:leadpassedtopartner].Id;        
        }
        owner=userInfo.getName();             
        check=true;
        showPartner=check;
        show=true;
        progressBar=PROGRESSBAR;  
       // System.debug('Lead record type = ' + lead.RecordTypeId);      
    }  
    public void  setLastName(string lName){
        this.lastName=lName;
    } 
    public string getLastName(){
        return lastName;
    }
    public void  setCompany(string company){
        this.company=company;
    } 
    public string getCompany(){
        return company;
    }
    public String getOwner(){
        return owner;
    }
    public string getLeadRecType(){
        return leadRecType;
    }    
    public void setCheck(Boolean check){
        this.check=check;
    }
    public Boolean getCheck(){
        return check;
    }   
    public void setShowPartner(Boolean show){
        this.showPartner=show;
    }
    public Boolean getShowPartner(){
        return showPartner;              
    }
    public String getStatus(){
    	return status;
    }
    public void setStatus(string status){
    	this.status=status;
    }
    //Added by     
    
    public List<SelectOption> getPartners(){
            options = new List<SelectOption>();
            options.add(new SelectOption('',''));
            lstUser=[Select u.ContactId, u.Contact.Account.Id, Contact.Account.Name, u.Contact.Id, u.Contact.Name, Contact.Account.Distributor_Only__c,
                    contact.Account.Type, u.Contact.Phone, u.Id, u.UserType__c from User u where u.Id=:userInfo.getUserId()];//00570000000oU12 geoffery '00570000000oTor'
            for(Distributor_Reseller__c distiRes:[Select d.Id, d.Account__c, d.Distributor_Reseller_Name__c, d.Partner_Type__c, d.Distributor_Reseller_Name__r.Name 
                from Distributor_Reseller__c d where d.Account__c=:lstUser.Contact.Account.Id order by d.Distributor_Reseller_Name__r.Name]){
                    System.debug('Distributor/Reseller = ' + distiRes.Distributor_Reseller_Name__r.Name);   
                    if(distiRes.Distributor_Reseller_Name__r.Name != null){            
                        options.add(new SelectOption(distiRes.Distributor_Reseller_Name__c,distiRes.Distributor_Reseller_Name__r.Name));
                    }
            }           
            options.add(new SelectOption(OTHERS,OTHERS));                       
          return options;  
    }
    public void setPartners(List<SelectOption> lstPat){
        options=lstPat;
    }
    public String newpartner{get; set;}
    public PageReference saverec(){
        Boolean isSave=dataSave();
        //system.debug('isSave::'+isSave);
        if(isSave==false){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select Partner.');
            ApexPages.addMessage(msg);
            return null;
        }else{
            try{
            if(id!=null){
                update lead;
            }else{
                insert lead;
            }
            
        }catch(DMLException dme){
            //system.debug('DML Exception:'+dme.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, dme.getDMLMessage(0));
            ApexPages.addMessage(msg);
            return null;
        }
        PageReference detailPage = new PageReference('/'+lead.Id);
        detailPage.setRedirect(true);   
        return detailPage; 
        }
    }
    private Boolean dataSave(){
        Boolean isSave=false;
        lead.LastName=lastName;
        lead.Company=company;
        lead.Status=status;
        lead.postalcode = lead.OS_Postal_Code__c;
        lead.city = lead.OS_City__c;
        lead.street = lead.OS_Address1__c;
        lead.state = lead.state__c;
        lead.country = lead.country__c;
        Id recType;
        if(this.lead.Id != null){
            recType = [select recordtypeId from Lead where id = :this.lead.Id].recordtypeId;
        }
       String accType = lstUser.contact.Account.Type == null ? '' : lstUser.contact.Account.Type;
        if(accType.equalsIgnoreCase(DISTRIBUTOR)){
            lead.Sold_To_Partner__c=lstUser.Contact.Account.Id;
            lead.Sold_To_Partner_User__c=userInfo.getUserId();
            lead.Sold_to_Partner_Contact__c=lstUser.ContactId;
            lead.Tier2_Partner_Contact__c=contact;
            system.debug('***contact:'+contact);
            //lead.Tier2_User__c=[select id from user where ContactId=:contact].Id;
            if(partner!=null && partner.equalsIgnoreCase(OTHERS)){
                Account newRec=[select id,name from account where name=:OTHERS and 
                    Type=:VAR];
                    lead.Tier2__c=newRec.id;
                    isSave=true;
            }else if(partner!=null){
                lead.Tier2__c=partner;
                isSave=true;
            }       
           // System.debug('Lead record type = ' + recType);      
            if(recType == null){ //added by Ankita on 5/10/2010 for lead passed to Partner: Record type should not be assigned if it is not null
                recType =[select id,name from RecordType where SobjectType='lead' and name =:leadpassedtopartner].Id;
              //  System.debug('Lead record type = ' + lead.RecordTypeId);      
            }
            isSave=true;
        }else{
            if(partner!=null && partner.equalsIgnoreCase(OTHERS)){
                Account newRec=[select id,name from account where name=:OTHERS and 
                    Type=:DISTRIBUTOR];
                lead.Sold_To_Partner__c=newRec.id;
                lead.Sold_To_Partner_User__c = null;//added by Ankita 3/8/2010 to clear out Sold To Partner User if None of the Above is selected
                isSave=true;
            }else if(partner!=null){
                lead.Sold_To_Partner__c=partner;
                lead.Tier2__c=lstUser.Contact.Account.Id;
                lead.Tier2_User__c=userInfo.getUserId();
                isSave=true;
            }else if(partner==null && !lstUser.Contact.Account.Distributor_Only__c){
                lead.Sold_To_Partner__c=lstUser.Contact.Account.Id;
                lead.Sold_To_Partner_User__c=userInfo.getUserId();
                isSave=true;
            }
            if(contact!=null) lead.Sold_To_Partner_Contact__c=contact; 
          //  System.debug('Lead record type = ' + recType);      
            if(recType == null){ 
                recType =[select id,name from RecordType where SobjectType='lead' and name =:leadpassedtopartner].Id;
           //     System.debug('Lead record type = ' + recType);      
            }
        }        
        lead.Is_Partner_Involved__c=check;
        lead.Partner__c=partner;
        if(newPartner!=null||newPartner!=''){
            lead.Type_Reseller_Name_Here__c=newPartner;
        }
        return isSave;
    }
    public void setPartner(string partner){
        this.partner=partner;
    }
    public string getPartner(){
        return partner;
    } 
    public void setShow(Boolean show){
        this.show=show;
    }    
    public Boolean getShow(){       
        return show;
    }  
    public PageReference isVisible(){
        system.debug('Check:'+check);
        if(!check&&!lstUser.Contact.Account.Distributor_Only__c){
            showPartner=false;
            show=true;           
        }else {
            showPartner=true;
            check=true;
            show=true;
        }
        return null;
    }
    public void isContact(){
        system.debug('inside contact:'+partner);
        contacts = new List<SelectOption>();
        for(Contact lstContact:[Select c.id,c.name,c.User_Profile__c from Contact c  where c.accountId=:partner
                                and c.User_Profile__c in ('Orders','Admin') order by name]){                
            contacts.add(new SelectOption(lstContact.Id,lstContact.Name));
            contact = lstContact.Id;
        }
        system.debug('Now size of contacts : '+contacts.size());  
    }
    public void setContacts(List<SelectOption> lstAllContacts){
        contacts=lstAllContacts;
    }
    public List<SelectOption> getContacts(){
        return contacts;
    }
    public void setContact(string contact){
        this.contact=contact;
    }
    public string getContact(){
        return contact;
    } 
    public void setProgressBar(string url){
        this.progressBar=url;
    }
    public string getProgressBar(){
        return progressBar;
    }
    
}