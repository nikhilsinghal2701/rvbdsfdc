public class DealRegApprovalActionTestUtil {
    static String DealReg = 'Deal Registration';
    static String DealRegDisti = 'Deal Registration';
	public static final String ISRPROFILE = 'ISR - Deal Reg Checkbox'; 

	public static Lead createLeadDR(){
	    RecordType[] leadRT=[select id,name from recordtype where SobjectType ='Lead'  and name = :DealReg];        
        Lead led=new Lead(AnnualRevenue = 1200000, Number_Of_Employees__c = 200, Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
        					Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', Secondary_Application__c = 'UDP');
        led.Status='Open';
        led.FirstName='Benedict';
        led.LastName='Finn';
        led.Company='Edit Corp';
        led.Title='Contractor';
        led.Email='finn@editcorp.com';
        led.Project_Name__c='MultiTier';
        led.Opportunity_Value__c=70000;
        led.Competitors__c='Cisco';
        led.Phone='4154154156';
        led.MobilePhone='4154154156';
        led.Fax='4154154156';
        led.Industry='Education';
        led.Website='www.editcorp.com';
        led.Street='199 fremont street';
        led.City='San Francisco';
        led.State='CA';
        led.Country='US';
        led.RecordTypeId=leadRT[0].Id;
        insert led;
		return led;
	}
	
	public static Lead createLeadDRD(){
	    RecordType[] leadRT=[select id,name from recordtype where SobjectType ='Lead'  and name = :DealRegDisti];        
        Lead led=new Lead(AnnualRevenue = 1200000, Number_Of_Employees__c = 200, Partner_Sales_Rep_Phone__c = '2152341234', Primary_Application__c = 'Consolidation',
        					Project_Close_Date__c = date.today(), Verified_Budget__c = 'Yes', Primary_App__c = 'CAD', Secondary_Application__c = 'UDP');
        led.Status='Open';
        led.FirstName='Benedict';
        led.LastName='Finn';
        led.Company='Edit Corp';
        led.Title='Contractor';
        led.Email='finn@editcorp.com';
        led.Project_Name__c='MultiTier';
        led.Opportunity_Value__c=70000;
        led.Competitors__c='Cisco';
        led.Phone='4154154156';
        led.MobilePhone='4154154156';
        led.Fax='4154154156';
        led.Industry='Education';
        led.Website='www.editcorp.com';
        led.Street='199 fremont street';
        led.City='San Francisco';
        led.State='CA';
        led.Country='US';
        led.RecordTypeId=leadRT[0].Id;
        insert led;
		return led;
	}

	public static void submitForApproval(sObject obj){
		// Create an approval request for the account 
		    
		Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
		req1.setComments('Submitting request for approval.');
		req1.setObjectId(obj.id);
		
		// Submit the approval request for the account 
		    
		Approval.ProcessResult result = Approval.process(req1);
		
		// Verify the result 
		    
		System.assert(result.isSuccess());
		
		System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
	}
}