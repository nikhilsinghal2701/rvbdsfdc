public with sharing class CaseJumpListController {

    public Integer day;
	public Integer month;
	public Integer year;
	public Id caseid;
	public List<List<Case>> caselist;
	
	public CaseJumpListController() {
		day=null;
		month=null;
		year=null;
		caselist=null;
		caseid=null;
	}
	
	public void setCaseMonth(String s) {
		month= s==null ? null : Integer.valueOf(s);
	}
	public Integer getCaseMonth() {
		return month;
	}
	public void setCaseYear(String s) {
		year= s==null ? null : Integer.valueOf(s); 
	}
	public Integer getCaseYear() { 
		return year; 
	}
	
	public void setCaseId(String s) {
		caseid = s;
	}
	public String getCaseId() {
		return caseid;
	}
	
	public Case getViewCase() {
		// return [SELECT Id,Account.Name,Contact.Name FROM Case WHERE Case.Id=:caseid LIMIT 1];
		return [SELECT c.Subject, c.Status, c.Products__c, c.Product__c, 
		               c.Description,
		               (Select CommentBody From CaseComments) 
		               From Case c WHERE Case.Id=:caseid LIMIT 1];
	}
		
	public List<List<Case>> getCaseList() {
		List<Case> clist;
		if (caselist==null) {
			DateTime startday = DateTime.newInstance(year, month, 1, 0, 0, 0);
			DateTime refday = startday.addDays(32);
			DateTime endday = DateTime.newInstance(refday.year(), refday.month(), 1, 0, 0, 0);
			// DateTime endday = startday.addDays(1);
			System.debug('START: ' + startday.year()+'-'+startday.month()+'-'+startday.day());
			System.debug('  END: ' + endday.year()+'-'+endday.month()+'-'+endday.day());
			String query = 'SELECT id FROM Case WHERE createdDate>=' + startday.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'') +
			                                     ' AND createdDate<' + endday.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
			System.debug(query);
			clist = Database.query(query);
			caselist = new List<List<Case>>();
			Integer counter = 0;
			List<Case> curlist = null;
			for (Case c : clist) {
				if (curlist==null) curlist = new List<Case>();
				curlist.add(c);
				counter++;
				if (counter==1000) {
					counter=0;
					caselist.add(curlist);
					curlist = null;
				}
			}
			if (curlist!=null) {
				caselist.add(curlist);
			}
		}
		return caselist;

	}
	
	public List<String> getYearList() {
		List<String> slist = new List<String>();
		Integer startYear = 2003;
		Date now = Date.today();
		Integer endYear = now.year();
		for (Integer i=startYear; i<=endYear; i++) {
			slist.add(''+i);
		}
		return slist;
	}
	
	public String getNextYear() {
		DateTime startday = DateTime.newInstance(year, month, 1, 0, 0, 0);
		DateTime refday = startday.addDays(32);
		DateTime endday = DateTime.newInstance(refday.year(), refday.month(), 1, 0, 0, 0);
		DateTime now = DateTime.now();
		if (endday>now) {
			return null;
		}	
		return ''+endday.year();
	}
	
	public String getNextMonth() {
		DateTime startday = DateTime.newInstance(year, month, 1, 0, 0, 0);
		DateTime refday = startday.addDays(32);
		DateTime endday = DateTime.newInstance(refday.year(), refday.month(), 1, 0, 0, 0);
		DateTime now = DateTime.now();
		if (endday>now) {
			return null;
		}	
		return ''+endday.month();
	}
	
	static testMethod void testCaseJump() {
		CaseJumpListController cntrl = new CaseJumpListController();
		List<String> years = cntrl.getYearList();
		cntrl.setCaseYear('2011');
		Integer yr = cntrl.getCaseYear();
		cntrl.setCaseMonth('2');
		String mos = cntrl.getNextMonth();
		Integer mo = cntrl.getCaseMonth();
		List<List<Case>> clist = cntrl.getCaseList();
	}
	
}