/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest(SeeAllData=true)
private class DiscountApprovalVFTemplateHlpCtrlTest {
        static testMethod void testMarginsGreaterThanMarginMax() {
                DiscountApprovalVFTemplateHlpCtrl da = new DiscountApprovalVFTemplateHlpCtrl();

                Quote__c q = DiscountApprovalTestHlp.createQuoteWithoutLineItems(null);

                //Add applicane and support line items with margin > margin maximum
                List<Product2> prodList=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE' or ProductCode ='MNT-GLD-SHA-00300'];
                Product2 prod,prod1;
                for(Product2 p :prodlist)
                {
                if(p.ProductCode=='SHA-06050-BASE')
                prod=p;
                else
                prod1=p;
                }

                List<Quote_Line_Item__c> qliList = new List<Quote_Line_Item__c>();
                Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='A',Product_Code__c=prod.productCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=64070);
                qliList.add(qli);
                qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='B',Product_Code__c=prod1.productCode,Product2__c=prod1.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=54000);
                qliList.add(qli);
                insert qliList;

                //Quote__c q = DiscountApprovalTestHlp.createQuote(null);
                Profile p = [Select Id from Profile Where Profile.Name = 'RSM'];
                User submitter = DiscountApprovalTestHlp.createUser('testvftemplate@test.com',p.Id);
                User usr=[select Id,Name,email from User where Id=:q.RSM__c];

                q.Submitter__c = submitter.Id;
                q.Approver1__c = usr.Id;
                //update q;

                Test.startTest();
                //Email to approver
                da.relatedToQuote = q;
                //System.assertNotEquals(da.marginStatus,'');
                //System.assert(da.getDiscountDetails().size() > 0);
                Test.stopTest();
        }

        static testMethod void testMarginsBetweenMarginMinMax() {
                DiscountApprovalVFTemplateHlpCtrl da = new DiscountApprovalVFTemplateHlpCtrl();

                Quote__c q = DiscountApprovalTestHlp.createQuoteWithoutLineItems(null);

                //Add applicane and support line items with margin > margin maximum
                List<Product2> prodList=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE' or ProductCode ='MNT-GLD-SHA-00300'];
                Product2 prod,prod1;
                for(Product2 p :prodlist)
                {
                if(p.ProductCode=='SHA-06050-BASE')
                prod=p;
                else
                prod1=p;
                }

                List<Quote_Line_Item__c> qliList = new List<Quote_Line_Item__c>();
                Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='A',Product_Code__c=prod.productCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25628);
                qliList.add(qli);
                qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='B',Product_Code__c=prod1.productCode,Product2__c=prod1.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=1500);
                qliList.add(qli);
                insert qliList;

                Test.startTest();
                //Email to approver
                da.relatedToQuote = q;
                System.assertNotEquals(da.marginStatus,'');
                Test.stopTest();
        }

        static testMethod void testMarginsBelownMarginMin() {
                DiscountApprovalVFTemplateHlpCtrl da = new DiscountApprovalVFTemplateHlpCtrl();

                Quote__c q = DiscountApprovalTestHlp.createQuoteWithoutLineItems(null);

                //Add applicane and support line items with margin > margin maximum
                List<Product2> prodList=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE' or ProductCode ='MNT-GLD-SHA-00300'];
                Product2 prod,prod1;
                for(Product2 p :prodlist)
                {
                if(p.ProductCode=='SHA-06050-BASE')
                prod=p;
                else
                prod1=p;
                }

                List<Quote_Line_Item__c> qliList = new List<Quote_Line_Item__c>();
                Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='A',Product_Code__c=prod.productCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=21356);
                qliList.add(qli);
                qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=25,Qty_Ordered__c=1,Non_Standard_Discount__c=50,Category__c='B',Product_Code__c=prod1.productCode,Product2__c=prod1.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=915);
                qliList.add(qli);
                insert qliList;

            //User usr=[select Id,Name,email from User where Id=:q.RSM__c];

                Test.startTest();
                //Email to approver
                da.relatedToQuote = q;
                System.assertNotEquals(da.marginStatus,'');
                Test.stopTest();
        }
}