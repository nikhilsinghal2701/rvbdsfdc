@isTest
private class TestVewAllOpptys {

    static testMethod void testViewAllOpptyController() {
      
      Account acc = new Account();
      acc.Name = 'Test';
      acc.Type='VAR';
      acc.Industry='Education';
      acc.Geographic_Coverage__c='Americas';
      acc.Region__c='Latin America';
      acc.BillingCountry='Argentina';
      insert acc ;
      
      Contact con = new Contact();
      con.LastName = 'Test Contact';
      con.Birthdate = Date.today();
      insert con;
      
      Contact con1 = new Contact();
      con1.LastName = 'Test Contact1';
      con1.Birthdate = Date.today();
      insert con1;
      
      Opportunity opty = new Opportunity();
      opty.name = 'testopp';
      opty.accountId = acc.Id;
      opty.Primary_ISR__c = UserInfo.getUserId();
      opty.Type = 'New';
      opty.Channel__c = 'Direct';
      opty.LeadSource = 'Email';
      opty.CloseDate = System.today()+5;
      opty.StageName = '0-prospect';
      opty.Forecast_Category__c = 'Commit';
      opty.Competitors__c = 'Citrix';
      insert opty;
      
      PageReference pageRef1 = Page.ViewAllOpptyPage;
      Test.setCurrentPage(pageRef1);
      Apexpages.currentPage().getParameters().put('id',opty.Id);
     
      ApexPages.StandardController std1 = new ApexPages.StandardController(acc);
	  ViewAllOpptyController controller1 = new ViewAllOpptyController(std1);
	  controller1.c1 = con;
	  controller1.c2 = con1;
	  controller1.opp = opty;
	  controller1.getStages();
	  controller1.Search();
	  controller1.getOpptyInfo();
      controller1.getc1();
      controller1.getc2();
      controller1.getopp();
      controller1.getliopp();
      
      }
}