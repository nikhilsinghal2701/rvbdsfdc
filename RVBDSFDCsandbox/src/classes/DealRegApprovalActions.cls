public without sharing class DealRegApprovalActions {

	public Lead lead {get; set;}
	public String pageTitle {get; private set;}
	public String buttonLabel {get; private set;}
	public Boolean isPartner {get; private set;}
	public String mode {get; private set;}
	public Boolean error {get; private set;}
	public String errorMessage {get; private set;}
	public String severity {get; private set;}
	public String actor {get;private set;}
	List<ProcessInstance> pInstance; 
	String status = '';
	
	public DealRegApprovalActions(ApexPages.StandardController controller){
		Id leadId = controller.getId();
		mode = ApexPages.currentPage().getParameters().get('mode');
		mode = mode == null ? '' : mode;
		this.lead = [select id, name, status, Pending_Reason__c, Rejection_Reason__c, Comments__c, Comment_Log__c, Opportunity_Requested_on_Rejected_Lead__c, 
					CreatedById, Assign_Approver__c, Deal_Reg_Authorized__c  
					from Lead where id = :leadId ];
		status = lead.status == null ? '' : lead.status;
		if(mode.equalsIgnoreCase('reject')){
			pageTitle = 'Reject With Reason' ; 
			buttonLabel = 'Reject';
		}else if(mode.equalsIgnoreCase('info')){
			pageTitle = 'Request More Info';
			buttonLabel = 'Request';
		}else if(mode.equalsIgnoreCase('reassign')){
			pInstance = [Select p.TargetObjectId, p.Status, p.Id, 
						(Select Id, ActorId, Actor.Name, IsDeleted From Workitems order by SystemModStamp limit 1) 
						From ProcessInstance p 
						where targetobjectid = :lead.Id and
						status = 'Pending'];
			System.debug('ProcessInstance  = '+ pInstance );	
			actor = pInstance.get(0).WorkItems[0].Actor.Name;
			System.debug('Actor = ' + actor);
		}
		//Added for EMC
		else if(mode.equalsIgnoreCase('EMCAPR')){
			pageTitle = 'Approve for EMC' ; 
			buttonLabel = 'Approve for EMC';
		}else{
			System.debug('Do nothing');
		}
	}
	
	public PageReference reply(){
		if(!status.equalsIgnoreCase('Pending - More Info')){
			error = true;
			errorMessage = Label.Cannot_Reply;
			severity = 'error';
		}
		return null;
	}
	
    public PageReference request(){
    	if(!status.equalsIgnoreCase('Rejected')){
    		error = true;
    		errorMessage = Label.Cannot_Request_Opportunity;
    		severity = 'error';
    		return null;
    	}else if(lead.Opportunity_Requested_on_Rejected_Lead__c){
    		error = true;
    		errorMessage = Label.Multiple_Opportunity_Requests;
    		severity = 'info';
    		return null;
    	}else{
    		lead.Opportunity_Requested_on_Rejected_Lead__c = true;
    		try{
    			update lead;
    			error = true;
    			errorMessage = Label.Confirm_Opportunity_Request;
	    		severity = 'confirm';
    			return null;
    		}catch(DMLException e){
    			System.debug('Exception in requesting opportunity : '+ e.getDMLMessage(0));
    			error = true;
    			errorMessage = e.getDMLMessage(0);
	    		severity = 'error';
    			return null;
    		}
    	}
   		return new PageReference('/'+lead.Id);
    }     
    
    public Pagereference submitReply(){
    	String log = lead.Comment_Log__c == null? '' : lead.Comment_Log__c ;
    	if(log.equals('')){
			log = UserInfo.getName() +  ' ' + String.valueOf(datetime.now()) +  ' : ' + lead.Comments__c;
    	}else{
    		String s = '-----------------------------------------------------------';
    		log = log + '\n' + s + '\n' + UserInfo.getName() + ' ' + String.valueOf(datetime.now()) + ' : ' + lead.Comments__c;
    	}
   		lead.Status = 'Pending - Resubmitted';
    	lead.comment_log__c = log;
		lead.comments__c = '';
    	try{
    		update lead;
    	}catch(DMLException e){
    		System.debug('Exception in submitting partner response : ' + e.getDMLMessage(0));
			error = true;
			errorMessage = e.getDMLMessage(0);
    		severity = 'error';
   			return null;
    	}
    	return new PageReference('/'+lead.Id);
    }
    
    public Pagereference saveAction(){
    	System.Debug('mode  ==>> '+mode);
    	String log = lead.Comment_Log__c == null? '' : lead.Comment_Log__c ;
    	if(mode.equalsIgnoreCase('reject')){
    		//approvalAction(lead.id, 'Reject', lead.comments__c);
    	}
    	//Added for EMC
    	else if(mode.equalsIgnoreCase('EMCAPR')){
    		
    	}
    	else{
    		lead.status = 'Pending - More Info';
	    	if(log.equals('')){
				log = UserInfo.getName() +  ' ' + String.valueOf(datetime.now()) +  ' : ' + lead.Comments__c;
	    	}else{
	    		String s = '-----------------------------------------------------------';
	    		log = log + '\n' + s + '\n' + UserInfo.getName() + ' ' + String.valueOf(datetime.now()) + ' : ' + lead.Comments__c;
	    	}
	    	lead.comment_log__c = log;
			lead.comments__c = '';
    	}
    	try{
    		update lead;
    	}catch(DMLException e){
    		System.debug('Exception in updating lead : ' + e.getDMLMessage(0));
    			error = true;
    			errorMessage = e.getDMLMessage(0);
    			severity = 'error';
    			return null;
    	}
    	if(mode.equalsIgnoreCase('reject')){
    		LeadApprovalActions.approvalAction(lead.id, 'Reject', lead.comments__c);
	    	try{
	    		lead.Status = 'Rejected';
	    		update lead;
	    	}catch(DMLException e){
	    		System.debug('Exception in updating lead : ' + e.getDMLMessage(0));
	    			error = true;
	    			errorMessage = e.getDMLMessage(0);
	    			severity = 'error';
	    			return null;
	    	}
    	}
    	//Added for EMC
    	else if(mode.equalsIgnoreCase('EMCAPR'))
    	{
    	//	
    	System.Debug('EMC PARTNER ***');
    		LeadApprovalActions.approvalAction(lead.id, 'Approve', lead.comments__c);
    		try{
	    		lead.Status = 'Approved';
	    		update lead;
	    	}catch(DMLException e){
	    		System.debug('Exception in updating lead : ' + e.getDMLMessage(0));
	    			error = true;
	    			errorMessage = e.getDMLMessage(0);
	    			severity = 'error';
	    			return null;
	    	}
    	}
    	
    	return new PageReference('/'+lead.Id);
    }

    public PageReference cancel(){
    	return new PageReference('/'+lead.Id);
    }

	public PageReference reassign(){
		//1. reassign the aproval request
		ProcessInstance	p = pInstance.get(0);
		for(ProcessInstanceWorkItem i : p.WorkItems){
			i.ActorId = lead.Assign_Approver__c;
		}
		//2. set email of new approver to email isr field
		System.debug('New Approver = ' + lead.Assign_Approver__c);
		User u = [select id, email from User where id = :lead.Assign_Approver__c];
		lead.Email_ISR__c = u.Email;
		lead.Riverbed_ISR__c = u.Id;
		try{
			update p.WorkItems;
		}catch(DMLException e){
			System.debug('Exception in reassigning lead : ' + e.getDMLMessage(0));
		}
		try{
			lead.Assign_Approver__c = null;//null the field value so it appears blank to the users next time they come to the reassign page
			update lead;
		}catch(DMLException e){
			System.debug('Exception in setting email isr on lead: ' + e.getDMLMessage(0));
		}
		return new PageReference('/'+lead.Id);
	} 
}