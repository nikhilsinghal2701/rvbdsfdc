public with sharing class CombinedCaseController {
    public List<Case> caseList {get;set;}
    public List<Case> caseTenList {get;set;}
    //flag for showing show all link
    public boolean flag{get;set;}
    //flag render the section all records
    public boolean noRecFlag{get;set;}
    //flag render the no record flag
    public string msg{get;set;}
    //flag render the no record message
    public boolean showAllFlag{get;set;}
     //flag hides first section all records
    public boolean showTenFlag {get;set;}
    Account a;
    List<Account> accList;
    Id recId;
    Id accId;
    String showAll;
    public CombinedCaseController(ApexPages.StandardController con){
        recId = con.getId();
        String id=(String) recId;
        if(id.startsWith('500')){
            accId=[select AccountId from Case where id=:id].AccountId;
            if(accId!=Null){
                flag = false;
                noRecFlag = false;
                showTenFlag = true;
                showAllFlag = false ; 
                caseList = getAllOpenCases(); 
            }
        }else if(id.startsWith('001')){
        	showAll = ApexPages.currentPage().getParameters().get('type');
        	if(showAll==Null)showAll='0';
        	a = [select Global_Support_Account__c from Account where id = :recId];
        	//System.debug(LoggingLevel.INFO, 'Account = ' + a.Global_Support_Account__c);
            if(a.Global_Support_Account__c!=Null){
        		accList = [select id,Global_Support_Account__c from Account where Global_Support_Account__c != null and (Global_Support_Account__c = :recId or  
	                       Global_Support_Account__c = :a.Global_Support_Account__c or id = :a.Global_Support_Account__c)];
	            //System.debug(LoggingLevel.INFO, 'AccountList = ' + accList);
	            flag = false;
	            noRecFlag = false;
	            showTenFlag = true;
	            showAllFlag = false ;       
	            caseList = getCaseList();
        	}else{
        		accList = [select id,Global_Support_Account__c,ParentId from Account where ParentId =:recId];
		        //System.debug(LoggingLevel.INFO, 'AccountList = ' + accList);
		        if(accList!=Null && accList.size()>0){
		        	flag = false;
		            noRecFlag = false;
		            showTenFlag = true;
		            showAllFlag = false ;       
		            caseList = getCaseList();	
		        }else{
	        		noRecFlag = true;
	        		msg='This account is not linked to any Global Support Account';
            	}
            }
        }
    }
    public List<Case> getCaseList(){
        caseList = [select id,CaseNumber, Subject, OwnerId, account.Name, contact.Name,Status  from Case where AccountId in :accList or AccountId=:recId];
        //system.debug('CASElIST:'+caseList);
        if(showAll=='1' && caseList.size()>0){
            showAllFlag=true;
            showTenFlag=false;
        }else if(showAll=='0' && caseList.size() > 10){
            showAllFlag = false;
            flag = true;
            caseTenList = [select id,CaseNumber, Subject, OwnerId, account.name,contact.Name, Status  from Case where AccountId in :accList or AccountId=:recId limit 10];
        }else if(caseList.size()==0){
            noRecFlag = true;
            msg='No Record to Display';
        }else{
             showAllFlag = true;
             showTenFlag = false;
        }
        return caseList;
    }
    
    public Pagereference getAllCases(){
        if(caseList.size() > 10){
            showAllFlag = true;
            showTenFlag = false;
        }
        return (new PageReference('/apex/LinkedCases?id='+recId+'&type=1'));
    }
    
    public List<Case> getAllOpenCases(){
        //system.debug('AccId:'+accId);
        caseList = [select id, CaseNumber, Subject, OwnerId, AccountId, Account.Name, ContactId, Contact.Name, Status  from Case where AccountId=:accId and Status NOT IN ('Closed','Closed - Resolved','Closed - Resolved (First Time Fix)','Closed - Unresolved','Closed - Duplicate')];
        if(caseList.size()==0){
        	noRecFlag = true;
        	msg='No Record to Display';
        }else showAllFlag = true;
        return caseList;
    } 

}