@isTest
private class updatePartnerTest {

static testMethod void updatePartnerTest() 
{
		SFDC_Channel_Budget__c budget = new SFDC_Channel_Budget__c();
	insert budget;
	
        SFDC_MDF__c fr = new SFDC_MDF__c(Request_Name__c='Test Marketing Fund', SFDC_Channel_Budget__c = budget.id);
        
        try
        {
            insert fr;
        }
        catch (DmlException e) 
        {
            System.debug(e.getMessage());
        }
        
        SFDC_MDF__c getfrOwnerId = [SELECT account__c FROM SFDC_MDF__c WHERE Id=:fr.Id];
        
        //User aId = [Select Contact.Account.Id  from User where Id = :getfrOwnerId.OwnerId]; 
        //Added TestPartner User ID to test it otherwise eclipse is picking my user id
        User aId = [Select Contact.Account.Id  from User where Id = '00570000000nJdP'];
        
        try
        {
            if ((aId.Contact.Account.Id != NULL) && (getfrOwnerId.account__c == NULL))
            {
//              System.debug('Partner Account ID ' + aId.Contact.Account.Id);
                getfrOwnerId.account__c = aId.Contact.Account.Id;
                update getfrOwnerId;
            }
        }
        catch (DmlException e) 
        {
            System.debug(e.getMessage());
        }
}

}