@isTest
private class TestAutomationOFiSellISOandCountryCode {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        List<Account> accountList = new List<Account>();
        List<String> accountIdList = new List<String>();
        Account a = new Account();
        a.Name = 'Test Account';
        a.ISO_Country_Code__c = 'CA';
        accountList.add(a);
         
        Account a1 = new Account();
        a1.Name = 'Test Account';
        a1.D_B_Country__c = 'Australia';
        accountList.add(a1);
        
        insert accountList;
        accountIdList.add(accountList.get(0).Id);
        accountIdList.add(accountList.get(1).Id);
        
        accountList = [Select Id, ISO_Country_Code__c, D_B_Country__c FROM Account WHERE Id IN : accountIdList];
        accountList.get(0).ISO_Country_Code__c = 'US';
        accountList.get(1).D_B_Country__c = 'New Zealand';
        
        update accountList;
        
    }
}