Public class RenewalContactPermissionAssigmentUtil{
    @future
    public static void createPermissionAssig(Set<Id> userIds){
    List<PermissionSetAssignment> newPermissionSetAccess = new List<PermissionSetAssignment>(); //list for new permission sets
        Set<String> usersWithAccess = new Set<String>(); //set for users Ids with access to the permission set already
        Id psaId; //Id of the permission set we want Users to be assigned
        List<User> userList = [Select id from user where Id in :userIds];
        //query for the permission set Id
        for (PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'Renewal_Contact_Permissions']) {
            psaId = ps.Id; //assign the premission set Id
            for (PermissionSetAssignment psa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :ps.Id]) { //query for the permission set Users that are already assigned
                usersWithAccess.add(psa.AssigneeId); //add the Id of each assigned User to our set
            }
        }

        //compare the list of all users to the list of already granted users and make another list of those missing access
        //take that list of missing access and grant access
        for (User u : userList) { //for all objects from our batch
            
            if (!usersWithAccess.contains(u.Id)) { //if the User is not in our 'already has access' set
                PermissionSetAssignment newPSA = new PermissionSetAssignment(); //PermissionSetAssignment sobject
                newPSA.PermissionSetId = psaId; //set the permission set Id
                newPSA.AssigneeId = u.Id; //set the User Id
                newPermissionSetAccess.add(newPSA); //add the record to our list
            }
        }
        if (!newPermissionSetAccess.isEmpty()) { //if there are records to insert
            insert newPermissionSetAccess; //insert
        }
    }
}