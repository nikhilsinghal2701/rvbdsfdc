public class FileUploader
{
    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<CsvWrapper> wrapList ;
    public Document doc {get;set;}
    public Boolean downloadFile {get;set;}
    
   
    public void ReadFile()
    {
        System.debug('In method');
        nameFile= contentFile.toString();
        downloadFile  = false;
        doc  = new Document();
        System.debug('nameFile:::'+nameFile);
        filelines = nameFile.split('\n');
        System.debug('filelines :::'+filelines );
        Set<String> emailSet = new Set<String>();
        wrapList  = new List<CsvWrapper>();
        for (Integer i=0;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            System.debug('inputvalues '+inputvalues );
            emailSet.addAll(inputvalues); 
            
        }
        Set<String> emailsSet = new Set<String>();
        for(String s : emailSet){
            emailsSet.add(s.trim().toLowerCase()); 
        }
        System.debug('In method'+emailSet);
        if(!emailSet.isEmpty())
            processContacts(emailsSet);
        
    }
    
    public void processContacts(Set<String> emailSet){
        Map<String, List<Contact>> EmailIdToConMap = new Map<String, List<Contact>>();
        Map<Id, Id> conIdToUserId = new Map<Id, Id>();
        Map<String, List<CsvWrapper>> csvWrapMap = new Map<String, List<CsvWrapper>>();
        List<Contact> conList;
        Set<Id> conId = new Set<Id>();
        for(Contact c : [Select id , Name , Email , LastName, Renewal_Contact__c  from Contact where Email in :emailSet AND Email != null]){
            conList = new List<Contact>();
            conId.add(c.id);
            if(EmailIdToConMap  != null && EmailIdToConMap.containsKey(c.Email)){
                conList = EmailIdToConMap.get(c.Email);
                
                 
            }
            conList.add(c);
            EmailIdToConMap.put(c.Email, conList);
             
        }
        System.debug('EmailIdToConMap:::'+EmailIdToConMap+ 'conId::::'+conId);
        List<Contact> updateConList = new List<Contact>();
        if(!conId.isEMpty()){
            for(User u : [Select id, Name , ContactId from User Where ContactId in : conId]){
                conIdToUserId.put(u.ContactId , u.Id); 
            }
            System.debug('conIdToUserId:::'+conIdToUserId+ 'conId::::'+conId);
            
        }
        
        for(String s : emailSet){
            if(EmailIdToConMap != null && EmailIdToConMap.containsKey(s)){
                List<Contact> tempList = EmailIdToConMap.get(s);
                System.debug('tempList :::'+tempList);
                if(tempList != null && tempList.size() == 1){
                    if(conIdToUserId != null && conIdToUserId.containsKey(tempList[0].Id)){
                        wrapList.add(new CsvWrapper (tempList[0], '', '',true, true));
                        tempList[0].Renewal_Contact__c = true; 
                        updateConList.add(tempList[0]); 
                        
                    }else{
                        wrapList.add(new CsvWrapper (tempList[0], '', '',false, true));
                        tempList[0].Renewal_Contact__c = true; 
                        updateConList.add(tempList[0]);
                    }
                }else if(tempList != null && tempList.size() > 1){
                    Boolean flag = false;
                    Contact con = new Contact();
                    for(Contact c : tempList){
                        if(conIdToUserId != null && conIdToUserId.containsKey(c.Id)){
                            flag =  true;
                            c.Renewal_Contact__c = true;
                            updateConList.add(c);  
                            con = c;
                            wrapList.add(new CsvWrapper (con, '', '',true, true));
                            
                        }else{
                            wrapList.add(new CsvWrapper (tempList[0], '', '',false, false));    
                        }
                        
                    }
                    
                }
                
            }else{
                    Boolean temp;
                    Contact c = new Contact(Email = s);
                   wrapList.add(new CsvWrapper (c, 'RESULT -D', 'No matching SFDC Contact found with this email address-updates made',temp, temp));     
           }
        }// End of for loop
        Map<Id, Database.SaveResult> updatestatusMap =  new Map<Id, Database.SaveResult>();
        System.debug('updateConList:::'+updateConList);
        if(updateConList != null && updateConList.size()>0){
            
            Database.SaveResult[] srList = Database.update(updateConList, false);
            for (Database.SaveResult sr : srList) {
                updatestatusMap.put(sr.getId(), sr);
            }
         
        }
        System.debug('updatestatusMap:::'+updatestatusMap);
        String csvFile = 'Email, SFDC Id, Result, Result Message' + '\n';
        Map<String, List<CsvWrapper>> csvMap  = new Map<String, List<CsvWrapper>> ();
            
        csvMap   = emailTocsvList(wrapList);
            
        for(String s : emailSet){
            if(csvMap  != null && csvMap.containsKey(s)){
                List<CsvWrapper> csv = csvMap.get(s);
                for(CsvWrapper c : csv){
                    if(c.con != null && c.con.Id != null){
                        csvFile += c.con.Email + ',' + c.con.Id+ ',' + c.result + ',' + c.resultMessage+ ',';
                        if(updatestatusMap != null && updatestatusMap.containsKey(c.con.Id)){
                            if(updatestatusMap.get(c.con.Id).isSuccess()){
                                csvFile += 'Success';
                            }else{
                                csvFile += 'Error:';
                            }
                        }
                    }else{
                        csvFile += s + ',' + '' + ',' + c.result + ',' + c.resultMessage+ ',';
                    } 
                    
                    csvFile += '\n';
                    
                }
                
            }
        }
            
        System.debug('csvFile :::'+csvFile );
        List<Document> docList = [Select id, Name, body from Document Where Name = 'Contact Update File'];
        if(docList != null && docList.size()>0){
            delete docList ;
        }
        doc  = new Document(
            Body = blob.valueof(csvFile),
            Name = 'Contact Update File',
            Type = '.csv',
            FolderId = '00lM0000000QjuU'
            
        );
        insert doc;
        downloadFile  = true;
            
        
    }
    public Map<String, List<CsvWrapper>> emailTocsvList(List<CsvWrapper> csvList){
        Map<String, List<CsvWrapper>> csvMap = new Map<String, List<CsvWrapper>>();
        List<CsvWrapper> tempList;
        for(CsvWrapper c : csvList){
            tempList = new List<CsvWrapper>();
            if(csvMap != null && c.con != null && c.con.Email != null  && csvMap.containsKey(c.con.Email)){
                tempList.addAll(csvMap.get(c.con.Email));     
            }
            tempList.add(c);
            csvMap.put(c.con.Email, tempList); 
                 
            
        }
        return csvMap;
    }
    
    public class CsvWrapper{
        public Contact con ;
        public String result;
        public String resultMessage;
        public Boolean isPartner;
        public Boolean isDup;
        public  CsvWrapper(Contact con, String result, String resultMessage, Boolean isPartner, Boolean isDup){
            this.con = con;
            this.isDup = isDup;
            
            this.isPartner =  isPartner;
            if(result != null && result.equals('RESULT -D')){
                this.result = result ;
                this.resultMessage  = resultMessage ;
            }else {
                if(isDup != null && (!isDup)  && isPartner != null && (!isPartner)){
                    this.result = 'RESULT -C';
                    this.resultMessage = 'For this email. there is more than one contact matching. so only contacts associated with partner Users are being updated and this one is not associated to apartner user . so No update is being made to this contact';
                    
                }else if(isPartner != null && isPartner){
                    this.result = 'RESULT -A';
                    this.resultMessage = 'Updated Contact with Renewal Contact flag(FYI Contact has a partner user associated)';
                    
                }else if(isPartner != null && (!isPartner)){
                    this.result = 'RESULT -B';
                    this.resultMessage = 'Updated Contact with Renewal Contact flag(FYI Contact does NOT have a partner user associated';
                    
                }
           }
        }
        
        
   }
    
         
}