/*
Requirement: Lead approval/rejection comments added to VF Email template
Author: prashant@riverbed.com
Date: 4/12/2011
*/
public class LeadApprovalComment {
    public Lead lead;
    public String leadId;
    public String comment {get;private set;}
    List<ProcessInstanceHistory> pInstance;
    public void LeadApprovalComment(){
    	system.debug('LeadId:'+leadId);
        if(leadId!=null){
            lead = [Select Id,
                    (Select TargetObjectId, SystemModstamp, ProcessInstanceId, Id, Comments, ActorId From ProcessSteps order by SystemModstamp desc limit 1)
                    From Lead l where Id = :leadId order by SystemModstamp desc];
            //system.debug('Lead:'+lead);
            if(lead!=null){
                pInstance=lead.ProcessSteps;
                if(pInstance.size()>0)comment=pInstance[0].Comments;
            }
        }
    }
    public void setLeadId (String Id) {
        leadId = Id;
        LeadApprovalComment();
    }
    public String getLeadId() {
        return leadId;
    }
    //Test Class
    static testmethod void testReject(){
		//create lead
		Lead l = DealRegApprovalActionTestUtil.createLeadDRD();
		//submit for approval
		//DealRegApprovalActionTestUtil.submitForApproval(l);
		//Reject lead
		l.Status='Rejected';
		l.Rejection_Reason__c = 'Already registered to another partner';		
		l.Comments__c = 'Rejecting Lead';
		update l;
		LeadApprovalComment testLeadcom=new LeadApprovalComment();
		testLeadcom.setLeadId(l.Id);
		Lead lead = [select Status from Lead where id = :l.Id];
		System.assertEquals('Rejected',lead.Status);
	}
}