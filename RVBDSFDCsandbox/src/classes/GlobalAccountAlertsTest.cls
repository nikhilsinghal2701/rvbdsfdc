@isTest
/*	Test class to test Global Account Alerts field update trigger.
*/
private class GlobalAccountAlertsTest {
	
	static Account createAccount(String accName, String coverage){
		Account a = new Account(name = accName, Account_Coverage_Program__c = coverage);
		insert a;
		return a;
	}
	
	
	static testmethod void testParentUpdate(){
		Account pAcc = createAccount('Parent1', 'GAP');
		pAcc.Global_Account_Alerts__c = 'test1';
		update pAcc;
		Test.startTest();
		Account cAcc1 = new Account(name='Child1', parentId = pAcc.id);
		insert cAcc1;
		Test.stoptest();
	}
	
	static testmethod void testChildInsert(){
		Account pAcc = createAccount('Parent1', 'GAP');
		Account cAcc1 = new Account(name='Child1', parentId = pAcc.id);
		insert cAcc1;
		Test.startTest();
		pAcc.Global_Account_Alerts__c = 'test1';
		update pAcc;
		Test.stoptest();
	}
}