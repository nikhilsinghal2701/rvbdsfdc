global class SchedulerWWHQAssignLeads implements Schedulable
{   
  global void execute(SchedulableContext SC) {
                
     BatchAssignLead bal = new BatchAssignLead() ;
     Database.executeBatch(bal,3);
     
     BatchContactScoring bcs = new BatchContactScoring() ;
     Database.executeBatch(bcs,5);     
  }
}