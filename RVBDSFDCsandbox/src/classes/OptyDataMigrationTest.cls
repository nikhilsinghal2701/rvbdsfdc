/*************************************************************************************************************
 Name:OptyDataMigrationTest
 Author: Santoshi Mishra
 Purpose: This is test class for the class OptyDataMigration.
 Created Date : Dec 2011
*************************************************************************************************************/

@isTest
private class OptyDataMigrationTest {

    static testMethod void OptyDataMigrationTest() {
        // TO DO: implement unit test
        
        // TO DO: implement unit test
      
         Test.StartTest();
        Opportunity o=DiscountApprovalTestHlp.createOpp();
        System.debug('inserted detail---------');
        List<Category_Master__c> catList= new List<Category_Master__c>();
        Category_Master__c cat = new Category_Master__c(Name='B1' , Group__c='Appliance');
        Category_Master__c cat1 = new Category_Master__c(Name='B2' , Group__c='Software');
        catList.add(cat);
        catList.add(cat1);
        insert(catList);
        Map<Id,Category_Master__c> catMap= new Map<Id,Category_Master__c>(catList);
        OptyDataMigrationClass cls1 = new OptyDataMigrationClass(); 
        OptyDataMigrationClass cls = new OptyDataMigrationClass(true); 
        Database.executeBatch(cls);
         Test.StopTest();
        List<Discount_Detail__c> childList = [select Id,Opportunity__C,isopp__C,category_master__C,category_master__r.Name,Discount__c,Uplift__c,Special_Discount__c
             from Discount_Detail__c where Opportunity__C =: o.Id and category_master__r.Name =:cat.name and isopp__C=true limit 1 ];
/*          System.debug('------------'+o.UpliftProducts__c+';;;;;;;;;;;;;;;;;;'+childlist[0].Uplift__c);   
            
            System.assertequals(o.DiscountProduct__c,childlist[0].Discount__c);
            System.assertequals(childlist[0].Discount__c,10);
            System.assertequals(o.Approved_Discount_A__c,childlist[0].Special_Discount__c);
        
    */
    
    }
}