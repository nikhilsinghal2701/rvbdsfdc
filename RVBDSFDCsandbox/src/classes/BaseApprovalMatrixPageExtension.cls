/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This is the extension class to BaseApprovalMatrixPage
Created Date : Oct 2011
*******************************************************************************************************/

public with sharing class BaseApprovalMatrixPageExtension {
    
    Base_Approval_Matrix__c pageObj ;
    Map<String,Category_Master__c> catMap = new Map<String,Category_Master__C>();
    List<Category_Master__c> catList;
    public List<Base_Approval_Matrix_Detail__c> detailList = new List<Base_Approval_Matrix_Detail__c>();
    public String [] selectedCats {get;set;}
    List<Base_Approval_Matrix_Detail__c> existingChildList= new List<Base_Approval_Matrix_Detail__c>();
    List<Base_Approval_Matrix_Detail__c>  detList;
    Map<String,Base_Approval_Matrix_Detail__c>existingChildMap = new  Map<String,Base_Approval_Matrix_Detail__c>();
    Set <String> updatedCatSet = new Set<String>();  
    public String RecName {get;set;}
    Boolean EditFlag = false;
    public BaseApprovalMatrixPageExtension(Apexpages.StandardController controller)
    {
        pageObj= (Base_Approval_Matrix__c) controller.getrecord();
        catList= [Select c.Name, c.Maximum_value__c, c.Locking_Value__c, c.Group__c, c.Description__c From Category_Master__c c order by c.name];
        selectedCats = new String []{};
        if(System.currentPageReference().getParameters().get('id')!=null)
            {
                List<Base_Approval_Matrix_Detail__c> existingChildList = [select Id,Category_Master__c,Category_Master__r.Name,Base_Approval_Matrix__c,active__c from Base_Approval_Matrix_Detail__c where Base_Approval_Matrix__c=: System.currentPageReference().getParameters().get('id') ];
                 detList=existingChildList;
                for(Base_Approval_Matrix_Detail__c bas :existingChildList )
                {
                    if(bas.active__C==true) 
                    selectedCats.add(bas.Category_Master__r.Name);  
                    existingChildMap.put(bas.Category_Master__r.Name,bas);
               
                }
                     RecName = pageObj.Name;
                     EditFlag=true;
            }
            
            else
            		RecName = 'New Base Approval Matrix';
    }
    
     public List<SelectOption> getCategories() {
            List<SelectOption> options = new List<SelectOption>();
            
           for(Category_Master__c cat :catList )
           {
            options.add(new SelectOption(cat.Name,cat.Name));
            catMap.put(cat.Name,cat);
            
           } 
           return options;
        }
        
        public Pagereference Save()
        {
            try{
            upsert(pageObj);
            }catch (Exception e)
            {
                Apexpages.addmessages(e);
                return null;
            }
           if(pageObj.BM_Active__c==true) 
           {
            for(String str : selectedCats)
            {
                
                if (catMap.get(str)!=null &&existingChildMap!=null && !existingChildMap.containsKey(str))
            {   
                Base_Approval_Matrix_Detail__c detail = new Base_Approval_Matrix_Detail__c();
                detail.Category_Master__c = catMap.get(str).Id;
                detail.Base_Approval_Matrix__c = pageObj.Id;
               	detail.Start__c=pageobj.BM_Start_Pct__c;
                detail.End__c = pageObj.BM_End_Pct__c;
                detail.Active__C = true;
                detailList.add(detail);
                
            }   
            
            else if(catMap.get(str)!=null &&existingChildMap!=null && existingChildMap.containsKey(str))
            {
                Base_Approval_Matrix_Detail__c detail =existingChildMap.get(str);
                detail.Active__C = true;
                detailList.add(detail);
                updatedCatSet.add(str);
            }
             
              
           
              	
                
            }
            
            for(Base_Approval_Matrix_Detail__c b : existingChildMap.Values())
            {
            	 if(!updatedCatSet.contains(b.Category_Master__r.Name))
              {
              		Base_Approval_Matrix_Detail__c detail =existingChildMap.get(b.Category_Master__r.Name);
               	    detail.Active__C = false;
                	detailList.add(detail);
              				
              }
            }
            
           }
           else
           {
           	
           	if(detList !=null)	 
           	for(Base_Approval_Matrix_Detail__c b: detList)
           	{
           		 b.Active__C=false;
           		detailList.add(b);
           	}
           	
           } 
            
            try{
            upsert(detailList);
            }catch (Exception e)
            {
                Apexpages.addmessages(e);
                return null;
            }
            
            return (new PageReference('/'+pageObj.id));
        }
    

}