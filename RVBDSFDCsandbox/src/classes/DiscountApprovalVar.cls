public class DiscountApprovalVar {
public static final Id WW_SALES =  '00E30000000dLJZ';
public static final Id CEO =  '00570000000mvxC';
public static final Id CFO =  '00530000000cYPV';
public static final Id APPROVAL_EXCEPTION =  '00570000000mfzm';
public static final Id PS_DIRECTOR = '00E70000000wFJ5';
public static final Id VP_SERVICES = '00E70000000xlcr';
public static final String REPLY_TO = 'discount_approval@1rsb8sobslats797rguqqbb34s3zle3lb9rh7m1s3vaf7kzigp.m-1essfmam.cs7.apex.sandbox.salesforce.com';
public static final String URL = 'riverbed--sfdc.cs7.my.salesforce.com/';
}