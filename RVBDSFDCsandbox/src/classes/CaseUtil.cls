/*Requirement:future method to update ‘Support_Handling_Notes__c’ on Case when Account Support_Handling_Notes_del__c changes.
**By Prashant on 06/06/2012 - Start

global class CaseUtil implements Database.Batchable<sObject>{
    global Transient Map<Id, String> caseUpdateMap;
    //Transient  global List<Case> caseUpdatelist = new List<Case>();
    global CaseUtil(Map<Id, String> tempMap) {
        caseUpdateMap=tempMap;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        Transient SET<ID> keys = caseUpdateMap.keyset();
        Transient String query = 'SELECT Id, Support_Handling_Notes__c FROM Case WHERE Id IN ';
        query+=':keys';
        return Database.getQueryLocator(query); 
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){     
        if (scope.size() > 0){
            Transient List<Case> records = (List<Case>)scope;
            for (Case tempCase : records){
                tempCase.Support_Handling_Notes__c=caseUpdateMap.get(tempCase.Id);
                //caseUpdatelist.add(tempCase);
            }
            try{
                update records;
            }catch (Exception e){
                System.debug(e);
            }
            records.clear();
            caseUpdateMap.clear();   
        }       
    }
    
    global void finish(Database.BatchableContext BC){       
    }
}*/
public class CaseUtil{
    
    /**Requirement#1: Support Handling Notes Enhancement upon new case creation.
    **Date: 12/8/2011
    **By: prashant.singh@riverbed.com
    
    public static void setSupportInfoFromAccount(List<Case> caseList){
        set<Id> accIds=new set<Id>();
        Map<Id,Account> accMap=new Map<Id,Account>();
        for(Case temp: caseList){
            accIds.add(temp.AccountId);
        }       
        if(accIds.size()>0){
            accMap=new Map<Id,Account>([select id,Support_Handling_Notes_del__c,Authorizations_Specializations__c from Account where Id IN:accIds]);//commented by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
            for(Case temp: caseList){
                if(accMap!=null && accMap.containsKey(temp.AccountId)){
                    //setting support handling notes to case from account
                    temp.Support_Handling_Notes__c=accMap.get(temp.AccountId).Support_Handling_Notes_del__c;
                    //setting sccount authorization to case from account
                    temp.Account_Authorization__c=accMap.get(temp.AccountId).Authorizations_Specializations__c;//added by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
                }
            }
        }
    }
    */
    public static void setSupportInfoFromAccount(List<Case> caseList,Map<Id,Case> oldMap){       
        set<Id> accIds=new set<Id>();
        set<Id> assetIds=new set<Id>();//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
        //Rev1
        Set<Id> contactIdSet = new Set<Id>();   
        Set<Id> assetIdSet = new Set<Id>();
        Set<Id> opnetGrpIdSet = new Set<Id>();
        //
        Map<Id,Account> accMap=new Map<Id,Account>();
        //Map<Id,Asset> assetMap=new Map<Id,Asset>();//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
        Map<Id,Id> assetMap=new Map<Id,Id>();//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
        for(Case cTemp: caseList){            
            if(cTemp.AccountId!=null||cTemp.AccountId!='')//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                accIds.add(cTemp.AccountId);
            if((cTemp.AssetId!=null||cTemp.AssetId!='')&&(cTemp.Asset_Account__c!=cTemp.AccountId)){//Start:added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                //assetIds.add(cTemp.AssetId);//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                accIds.add(cTemp.Asset_Account__c);
                assetMap.put(cTemp.AssetId,cTemp.Asset_Account__c);
            }//End:added by psingh@riverbed.com on 9/24/2013 for ticket#159029
            //Rev1
            if(cTemp.ContactId != null){
                if(Trigger.isUpdate){
                    if(cTemp.ContactId != oldMap.get(cTemp.Id).get('ContactId')){
                        contactIdSet.add(cTemp.ContactId);
                    }
                }
                else{
                    contactIdSet.add(cTemp.ContactId);
                }
            }  
                    
            if(cTemp.RecordTypeId==CaseBeforeInsert.recTypeNameToIdMap.get('OPNET').getRecordTypeId() && cTemp.AssetId != null){
                if(Trigger.isInsert){                   
                    assetIdSet.add(cTemp.AssetId);              
                }
            }       
            if(cTemp.Opnet_Group_ID__c != null && cTemp.ContactId != null &&
                (Trigger.isInsert || 
                (Trigger.isUpdate && 
                    (cTemp.Opnet_Group_ID__c != oldMap.get(cTemp.Id).get('Opnet_Group_ID__c') || cTemp.ContactId != oldMap.get(cTemp.Id).get('ContactId'))
                ))){
                    opnetGrpIdSet.add(cTemp.Opnet_Group_ID__c);
            }               
            //      
        }        
        if(accIds.size()>0){
            accMap=new Map<Id,Account>([select id,Support_Handling_Notes_del__c,Authorizations_Specializations__c from Account where Id IN:accIds]);//added by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
            if(accMap!=null && accMap.size()>0){//added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                for(Case temp: caseList){
                    if(accMap!=null && accMap.containsKey(temp.AccountId)){
                        //setting support handling notes to case from account
                        temp.Support_Handling_Notes__c=accMap.get(temp.AccountId).Support_Handling_Notes_del__c;
                        //setting sccount authorization to case from account
                        temp.Account_Authorization__c=accMap.get(temp.AccountId).Authorizations_Specializations__c;//added by prashant.singh@riverbed.com on 09/21/2012 as per ticket#122452
                    }
                    if(temp.AssetId!=null && assetMap!=null && assetMap.containsKey(temp.AssetId)){//Start:added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                        //setting support handling notes to case from Asset's account
                        temp.Asset_Account_Support_Handling_Notes__c=accMap.get(assetMap.get(temp.AssetId)).Support_Handling_Notes_del__c;
                    }else if((temp.AssetId!=null||temp.AssetId!=''||temp.Asset_Account_Support_Handling_Notes__c!='')&&((temp.Asset_Account__c==null)||(temp.Asset_Account__c==temp.AccountId))){
                        temp.Asset_Account_Support_Handling_Notes__c='';
                    }//End:added by psingh@riverbed.com on 9/24/2013 for ticket#159029
                }
            }
        }
        //Rev1  
            if(!contactIdSet.isEmpty()){
                CaseBeforeInsert.addContactStickyNotesToCase(caseList,contactIdSet);
            }           
            if(!assetIdSet.isEmpty()){
                CaseBeforeInsert.updateOpnetDetailsOnCase(caseList,assetIdSet);
            }           
            if(!opnetGrpIdSet.isEmpty()){
                CaseBeforeInsert.updatePocIdOnCase(caseList,opnetGrpIdSet);
            }
        //
    }
    
    /*      
    **Requirement#2: populating staff engineer role based on staff engineer selected on case.
    **Ticket#: 124383/Enh#1822
    **Date: 01/02/2013
    **By: prashant.singh@riverbed.com
    */
    private static set<Id> userRoleIds=new set<Id>();
    public static void setStaffEngineerRole(List<Case> caseList,Map<Id,Case> oldMap){
        set<string> staffEngName=new set<string>();         
        Map<string,User> userMap=new Map<string,User>();
        Map<Id,UserRole> userRoleMap;
        for(Case temp: caseList){
            if(temp.Staff_Engineer__c!=null)
                staffEngName.add(temp.Staff_Engineer__c);   
        }
        if(staffEngName!=null && staffEngName.size()>0)
            userMap=getUserMap(staffEngName);
        if(userRoleIds!=null && userRoleIds.size()>0)
            userRoleMap=getUserRoleMap(userRoleIds);
        for(Case temp: caseList){
            //setting staff engineer role to case
            if(temp.Staff_Engineer__c==Null){
                temp.Staff_Engineer_Role__c=Null;
            }else if(userMap.containsKey(temp.Staff_Engineer__c)){                  
                temp.Staff_Engineer_Role__c=userRoleMap.get((userMap.get(temp.Staff_Engineer__c).userRoleId)).Name;
            }else{
                temp.Staff_Engineer_Role__c=Null;
            }
        }
    }
    private static Map<Id,UserRole> getUserRoleMap(set<Id>tempUserRoleIds){
        Map<Id,UserRole> tempUserRoleMap=new Map<Id,UserRole>([select id,name from UserRole where id IN:tempUserRoleIds]);
        return tempUserRoleMap;
    }
    private static Map<string,User> getUserMap(set<string>staffEngName){
        Map<string,User> tempUserMap=new Map<string,User>();
        List<User> userList=[select id,name,userRoleId from User where name IN:staffEngName and IsActive=true and UserType='Standard'];
        if(userList!=null && userList.size()>0){
            for(User usr:userList){
                userRoleIds.add(usr.userRoleId);
                //system.debug('user name:'+usr.Name);
                tempUserMap.put(String.valueof(usr.name),usr);
            }
        }
        return tempUserMap;
    }
  
  public static void chkFieldRequired(Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap) {
     set<Id> astIds = new set<Id>();
     set<Id> conIds = new set<Id>();
     map<id,Contact> conMap = new map<id,Contact>();
     map<id,string> astmap = new map<id,string>();
     for(Case newca: newCaseMap.values()){
        if(newca.AssetId !=null){         
            astIds.add(newca.AssetId); 
        }
        if(newca.ContactId!=null){
            conIds.add(newca.ContactId);
        }
     }     
    if(!astIds.isEmpty()){
        for(Asset ast :[select id,Support_Provider__r.name from Asset a where Id = :astIds]) {
            astmap.put(ast.id,ast.Support_Provider__r.name);
        }
    }
    if(!conIds.isEmpty()){
        for(Contact con:[Select Id,AccountId,Account.RecordType_Name__c from Contact where Id IN :conIds]){
            conMap.put(con.Id,con); 
        }
    }    
     for(Case newca: newCaseMap.values()){
       if(newca.AssetId !=null){         
          if (!(oldCaseMap.get(newca.Id).Status == 'Closed' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved (First Time Fix)' || oldCaseMap.get(newca.Id).Status == 'Closed - Unresolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Duplicate')
                 &&
             (newca.Status == 'Closed' || newca.Status == 'Closed - Resolved' || newca.Status == 'Closed - Resolved (First Time Fix)' || newca.Status == 'Closed - Unresolved')
                 &&
             (!astmap.get(newca.AssetId).equalsIgnoreCase('Riverbed Technology (for Partner Center Use)'))
                 &&
             (!(newca.type == 'Support Site Admin' || newca.type =='License request'))
                &&
             (newca.ContactId!=null && conMap!=null && conMap.get(newca.ContactId).Account.RecordType_Name__c=='Partner Account')
            ){                
                 if (newca.Was_the_escalation_appropriate__c  == null)
                     newca.addError('The field Was this a valid escalation need a value.');
                 if (newca.Technical_Expertise__c  == null)
                     newca.addError('The field Technical Expertise need a value.');
                 if (newca.Diagnostics_Data__c  == null)
                     newca.addError('The field Diagnostics/Data need a value.');
                 if (newca.Responsiveness__c  == null)
                     newca.addError('The field Responsiveness need a value.');    
             }
         if(!(oldCaseMap.get(newca.Id).Status == 'Closed' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved (First Time Fix)' || oldCaseMap.get(newca.Id).Status == 'Closed - Unresolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Duplicate')
                 &&
             (newca.Status == 'Closed' || newca.Status == 'Closed - Resolved' || newca.Status == 'Closed - Resolved (First Time Fix)' || newca.Status == 'Closed - Unresolved')
                 &&
             (!astmap.get(newca.AssetId).equalsIgnoreCase('Riverbed Technology (for Partner Center Use)'))
                 &&
             (!(newca.type == 'Support Site Admin' || newca.type =='License request'))
                &&
             (newca.ContactId!=null && conMap!=null && conMap.get(newca.ContactId).Account.RecordType_Name__c!='Partner Account')
            ){  
                newca.Invalid_Escalation_Reason__c='Customer direct';
                newca.Was_the_escalation_appropriate__c='No - Please select reason';
            }             
       }       
       if ( (newca.Status == 'Closed' || newca.Status == 'Closed - Resolved' || newca.Status == 'Closed - Resolved (First Time Fix)' || newca.Status == 'Closed - Unresolved')
                &&
               !(oldCaseMap.get(newca.Id).Status == 'Closed' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved (First Time Fix)' || oldCaseMap.get(newca.Id).Status == 'Closed - Unresolved')
                &&
               (!(newca.type == 'Support Site Admin' || newca.type =='License request' || newca.Resolution_Code__c == 'No support contract' || newca.Resolution_Code__c == '' || newca.Resolution_Code__c=='Customer not responding'))
                &&
               (newca.Version__c == null)
              ){
                newca.addError('Please select a version.');
        }              
        if ((oldCaseMap.get(newca.Id).Status == 'Closed' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Resolved (First Time Fix)' || oldCaseMap.get(newca.Id).Status == 'Closed - Unresolved' || oldCaseMap.get(newca.Id).Status == 'Closed - Duplicate')
             &&
            !(newca.Status == 'Closed' || newca.Status == 'Closed - Resolved' || newca.Status == 'Closed - Resolved (First Time Fix)' || newca.Status == 'Closed - Unresolved' || newca.Status == 'Closed - Duplicate')
            ){
           newca.Reopened_By_Request__c = true;       
         }
     }
  }
    
    public static void SendEmailOnOwnerChange(Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap) {   // by Sukhdeep Singh for Ticket# 169134
        Map<Case,Id> EmailCaseMap = new Map<Case,Id>();
        Set<id> newownerid = new set<id>();
        for(Case newca: newCaseMap.values()){
            if (newca.OwnerId != oldCaseMap.get(newca.Id).OwnerId) {
                 EmailCaseMap.put(newca,oldCaseMap.get(newca.Id).OwnerId);
                 newownerid.add(newca.OwnerId);
                 
            }
        }
        if(!EmailCaseMap.isEmpty()){
            EmailToOwners(EmailCaseMap,newownerid);
        }
  
        
    }
    public static void AssetToCaseUpdateAfter(List<case>newCaseList){  
       List<case> cslstToUpdate=new List<case>();
       List<case> cslstToReset=new List<case>();
       List<case> csLst=new List<case>([Select id,Products__c,Product_Family__c,AssetId,asset.Product_Family1__c,Asset.Product_Family__c From Case where id IN :newCaseList]);
       
       for(case c: csLst){
            //System.debug('Asset ID**:'+ c.AssetId);
            if (c.AssetID !=null){
                System.debug('Condition 1**:');
                c.Product_Family__c=c.asset.Product_Family1__c;
                c.Products__c=c.asset.Product_Family__c;
                cslstToUpdate.add(c);
            }
            else if (c.AssetID ==null){
                System.debug('Condition 2**:');
                c.Product_Family__c=null;
                c.Products__c=null; 
                cslstToReset.add(c);
            }
           if(cslstToUpdate.size()>0){
            Update cslstToUpdate;
           }
           if(cslstToReset.size()>0){
            Update cslstToReset;
            }
        }   
    }
     private static void EmailToOwners(Map<Case,Id> EmailCaseMap,Set<id> newownerid){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: 'noreply-techsupport@riverbed.com'];
        list<Messaging.Email> allMails = new List<Messaging.Email>();
        Map<Id,User> UserDetailMap = new Map<Id,User>();
        Map<Id,group> QueueDetailMap = new Map<Id,group>();
        list<Messaging.SingleEmailMessage> theMails = new list<Messaging.SingleEmailMessage>();
        String nName='';       
        for(User ud : [select Email,Name,id from User  where id in : EmailCaseMap.values() ]){
          UserDetailMap.put(ud.id,ud);         
        }
        for(User ud : [select Email,Name,id from User  where id in : newownerid ]){
          UserDetailMap.put(ud.id,ud);         
        }
        for(Group qd : [Select Id, Name, Type from Group where Type = 'Queue' and  id in : EmailCaseMap.values()]){
          QueueDetailMap.put(qd.id,qd);         
        }
        for(Group qd : [Select Id, Name, Type from Group where Type = 'Queue' and  id in : newownerid]){
          QueueDetailMap.put(qd.id,qd);         
        }       
        for(Case cs : EmailCaseMap.keySet()){
            String[] toAddresses = new String[] {}; 
            if(!QueueDetailMap.containskey(EmailCaseMap.get(cs)) && EmailCaseMap.get(cs) != UserInfo.getUserId() ){
                toAddresses.add(UserDetailMap.get(EmailCaseMap.get(cs)).Email);
                if(!QueueDetailMap.containskey(cs.OwnerId)){
                    nname=UserDetailMap.get(cs.OwnerId).name;
                } else {
                    nname=QueueDetailMap.get(cs.OwnerId).name;
                }
                String subject = '';
                if (cs.Subject != null)    // by sukhdeep singh for ticket# 193786 
                    subject = cs.Subject;   
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();         
                mail.setReplyTo('noreply-techsupport@riverbed.com');
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
                mail.setToAddresses(toAddresses);
                mail.setSubject('Case '+cs.CaseNumber+' was reassigned to a new owner' );
                mail.setPlainTextBody('Owner of Case: ' + UserDetailMap.get(EmailCaseMap.get(cs)).name + ' Changed to ' + nname);
                mail.setHtmlBody(' Case # '+cs.CaseNumber+' was reassigned to a new owner.<BR>  Subject: '+subject+'<BR>   Previous case owner: '+UserDetailMap.get(EmailCaseMap.get(cs)).name+' <BR> Current case owner:  '+nname+'  <BR> Modified by: '+UserInfo.getName()+'  <BR> Modified date: '+datetime.now()+' <BR> Link: '+URL.getSalesforceBaseUrl().toExternalForm() + '/' + cs.Id+' <BR><BR>  ');    // by sukhdeep singh for ticket# 193786
                theMails.add(mail);            
            }            
        }        
        if(!theMails.isEmpty()){            
            for( Integer j = 0; j < theMails.size(); j++ ){
                allMails.add(theMails.get(j));
            }            
        }        
        if(!allMails.isEmpty()){
            String emailErrorReport;            
            Messaging.SendEmailResult[] results = Messaging.sendEmail( allMails );
            list<Messaging.SendEmailError> errors = new List<Messaging.SendEmailError>();
            System.debug('ResultEmail1'+results);
            for( Messaging.SendEmailResult currentResult : results ) {            
                errors = currentResult.getErrors();            
                if( null != errors ) {            
                    for( Messaging.SendEmailError currentError : errors ) {            
                        emailErrorReport = emailErrorReport + '(' +  currentError.getStatusCode() + ') ' + currentError.getMessage() + '\r' ;            
                    }            
                    System.debug('ResultEmail2'+emailErrorReport);            
                }
            }      
        }
     }
}