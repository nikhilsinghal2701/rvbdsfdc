global class OpportunityService{


    
    
     
     /*webservice  static List<OpportunityFieldWrapper> getOpportunityDiscounts(List<Id> oppIds){
         System.debug('oppIds :::'+oppIds );
         List<OpportunityFieldWrapper> fieldWrapList = new List<OpportunityFieldWrapper>(); 
         Set<String> category = new Set<String>{'K', 'B'};
         Dom.Document doc = new Dom.Document();        
         Dom.Xmlnode rootNode = doc.createRootElement('response', null, null);
         for(Opportunity o : [Select id, Name , Support_Renewal_Rate_RVBD__c, Support_Renewal_Rate_RASP__c, (Select Name, Category_Master__c, Discount__c,
                   Disti_Discount__c, Disti_Special_Discount__c, Special_Discount__c, Uplift__c, Delta_Discount__c, Full_Discount__c,Category_Master__r.Name, 
                   Additional_Discount__c, Var_Discount__c, Customer_Discount__c From Discount_Details__r Where Category_Master__r.Name in :category)
                   From Opportunity Where Id in :oppIds]){
                  
             /*dom.XmlNode Extension= rootNode.addChildElement('Opportunity',null , null);
             dom.XmlNode FieldName1= Extension.addChildElement('Id',null , null);
             FieldName1.addTextNode(o.Id);
             dom.XmlNode FieldName2 = Extension.addChildElement('supportrenewalrate',null,null);
             FieldName2.addTextNode(String.valueof(o.Support_Renewal_Rate_RVBD__c) != null ? String.valueof(o.Support_Renewal_Rate_RVBD__c) : '');        
             dom.XmlNode FieldName3 = Extension.addChildElement('supportrenewalraterasp',null,null);
             FieldName3.addTextNode(String.valueof(o.Support_Renewal_Rate_RASP__c) != null ? String.valueof(o.Support_Renewal_Rate_RASP__c) : ''); */       
             
            /* List<OppDiscFieldWrapper> discFieldWrapper = new List<OppDiscFieldWrapper>();
             if(o.Discount_Details__r  != null && o.Discount_Details__r.size()>0){
                 for(Discount_Detail__c d :o.Discount_Details__r){
                     System.debug('Record::::'+d);
                     discFieldWrapper.add(new OppDiscFieldWrapper(checkNull(d.Category_Master__r.Name), checkNull(d.Discount__c), checkNull(d.Full_Discount__c), checkNull(d.Uplift__c), checkNull(d.Special_Discount__c))); */
                     /*dom.XmlNode disc = Extension.addChildElement('cat_disc',null , null);
                     dom.XmlNode cat = disc.addChildElement('cat',null , null);
                     cat.addTextNode(d.Category_Master__r.Name != null ? d.Category_Master__r.Name : '');
                     dom.XmlNode dis = disc.addChildElement('base',null , null);
                     dis.addTextNode(String.valueof(d.Discount__c) != null ? String.valueof(d.Discount__c) : '');
                     dom.XmlNode full = disc.addChildElement('full',null , null);
                     full.addTextNode(String.valueof(d.Full_Discount__c) != null ? String.valueof(d.Full_Discount__c) : '');
                     dom.XmlNode uplift = disc.addChildElement('uplift',null , null);
                     uplift.addTextNode(String.valueof(d.Uplift__c) != null ? String.valueof(d.Uplift__c) : '');
                     dom.XmlNode special = disc.addChildElement('special',null , null);
                     special.addTextNode(String.valueof(d.Special_Discount__c) != null ? String.valueof(d.Special_Discount__c) : '');*/
           /*  
                      
                 }
             } 
             fieldWrapList.add(new OpportunityFieldWrapper( o.Id, checkNull(o.Support_Renewal_Rate_RVBD__c), checkNull(o.Support_Renewal_Rate_RASP__c), discFieldWrapper));   
         }
         
         system.debug('fieldWrapList:::'+fieldWrapList);                 
         //system.debug(doc.toXmlString());             
         //return doc.toXmlString();
         return fieldWrapList;
     
     }*/
     
     webservice  static String getOpportunityDiscounts(List<Id> oppIds){
         System.debug('oppIds :::'+oppIds );
         List<OpportunityFieldWrapper> fieldWrapList = new List<OpportunityFieldWrapper>(); 
         Set<String> category = new Set<String>{'K', 'B'};
         Dom.Document doc = new Dom.Document();        
         Dom.Xmlnode rootNode = doc.createRootElement('response', null, null);
         for(Opportunity o : [Select id, Name , Support_Renewal_Rate_RVBD__c, Support_Renewal_Rate_RASP__c, (Select Name, Category_Master__c, Discount__c,
                   Disti_Discount__c, Disti_Special_Discount__c, Special_Discount__c, Uplift__c, Delta_Discount__c, Full_Discount__c,Category_Master__r.Name, 
                   Additional_Discount__c, Var_Discount__c, Customer_Discount__c From Discount_Details__r Where Category_Master__r.Name in :category)
                   From Opportunity Where Id in :oppIds]){
                  
             dom.XmlNode Extension= rootNode.addChildElement('Opportunity',null , null);
             dom.XmlNode FieldName1= Extension.addChildElement('Id',null , null);
             FieldName1.addTextNode(o.Id);
             dom.XmlNode FieldName2 = Extension.addChildElement('supportrenewalrate',null,null);
             FieldName2.addTextNode(String.valueof(o.Support_Renewal_Rate_RVBD__c) != null ? String.valueof(o.Support_Renewal_Rate_RVBD__c) : '');        
             dom.XmlNode FieldName3 = Extension.addChildElement('supportrenewalraterasp',null,null);
             FieldName3.addTextNode(String.valueof(o.Support_Renewal_Rate_RASP__c) != null ? String.valueof(o.Support_Renewal_Rate_RASP__c) : '');        
             List<OppDiscFieldWrapper> discFieldWrapper = new List<OppDiscFieldWrapper>();
             if(o.Discount_Details__r  != null && o.Discount_Details__r.size()>0){
                 for(Discount_Detail__c d :o.Discount_Details__r){
                     System.debug('Record::::'+d);
                     discFieldWrapper.add(new OppDiscFieldWrapper(checkNull(d.Category_Master__r.Name), checkNull(d.Discount__c), checkNull(d.Full_Discount__c), checkNull(d.Uplift__c), checkNull(d.Special_Discount__c))); 
                     dom.XmlNode disc = Extension.addChildElement('cat_disc',null , null);
                     dom.XmlNode cat = disc.addChildElement('cat',null , null);
                     cat.addTextNode(d.Category_Master__r.Name != null ? d.Category_Master__r.Name : '');
                     dom.XmlNode dis = disc.addChildElement('base',null , null);
                     dis.addTextNode(String.valueof(d.Discount__c) != null ? String.valueof(d.Discount__c) : '');
                     dom.XmlNode full = disc.addChildElement('full',null , null);
                     full.addTextNode(String.valueof(d.Full_Discount__c) != null ? String.valueof(d.Full_Discount__c) : '');
                     dom.XmlNode uplift = disc.addChildElement('uplift',null , null);
                     uplift.addTextNode(String.valueof(d.Uplift__c) != null ? String.valueof(d.Uplift__c) : '');
                     dom.XmlNode special = disc.addChildElement('special',null , null);
                     special.addTextNode(String.valueof(d.Special_Discount__c) != null ? String.valueof(d.Special_Discount__c) : '');
             
                      
                 }
             } 
             fieldWrapList.add(new OpportunityFieldWrapper( o.Id, checkNull(o.Support_Renewal_Rate_RVBD__c), checkNull(o.Support_Renewal_Rate_RASP__c), discFieldWrapper));   
         }
         
         system.debug('fieldWrapList:::'+fieldWrapList);                 
         system.debug(doc.toXmlString());             
         return doc.toXmlString();
         //return fieldWrapList;
     
     }
     
     public static String checkNull(Object val) {
        return (val == null) ? '' : String.valueOf(val);
    }
    
     Global Class OppDiscFieldWrapper{
         webservice String cat_disc{get;set;}
         webservice String base{get;set;}
         webservice String full{get;set;}
         webservice String uplift{get;set;}
         
         webservice String special{get;set;}
         Global OppDiscFieldWrapper(String cat_disc, String base,String full,  String uplift, String special){
             this.cat_disc = cat_disc;
             this.base = base;
             this.full = full;
             this.uplift =  uplift;
             this.special = special;
             
         }
         
     }
     
     Global Class OpportunityFieldWrapper{
         webservice String oppId {get;set;}
         webservice String supportrenewalrate {get;set;}
         webservice String supportrenewalraterasp{get;set;}
         webservice List<OppDiscFieldWrapper> DiscList {get;set;}
         Global OpportunityFieldWrapper(String oppId, String supportrenewalrate, String supportrenewalraterasp, List<OppDiscFieldWrapper> DiscList){
             this.oppId  = oppId ;
             this.supportrenewalrate  = supportrenewalrate ;
             this.supportrenewalraterasp = supportrenewalraterasp;
             this.DiscList  = DiscList ;
             
         }
     }
     
     
     
   }