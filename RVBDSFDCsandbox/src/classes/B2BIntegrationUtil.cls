public with sharing class B2BIntegrationUtil {
    public static Boolean isTestMethod=false;
    @future(callout=true) 
    public static void asyncUserInsertUpdateService(List<Id> recIds,String wsdlStr){
        userInsertUpdateService(recIds,wsdlStr);
    }
    
    public static void userInsertUpdateService(List<Id> recIds,String wsdlStr){
        String ENDPOINT=getUrl('user_service');
        //system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        HttpResponse response=tibcoServiceAPICall(ENDPOINT,wsdlStr);
        //System.debug('***Response:'+response);
        if(response!=null && response.getStatusCode()==200){            
        }else if(response!=null && response.getStatusCode()!=200){
            syncFailureNotification(recIds,'user',response.getStatus(),response.getStatusCode());
        }
        if(!isTestMethod){
            if(response != null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
            }
        }
    }
    
    
    @future(callout=true)
    public static void contactDeleteService(Set<Id> conIds){
        String ENDPOINT=getUrl('contact_service_rm');
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04k40000000TND8</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(Id i : conIds){
            temp+='<Notification>'+'\n'+'<Id>'+i+'</Id>'+'\n'+'<sObject xsi:type="sf:Contact" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+i+'</sf:Id>'+'\n';
            temp=temp+'<sf:IsDeleted>true</sf:IsDeleted>'+'\n';
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        //system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        /*HttpResponse response=tibcoServiceAPICall(ENDPOINT,wsdlStr);
        if(response!=null && response.getStatusCode()==200){
        }else if(response!=null && response.getStatusCode()!=200){
            //syncFailureNotification(conIds,'contact',response.getStatus(),response.getStatusCode());
        }*/
        List<Id> conId = new List<Id>();
        conId.addAll(conIds);
        if(system.isFuture()|| system.isBatch()){
            contactInsertUpdateService(conId,wsdlStr,'Delete', new map<Id,Id>());
        }else{
            asyncConInsertUpdateService(conId,wsdlStr,'Delete', new map<Id,Id>());
        }
         
        /*if(!isTestMethod){
            if(response != null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
            }
        }*/
    }
    
    //Generate User wsdl for insert/update.
    public static void generateUserWSDL(List<User> newTrigger){
        List<Id> recIds=new List<Id>();
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04kS000000096vdIAA</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(User user : newTrigger){
            recIds.add(user.Id);
            temp+='<Notification>'+'\n'+'<Id>'+user.Id+'</Id>'+'\n'+'<sObject xsi:type="sf:user" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+user.Id+'</sf:Id>'+'\n';
            temp=temp+'<sf:IsActive>'+user.IsActive+'</sf:IsActive>'+'\n';
            temp=temp+'<sf:FirstName>'+user.FirstName+'</sf:FirstName>'+'\n';
            temp=temp+'<sf:LastName>'+user.LastName+'</sf:LastName>'+'\n';
            temp=temp+'<sf:Email>'+user.Email+'</sf:Email>'+'\n';
            temp=temp+'<sf:Username>'+user.Username+'</sf:Username>'+'\n'; 
            temp=temp+checkIfNull('User_Role_ID__c',user.User_Role_ID__c)+'\n';
            temp=temp+checkIfNull('UserRoleId',user.UserRoleId)+'\n';
            temp=temp+checkIfNull('ProfileId',user.ProfileId)+'\n';
            temp=temp+checkIfNull('ContactId',user.ContactId)+'\n';
            temp=temp+checkIfNull('Country',findReplace(user.Country))+'\n';
            temp=temp+checkIfNull('State',findReplace(user.State))+'\n';
            temp=temp+checkIfNull('Street',findReplace(user.Street))+'\n';
            temp=temp+checkIfNull('City',findReplace(user.City))+'\n';
            temp=temp+checkIfNull('PostalCode',findReplace(user.PostalCode))+'\n';
            temp=temp+checkIfNull('Phone',user.Phone)+'\n';
            temp=temp+checkIfNull('Fax',user.Fax)+'\n';            
            temp=temp+'<sf:CreatedById>'+user.CreatedById+'</sf:CreatedById>'+'\n';
            temp=temp+'<sf:CreatedDate>'+user.CreatedDate+'</sf:CreatedDate>'+'\n';
            temp=temp+'<sf:LastModifiedDate>'+user.LastModifiedDate+'</sf:LastModifiedDate>'+'\n';
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        //system.debug('IsFuture:'+System.isFuture());
        if(system.isFuture()|| system.isBatch()){
            userInsertUpdateService(recIds,wsdlStr);
        }else{
            asyncUserInsertUpdateService(recIds,wsdlStr);
        }
    }
    
    public static void generateContactWSDL(List<Contact> newTrigger, String operation, Map<Id, Id>conIdrmId){
        List<Id> recIds = new List<Id>();
        Set<Id> accIds = new Set<Id>();
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04kS000000096vdIAA</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(Contact c : newTrigger){
            recIds.add(c.Id);
            accIds.add(c.AccountId);
        }
        Map<Id,Account> accMap = new Map<Id, Account>([Select id ,Name, OracleCustomerId__c from Account where Id in :accIds]);
        Map<Id,User> userMap = new Map<Id, User>([Select id ,Name, isActive, UserName from User where ContactId in :recIds]);
        
        for(Contact c : newTrigger){
            recIds.add(c.Id);
            temp+='<Notification>'+'\n'+'<Id>'+c.Id+'</Id>'+'\n'+'<sObject xsi:type="sf:contact" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+c.Id+'</sf:Id>'+'\n';
            temp=temp+'<sf:AccountId>'+c.AccountId+'</sf:AccountId>'+'\n';
            temp=temp+'<sf:FirstName>'+c.FirstName+'</sf:FirstName>'+'\n';
            temp=temp+'<sf:LastName>'+c.LastName+'</sf:LastName>'+'\n';
            temp=temp+'<sf:Email>'+c.Email+'</sf:Email>'+'\n';
            if(c.IsDeleted ==  false){
                temp=temp+'<sf:Status>'+'1'+'</sf:Status>'+'\n';    
            }else if(c.IsDeleted){
                temp=temp+'<sf:Status>'+'0'+'</sf:Status>'+'\n';
            }
            
            temp=temp+'<sf:NamePrefix>'+''+'</sf:NamePrefix>'+'\n';
            temp=temp+'<sf:Alias>'+''+'</sf:Alias>'+'\n';
            temp=temp+'<sf:AddressID>'+''+'</sf:AddressID>'+'\n';
            temp=temp+'<sf:Address1>'+''+'</sf:Address1>'+'\n';
            temp=temp+'<sf:Address2>'+''+'</sf:Address2>'+'\n';
            temp=temp+'<sf:Address3>'+''+'</sf:Address3>'+'\n';
            temp=temp+'<sf:City>'+''+'</sf:City>'+'\n';
            temp=temp+'<sf:StateProvince>'+''+'</sf:StateProvince>'+'\n';
            temp=temp+'<sf:PostalCode>'+''+'</sf:PostalCode>'+'\n';
            temp=temp+'<sf:Country>'+''+'</sf:Country>'+'\n';
            //temp=temp+'<sf:DoNotContact>'+''+'</sf:DoNotContact>'+'\n';
            //temp=temp+'<sf:DoNotEmail>'+''+'</sf:DoNotEmail>'+'\n';
            //temp=temp+'<sf:SupervisorContactId>'+''+'</sf:SupervisorContactId>'+'\n';
            //temp=temp+'<sf:MaxDiscountPctg>'+''+'</sf:MaxDiscountPctg>'+'\n';
            //temp=temp+'<sf:LastContact>'+''+'</sf:LastContact>'+'\n';
            //temp=temp+'<sf:RenewalContactType>'+''+'</sf:RenewalContactType>'+'\n';
            temp=temp+'<sf:Notes>'+''+'</sf:Notes>'+'\n';
            temp=temp+'<sf:xUDFDate1>'+''+'</sf:xUDFDate1>'+'\n';
            temp=temp+'<sf:xUDF1>'+''+'</sf:xUDF1>'+'\n';
            temp=temp+'<sf:xUDF2>'+''+'</sf:xUDF2>'+'\n';
            temp=temp+'<sf:xUDF3>'+''+'</sf:xUDF3>'+'\n';
            temp=temp+'<sf:xUDF4>'+''+'</sf:xUDF4>'+'\n';
            temp=temp+'<sf:xUDF5>'+''+'</sf:xUDF5>'+'\n';
                
            
            if(accMap != null && accMap.containskey(c.AccountId)){
                temp=temp+checkIfNull('CompanyID',accMap.get(c.AccountId).OracleCustomerId__c)+'\n';
            } 
            if(userMap != null && userMap.containskey(c.Id)){
                temp=temp+checkIfNull('SFDCPartnerUserActive',String.valueof(userMap.get(c.Id).isActive))+'\n';
                temp=temp+checkIfNull('SFDCPartnerUserId',userMap.get(c.Id).UserName)+'\n';
            }
            temp=temp+checkIfNull('Department',c.Department)+'\n';  
            if(c.Renewal_Contact__c != null && c.Renewal_Contact__c){
                temp=temp+'<sf:Renewal_Contact__c>'+'Y'+'</sf:Renewal_Contact__c>'+'\n';
            }else if(c.Renewal_Contact__c == null ||(!c.Renewal_Contact__c)){
                temp=temp+'<sf:Renewal_Contact__c>'+'N'+'</sf:Renewal_Contact__c>'+'\n';
            }
            //mp=temp+checkIfNull('Renewal_Contact__c',String.valueof(c.Renewal_Contact__c))+'\n';
            temp=temp+checkIfNull('Salutation',c.Salutation)+'\n';
            temp=temp+checkIfNull('Title',c.Title)+'\n';
            temp=temp+checkIfNull('User_Profile__c',c.User_Profile__c)+'\n';
            temp=temp+checkIfNull('MobilePhone',c.MobilePhone)+'\n';
            temp=temp+checkIfNull('PartnerRole__c',c.PartnerRole__c)+'\n';
            temp=temp+checkIfNull('Phone',String.valueof(c.Phone))+'\n';
            temp=temp+checkIfNull('Fax',String.valueof(c.Fax))+'\n';            
            //temp=temp+'<sf:CreatedById>'+c.CreatedById+'</sf:CreatedById>'+'\n';
            //temp=temp+'<sf:CreatedDate>'+c.CreatedDate+'</sf:CreatedDate>'+'\n';
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        //system.debug('IsFuture:'+System.isFuture());
         System.debug('Contact WSDL:'+wsdlStr);
        if(system.isFuture()|| system.isBatch() || operation.equals('Upsert')){
            contactInsertUpdateService(recIds,wsdlStr,operation, conIdrmId);
        }else{
            asyncConInsertUpdateService(recIds,wsdlStr,operation, conIdrmId);
        }
    }
    
    @future(callout=true) 
    public static void asyncConInsertUpdateService(List<Id> recIds,String wsdlStr, String Operation,Map<Id,Id> conIdRmId){
        contactInsertUpdateService(recIds,wsdlStr, Operation, conIdRmId);
    }
    
    public static void contactInsertUpdateService(List<Id> recIds,String wsdlStr, String operation, Map<Id,Id> conIdRmId){
        List<RM_Contact_Sync_Log__c> rmList = new List<RM_Contact_Sync_Log__c>();
        Map<Id, Contact> contIdToRec = new Map<Id, Contact>();
        Set<Id> conIds  = new Set<Id>();
        if(conIdRmId != null && conIdRmId.size()>0){
            conIds.addAll(conIdRmId.values());  
        }
        if(recIds != null && recIds.size()>0){
            conIds.addAll(recIds);  
        }
        for(Contact c : [Select Email, Phone, Id, Name , Renewal_Contact__c from Contact Where Id in :conIds]){
            contIdToRec.put(c.id, c);     
        }
        if(operation != null && operation.equals('Upsert') && conIdRmId != null){
            String rIds;
            for(Id r: conIdRmId.keyset()){
                RM_Contact_Sync_Log__c rmLog = new RM_Contact_Sync_Log__c(
                    Contact__c = r,
                    Operation__c    = operation,
                    Id  = conIdRmId.get(r)
                );
                if(contIdToRec.get(conIdRmId.get(r)).Email != null)
                    rmlog.Email__c = contIdToRec.get(conIdRmId.get(r)).Email;
                if(contIdToRec.get(conIdRmId.get(r)).Phone != null)
                    rmlog.Phone__c = contIdToRec.get(conIdRmId.get(r)).Phone;
                if(contIdToRec.get(conIdRmId.get(r)).Renewal_Contact__c  != null)
                    rmlog.Renewal_Contact__c  = contIdToRec.get(conIdRmId.get(r)).Renewal_Contact__c; 
                
                rmList.add(rmLog);
                
            }
                
        }else{
            for(Id r: recIds){
                RM_Contact_Sync_Log__c rmLog = new RM_Contact_Sync_Log__c(
                    
                    Operation__c    = operation
                );
                if(operation != null && operation.equals('Delete')){
                    rmLog.ContactId__c =  r; 
                }else{
                    rmLog.Contact__c = r;
                    rmLog.ContactId__c =  r; 
                }
                Contact con  = contIdToRec.get(r);
                if(con != null && con.Email != null)
                    rmlog.Email__c = contIdToRec.get(r).Email;
                if(con != null && con.Phone != null)
                    rmlog.Phone__c = contIdToRec.get(r).Phone;
                if(con != null && con.Renewal_Contact__c  != null)
                    rmlog.Renewal_Contact__c  = contIdToRec.get(r).Renewal_Contact__c; 
                
                rmList.add(rmLog);
            }
            
        }
        
        try{
        //String ENDPOINT='http://wstest.riverbed.com/Processes/IntegrationInterfaces/Services/UserService.serviceagent/NotificationPortEndpoint111?env=stage';
        
        String ENDPOINT=getUrl('contact_service_rm');
        //system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        
        HttpResponse response=tibcoServiceAPICall(ENDPOINT,wsdlStr);
        for(RM_Contact_Sync_Log__c r: rmList){
            r.Status__c = String.valueof(response.getStatusCode()); 
        }
        
        //System.debug('***Response:'+response);
        if(response!=null && response.getStatusCode()==200){            
        }else if(response!=null && response.getStatusCode()!=200){
            syncFailureNotification(recIds,'Contact',response.getStatus(),response.getStatusCode());
        }
        if(!isTestMethod){
            if(response != null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
            }
        }
        }catch(Exception e){
            
            for(RM_Contact_Sync_Log__c r: rmList){
                r.Exception__c = String.valueof(e.getMessage());    
            }
        }
        upsert rmList;
    }
    
    
    //Generate Quote wsdl for update.
    public static void generateQuoteWSDL(List<Quote__c> newTrigger){
        String date_time='yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        String ENDPOINT=getUrl('quote_service');
        List<Id>recIds=new List<Id>();
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04kS000000096vdIAA</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(Quote__c quote : newTrigger){
            recIds.add(quote.Id);
            temp+='<Notification>'+'\n'+'<Id>'+quote.Id+'</Id>'+'\n'+'<sObject xsi:type="sf:Quote__c" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+quote.Id+'</sf:Id>'+'\n';
            temp=temp+'<sf:Discount_Status__c>'+quote.Discount_Status__c+'</sf:Discount_Status__c>'+'\n';
            temp=temp+'<sf:Name>'+quote.Name.replaceAll('&', '&amp;')+'</sf:Name>'+'\n';
            temp=temp+'<sf:LastModifiedDate>'+quote.LastModifiedDate.format(date_time)+'</sf:LastModifiedDate>'+'\n';//Added by prashant.singh@riverbed.com/SFDC outage Project/10/8/2012 
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        System.debug('Quote WSDL:'+wsdlStr);
        //quoteUpdateService(recIds,wsdlStr);
        if(system.isFuture()|| system.isBatch()){
            quoteUpdateService(recIds,wsdlStr,ENDPOINT);
        }else{
            asyncQuoteUpdateService(recIds,wsdlStr,ENDPOINT);
        }
    }
    public static void dealDeskSyncToPwsWSDL(List<Quote__c> newTrigger){
        String date_time='yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
        String ENDPOINT=getUrl('dealDeskQuote_service');
        List<Id>recIds=new List<Id>();
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04kS000000096vdIAA</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(Quote__c quote : newTrigger){
            recIds.add(quote.Id);
            temp+='<Notification>'+'\n'+'<Id>'+quote.Id+'</Id>'+'\n'+'<sObject xsi:type="sf:Quote__c" xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+quote.Id+'</sf:Id>'+'\n';
            temp=temp+'<sf:Deal_Desk_Status__c>'+quote.Deal_Desk_Status__c+'</sf:Deal_Desk_Status__c>'+'\n';
           // temp=temp+'<sf:Name>'+quote.Name.replaceAll('&', '&amp;')+'</sf:Name>'+'\n';
            //temp=temp+'<sf:LastModifiedDate>'+quote.LastModifiedDate.format(date_time)+'</sf:LastModifiedDate>'+'\n';//Added by prashant.singh@riverbed.com/SFDC outage Project/10/8/2012 
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        System.debug('Quote DealDesk Status WSDL:'+wsdlStr);
        //quoteUpdateService(recIds,wsdlStr);
        if(system.isFuture()|| system.isBatch()){
            quoteUpdateService(recIds,wsdlStr,ENDPOINT);
        }else{
            asyncQuoteUpdateService(recIds,wsdlStr,ENDPOINT);
        }
    }
   
   @future(callout=true) 
    public static void asyncQuoteUpdateService(List<Id> recIds,String wsdlStr,String ENDPOINT){
        quoteUpdateService(recIds,wsdlStr,ENDPOINT);
    }
    
    public static void quoteUpdateService(List<Id> recIds,String wsdlStr,String ENDPOINT){
        //String ENDPOINT=getUrl('quote_service');
        //system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        HttpResponse response=tibcoServiceAPICall(ENDPOINT,wsdlStr);
        if(response!=null && response.getStatusCode()==200){
        }else if(response!=null && response.getStatusCode()!=200){
            syncFailureNotification(recIds,'quote',response.getStatus(),response.getStatusCode());
        }
        if(!isTestMethod){
            if(response != null){
                system.debug(LoggingLevel.INFO,'GetResponse DeaDesk:'+response.getBody());
            }
        }
    }
   // Code Refactored by Anil Madithati for NPI 4.0 09.28.2014
    public static void objectDeleteServiceForTIBCO(List<Id> objIds){
        
        Schema.SObjectType objType = objIds[0].getSObjectType();
        Schema.DescribeSObjectResult dr = objType.getDescribe();
        String ObjName=dr.getName();
        system.debug('ObjectName***:'+dr.getName());
        String wsdlStr='<?xml version="1.0" encoding="UTF-8"?>'+'\n'+'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+'\n'+'<soapenv:Body>'+'\n'+
                        '<notifications xmlns="http://soap.sforce.com/2005/09/outbound">'+'\n';
        wsdlStr=wsdlStr+'<OrganizationId>'+userInfo.getOrganizationId()+'</OrganizationId>'+'\n'+'<ActionId>04k40000000TNQ5</ActionId>'+'\n'+'<SessionId xsi:nil="true"/>'+'\n'+
        '<EnterpriseUrl>'+getUrl('enterprise_url')+'</EnterpriseUrl>'+'\n'+
        '<PartnerUrl>'+getUrl('partner_url')+'</PartnerUrl>'+'\n';
        String temp='';
        for(Id i : objIds){
            temp+='<Notification>'+'\n'+'<Id>'+i+'</Id>'+'\n'+'<sObject xsi:type="sf:'+ObjName+'"' + '\n';
            temp+='xmlns:sf="urn:sobject.enterprise.soap.sforce.com">'+'\n';
            temp=temp+'<sf:Id>'+i+'</sf:Id>'+'\n';
            temp=temp+'<sf:IsDeleted>true</sf:IsDeleted>'+'\n';
            temp=temp+'</sObject>'+'\n'+'</Notification>'+'\n';
            system.debug('temp***:'+temp);
        }
        wsdlStr=wsdlStr+temp.trim()+'\n'+'</notifications>'+'\n'+'</soapenv:Body>'+'\n'+'</soapenv:Envelope>'+'\n';
        system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        
        if(system.isFuture()|| system.isBatch()){
            syncObjectDeleteServiceForTIBCO(objIds,wsdlStr,ObjName);
        }else{
            asyncObjectDeleteServiceForTIBCO(objIds,wsdlStr,ObjName);
        }
        
    }
    @future(callout=true) 
    public static void asyncObjectDeleteServiceForTIBCO(List<Id> recIds,String wsdlStr, String ObjName){
        system.debug(LoggingLevel.INFO,'MethodInputs***:'+wsdlStr+';'+recIds+';'+ObjName);
        syncObjectDeleteServiceForTIBCO(recIds,wsdlStr,ObjName);
    }
    
    public static void syncObjectDeleteServiceForTIBCO(List<Id> recIds,String wsdlStr, String ObjName){
        system.debug(LoggingLevel.INFO,'MethodInputs123***:'+wsdlStr+';'+recIds+';'+ObjName);
        String ENDPOINT=getUrl(ObjName);
        system.debug(LoggingLevel.INFO,'ENDPOINT***:'+ENDPOINT);
        system.debug(LoggingLevel.INFO,'WSDL***:'+wsdlStr);
        HttpResponse response=tibcoServiceAPICall(ENDPOINT,wsdlStr);
        if(response!=null && response.getStatusCode()==200){
        }else if(response!=null && response.getStatusCode()!=200){
            syncFailureNotification(recIds,ObjName,response.getStatus(),response.getStatusCode());
        }
        if(!isTestMethod){
            if(response != null){
                system.debug(LoggingLevel.INFO,'GetResponse:'+response.getBody());
            }
        }
    }
    
    private static HttpResponse tibcoServiceAPICall(String urlStr , String wsdlStr){  
        system.debug(LoggingLevel.INFO,'MethodInputs111***:'+wsdlStr+';'+urlStr);     
        HttpResponse response=null;     
        try{
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint(urlStr);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'text/xml; charset=utf-8');
            req.setTimeout(60000);
            req.setBody(wsdlStr);
            //req.setCompressed(true);            
            if(!isTestMethod){
                System.debug(LoggingLevel.INFO,'Seding request to: ' + urlStr);
                System.debug('Number of Web service statements Processed:'+Limits.getCallouts());
                System.debug('the total number of Web service statements processed in the current context:'+Limits.getLimitCallouts());
                response = h.send(req);
                system.debug('Response Body:'+response.getBody());   
            }         
        }catch(System.CalloutException ce){
            System.debug(LoggingLevel.INFO,'Error in sending request: ' + ce.getMessage());
        }catch(Exception e){ 
           System.debug(LoggingLevel.INFO, 'Error : ' + e.getMessage());
        }
        return response;  
    }
    private static String checkIfNull(String strField,String strValue){
        string tempStr;
        if(strValue==null){
            tempStr='<sf:'+strField+'></sf:'+strField+'>';
        }else{
            tempStr='<sf:'+strField+'>'+strValue+'</sf:'+strField+'>';
        }
        return tempStr;
    }
    
    //To find and replace '&' to 'amp;'
    private static string findReplace(String str){
        if(str != null){
            return str.replaceAll('&', '&amp;');
        }
        else return null; 
    }
    //To get end point urls
    private static string getUrl(string serviceType){
        SFDC_PWS_Integration_Settings__c iProp=SFDC_PWS_Integration_Settings__c.getValues('IntegrationEndPoints');
        if(iprop != null){
        if(serviceType.equalsIgnoreCase('Account')){
            return iProp.account_service__c;
        }else if(serviceType.equalsIgnoreCase('Contact')){
            return iprop.contact_service__c;
        }else if(serviceType.equalsIgnoreCase('Opportunity')){
            return iProp.opportunity_service__c;
        }else if(serviceType.equalsIgnoreCase('user_service')){
            return iProp.user_service__c;
        }else if(serviceType.equalsIgnoreCase('quote_service')){
            return iProp.quote_service__c;
        }else if(serviceType.equalsIgnoreCase('enterprise_url')){
            return iProp.enterprise_url__c;
        }else if(serviceType.equalsIgnoreCase('partner_url')){
            return iProp.partner_url__c;
        }else if(serviceType.equalsIgnoreCase('Additional_Discount_Detail__c')){
            return iProp.addService_url__c;
        }else if(serviceType.equalsIgnoreCase('Account_Authorizations__c')){
            return iProp.accAuth_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Discount_Detail__c')){
            return iProp.DiscountDetail_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Category_Master__c')){
            return iProp.CategoryMaster_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Opnet_Account_Group__c')){
            return iProp.OPNETAccountGroup_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Opnet_Group_ID__c')){
            return iProp.OPNETGroupID_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Authorizations_Master__c')){
            return iProp.AuthorizationsMaster_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Product_Family_Authorizations__c')){
            return iProp.ProductFamilyAuthorizations_Service__c;
        }
        else if(serviceType.equalsIgnoreCase('Linked_Account__c')){
            return iProp.LinkedAccount_Service__c;
        }else if(serviceType.equalsIgnoreCase('contact_service_rm')){
            return iprop.Contact_RM__c;
        }else if(serviceType.equalsIgnoreCase('Asset')){
            return iProp.Asset_Service__c;
        }else if(serviceType.equalsIgnoreCase('dealDeskQuote_service')){
            return iProp.DealDesk_Quote_Service__c;
        }
        return ' ';
        } else {
            return ' ';
        }
        
    }
    private static void syncFailureNotification(List<Id> recIds, String sObj, String responseStr, Integer responseCode){
    System.debug('SUKHDEEP B2BIntegrationUtil.objectDeleteServiceForTIBCO  syncFailureNotification');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'rvbd-sfdc-support@riverbed.com','Ankita.Goel@riverbed.com'};
        toAddresses.add('Anil.Madithati@riverbed.com');
        mail.setToAddresses(toAddresses);
        mail.setSubject('SFDC to PWS sync through Tibco Fails"B2BIntegrationUtil.syncFailureNotification()"'); 
        mail.setHtmlBody('The SFDC to PWS sync through Tibco of '+sObj+' fails for below records:' + recIds +'<br>'+'Response Code:'+ responseCode + '<br>'+'Resconse Reason:'+responseStr+'<br>'+'Total Count:'+recIds.size()+'<br><br><br>'+'Thank You'+'<br>'+'RVBD Support Team');
        System.debug('SUKHDEEP B2BIntegrationUtil.objectDeleteServiceForTIBCO  syncFailureNotification mail --> '+mail);
        try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }catch(System.EmailException e){
            system.debug('Error:'+e);
        }
    }
}