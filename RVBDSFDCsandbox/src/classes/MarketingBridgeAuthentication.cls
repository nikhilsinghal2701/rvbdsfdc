public with sharing class MarketingBridgeAuthentication {
    private static string algorithmName='MD5';
    private static string siteUrl='http://www.riverbed.com/';
    private static string SECRET='RVBDPartner199';
    //test token url    replace /action/riverbed/site/user/Login with /action/riverbed/Default
    //private static string ENDPOINT='http://riverbed.marketingbridge.com/action/riverbed/site/user/Test4SO';
    //private static string ENDPOINT='https://riverbed.marketingbridge.com/action/riverbed/site/user/Login?ticket='; //commented on 08/27/2010
    private static string ENDPOINT='https://riverbed.marketingbridge.com/action/riverbed/Default?ticket=';    
    private static string GMTTIME;
    private static string USERNAME;
    private static string USER_ID;
    private static string EMAIL;
    private static string ORGANIZATION_GROUPS;
    private static string ORGANIZATION_NAME;
    private static string ORGANIZATION_ID;
    private static string FNAME;
    private static string LNAME;
    private static string GROUPS='Partner';
    private string responseBody;
    private String responseURL = '';
    private String frameHeight = '0px';
    private String styleValue = 'block';
    private static Boolean isTestMethod=false;
    private PageReference retPage;
    
    public MarketingBridgeAuthentication(){     
    } 
    /*  
    public void tokenAuthentication(){
        GMTTIME=getTimeInGMT();
        USERNAME=userInfo.getUserName();
        USER_ID=userInfo.getUserId();
        EMAIL=[select Email from user where id=:USER_ID].Email;
        User lstUser=[Select u.ContactId, u.Contact.Account.Id, u.Contact.Account.Name, u.Contact.Id, u.Contact.Name, Contact.Account.Partner_Level__c,
                contact.Account.Type, u.Contact.Phone, u.Id, u.UserType__c,u.FirstName,u.LastName from User u where u.Id=:userInfo.getUserId()];
        ORGANIZATION_GROUPS=GROUPS+'|'+isNull(lstUser.contact.Account.Type)+'|'+isNull(lstUser.contact.Account.Partner_Level__c);
        ORGANIZATION_NAME=isNull(lstUser.contact.Account.Name);
        ORGANIZATION_ID=isNull(lstUser.contact.Account.Id);
        FNAME=isNull(lstUser.FirstName);
        LNAME=isNull(lstUser.LastName);
        //string dataString = GMTTIME+','+USERNAME+','+ EMAIL+','+GROUPS+','+USER_ID+','+ORGANIZATION_GROUPS;
        string dataString = GMTTIME+','+isNull(USERNAME)+','+ isNull(EMAIL)+','+GROUPS+','+USER_ID+','+
                        ORGANIZATION_NAME+','+ORGANIZATION_GROUPS+','+ORGANIZATION_ID+','+FNAME+','+LNAME;
        string digestString=SECRET+','+dataString;              
        Blob myDigest = Crypto.generateDigest(algorithmName, Blob.valueOf(digestString));
        string myDigestString = EncodingUtil.ConvertToHex(myDigest);
        string token=isNull(myDigestString)+':'+dataString;
        checkResponse(ENDPOINT,token);                   
    }
    private void checkResponse(String urlStr , String body){   
        responseURL = ENDPOINT+ body;
        frameHeight = '875px';
    }
    public String getResponseURL(){
        return responseURL;
    }    
    public String getFrameHeight(){
        return frameHeight;
    }    
    public void setFrameHeight(String strHeight){
        this.frameHeight = strHeight;
    }
    */
    public PageReference tokenAuthentication(){
    	try{
	        GMTTIME=getTimeInGMT();
	        USERNAME=userInfo.getUserName();
	        USER_ID=userInfo.getUserId();
	        EMAIL=[select Email from user where id=:USER_ID].Email;
	        User lstUser=[Select u.ContactId, u.Contact.Account.Id, u.Contact.Account.Name, u.Contact.Id, u.Contact.Name, Contact.Account.Partner_Level__c,
	                contact.Account.Type, u.Contact.Phone, u.Id, u.UserType__c,u.FirstName,u.LastName from User u where u.Id=:userInfo.getUserId()];
	        ORGANIZATION_GROUPS=GROUPS+'|'+isNull(lstUser.contact.Account.Type)+'|'+isNull(lstUser.contact.Account.Partner_Level__c);
	        ORGANIZATION_NAME=isNull(lstUser.contact.Account.Name);
	        ORGANIZATION_ID=isNull(lstUser.contact.Account.Id);
	        FNAME=isNull(lstUser.FirstName);
	        LNAME=isNull(lstUser.LastName);
	        //string dataString = GMTTIME+','+USERNAME+','+ EMAIL+','+GROUPS+','+USER_ID+','+ORGANIZATION_GROUPS;
	        string dataString = GMTTIME+','+isNull(USERNAME)+','+ isNull(EMAIL)+','+GROUPS+','+USER_ID+','+
	                        ORGANIZATION_NAME+','+ORGANIZATION_GROUPS+','+ORGANIZATION_ID+','+FNAME+','+LNAME;
	        string digestString=SECRET+','+dataString;              
	        Blob myDigest = Crypto.generateDigest(algorithmName, Blob.valueOf(digestString));
	        string myDigestString = EncodingUtil.ConvertToHex(myDigest);
	        string token=isNull(myDigestString)+':'+dataString;
	        //system.debug('TOKEN:'+token);
	        //token=EncodingUtil.urlEncode(token, 'UTF-8');
	        //system.debug('Original Token:'+token);
	        retPage = new PageReference(checkResponse(ENDPOINT,token));
    	}catch(Exception e){
    		system.debug('Exception:'+e);
    	}
        return retPage;                   
    }
    
    private string checkResponse(String urlStr , String body){   
        responseURL = urlStr+body;
        return responseURL;
    }      
    
    private string getTimeInGMT(){
        DateTime d = System.now();
        String zero='0';
        String month=''+d.monthGmt();
        if(month.length()==1) month=zero+month;
        String day=''+d.dayGmt();
        if(day.length()==1)day=zero+day;
        String hour=''+d.hourGmt();
        if(hour.length()==1)hour=zero+hour;
        String minute=''+d.minuteGmt();
        if(minute.length()==1) minute=zero+minute;
        String second=''+d.secondGmt();
        if(second.length()==1)second=zero+second;
        String timeStamp = ''+d.yearGmt()+month+day+hour+minute+second;
        String timeFormat = d.formatGmt(timeStamp);
        return timeFormat;
    } 
    /*
    private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return originalStr;
    }  
    */
    
    private string isNull(string originalStr){
        string str='';
        if(originalStr==null)return str;
        else return getURLEncode(originalStr);
    }  
    private string getURLEncode(string originalStr){
        string encodeStr=EncodingUtil.urlEncode(originalStr, 'UTF-8');
        //system.debug('>>> String:'+encodeStr);
        return encodeStr;
    }
    
    //Test Method for MarketingBridgeAuthentication Controller 
    static testMethod void testMarketingBridgeAuthentication(){
        isTestMethod=true;
        MarketingBridgeAuthentication mba=new MarketingBridgeAuthentication();
        mba.tokenAuthentication();
    }
    
}