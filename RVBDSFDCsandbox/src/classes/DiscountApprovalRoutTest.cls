@isTest(SeeAllData=true)
private class DiscountApprovalRoutTest {
    testMethod public static void testRequired() {  
        
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        Id approver1 = null;
        Id approver2 = null;
        System.assertEquals('Required', q.Discount_Status__c);
        List<Delegated_Approver__c> d1=[SELECT Delegation_Detail__c,start_date__c,end_date__c, Delegation_Detail__r.Requester__r.Name FROM Delegated_Approver__c where  Delegation_Detail__r.Requester__r.Name='Denis Murphy' limit 2 ];
          for( Delegated_Approver__c d :d1)    
          {
            d.Start_Date__c=System.today()-30;
            d.end_date__C =System.today()+30;
            d.Geo__c=null;
            d.Region__c=null;
          }     
          update (d1);   
        //DiscountApprovalTestHlp.submitQuote(q);
        q =  DiscountApprovalTestHlp.getQuote(q.Id); 
        System.debug('Quote:' + q);
        System.assert(q.Approver1__c != null);
        approver1 = q.Approver1__c;
        Test.startTest();
        //DiscountApprovalTestHlp.approveQuote(q.Id, 'Approve');
    
        q =  DiscountApprovalTestHlp.getQuote(q.Id); 
        q.Discount_Code__c='Promo - Loyalty Promotion SHAxx10 to SHAxx50';
        
        
        update q;
        test.stoptest();
    }
    
    testMethod public static void testRequired1() { 
        Opportunity opp = DiscountApprovalTestHlp.createOpp();  
        List<Quote_Line_Item__C> qlist = new List<Quote_Line_Item__C>();
        Quote__c q = new Quote__c (Opportunity__c=opp.Id,Discount_Status__C='Approved',Promotion_Expired__c = false,Promotional__c = false, Name='test', RSM__c=DiscountApprovalTestHlp.getRSM(), Discount_Code__c='Competitive Pricing', Discount_Reason__c='this',Discount_Product__c=9);
        insert q;
        update(q);
        test.starttest();
        List<Product2> prodList=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE' or ProductCode ='SVC-PSD-00209'];
        //Product2 p1 = new Product2(Name = 'SHA-06050-BASE', ProductCode = 'SHA-06050-BASE');
        //Product2 p2 = new Product2(Name = 'SVC-PSD-00209',ProductCode = 'SVC-PSD-00209');
        //List<Product2> prodList=new List<Product2>{p1,p2};
        //insert prodList;
        Product2 prod,prod1;
        for(Product2 p :prodlist)
        {
         if(p.ProductCode=='SHA-06050-BASE')    
         prod=p;
         else
         prod1=p;
        }
        /*Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;*/
        //Test.startTest();
        Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=0,Unit_List_Price__c=0);
        //added by santoshi on 10/25/2011 to add multiple quote line items for testing purpose
        Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Quote__c=q.Id,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25);
        Quote_Line_Item__c qli2 = new Quote_Line_Item__c(Quote__c=q.Id,Qty_Ordered__c=1,Category__c='E',Product_Code__c=prod1.ProductCode,Product2__c=prod1.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25); 
        qlist.add(qli);
        qlist.add(qli1);
        qlist.add(qli2);
        
        insert qlist;       
        q =  DiscountApprovalTestHlp.getQuote(q.Id); 
        q.Discount_Code__c='Promo - Loyalty Promotion SHAxx10 to SHAxx50';
        
        update q;
        test.stoptest();
    }

    testMethod public static void testTwoLevel() {
        Test.startTest();
        DiscountApproval.isTest = true;
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        
        Id approver1 = null;
        Id approver2 = null;
        Product2 prod = [SELECT Name, ProductCode FROM Product2 WHERE ProductCode = 'SVC-PSD-00002'];
        Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Quote__c=q.Id, Non_Standard_Discount__c=30,Qty_Ordered__c=1, Category__c='E', Product_Code__c=prod.ProductCode, Product2__c=prod.Id, CurrencyIsoCode=UserInfo.getDefaultCurrency()); 
        //insert qli1;
        
       // System.assertEquals(q.Discount_Status__c, QuoteApprovalHlp.STATUS_REQUIRED);    
        //System.assertEquals('Required', q.Discount_Status__c);
        Test.stoptest();
        DiscountApprovalTestHlp.submitQuote(q);
        q =  DiscountApprovalTestHlp.getQuote(q.Id); 
        
        System.assert(q.Approver1__c != null);
        approver1 = q.Approver1__c;
        System.assertEquals(QuoteApprovalHlp.STATUS_PENDING, q.Discount_Status__c);
        
        System.runAs(new User(Id=q.Approver1__c)){
            DiscountApprovalTestHlp.approveQuote(q.Id, 'Approve');
            //q =  DiscountApprovalTestHlp.getQuote(q.Id); 
            System.debug('Quote:' + q);     
        }
        approver2 = q.Approver1__c;
        System.runAs(new User(Id=q.Approver1__c)){
            DiscountApprovalTestHlp.approveQuote(q.Id, 'Approve');
            //q =  DiscountApprovalTestHlp.getQuote(q.Id); 
        }
        System.debug('Approver2:' + approver2 + '::' + q.Approver1__c);
        Approvers_Routing_Matrix__c aprvMatrix=new Approvers_Routing_Matrix__c();
        System.assert(q.Id != null);
        aprvMatrix.Quote__c=q.Id;
        aprvMatrix.Category__c='A';
        aprvMatrix.Reference_Role__c='RSM';
    
        
        insert aprvMatrix;
        List<Approvers_Routing_Matrix__c> applist = new List<Approvers_Routing_Matrix__c> {aprvMatrix};
        DiscountApproverCache dc = new DiscountApproverCache();
        Id app=q.Approver1__c;
        DiscountApproverCache.QuoteApprovers qs=dc.getOrCreateApprovers(q.Id);
        qs.hasUserApproved(app);
        qs.getApproverByUserId(app);
        qs.size();
        qs.getLastApprover(applist);
    }
    
    
     testMethod static void DisDetailtest2() {
        test.starttest();
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
        Opportunity opp =DiscountApprovalTestHlp.createOpp();
        List<Discount_Detail__c> detailList1= [select Discount__c, Uplift__c,Special_Discount__c, IsOpp__c,Opportunity__c, Category_Master__c,Category_Master__r.Name from Discount_Detail__c where Opportunity__c = :opp.Id and IsOpp__c = true];
        delete(detailList1);
        List<Discount_Detail__c> detailList = DiscountApprovalTestHlp.createDiscountDetail(catList,opp,null); 
        List<Quote_Line_Item__C> qlist = new List<Quote_Line_Item__C>();
        Quote__c quote = new Quote__c (Opportunity__c=opp.Id, Name='test', RSM__c=DiscountApprovalTestHlp.getRSM(), Discount_Code__c='Competitive Pricing', Discount_Reason__c='this');
        insert quote;
        Product2 prod=[SELECT Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE'];
        /*Product2 prod = new Product2(Name = 'SHA-06050-BASE',ProductCode = 'SHA-06050-BASE');
        insert prod;
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(
        Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        PricebookEntry customPrice = new PricebookEntry(Pricebook2Id = customPB.Id, Product2Id = prod.Id,UnitPrice = 12000, IsActive = true);
        insert customPrice;*/
        //Test.startTest();  
        Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=quote.Id,Non_Standard_Discount__c=2,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency());
        //added by santoshi on 10/25/2011 to add multiple quote line items for testing purpose
    //  Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Quote__c=quote.Id,Qty_Ordered__c=1,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency());
         
    //  qlist.add(qli);
    //  qlist.add(qli1);
        
        insert qli; 
        
        DiscountApproval.isTest = true;
        detailList1= [select Discount__c, Uplift__c,Special_Discount__c, IsOpp__c,Opportunity__c, Category_Master__c,Category_Master__r.Name from Discount_Detail__c where Opportunity__c = :opp.Id and IsOpp__c = true and Category_Master__r.Name='A'];
        detailList1[0].Special_Discount__c=3;
        update(detailList1);
        System.debug(LoggingLevel.INFO,'quote updated..'+qli);
        quote.Discount_Status__C='Approved';
        quote.Promotional__c = true;
        quote.Promotion_Expired__c =true;
        //update quote;
        //System.assertEquals(QuoteApprovalHlp.STATUS_APPROVED, quote.Discount_Status__c);
        Test.stopTest();
    }
   /** testMethod static void testMultiLevelDelegation() {
        //Test.startTest();//Commented here and added below by Rucha on 7/12/2013 to reduce SOQL Query limit error
        DiscountApproval.isTest = true;
        User u1 = DiscountApprovalTestHlp.createUser('delegated.approver1@riverbed.com',UserInfo.getProfileId());
        User u2 = DiscountApprovalTestHlp.createUser('delegated.approver2@riverbed.com',UserInfo.getProfileId());
        u1.UserRoleId = '00E30000000vAM7';
        u2.UserRoleId = u1.UserRoleId;
        u2.Segment__c = 'RVBD Operating Unit - US';
        System.runAs(new User(Id=UserInfo.getUserId())) {
            insert new List<User>{u1,u2};
        }
        
        //Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        Id approverId = [SELECT User__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :q.Id AND Level__c > 0 ORDER BY Level__c][0].User__c;
        Delegation_Detail__c d1 = new Delegation_Detail__c(Requester__c=approverId);
        try {
            d1 = [SELECT Active__c FROM Delegation_Detail__c WHERE Requester__c = :approverId];
            // Delete any existing delegations so we can determine the correct result
            delete [SELECT Id FROM Delegated_Approver__c WHERE Delegation_Detail__c = :d1.Id];
        } catch (QueryException qe) {
        }
        d1.Active__c = true;
        Delegation_Detail__c d2 = new Delegation_Detail__c(Requester__c=u1.Id,Active__c=true);
        upsert new List<Delegation_Detail__c>{d1,d2};
        
        Delegated_Approver__c da1 = new Delegated_Approver__c(Delegation_Detail__c=d1.Id,User__c=u1.Id,Active__c=true,Start_Date__c=System.today(),End_Date__c=System.today());
        Delegated_Approver__c da2 = new Delegated_Approver__c(Delegation_Detail__c=d2.Id,User__c=u2.Id,Active__c=true,Start_Date__c=System.today(),End_Date__c=System.today());
        //insert new List<Delegated_Approver__c>{da1,da2};
        
        // Submit for approval
        Test.startTest();//Added by Rucha on 7/12/2013 to avoid SOQL limit error
        DiscountApprovalTestHlp.submitQuote(q);
        Test.stopTest();//Added by Rucha on 7/12/2013 to avoid SOQL limit error
        //q =  DiscountApprovalTestHlp.getQuote(q.Id); //commented by prashant on 1/16/2013 for reducing SOQL limit error in test code 
        //System.assertEquals(QuoteApprovalHlp.STATUS_PENDING, q.Discount_Status__c);//commented by prashant on 1/16/2013 for reducing SOQL limit error in test code 
        //System.assert(q.Approver1__c != null);//commented by prashant on 1/16/2013 for reducing SOQL limit error in test code 
    //  System.assertEquals(u2.Id, q.Approver1__c);
    } **/
    
    testMethod static void testPromotionActive() {
        test.starttest();
        Quote__c quote = DiscountApprovalTestHlp.createQuote(null);
        System.assertEquals(quote.Discount_Status__c, QuoteApprovalHlp.STATUS_REQUIRED);
        
        Discount_Schedule__c schedule = new Discount_Schedule__c(Active__c=true);
        schedule.Start_Date__c=System.today().addMonths(-4);
        schedule.End_Date__c=System.today().addMonths(1);
        schedule.Reason_Code__c = 'TEST';
        schedule.Reason_Text__c = 'Just testing';
        insert schedule;
        
        quote.Discount_Code__c = schedule.Reason_Code__c;
        quote.Discount_Status__C='Approved';
        //update quote;
        
        quote = [SELECT Discount_Status__c, Promotional__c, Promotion_Expired__c FROM Quote__c WHERE Id = :quote.Id];
        //System.assert(quote.Promotional__c);
        //System.assert(!quote.Promotion_Expired__c);
        //System.assertEquals(QuoteApprovalHlp.STATUS_APPROVED, quote.Discount_Status__c);
        Test.stopTest();
    }
    
    testMethod static void testPromotionExpired() {
        Test.startTest();
        Quote__c quote = DiscountApprovalTestHlp.createQuote(null);
        System.assertEquals(quote.Discount_Status__c, QuoteApprovalHlp.STATUS_REQUIRED);
        
        Discount_Schedule__c schedule = new Discount_Schedule__c(Active__c=true);
        schedule.Start_Date__c=System.today().addMonths(-5);
        schedule.End_Date__c=System.today().addMonths(-1);
        schedule.Reason_Code__c = 'TEST';
        schedule.Reason_Text__c = 'Just testing';
        insert schedule;
        
        quote.Discount_Code__c = schedule.Reason_Code__c;
        //update quote;
        
        //quote = [SELECT Discount_Status__c, Promotional__c, Promotion_Expired__c FROM Quote__c WHERE Id = :quote.Id];
        //System.assert(quote.Promotional__c);
        //System.assert(quote.Promotion_Expired__c);
        //System.assertEquals(QuoteApprovalHlp.STATUS_REQUIRED, quote.Discount_Status__c);
        Test.stopTest();
    }
    
    testMethod static void testProductFamilyRouting(){
        User u = DiscountApprovalTestHlp.createUser('dartTest@test.com',UserInfo.getProfileId());
        
        System.runAs(u){
            //Create custom setting
            NSDOverlays__c nso = new NSDOverlays__c(Name='Test Overlay Stingray',Oracle_Category__c='TestStingray',Overlay_Role__c='Overlay-Stingray');
            insert nso;
        }
        
        //Create record in base approval matrix
        Id catAId;
        for(Category_Master__c cm : DiscountApprovalTestHlp.insertCategoryMasterData()){
            if(cm.Name.equals('A')){
                catAId = cm.Id;
            }
        }
        Base_Approval_Matrix__c bam = new Base_Approval_Matrix__c(BM_Active__c=true,BM_Role__c='Overlay-Stingray',BM_Start_Pct__c=0.01,BM_End_Pct__c=100);
        insert bam;
        
        Base_Approval_Matrix_Detail__c bamDetail = new Base_Approval_Matrix_Detail__c(Active__c=true,Base_Approval_Matrix__c=bam.Id,Category_Master__c=catAId,Start__c=0.01,End__c=100);
        insert bamDetail;
        
        //Create record in exception mapping
        
        Exception_Mapping__c em = new Exception_Mapping__c(Active__c=true,Role__c='Overlay-Stingray',Exception_User__c=u.Id);
        insert em;
        
        //Create oracle category
        Oracle_Category__c oc = new Oracle_Category__c(Name='TestStingray',Active__c=true,Category_Set_Id__c='test123',Category_Set_Name__c='test123',Oracle_Category_Id__c='dartTest123');
        insert oc;
        
        //Create product oracle category
        try{
            //Product2 p = [SELECT Id FROM Product2 WHERE ProductCode = 'SHA-06050-BASE'];
            Product2 p = new Product2(Name = 'SHA-06050-BASE',ProductCode = 'SHA-06050-BASE');
            insert p;
            Product_Oracle_Category__c poc = new Product_Oracle_Category__c(PWS_Oracle_category_id__c='dartTest123',Product__c=p.Id);
            insert poc;
            
            Quote__c q = DiscountApprovalTestHlp.createQuote(null);
            system.debug('Quote prod fam: ' + [Select Id,Oracle_Category__c from Quote_Line_Item__c Where Quote__c=:q.Id]);
           /** List<Approvers_Routing_Matrix__c> arm = [Select User__c, Route__c,Reference_Role__c, Name, Id, Category__c, Actual_Role__c 
                                                    From Approvers_Routing_Matrix__c
                                                    WHERE Quote__c=:q.Id
                                                        and User__c=:u.Id
                                                        and Reference_Role__c='Overlay-Stingray'];
            //System.assert(arm.size() != 0); **/
            
        }
        catch(Exception e){
            system.debug('DiscountApprovalRoutTest.testProductFamilyRouting: Product with product code SHA-06050-BASE not found');
        }   
    }
    
    testMethod static void testSGDelegation() {     
        DiscountApproval.isTest = true;
        User u1 = DiscountApprovalTestHlp.createUser('delegated.approver1@riverbed.com',UserInfo.getProfileId());       
        u1.UserRoleId = '00E30000000vAM7';      
        u1.Segment__c = 'Riverbed Operating Unit - SG';
        System.runAs(new User(Id=UserInfo.getUserId())) {
            insert u1;
        }
        
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        q.Segment__c = 'Riverbed Operating Unit - SG';
        //update q;       
        
        Id approverId = [SELECT User__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :q.Id AND Level__c > 0 ORDER BY Level__c][0].User__c;
        Delegation_Detail__c d1 = new Delegation_Detail__c(Requester__c=approverId);
        try {
            d1 = [SELECT Active__c FROM Delegation_Detail__c WHERE Requester__c = :approverId];
            // Delete any existing delegations so we can determine the correct result
            delete [SELECT Id FROM Delegated_Approver__c WHERE Delegation_Detail__c = :d1.Id];
        } catch (QueryException qe) {
        }
        d1.Active__c = true;        
        upsert new List<Delegation_Detail__c>{d1};
        
        Delegated_Approver__c da1 = new Delegated_Approver__c(Delegation_Detail__c=d1.Id,User__c=u1.Id,Active__c=true,Start_Date__c=System.today(),End_Date__c=System.today().addDays(1));      
        //insert new List<Delegated_Approver__c>{da1};//da2};
        
        // Submit for approval
        Test.startTest();
        DiscountApprovalTestHlp.submitQuote(q);     
        DiscountApprovalTestHlp.approveQuote(q.Id, 'Approve');
        List<Approvers_Routing_Matrix__c> arm = [SELECT Id FROM Approvers_Routing_Matrix__c WHERE Quote__c = :q.Id AND Actual_Role__c='Singapore GM'];
        //System.assert(arm.size() != 0);
        Test.stopTest();
    }
    
    testMethod static void testMissingApporverUsers() {
        try{
            List<Base_Approval_MAtrix_Detail__c> bamDetList= [SELECT Id, Start__c,End__c,Base_Approval_Matrix__r.BM_Role__c
                                                                FROM  Base_Approval_MAtrix_Detail__c 
                                                                WHERE Active__c=true
                                                                    and Category_Master__r.Name='A'
                                                                    and Base_Approval_Matrix__r.BM_Role__c in ('RSM','Reg Director','RVP','Geo VP')];
            List<Base_Approval_MAtrix_Detail__c> bamDetListNew = new List<Base_Approval_MAtrix_Detail__c>();                                                        
            for(Base_Approval_MAtrix_Detail__c bam : bamDetList){
                if(bam.Base_Approval_Matrix__r.BM_Role__c.equals('RSM')){                   
                    bamDetListNew.add(new Base_Approval_MAtrix_Detail__c(Id=bam.Id,Start__c=0.01,End__c=2.5));
                }
                else if(bam.Base_Approval_Matrix__r.BM_Role__c.equals('Reg Director')){                 
                    bamDetListNew.add(new Base_Approval_MAtrix_Detail__c(Id=bam.Id,Start__c=2.51,End__c=5));
                    
                }
                else if(bam.Base_Approval_Matrix__r.BM_Role__c.equals('RVP')){                  
                    bamDetListNew.add(new Base_Approval_MAtrix_Detail__c(Id=bam.Id,Start__c=5.01,End__c=10));
                    
                }
                else if(bam.Base_Approval_Matrix__r.BM_Role__c.equals('Geo VP')){                   
                    bamDetListNew.add(new Base_Approval_MAtrix_Detail__c(Id=bam.Id,Start__c=10.01,End__c=20));          
                }
            }
            update bamDetListNew;
            List<User> userList = new List<User>();
            Profile p = [Select Id from Profile Where Name = 'RSM'];
            User u1 = DiscountApprovalTestHlp.createUser('tmauDiscApprRoutTest1@test.com',p.Id);
            u1.Approver__c = true;          
            
            p = [Select Id from Profile Where Name = 'VP Sales WW w/ Margin Analysis'];
            User u2 = DiscountApprovalTestHlp.createUser('tmauDiscApprRoutTest2@test.com',p.Id);
            u2.Approver__c = true;      
            
            userList.add(u1);
            userList.add(u2);
            
            System.runAs(new User(Id=UserInfo.getUserId())){
                insert u2;
                
                u1.User_Manager__c = u2.Id;
                insert u1;
            }           
            
            Quote__c q = DiscountApprovalTestHlp.createQuoteWithoutLineItems(null);
            
            //Product2 prod = [SELECT Id,Name,ProductCode FROM Product2 WHERE ProductCode = 'SHA-06050-BASE'];
            Product2 prod = new Product2(Name = 'SHA-06050-BASE',ProductCode = 'SHA-06050-BASE');
            insert prod;
            Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=q.Id,D_Non_Standard_Discount__c=75,Qty_Ordered__c=1,Non_Standard_Discount__c=7,Category__c='A',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25000);
            insert qli;
            
            Test.startTest();           
                q.RSM__c = u1.Id;
                update q;
            Test.stopTest();
        }
        catch(Exception e){
            system.debug('Profile not found');
        }
    }                 
}