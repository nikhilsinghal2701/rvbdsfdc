/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest(SeeAllData=True)
private class Test_DiscountDetailsCtrl {

    private static testMethod void DiscountDetailsCtrlTest() {
        DiscountApproval.isTest = true;
        
        /*TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
        
        RVBD_Email_Properties__c rvbdEmail = new RVBD_Email_Properties__c(Name = '',
        mc_org_wide_email_Id__c = 'false');*/
        
        List<Account> lstAcc=new List<Account>();
        Opportunity testOpp=new Opportunity();
        Opportunity testOpp1=new Opportunity();
        RecordType accRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=accCusRT.Id;
        cusAcc.name='CustomeraAAccount';
        cusAcc.Type='Customer';
        cusAcc.Industry='Education';
        lstAcc.add(cusAcc);

        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        lstAcc.add(stpAcc);

        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId=accRT.Id;
        tier2Acc.name='stpAccount';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Authorizations_Specializations__c='RASP';
        lstAcc.add(tier2Acc);

        Account tier3Acc=new Account();
        tier3Acc.RecordTypeId=accRT.Id;
        tier3Acc.name='stpAccount';
        tier3Acc.Type='VAR';
        tier3Acc.Industry='Education';
        tier3Acc.RASP_Status__c='Not Reviewed';
        lstAcc.add(tier3Acc);

        Account influAcc=new Account();
        influAcc.RecordTypeId=accRT.Id;
        influAcc.name='stpAccount';
        influAcc.Type='VAR';
        influAcc.Industry='Education';
        influAcc.RASP_Status__c='Not Reviewed';
        lstAcc.add(influAcc);

        insert lstAcc;

        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];

        testOpp.AccountId=cusAcc.Id;
        testOpp.Channel__c='Reseler';
        testOpp.Sold_To_Partner__c=stpAcc.Id;
        testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp.Primary_ISR__c=primaryISR.Id;
        testOpp.Name='TestOpportunity';
        testOpp.Type='New';
        testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp.CloseDate=System.today();
        testOpp.StageName='0-Prospect';
        testOpp.Forecast_Category__c='PipeLine';
        testOpp.Competitors__c='cisco';
        testOpp.LeadSource='Email';
        //testOpp.Support_Provider__c=stpAcc.Id;
        //testOpp.Support_Provided__c='Reseller';
        insert testOpp;
        
        Quote__c qt = new Quote__c();
        qt.name=testOpp.name;
        qt.Opportunity__c=testOpp.id;
        qt.RSM__c=primaryISR.Id;
        qt.Discount_Code__c='test';
        qt.Discount_Reason__c='test';
        qt.Deal_Desk_Status__c='Not Required';
        qt.Used_For_Ordering__c=True;
        qt.Segment__c='RVBD Operating Unit - US';
        insert qt;
        
        Product2 prod = new Product2(Name = 'SteelHead X200', Family = 'Hardware',ProductCode='test');
        insert prod;
        
        Id pricebookId = Test.getStandardPricebookId();
       // Standard price book entries require the standard price book ID we got earlier.
        /*PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;*/
        
        // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        testOpp.Pricebook2Id = customPB.Id;
        update testOpp;
        
        List<Quote_Line_Item__C> qlist = new List<Quote_Line_Item__C>();
        Quote_Line_Item__c qli = new Quote_Line_Item__c(Quote__c=qt.Id,Category__c='A',Additional_Discount__c=10,Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=0,Unit_List_Price__c=100,D_Non_Standard_Discount__c=10,Qty_Ordered__c=5);
        Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Quote__c=qt.Id,Category__c='A',Additional_Discount__c=10,Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25,D_Non_Standard_Discount__c=10,Qty_Ordered__c=5);
        Quote_Line_Item__c qli2 = new Quote_Line_Item__c(Quote__c=qt.Id,Category__c='E',Additional_Discount__c=10,Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25,D_Non_Standard_Discount__c=10,Qty_Ordered__c=5); 
        Quote_Line_Item__c qli3 = new Quote_Line_Item__c(Quote__c=qt.Id, Non_Standard_Discount__c=30,Additional_Discount__c=10, Category__c='H', Product_Code__c=prod.ProductCode, Product2__c=prod.Id, CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_List_Price__c=25000,D_Non_Standard_Discount__c=10,Qty_Ordered__c=5);
        Quote_Line_Item__c qli4 = new Quote_Line_Item__c(Quote__c=qt.Id,Non_Standard_Discount__c=2,Additional_Discount__c=10,Category__c='K',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_List_Price__c=25000,D_Non_Standard_Discount__c=10,Qty_Ordered__c=5);
        Quote_Line_Item__c qli5 = new Quote_Line_Item__c(Quote__c=qt.Id,D_Non_Standard_Discount__c=75,Additional_Discount__c=10,Non_Standard_Discount__c=7,Category__c='B',Product_Code__c=prod.ProductCode,Product2__c=prod.Id,CurrencyIsoCode=UserInfo.getDefaultCurrency(),Unit_Cost__c=9,Unit_List_Price__c=25000,Qty_Ordered__c=5);
        
        qlist.add(qli);
        qlist.add(qli1);
        qlist.add(qli2);
        qlist.add(qli3);
        qlist.add(qli4);
        qlist.add(qli5);
                
        insert qlist;       
        
        List<Category_Master__c> catList = DiscountApprovalTestHlp.insertCategoryMasterData();
        List<Discount_Detail__c> opptyDetailList = DiscountApprovalTestHlp.createDiscountDetail(catList,testOpp,qt); 
        List<Discount_Detail__c> quoteDetailList = DiscountApprovalTestHlp.createDiscountDetail(catList,null,qt); 
        ApexPages.StandardController sc = New ApexPages.StandardController(testOpp);
        System.currentPageReference().getParameters().put('id',testOpp.id);
        
        //PageReference VFpage = Page.Quote_DiscountDetail;
        //test.setCurrentPage(VFpage);
        
        DiscountDetailsCtrl newController=new DiscountDetailsCtrl(sc);
        
        ApexPages.StandardController sc1 = New ApexPages.StandardController(qt);
        System.currentPageReference().getParameters().put('id',qt.id); 
        DiscountDetailsCtrl newController1=new DiscountDetailsCtrl(sc1);
        
        }

}