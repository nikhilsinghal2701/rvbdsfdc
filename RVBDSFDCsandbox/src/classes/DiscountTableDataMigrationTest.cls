/*************************************************************************************************************
 Name:DiscountManagementHelperTest
 Author: Santoshi Mishra
 Purpose: This is test class for the class DiscountTableDataMigrationClass.
 Created Date : Nov 2011
*************************************************************************************************************/
@isTest
private class DiscountTableDataMigrationTest {

    static testMethod void DiscountTableDataMigrationTest() {
        // TO DO: implement unit test
      
         Test.StartTest();
        List<DiscountCategory__c> detList = new List<DiscountCategory__c>();
        DiscountCategory__c det = new DiscountCategory__c(DiscountDemos__c=5,DiscountProducts__c =8,DiscountSpare__c=3,DiscountSupportBand0__c=6,Standard_Discount_Products__c=9,Standard_Discount_Spares__c=33);
        detList.add(det);
        insert(detList);   
        System.debug('inserted detail---------');
        List<Category_Master__c> catList= new List<Category_Master__c>();
        Category_Master__c cat = new Category_Master__c(Name='B1' , Group__c='Appliance');
        Category_Master__c cat1 = new Category_Master__c(Name='B2' , Group__c='Software');
        catList.add(cat);
        catList.add(cat1);
        insert(catList);
        Map<Id,Category_Master__c> catMap= new Map<Id,Category_Master__c>(catList);
        
        Map<Id,DiscountCategory__c> detFinal= new Map<Id,DiscountCategory__c>(detList);
            DiscountTableDataMigrationClass cls1 = new DiscountTableDataMigrationClass();
            DiscountTableDataMigrationClass cls = new DiscountTableDataMigrationClass(true);
            Database.executeBatch(cls);
         Test.StopTest();
         
         
         List<Discount_Category_Detail__c> childList = [select Id,Name,category__C,category__r.Name,Discount_Table__C,Standard_Discount__C, createddate,
                            Registered_Discount__C from Discount_Category_Detail__c where Discount_Table__C in : detFinal.keySet() and category__r.Name = :cat.name  limit 1 ];
            System.debug('------------'+catList+';;;;;;;;;;;;;;;;;;'+childList);    
            for(Discount_Category_Detail__c c :childList)   
            {
                System.debug('++++++++++++'+c+'pppppppppp'+c.category__r.name);
            }       
        /*System.assertEquals(childList[0].Registered_Discount__c,8);
        System.assertequals(det.Standard_Discount_Products__c,childlist[0].Standard_Discount__c);
        System.assertequals(det.DiscountProducts__c,childlist[0].Registered_Discount__c);
            
    */
    }
}