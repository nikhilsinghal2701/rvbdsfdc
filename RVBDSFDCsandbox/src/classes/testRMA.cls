public class testRMA {

 static testMethod void testCaseUpdate() {
      Id caseid;
      string RMA_number;
      
      case ca = [SELECT id from Case WHERE rma__c  != null LIMIT 1];
      caseid = ca.id;
      
      RMA__c rma = new RMA__C(case_rma__c = ca.id, RMA_Order_Number__c = '1235');
      
      insert rma;
      
      RMA_number = [SELECT id,rma__c from Case WHERE id = : caseid].rma__c;
      
      System.assert(RMA_number.contains('1235'));

    }
}