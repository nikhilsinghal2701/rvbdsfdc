/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestTerritoryPlanning {

    static testMethod void myUnitTest() {
    	Territory__c T= new Territory__c(name = 'Test123');
		insert T;
        Territory_Plan__c Tp= new Territory_Plan__c(name = 'Territory Plan Test123',Territory__c = t.id);
		insert Tp;
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User u = new User(Alias = 'standt', Email=System.now().millisecond() +'standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=System.now().millisecond() + 'standarduser@testorg.com');
      insert u;
      Territory_Team__c tpt= new Territory_Team__c(User__c=u.id,Territory_Plan__c=Tp.id,Access__c='Read');
		PageReference pageRef = Page.TerritoryPlanning;
	    Test.setCurrentPageReference(pageRef);
		ApexPages.CurrentPage().getparameters().put('tid',t.id);
	    ApexPages.StandardController sc = new ApexPages.standardController(Tp);
	    test.startTest();
	    TerritoryPlanningExtension sic = new TerritoryPlanningExtension(sc);
	    insert tpt;
	    sic.Cancel();
	    sic.Cancelv();
	    sic.CClone();
	    sic.Save();
	    sic.Savetp();
	    sic.Savetpd();
	    Attachment attachment1 = new Attachment( );
		attachment1.Body = Blob.valueOf( 'Test 123');
		attachment1.Name = 'Test attachment.jpg';
		sic.attachment1 = attachment1;
	    sic.upload();
	    tpt.Access__c='Read/Edit';
    	update tpt;
	    test.stopTest();
    }
    
    static testMethod void myUnitTest2() {
    	Territory__c T= new Territory__c(name = 'Test123');
		insert T;
        Territory_Plan__c Tp= new Territory_Plan__c(name = 'Territory Plan Test123',Territory__c = t.id);
		insert Tp;
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User u = new User(Alias = 'standt', Email=System.now().millisecond() +'standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=System.now().millisecond() + 'standarduser@testorg.com');
      insert u;
      Territory_Team__c tpt= new Territory_Team__c(User__c=u.id,Territory_Plan__c=Tp.id,Access__c='Read');
      insert tpt;
		PageReference pageRef = Page.TerritoryPlanning;
	    Test.setCurrentPageReference(pageRef);
		ApexPages.CurrentPage().getparameters().put('tid',t.id);
	    ApexPages.StandardController sc = new ApexPages.standardController(Tp);
	    test.startTest();
	    TerritoryPlanningExtension sic = new TerritoryPlanningExtension(sc);
	    sic.AddT();
	    sic.AddTp();
	    sic.Cancel();
	    sic.CClone();
	    sic.Save();
	    sic.Savetp();
	    sic.Savetpd();
	    Attachment attachment1 = new Attachment( );
		attachment1.Body = Blob.valueOf( 'Test 123');
		attachment1.Name = 'Test attachment.jpg';
		sic.attachment1 = attachment1;
	    sic.upload();
    	delete tpt;
	    test.stopTest();
    }
       static testMethod void myUnitTest3() {
    	Territory__c T= new Territory__c(name = 'Test123');
		insert T;
        Territory_Plan__c Tp= new Territory_Plan__c(name = 'Territory Plan Test123',Territory__c = t.id);
	
		PageReference pageRef = Page.TerritoryPlanning;
	    Test.setCurrentPageReference(pageRef);
		ApexPages.CurrentPage().getparameters().put('tid',t.id);
	    ApexPages.StandardController sc = new ApexPages.standardController(Tp);
	    test.startTest();
	    TerritoryPlanningExtension sic = new TerritoryPlanningExtension(sc);
	    
	    test.stopTest();
    }
    
    static testMethod void myUnitTestterritorypdf() {
    	Territory__c T= new Territory__c(name = 'Test123');
		insert T;
        Territory_Plan__c Tp= new Territory_Plan__c(name = 'Territory Plan Test123',Territory__c = t.id);
		insert Tp;
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    User u = new User(Alias = 'standt', Email=System.now().millisecond() +'standarduser@testorg.com', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName=System.now().millisecond() + 'standarduser@testorg.com');
      insert u;
      Territory_Team__c tpt= new Territory_Team__c(User__c=u.id,Territory_Plan__c=Tp.id,Access__c='Read');
      insert tpt;
		PageReference pageRef = Page.TerritoryPlanning;
	    Test.setCurrentPageReference(pageRef);
		ApexPages.CurrentPage().getparameters().put('tid',t.id);
	    ApexPages.StandardController sc = new ApexPages.standardController(Tp);
	    test.startTest();
	    TerritoryPlanningExtensionPDF sic = new TerritoryPlanningExtensionPDF(sc);
	    test.stopTest();
    }
}