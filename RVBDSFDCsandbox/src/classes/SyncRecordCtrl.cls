public with sharing class SyncRecordCtrl {
    ApexPages.StandardController setCon;
    public RM_Contact_Sync_Log__c rm {get; set;}
    
    public SyncRecordCtrl(ApexPages.StandardController controller) {
        setCon = controller;
        this.rm = (RM_Contact_Sync_Log__c)setCon.getRecord();
        this.rm = [Select id, Name, Status__c,  Contact__c, ContactId__c from RM_Contact_Sync_Log__c where Id =:this.rm.id];
        
    }
    
    public PageReference init(){
        if(rm != null && rm.Id != null && rm.Contact__c != null){
            //List<String> conIds = rm.ContactId__c.split(',');
            //System.debug('conIds::::'+conIds);
            //Set<String> con = new Set<String>();
            //if(!conIds.isEmpty()){
              //  con.addALL(conIds);
            //}
            List<Contact> conList = new List<Contact>();
            for(Contact c : [Select  c.Zeus_ID__c, c.Zeus_Account_ID__c, c.User_Profile__c, c.Use_of_Logo__c, c.Type__c, 
            c.Twitter_Profile__c, c.Training_Type__c, c.Training_Date__c, c.Title, c.Renewal_Contact__c,
            c.Technical_Expertise__c, c.SystemModstamp, c.Support_Role__c, c.Support_Manager__c, 
            c.SupportConversionStatus__c, c.Status__c, c.Status_Last_Set__c, c.Speak_at_Sponsored_Event__c, 
            c.Salutation, c.Role_in_IT_Decision__c, c.Revisit_Time__c, c.Return_to_Profile__c, c.Responsiveness__c, 
            c.Response_Status__c, c.ReportsToId, c.Reporters__c,c.Relevance_to_RVBD__c, c.Region__c, 
            c.Reference_Type__c, c.Reference_Status__c, c.Reference_Contact__c, c.RecordTypeId, c.RVBD_Solution_of_Interest__c, c.RCSP_W__c, c.RCSP_W_Cert_Date__c, 
            c.RCSP_V_Cert_Date__c, c.RCSP_NPM__c, c.RCSP_Cert_Date__c, c.RCSI_Cert_Date__c, c.RCSC_Cert_Date__c, c.RCSA_W_Cert_Date__c, c.RCSA_V_Cert_Date__c, c.RAS_Contact__c, c.Quote__c, c.Promote_Riverbed__c,  c.Primary_Contact__c, c.Press_Reference__c, c.PhotoUrl, c.Phone, c.PasswordHash__c, c.PasswordChange__c, c.Partner_Win_Story_Contact__c, c.PartnerRole__c, c.PS_Engagement_Types__c, c.PS_Certifications__c, c.OwnerId,  c.Original_Lead_Score__c, c.OracleContactId__c, c.OracleAddressId__c, c.Optout_Web_Seminars__c, c.Optout_Special_Offers__c, 
            c.Optout_Security_Alerts__c, c.Optout_Product_Updates__c, c.Optout_Newsletters__c, c.Optout_Events__c, c.Opt_Out_Support_Survey__c, c.Opt_Out_Support_Announcements__c, c.Opt_Out_Date__c, c.Opnet_POC_Id__c, c.Opnet_Group_Id__c, c.Offer__c, c.OPNET_Username__c, c.OPNET_SendPasswordResetEmail__c, c.OPNET_PasswordReset__c, c.OPNET_PasswordResetEmailSent__c, c.Notes_about_Lead__c, c.No_Longer_With_Company__c, c.Name, c.MobilePhone, c.Merge_Key__c, c.Mazu_Reports_To_ID__c, c.Mazu_Owner__c, c.Mazu_Contact_ID__c, c.Mazu_Account_ID__c, c.MasterRecordId, c.Manticore_Total_Web_Visits__c, c.Manticore_Last_Web_Visit__c, c.Manticore_Last_Search_Phrase__c, c.MailingStreet, c.MailingState, c.MailingPostalCode, c.MailingLongitude, 
            c.MailingLatitude, c.MailingCountry, c.MailingCity, c.MailingAddress, c.Log_A_Call_LM__c, c.Log_A_CONNECT__c, c.LinkedIn_Search__c, c.LinkedIn_Profile__c, c.LeftCompany__c, c.Lead_Score_Implicit__c, c.Lead_Score_Explicit__c, c.Lead_Score_Date_Most_Recent__c, c.Lead_Rating_Implicit__c, c.Lead_Rating_Explicit__c, c.Lead_Rating_Combined__c, c.Lead_Nurture_Exit_Date__c, c.Lead_Gen_Additional_Info__c, c.LeadSource, c.Latest_Partner_Remote_Login__c, c.Latest_Partner_Remote_Login_Arrow__c, c.Last_Rapleaf_Check__c, c.Last_LMS_Activity_Date__c, c.LastViewedDate, c.LastReferencedDate, c.LastName, c.LastModifiedDate, c.LastModifiedById, c.LastCUUpdateDate, c.LastCURequestDate, c.LastActivityDate, c.Language_Preference__c, c.LMS_Status__c, c.Key_Contact__c, c.Job_Level__c, c.JigsawContactId, c.Jigsaw, c.JS_Title__c, c.JS_Street__c,
             c.JS_State__c, c.JS_Postal_Code__c, c.JS_Phone__c, c.JS_Last_Update_Date__c, 
             c.JS_Last_Name__c, c.JS_Key_ID__c, c.JS_First_Name__c, c.JS_Email__c, c.JS_Data_Consume_Date__c, 
             c.JS_Country__c, c.JS_Company_Name__c, c.JS_City__c, c.IsEmailBounced, c.IsDeleted, c.Invalid_Mailing_Address__c, c.Invalid_Email__c, c.Industry_Analysts__c, c.Import_Action__c, c.Id, c.Hourly_Rate__c, c.HomePhone, c.HasOptedOutOfEmail, c.Goodleads_Contact_Target__c, c.Geographic_Coverage__c, c.Geo_Coverage_Region__c, c.Function__c, c.Full_Promotion_Code__c, c.Forum_User_Name__c, c.FirstName, c.Field_Training__c, c.Fax, c.FCRM__FCR_Temp_Lead_Created_Date__c, c.FCRM__FCR_Take_Scoring_Snapshot__c, c.FCRM__FCR_Superpower_Field__c, c.FCRM__FCR_Status__c, c.FCRM__FCR_Status_Synced_From_Response__c, c.FCRM__FCR_Response_Score__c, c.FCRM__FCR_Prior_Contact_Score__c, c.FCRM__FCR_PostAssignNotificationPending__c, c.FCRM__FCR_Nurture_Timeout__c, c.FCRM__FCR_Name_Created_Date__c, c.FCRM__FCR_Created_By_Lead_Conversion__c, c.FCRM__FCR_Admin_Update_Counter__c, c.FCRM__FCR_Admin_Person_RecordType__c, c.FCCRM_Score__c, c.Eval_Key_Sent__c, c.Eval_Key_Activated__c, c.Eval_Contact__c, c.Email_Opt_In__c, c.Email_Opt_In_News__c, c.Email_Opt_In_Events__c, c.Email_Opt_In_Alerts__c, c.Email_Domain_excludes__c, c.EmailBouncedReason, c.EmailBouncedDate, c.Email, c.E_Mail_Lists__c, c.DoNotCall, c.Division__c, c.Disposition_Status__c, c.Diagnostics_Data__c, c.Description, c.Department, c.Customer_Reference__c, c.Customer_Conference__c, c.CurrencyIsoCode, 
             c.CreatedDate, c.CreatedById, c.Contacts__c, c.Contact_Type__c, c.Contact_Sticky_Notes__c, c.Contact_Score__c, c.Contact_Region__c, c.Contact_Engagement__c, c.Concierge_Rep__c, c.Communications__c, c.Case_Study__c, c.BudgetallocatedforSecurity__c, c.Birthdate, c.BL__SynchWithBoulderLogic__c, c.Attend_RBT_Event__c, c.Asst_E_mail__c, c.AssistantPhone, c.AssistantName, c.Amazon_Wishlist__c, c.AgreedToSupportTermsOfAgreement__c, c.Acct_Related_Company_Category__c, c.Acct_Purchasing_Profile__c, c.Acct_IsMyAlign__c, c.Acct_AcctEngage__c, c.Acct_AcctEngageDate__c, c.Acct_AcctDisp__c, c.Acct_Account_Related_Company__c, c.Acct_Account_Disposition_Engagement__c, c.AcctTgtNonCustPursuitSegment__c, c.AcctTgtCustoPriorityOverall__c, c.AcctTgtCustPurchTrend__c, c.AcctTgtAcctType__c, c.AcctTargetAcct__c, c.AcctStatus__c, c.AcctSalesExecTgtPri__c, c.AcctCategorySummary__c, c.Account_Type__c, c.Account_SubSegment__c, 
             c.Account_Segmentation__c, c.Account_Region__c, c.Account_Partner_Level__c, 
             c.Account_Owner__c, c.Account_Name__c, c.Account_ID_for_list_view__c, c.Account_Geo__c, c.AccountId, 
             c.ADRA__LastAllocation__c, c.ADRA__LastAllocationError__c From Contact c  Where Id = :rm.Contact__c]){
                conList.add(c); 
             }
             if(!conList.isEmpty()){
                Map<Id, Id> rmMap = new Map<Id, Id> ();
                rmMap.put(rm.Contact__c, rm.Id);
                B2BIntegrationUtil.generateContactWSDL(conList, 'Upsert', rmMap);
             }
        }
        PageReference ref = new PageReference('/'+ rm.Id);
        return ref;
    }

}