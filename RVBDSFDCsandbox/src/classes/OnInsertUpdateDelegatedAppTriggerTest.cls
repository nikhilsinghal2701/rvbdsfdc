@isTest(SeeAllData=true)
private class OnInsertUpdateDelegatedAppTriggerTest {

    static testMethod void myUnitTest() {
        //List<User> usr=[select Id,Name from User where isActive=true and UserType='Standard' limit 5];
        Delegation_Detail__c dd=new Delegation_Detail__c();
        dd.Active__c=true;
        //dd.Requester__c=usr[0].Id;
        dd.Requester__c=UserInfo.getUserId();
        insert dd;
        
        Delegated_Approver__c da=new Delegated_Approver__c();
        da.Active__c=true;
        da.Delegation_Detail__c=dd.Id;
        da.Start_Date__c=system.today();
        da.End_Date__c=date.newInstance(2009, 11, 16);
        //da.User__c=usr[2].Id;
        da.User__c=UserInfo.getUserId();
        insert da;
        
        
        Delegated_Approver__c da1=new Delegated_Approver__c();
        da1.Active__c=true;
        da1.Delegation_Detail__c=dd.Id;
        da1.Start_Date__c=date.newInstance(2009, 11, 17);
        da1.End_Date__c=date.newInstance(2009, 11, 19);
        //da1.User__c=usr[3].Id;
        da1.User__c=UserInfo.getUserId();
        da1.Geo__c='AMERICAS';
        da1.Region__c='US - Central';
        insert da1;
        
        Delegated_Approver__c da2=new Delegated_Approver__c();
        da2.Active__c=true;
        da2.Delegation_Detail__c=dd.Id;
        da2.Start_Date__c=date.newInstance(2009, 11, 20);
        da2.End_Date__c=date.newInstance(2009, 11, 21);
        //da2.User__c=usr[3].Id;
        da2.User__c=UserInfo.getUserId();
        da2.Geo__c='AMERICAS';
        //da2.Region__c='US - Central';
        insert da2;
        
        Delegated_Approver__c da3=new Delegated_Approver__c();
        da3.Active__c=true;
        da3.Delegation_Detail__c=dd.Id;
        da3.Start_Date__c=date.newInstance(2009, 11, 22);
        da3.End_Date__c=date.newInstance(2009, 11, 23);
        //da3.User__c=usr[3].Id;
        da3.User__c=UserInfo.getUserId();
        //da3.Geo__c='AMERICAS';
        da3.Region__c='US - Central';
        insert da3;
    }
}