public without sharing class AccountGapTeamVisibility {
	public boolean showPage{get;set;}
	private Boolean visible;
	Id aId;
	public AccountGapTeamVisibility(ApexPages.StandardController controller){
        if(System.currentPageReference().getParameters().get('id')!=null){
	        aId = System.currentPageReference().getParameters().get('id');     
        }		
	}
	//Method to take back to the GAP Portal page
    public pageReference actPage(){
        PageReference accPage = new PageReference ('/apex/GAPList');
        accPage.setRedirect(true);
        return accPage;
    }
	public pageReference checkAccess(){
		Boolean allow=false;
		try{
			visible=Schema.SObjectType.GAP_Team__c.isAccessible();
			allow=allowAccess();			
			if(visible && allow){				
				showPage=true;
			}else{
				String errorMsg='Welcome to the Global Accounts Program (GAP) virtual account plan.'+
	                            'Access to the account plans are restricted to team members of the specific GAP account and controlled by the Global Account Manager (GAM).'+
	                            'If you would like to join the team for this GAP account, please reach out to the GAM.';
	            PageReference retPage = new PageReference('/apex/accountPlanErrorPage?Id=GAPList'+'&errormsg='+errorMsg);        
	            retPage.setRedirect(true);
	            return retPage;
			}
		}catch(Exception e){
			String errorMsg=e.getMessage();
			//String errorMsg='Welcome to the Global Accounts Program (GAP) virtual account plan.'+
	        //                    'Access to the account plans are restricted to team members of the specific GAP account and controlled by the Global Account Manager (GAM).'+
	        //                    'If you would like to join the team for this GAP account, please reach out to the GAM.';
            PageReference retPage = new PageReference('/apex/accountPlanErrorPage?Id=GAPList'+'&errormsg='+errorMsg);        
            retPage.setRedirect(true);
            return retPage;
		}
		return null;
	}
	//checking access
	private Boolean allowAccess(){
		GAPPortalSettings__c gapProperty=GAPPortalSettings__c.getValues('GAPPortalControl');
    	Boolean allow=false;
    	List<GAP_Team__c> gapTeam=[select Id,Name,Account_Name__c,Team_Member__c,Role__c from GAP_Team__c where Account_Name__c=:aId];
    	Map<Id,String> teamMap=new Map<Id,String>();
    	set<GAP_Team__c> gapTeamSet=new set<GAP_Team__c>();
    	for(GAP_Team__c temp:gapTeam){
    		gapTeamSet.add(temp);
    	}
    	for(GAP_Team__c temp:gapTeamSet){
    		teamMap.put(temp.Team_Member__c,temp.Role__c);
    	}
    	Account acc=[select id,name,Global_Account_Manager__c,Global_Account_Manager__r.Name from Account where id=:aId limit 1];
    	if((teamMap.containsKey(userInfo.getUserId())== true)){
    		allow=true;
    	}else if(userInfo.getProfileId().substring(0, 15)==gapProperty.SysAdminProfileId__c||userInfo.getProfileId().substring(0,15)==gapProperty.LimitedAdminProfile__c){
    		allow=true;
    	}else if(userInfo.getUserName()== acc.Global_Account_Manager__r.Name){
    		allow=true;
    	}
    	return allow;
    }    
	
	//sharing account with GAP Team member
	public static void insertIntoAccountShare(List<GAP_Team__c> newTrigger){
		List<AccountShare> newAccountShares = new List<AccountShare>();
    	AccountShare thisAccountShare;	   			 	
		for(GAP_Team__c gapTeam : newTrigger){		
			thisAccountShare = new AccountShare();
			thisAccountShare.userorgroupid = gapTeam.Team_Member__c;		
			thisAccountShare.accountid = gapTeam.Account_Name__c;		
			thisAccountShare.accountaccesslevel = 'All';		
			thisAccountShare.OpportunityAccessLevel = 'None';		
			thisAccountShare.CaseAccessLevel = 'None';		
			thisAccountShare.ContactAccessLevel = 'Read';				
			newAccountShares.add(thisAccountShare);					
		}	
		try{
			if(newAccountShares.size()>0)insert newAccountShares;
		}catch(DMLException dme){
			system.debug('Exception:'+dme.getMessage());
		}	
	}
}