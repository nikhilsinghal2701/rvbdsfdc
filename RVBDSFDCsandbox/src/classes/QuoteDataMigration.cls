/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This class is the controller class for QuoteDataMigration
Created Date : Oct 2011
*******************************************************************************************************/


global  class QuoteDataMigration implements Database.Batchable <Sobject> {
        global Final String query;
    Boolean Test;
    global QuoteDataMigration()
        {
            String ids='\''+'a0o40000000nTzM'+'\'';
            query = 'Select q.Use_for_Forecasting__c,q.createddate,q.Id, q.Uplift_Support__c, q.Uplift_Support_3__c, q.Uplift_Support_2__c, q.Uplift_Support_1__c, q.Uplift_Spares__c, q.Uplift_Services__c, q.Uplift_Prodcuts__c, q.Uplift_Demos__c, q.UpliftOthers__c, q.Special_Discount_Training__c, q.Special_Discount_Support__c, q.Special_Discount_Spare__c, q.Special_Discount_Product__c, q.Special_Discount_E__c, q.Special_Discount_Demos__c, q.Special_Discount_D__c, q.Special_Discount_C__c, q.Special_Discount_B__c, q.Special_Discount_A__c, q.Special_DiscountOthers__c, q.Discount_Training__c, q.Discount_Support__c, q.Discount_Status__c, q.Discount_Spare__c, q.Discount_Schedule_Created__c, q.Discount_Reason__c, q.Discount_Product__c, q.Discount_Demos__c, q.Discount_Code__c, q.Discount_Category__c, q.DiscountSupport_3__c, q.DiscountSupport_2__c, q.DiscountSupport_1__c, q.DiscountOthers__c From Quote__c q where q.createddate >= 2009-04-01T00:00:00.0000Z';
            test=false;
        }
        
         global QuoteDataMigration(Boolean flag)
        {
            String ids='\''+'a0o40000000nTzM'+'\'';
            query = 'Select q.Use_for_Forecasting__c,q.createddate,q.Id, q.Uplift_Support__c, q.Uplift_Support_3__c, q.Uplift_Support_2__c, q.Uplift_Support_1__c, q.Uplift_Spares__c, q.Uplift_Services__c, q.Uplift_Prodcuts__c, q.Uplift_Demos__c, q.UpliftOthers__c, q.Special_Discount_Training__c, q.Special_Discount_Support__c, q.Special_Discount_Spare__c, q.Special_Discount_Product__c, q.Special_Discount_E__c, q.Special_Discount_Demos__c, q.Special_Discount_D__c, q.Special_Discount_C__c, q.Special_Discount_B__c, q.Special_Discount_A__c, q.Special_DiscountOthers__c, q.Discount_Training__c, q.Discount_Support__c, q.Discount_Status__c, q.Discount_Spare__c, q.Discount_Schedule_Created__c, q.Discount_Reason__c, q.Discount_Product__c, q.Discount_Demos__c, q.Discount_Code__c, q.Discount_Category__c, q.DiscountSupport_3__c, q.DiscountSupport_2__c, q.DiscountSupport_1__c, q.DiscountOthers__c From Quote__c q where q.createddate >= 2009-04-01T00:00:00.0000Z order by q.createddate desc limit 1';
            test=true;
        }
        
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
    }
      
     global void execute(Database.BatchableContext BC,List<Quote__c> queryList){
       
      Map<Id,Quote__c> quoteMap = new Map<Id,Quote__c>(queryList);
        Map<String,Discount_Detail__c> existingdetailMap = new Map<String,Discount_Detail__c>();    
        List<Category_Master__C> catList ;
      List<String> catSet = new List<String>{'A','B','B-2','B-3','B-4','C','D','E','F'};
            List<Discount_Detail__c> detailList = new List<Discount_Detail__c>();  
            if(test==true)
			{
			
			catSet.add('B1');
			catSet.add('B2');
			catList = [select Id,Name from Category_Master__C where name in : catSet order by createddate desc ];
			}
			else 
        catList = [select Id,Name from Category_Master__C where name in : catSet];
        Map<String,Category_Master__C> catMap = new Map<String,Category_Master__C>();
        for(Category_Master__C cat : catList )
            {
                catMap.put(cat.Name,cat);
            }
        
    
        
    List<Discount_Detail__c> existingList = [Select d.Uplift__c, d.Special_Discount__c, d.Quote__c,d.Name, d.IsOpp__c, d.Id,
                                                     d.Disti_Special_Discount__c, d.Disti_Discount__c, d.Discount__c, d.Date_Approved__c, 
                                                     d.CurrencyIsoCode, d.Category_Master__c,d.Category_Master__r.Name From 
                                                     Discount_Detail__c d where Quote__C in : quoteMap.keySet() 
                                                     and Category_Master__r.Name in :catMap.keyset() ];
                            
                        
                                
        for(Discount_Detail__c d : existingList)
        {
            existingdetailMap.put(d.Category_Master__r.Name+','+d.Quote__C, d);
            //detailIdsSet.add(d.Id);
        }       
        for(Quote__c q : quoteMap.values())
        {
            Discount_Detail__c det;
            for( Category_Master__C cat : catList)
            {
                if(existingdetailMap.containsKey(cat.Name+','+q.Id))
                det =existingdetailMap.get(cat.Name+','+q.Id);
                else
                {
                    det = new Discount_Detail__c();
                    det.Quote__C = q.Id;
                    
                }
                    det.Category_Master__c = cat.Id;
                    det.isopp__c=false;
                if(cat.Name=='A')
                {
                    
                    det.Uplift__c = q.Uplift_Prodcuts__c;
                    det.Discount__c = q.Discount_Product__c;
                    det.Special_Discount__c=q.Special_Discount_A__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B')
                {
                    
                    det.Uplift__c = q.Uplift_Support__c;
                    det.Discount__c = q.Discount_Support__c;
                    det.Special_Discount__c=q.Special_Discount_B__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B-2')
                {
                    
                    det.Uplift__c = q.Uplift_Support_1__c;
                    det.Discount__c = q.DiscountSupport_1__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B-3')
                {
                    
                    det.Uplift__c = q.Uplift_Support_2__c;
                    det.Discount__c = q.DiscountSupport_2__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='B-4')
                {
                    
                    det.Uplift__c = q.Uplift_Support_3__c;
                    det.Discount__c = q.DiscountSupport_3__c;
                    detailList.add(det);
                    
                }
                
                
                else if(cat.Name=='C')
                {
                    det.Uplift__c = q.Uplift_Demos__c;
                    det.Discount__c = q.Discount_Demos__c;
                    det.Special_Discount__c=q.Special_Discount_C__c;
                    detailList.add(det);
                    
                }
                
                 else if(cat.Name=='D')
                {
                    det.Uplift__c = q.Uplift_Spares__c;
                    det.Discount__c = q.Discount_Spare__c;
                    det.Special_Discount__c=q.Special_Discount_D__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='E')
                {
                
                    det.Uplift__c = q.Uplift_Services__c;
                    det.Discount__c = q.Discount_Training__c;
                    det.Special_Discount__c=q.Special_Discount_E__c;
                    detailList.add(det);
                    
                }
                
                else if(cat.Name=='F')
                {
                
                    det.Uplift__c = q.UpliftOthers__c;
                    det.Discount__c = q.DiscountOthers__c;
                    det.Special_Discount__c=q.Special_DiscountOthers__c;
                    detailList.add(det);
                    
                }
                
                
                
                
            }
              
            
                        
        }
        
        upsert(detailList);
        
        DiscountManagementHelper.insertMarginSummaryRecords(quoteMap,true); 
        
        System.debug('Data updated Successfully..'+detailList+'--'+detailList.size());  
   }

   global void finish(Database.BatchableContext BC){

   }
    
    


}