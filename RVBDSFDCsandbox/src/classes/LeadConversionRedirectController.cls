/*-----------------------------------------------------------------------------
* File Name      : LeadConversionRedirectController.cls
* Description    : Controller to redirect user to differnt Visualforce Page based on the owner on the Lead 
                   at the time of converting the Lead.
* @author        : Riverbed
* Modification Log
===============================================================================
* Ver    Date        Author                 Modification
-------------------------------------------------------------------------------
* 1.0    01/01/13    Raj Kumar
---------------------------------------------------------------------------- */
public with sharing class LeadConversionRedirectController {
    
    private Id id;
    public String message{get;set;}
    public Lead tempLead;
    public boolean show {get;set;}
    
  public LeadConversionRedirectController(ApexPages.StandardController stdController){}
  
  public PageReference onLoad(){ 
        id = ApexPages.currentPage().getParameters().get('id');
        tempLead = [select ownerid,ByPassValidationsForConversions__c from Lead where Id =: id];
        tempLead.ByPassValidationsForConversions__c=True;
        update tempLead;
        if(String.valueOf(tempLead.ownerId).startsWith('00G')){
            message=' Cannot Convert the Lead with Owner as a Queue, Please go back to the record and change Owner to User.';
            
            return null;
        }else {
            User usr = [Select id, isActive from User where Id=:tempLead.OwnerId];  
            if(usr.isActive == false)
            {
            message = 'The Lead owner is not Active. Please go back to the record and select appropriate Owner.';
            show=true;
            return null;
        	}
        }
        
            PageReference detailPage = new PageReference('/apex/RVBD_LeadConversion?id='+id);
            detailPage.setRedirect(true);
            return detailPage;
    }
    
     public PageReference cancel(){       
        PageReference Page = new PageReference('/'+id);
        system.debug('page reference::::::::::::::::::::::::::'+Page);
        Page.setRedirect(true);
        return Page;
    }    
}