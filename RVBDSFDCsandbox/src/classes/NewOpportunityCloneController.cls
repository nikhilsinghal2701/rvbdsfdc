public with sharing class NewOpportunityCloneController {
    //added an instance varaible for the standard controller
    private ApexPages.StandardController controller {get; set;}
     // add the instance for the variables being passed by id on the url
    private Opportunity opp {get;set;}
    public Opportunity opportunity{get;set;}
    string Id;
    public string ownerName {get;set;}
    // set the id of the record that is created -- ONLY USED BY THE TEST CLASS
    public ID newRecordId {get;set;}
 
    // initialize the controller
    public NewOpportunityCloneController(ApexPages.StandardController controller) { 
        //initialize the stanrdard controller
        //this.controller = controller;
        // load the current record
        opp = (Opportunity)controller.getRecord();
        
        Id = ApexPages.currentPage().getParameters().get('id');
        this.opportunity=(Opportunity)controller.getRecord();
        //system.debug('New Opportunity::'+opportunity);
        if(Id!=null){
            opportunity=[Select Id,AccountId,Account.Name,Bake_Off__c,Cascade_Rep__c,Competitors__c,Deal_Initiator__c,             
            Description,RTA_partner_influence__c,Federal_deal__c,GAM_Email__c,Federal__c,Influence_Partner__c,
            Key_Applications_by_name__c,LeadSource,Mid_Market_Account__c,
            Recommendations_to_Complete_Pilot__c,Opportunity_Disposition__c,Opportunity_Disposition_RD__c,Name,
            Partner_Involvement__c,Partner_Leverage__c,Reseller_Sales_Rep__c,Partner_Sales_Rep_Phone__c,
            Partner_SE_Involvement__c,Reseller_with_No_Account_Record__c,Pre_Qualified_by__c,Pre_Qualified_Date__c,
            Primary_App__c,Primary_Contact__c,Primary_Contact_Role__c,Primary_ISR__c,Segment__c,QL_Date__c,Registered_Deal__c,
            Registered_Partner__c,Registration_Period__c,Sale_Type__c,SecondaryApplication__c,Sold_to_Partner__c,Sold_to_Partner_Phone__c,
            Sold_To_Partner_User__c,Tier2__c,Tier2_Partner_Phone__c,Tier2_Partner_User__c,Tier3_Partner__c,Trial_Program__c,Type,Whitewater_Storage_Provider__c,
            Sold_to_Partner_Geo__c,Channel__c,CloseDate,StageName,Opnet_Group_Id__c,Product_Specialist_2__c,RD_RVP_Comments__c,Product_Specialist_4__c,Product_Specialist_3__c
            from Opportunity where Id=:opp.Id];//Added field Opnet_Group_Id__c by Rucha on 5/6/2012 
            //system.debug('opportunity::'+opportunity);
        }     
        ownerName=userinfo.getName();
        opportunity.name='Clone of '+opportunity.Name;
        opportunity.StageName='0 - Prospect';
        opportunity.CloseDate=system.today().addMonths(1);
        opportunity.Type='Existing';
        if (opportunity.Channel__c == 'VAR' || opportunity.Channel__c == 'Reseller' || opportunity.Channel__c == 'Distributor' || opportunity.Channel__c == 'SI/SP'){
            opportunity.Channel__c=null;
        }
        //system.debug('Opportunity:'+opp);
    }
    public PageReference newcancel() {
        PageReference opportunityPage = new PageReference('/'+opp.Id);
        opportunityPage.setRedirect(true);
        return opportunityPage;
    }
    
    // method called from the VF's action attribute to clone the opp save
    public PageReference cloneWithoutProducts() { 
        Opportunity newOpp; 
        newOpp = opportunity.clone(false);
        insert newOpp;
        // set the id of the new Opp created for testing
        newRecordId = newOpp.id; 
        return new PageReference('/'+newOpp.id);
    }
}