global class BatchAccountProductSubscriptions implements Database.Batchable<sobject>, Database.Stateful
{
    public String query; 
    global String error;
    global database.queryLocator start(Database.BatchableContext BC) 
    {       String query = 'select AccountId,Account.Product_Subscriptions__c, Id,oracle_product_family__c, RVBD_Product_Family__c FROM Asset where oracle_product_family__c!= \'\'';
            return database.getQueryLocator(query);  
    }  
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {  
        List<asset> assetList =(List<Asset>)scope;
        set<id>acIDs=new set<Id>();
        Map<Id,Set<String>> actPsubs = new Map<Id,Set<String>>();
        List<Account> updateAccountList = new List<Account>();
        Map<Id,Account> updateAccountMap = new Map<Id,Account>();
        System.Debug('assetList ****'+assetList); 
            for(Asset a :assetList){ 
                String prodstring='';
                set<string>unique=new set<String>();  
                for(String s:a.oracle_product_family__c.split(':')){
                unique.add(s);  
                }
                if(a.Account.Product_Subscriptions__c!=null && a.Account.Product_Subscriptions__c!=''){
                    System.Debug('at#1 ****');
                    for(String Key:a.Account.Product_Subscriptions__c.split(';')){
                            unique.add(key);
                            System.Debug('at#2 ****');
                        }  
                    } 
                if(unique.size()!=0){
                        for(String st : unique ){  
                            prodstring = prodstring +';'+ st;
                        }
                    }
                System.Debug('prodstring****'+prodstring);
                updateAccountMap.put(a.AccountId,new Account(Id = a.AccountId, Product_Subscriptions__c = prodstring));
                }
                System.Debug('updateAccountMap ****'+updateAccountMap);
                if(updateAccountMap.size() != 0){ 
                Database.SaveResult[] srList = Database.update(updateAccountMap.values(), false);
                        for(Database.SaveResult sr : srList)
                        {
                            for(Database.Error e : sr.getErrors())
                            error += '\n ' + e.getMessage();
                        }
                    }
          
       }
    
    global void finish(Database.BatchableContext BC){
    	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { BatchFinishEmailUtil.finishEmail(BC)});
   }   
}