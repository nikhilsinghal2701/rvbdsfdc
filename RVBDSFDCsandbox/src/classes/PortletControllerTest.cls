@isTest
private class PortletControllerTest {

    static testMethod void myUnitTest() {
        //Test.setCurrentPageReference(new PageReference('Page.headerPortlet')); 
	    System.currentPageReference().getParameters().put('tabName', 'Home');
	    PortletController c = new PortletController();
	    c.getLeftNavs();
	    c.selectLeftNav();
	    c.getRightNavs();
	    c.getHeadlines();

	    System.currentPageReference().getParameters().put('tabName', 'UnsupportedTab');
	    c = new PortletController();
	    c.getLeftNavs();
	    c.selectLeftNav();
	    c.getRightNavs();
	    c.getHeadlines();
    }
}