public class extSendToPartner_E2CP{

    //fields
    private final Case myCase;
    
    //constructor
    public extSendToPartner_E2CP(ApexPages.StandardController stdController) {
        this.myCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateField() {
        //get case
        Case c = [select Id,Status,ContactId,Contact.Email from Case where Id = :myCase.id limit 1];

        //update case
        c.Status = 'Working with Technology Partner';
        update c;

        //Add case team member if not already there
        //Id mId = '003S000000FBslh';//Id for contact record
        //Id rId = '0B7S0000000CaTr';//Id for case team role
        Contact[] con = [select id from contact where email = 'ProCurve_ONE_Call_Transfer@lists.hp.com' limit 1];
        CaseTeamRole[] ctr = [select id from caseteamrole where name = 'Technology Partner' limit 1];
       
        if(con.size() >0 && ctr.size() > 0){
            CaseTeamMember[] ctmTest = [select ParentId, MemberId, TeamRoleId from CaseTeamMember where ParentId = :c.Id and MemberId = :con[0].id and TeamRoleId = :ctr[0].id];
            if(ctmTest.size()<1){//Throws an error if the case team member already exists, so don't add it if it exists
                CaseTeamMember ctm = new CaseTeamMember(ParentId = c.Id, MemberId = con[0].id, TeamRoleId = ctr[0].id);
                insert ctm;
            }
        }
       
        PageReference pageRef = Page.E2CP__New_Comment;
        pageRef.getParameters().put('id', c.Id);
        pageRef.setRedirect(true);
        return pageRef;
       
    }
    
    static testMethod void extSendToPartner_E2CPTest() {
        
        //create objects
        CaseTeamRole ctr = new CaseTeamRole(name='Technology Partner');
        
        Contact con = [Select id, email from contact where email != null limit 1];
        con.email = 'ProCurve_ONE_Call_Transfer@lists.hp.com';
        update con;
        
        Case cs = new Case(Subject = 'Test', ContactId = con.id);
        insert cs;
        
        //instantiate class and run method
        extSendToPartner_E2CP eSTPe2c = new extSendToPartner_E2CP(new ApexPages.StandardController(cs));
        PageReference pg = eSTPe2c.updateField();
        
        //check results
        cs = [Select Id, CaseNumber, Contact.Email, Status From Case where id =: cs.id][0];
        CaseTeamMember[] ctmTest = [select ParentId, MemberId, TeamRoleId from CaseTeamMember where ParentId = :cs.Id];
        
        System.assert(cs.status == 'Working with Technology Partner');
        System.assert(ctmTest.size() >= 1);
        System.assert(pg.getParameters().get('id') == string.valueOf(cs.id));
       
    }
    
}