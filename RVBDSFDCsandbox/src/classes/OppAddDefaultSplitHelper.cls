public without sharing class OppAddDefaultSplitHelper {
    
    @future 
    public static void AddSplits(List<Id> oppIds){
        List<Split_Commissions__c> newSplits = new List<Split_Commissions__c>();
        for(Opportunity theOpp:[Select ID, OwnerId, CurrencyIsoCode from Opportunity where ID in :oppIds]){
            //System.Debug('OppOwner:::::::::: ' + theOpp.OwnerId);
            Split_Commissions__c newSplit = new Split_Commissions__c (
                Split_Comm_Rep__c = theOpp.OwnerId,
                Is_Opportunity_Owner__c = true,
                Commission_Percentage__c = 100,
                CurrencyIsoCode = theOpp.CurrencyIsoCode,
                Opportunity__c = theOpp.Id
            );
            newSplits.add(newSplit);
        }
        try{        
            //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg    // uninstalling with 'Jigsaw Advanced Data Management (Managed)'
            insert newSplits;
        }catch(DMLException e){
            System.debug('Exception in adding splits:'+e);
        }
    }
    public static void addGAMtoSalesTeam(List<Id> oppIds){
    //add GAM to Add To SalesTeam / Visible_to_Partners__c
        List<Visible_to_Partners__c> vpList = new List<Visible_to_Partners__c>();
        for(Opportunity opp:[Select Id, Account.Global_Account_Manager__c, Account.Global_Account_Manager__r.IsActive from Opportunity where ID in :oppIds]){
            if(opp.Account.Global_Account_Manager__c != null){
                if(opp.Account.Global_Account_Manager__r.IsActive){
                    Visible_to_Partners__c vp =  new Visible_to_Partners__c(
                        Role__c='Global Account Manager',
                        PartnerName__c=opp.Account.Global_Account_Manager__c ,
                        Opportunity_Name__c=opp.id,
                        Opportunity_Access__c='Edit');
                    vpList.add(vp);
                }
            }
        }
        try{
            insert vpList;
        }catch(DMLException e){
            System.debug('Exception in adding GAM to Opp Sales Team');
        }
   }
    
    @future
    public static void UpdateSplits(Map<Id,Id> oppOwnerMap){
        //system.debug('oppOwnerMap:'+oppOwnerMap);
        //System.Debug('Doing UpdateSplits');
        Double commission =0.00;
        List<Split_Commissions__c> newSplits = new List<Split_Commissions__c>();
        List<Split_Commissions__c> deleteSplits = new List<Split_Commissions__c>();
        //Added by Prashant on 12/03/2012;ticket#114219
         Map<Id,Split_Commissions__c> oldSplitMap = new Map<Id,Split_Commissions__c>();
         Map<Id,List<Split_Commissions__c>> splitsOnOpptyMap=new Map<Id,List<Split_Commissions__c>>();
         //system.debug('splitsOnOpptyMap:'+splitsOnOpptyMap);
         List<Split_Commissions__c> splitsOnOpptyList;
         Split_Commissions__c newSplit, newSplit1;
         List<Split_Commissions__c> oldSplitList = [Select Id, Split_Comm_Rep__c,Is_Opportunity_Owner__c, GAP_Split__c,
                    GAP_Additional__c,Commission_Percentage__c,CurrencyIsoCode, Opportunity__c from 
                    Split_Commissions__c where Opportunity__c in :oppOwnerMap.Keyset()];
                    //system.debug('oldSplitList:'+oldSplitList);
        if(oldSplitList!=Null && oldSplitList.size()>0){
          for(Split_Commissions__c oldSplit:oldSplitList){
            //system.debug('XXXXXXXX:'+splitsOnOpptyMap.containsKey(oldSplit.Opportunity__c));
            if(splitsOnOpptyMap.containsKey(oldSplit.Opportunity__c)){
                
                        splitsOnOpptyMap.get(oldSplit.Opportunity__c).add(oldSplit);
                        //system.debug('77777777:'+splitsOnOpptyMap.get(oldSplit.Opportunity__c));
                    }else{
                        splitsOnOpptyList=new List<Split_Commissions__c>();
                        splitsOnOpptyList.add(oldSplit);
                        splitsOnOpptyMap.put(oldSplit.Opportunity__c,splitsOnOpptyList);
                    }
          }
          //system.debug('splitsOnOpptyMap:'+splitsOnOpptyMap);
          if(splitsOnOpptyMap.size()>0){  
            for(Id id:oppOwnerMap.Keyset()){
              if(splitsOnOpptyMap.containsKey(id)){
                for(Split_Commissions__c temp:splitsOnOpptyMap.get(id)){
                    if(temp.Is_Opportunity_Owner__c){
                        List<Split_Commissions__c> tempObj=checkDuplicateCommissionRep(oppOwnerMap.get(id),splitsOnOpptyMap.get(id));
                        if(tempObj !=null && tempObj.size()>0){
                            newSplit = temp.clone(false,true);
                            commission=temp.Commission_Percentage__c;
                            for(Split_Commissions__c dupeSplit: tempObj){
                                commission+=dupeSplit.Commission_Percentage__c;
                                deleteSplits.add(dupeSplit);
                            }
                            //system.debug('newSplit Cloned:'+newSplit);
                            newSplit.Is_Opportunity_Owner__c=true;
                            newSplit.Split_Comm_Rep__c = oppOwnerMap.get(id);
                            newSplit.Commission_Percentage__c=commission;//temp.Commission_Percentage__c+tempobj.Commission_Percentage__c;
                            //system.debug('tempObj Duplicate:'+tempObj);
                            newSplits.add(newSplit);
                            //deleteSplits.add(tempObj);
                            deleteSplits.add(temp);
                        }else{
                            newSplit = temp.clone(false,true);
                            //system.debug('newSplit Cloned:'+newSplit);
                            newSplit.Is_Opportunity_Owner__c=true;
                            newSplit.Split_Comm_Rep__c = oppOwnerMap.get(id);
                            newSplits.add(newSplit);
                            deleteSplits.add(temp); 
                        }
                    }
                }
              }
            }  
          }
    }      
    for(Id id:oppOwnerMap.Keyset()){
      if(!splitsOnOpptyMap.containsKey(id)){
        newSplit = new Split_Commissions__c (
                Split_Comm_Rep__c = oppOwnerMap.get(id),
                Is_Opportunity_Owner__c = true,
                Commission_Percentage__c = 100,
                Opportunity__c = Id
              );
              newSplits.add(newSplit);
      }
      //system.debug('newSplits:'+newSplits);
      //system.debug('deleteSplits:'+deleteSplits);
    }
    try{    
      //jsADM.jsADM_ContextProcessController.isInFutureContext = true;//added by Ankita 7/18/2011 for Jigsaw ADM pkg    // uninstalling with 'Jigsaw Advanced Data Management (Managed)'
      if(deleteSplits.size() > 0) delete deleteSplits;
      if(newSplits.size()>0) insert newSplits;
    }catch(DMLException e){
        system.debug('split commission added:'+e);    
    }catch(Exception ex){
        system.debug('split commission added:'+ex);
    } 
    }
    //Added by Prashant on 12/03/2012;ticket#114219
    private static List<Split_Commissions__c> checkDuplicateCommissionRep(Id id, List<Split_Commissions__c> opptySplitList){
        List<Split_Commissions__c> dupeList=new List<Split_Commissions__c>();
      for(Split_Commissions__c temp:opptySplitList){
        if(id==temp.Split_Comm_Rep__c){
            dupeList.add(temp);
          //return temp;
          //break;
        }
      }
      If(dupeList.size()>0)return dupeList;
      else
      return null;
    }
}