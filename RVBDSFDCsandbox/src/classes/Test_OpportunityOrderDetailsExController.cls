@isTest
private class Test_OpportunityOrderDetailsExController {

    static testMethod void myTest_OpportunityOrderDetailsExController() {
      Account acc = new Account();
      acc.Name = 'Test';
      acc.Type='VAR';
      acc.Industry='Education';
      acc.Geographic_Coverage__c='Americas';
      acc.Region__c='Latin America';
      acc.BillingCountry='Argentina';
      insert acc ;
      
      Contact con = new Contact();
      con.LastName = 'Test Contact';
      con.Birthdate = Date.today();
      insert con;
      
      Contact con1 = new Contact();
      con1.LastName = 'Test Contact1';
      con1.Birthdate = Date.today();
      insert con1;
      
      Opportunity opty = new Opportunity();
      opty.name = 'testopp';
      opty.accountId = acc.Id;
      opty.Primary_ISR__c = UserInfo.getUserId();
      opty.Type = 'New';
      opty.Channel__c = 'Direct';
      opty.LeadSource = 'Email';
      opty.CloseDate = System.today()+5;
      opty.StageName = '0-prospect';
      opty.Forecast_Category__c = 'Commit';
      opty.Competitors__c = 'Citrix'; 
      insert opty;
      
      Discount_Detail__c dscDtail=new Discount_Detail__c();
      dscDtail.Discount__c=40;
      dscDtail.Customer_Discount__c=60;
      //dscDtail.Date_Approved__c=
      dscDtail.Category_Master__c='a3V40000000CaT7';
      dscDtail.IsOpp__c=true;
      dscDtail.Opportunity__c=opty.id;
      dscDtail.Special_Discount__c=10;
      dscDtail.Var_Discount__c=20;
      dscDtail.Uplift__c=5;
      insert dscDtail;
      
      PageReference pageRef1 = Page.OpportunityOrderDetailsEx;
      Test.setCurrentPage(pageRef1);
      Apexpages.currentPage().getParameters().put('id',opty.Id);
     
      ApexPages.StandardController std1 = new ApexPages.StandardController(opty);
      OpportunityOrderDetailsExController controller1 = new OpportunityOrderDetailsExController(std1);
      controller1.newSave(); 
    }
}