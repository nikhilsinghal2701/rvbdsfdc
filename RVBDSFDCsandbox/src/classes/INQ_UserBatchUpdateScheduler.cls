global class INQ_UserBatchUpdateScheduler implements Schedulable{
	global Boolean isTesting;
	
	global INQ_UserBatchUpdateScheduler() {
		isTesting=false;
	}
	
	global INQ_UserBatchUpdateScheduler(Boolean testMode) {
		this();
		isTesting = testMode;
	}
	
	global void execute(SchedulableContext sc) {
		INQ_UserBatchUpdate b = isTesting ? new INQ_UserBatchUpdate(175,isTesting) : new INQ_UserBatchUpdate();
		Id myBatchJobID = database.executeBatch(b);
	}
}