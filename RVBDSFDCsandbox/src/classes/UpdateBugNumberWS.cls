/*
*Description:Populate Bug # field based on solutions.
*Tracking the bug numbers in the Bug # field, and manualy populating the Bug # field.
*When bug is attached to the case after case is closed. 
*Created By:Prashant.Singh@riverbed.com
*Created Date:Sep 30,2010
*/
global class UpdateBugNumberWS {
	webService static String updateBugNumber(String cId) {
		Map<ID,List<ID>> csMap = new Map<ID,List<ID>>();
		Map<ID,Solution> solMap;
		set<ID> caseId=new set<ID>();
		set<ID> solutionId=new set<ID>();
		List<ID> solId;
		String delimiter=',';
		List<CaseSolution> csList;
		List<Solution> solList;
		Case tempCase = [select Id,Bug_No__c from Case where Id =:cId];
		if(tempCase!=NULL){
			csList=[select Id,CaseId,SolutionId from CaseSolution where CaseId =:cId];
			for(CaseSolution tempCS:csList){
				solutionId.add(tempCS.SolutionId);
				if(csMap.containsKey(tempCS.CaseId)){
					csMap.get(tempCS.CaseId).add(tempCS.SolutionId);		
				}else{
					solId=new List<ID>();
					solId.add(tempCS.SolutionId);
					csMap.put(tempCS.CaseId,solId);
				}
			}
			solMap = new Map<ID,Solution>([select Id,Bug_Number__c from Solution where ID IN:solutionId]);
			if(solMap.size()>0 && solMap!=NULL){
				if(csMap.containsKey(cId)){
					List<ID> tempSolId=csMap.get(cId);
					tempCase.Bug_No__c=null;
					for(ID tempId:tempSolId){
						if(solMap.containsKey(tempId)&& solMap.get(tempId).Bug_Number__c!=null){
							if(tempCase.Bug_No__c==null){
								tempCase.Bug_No__c=solMap.get(tempId).Bug_Number__c;
							}else{										
								tempCase.Bug_No__c=tempCase.Bug_No__c+delimiter+solMap.get(tempId).Bug_Number__c;
							}
						}						
					}
				}
			}
		}
		try{
			update tempCase;
			return 'success';
		}catch(Exception e){
			return e.getMessage();
		}
	}
}