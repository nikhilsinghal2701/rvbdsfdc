@isTest
private class TestAccountHeirarchy{
	
	static testMethod void testBatchClass()	{
		AccountHierarchy  bc = new AccountHierarchy ();
		bc.query = 'SELECT Id, OneSource__OSKeyID__c, OS_Parent_Key_ID__c, OS_Ultimate_Parent_Key_ID__c From Account where OS_Parent_Key_ID__c != null and OS_Ultimate_Parent_Key_ID__c != null Limit 200';
		Test.startTest();
		Database.executeBatch(bc, 200);
		Test.stopTest();
				
	}
	public static testMethod void testschedule() {


		test.starttest();
		AccountHierarchyScheduler sco = new AccountHierarchyScheduler();
		String sch = '0 0 23 * * ?';
		system.schedule('AccountHierarchy', sch, sco);
		test.stopTest();
}
	
	}