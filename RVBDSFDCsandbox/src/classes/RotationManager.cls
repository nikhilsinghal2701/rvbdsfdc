/*
* Developed by Luminosity-Group Inc, Authored by Richard Summerhayes  / www.luminosity-group.com richard@luminosity-group.com
*/
public with sharing class RotationManager {

    public String BannerSetName {get; set;}
   
    public List<Banners__c> getBanners() {
        List<Banners__c> bannerItems = [
                                    SELECT 
                                    Banner__r.Name,
                                    Banner__r.Image__c,
                                    Banner__r.Intro_Text__c,
                                    Banner__r.Headline_Text__c, 
                                    Banner__r.Supporting_Text__c,
                                    Banner__r.Text_Position__c,
                                    Banner__r.Link__c,                                    
                                    Banner__r.Text_Items_OFF__c,
                                    Banner__r.Text_Background_Box_OFF__c,
                                    Banner__r.Banner_Type__c,
                                    Banner__r.Text_Box_Width__c,
                                    Banner__r.Button_Text__c,
                                    Banner__r.Top__c,
                                    Banner__r.Left__c,
                                    Banner__r.Bottom_Value__c,
                                    Banner__r.Link_One_Text__c,
                                    Banner__r.Link_One_URL__c,
                                    Banner__r.Link_Two_Text__c,
                                    Banner__r.Link_Two_URL__c,
                                    Banner__r.Link_Three_Text__c,
                                    Banner__r.Link_Three_URL__c
                                    FROM Banners__c
                                    Where Parent_Rotator__r.Rotator_Name__c = :BannerSetName and Active__c = true 
                                    Order By Publish_Date__c
                                    LIMIT 15  ];
              return bannerItems;
    }//end of getBanners
    
    
    
    public List<Banners__c> getBannersPreview() {
        List<Banners__c> bannerItems = [
                                    SELECT
                                    Banner__r.Name,
                                    Banner__r.Image__c,
                                    Banner__r.Intro_Text__c,
                                    Banner__r.Headline_Text__c, 
                                    Banner__r.Supporting_Text__c,
                                    Banner__r.Text_Position__c,
                                    Banner__r.Link__c,                                    
                                    Banner__r.Text_Items_OFF__c,
                                    Banner__r.Text_Background_Box_OFF__c,
                                    Banner__r.Banner_Type__c,
                                    Banner__r.Text_Box_Width__c,
                                    Banner__r.Button_Text__c,
                                    Banner__r.Top__c,
                                    Banner__r.Left__c,
                                    Banner__r.Bottom_Value__c,
                                    Banner__r.Link_One_Text__c,
                                    Banner__r.Link_One_URL__c,
                                    Banner__r.Link_Two_Text__c,
                                    Banner__r.Link_Two_URL__c,
                                    Banner__r.Link_Three_Text__c,
                                    Banner__r.Link_Three_URL__c 
                                    FROM Banners__c
                                    Where Parent_Rotator__r.id = :ApexPages.currentPage().getParameters().get('id') and Active__c = true 
                                    Order By Publish_Date__c
                                    LIMIT 15  ];
              return bannerItems;
    }//end of getBannersPreview 
    
    static testMethod void testRotationManager() {
           RotationManager controller  = new RotationManager();
           
           
           Date myDate = date.newinstance(1960, 2, 17);
           Rotation_Manager__c testRotator = new Rotation_Manager__c(Rotator_Name__c='testBannerRotator');
           insert testRotator;
           
           Banner__c testBanner = new Banner__c(name='testBannerName', Image__c='<img src="testImage"/>', Intro_Text__c='Hottest Spot', Headline_Text__c='Entertain', Supporting_Text__c='Sub text', Link__c= 'TestLink', Text_Position__c='Right', Banner_Type__c = 'default', Text_Items_OFF__c = false, Text_Background_Box_OFF__c = false);
           insert testBanner;
           
           Banners__c testBannerJunction = new Banners__c( Parent_Rotator__c = testRotator.id, Banner__c = testBanner.id, Active__c = true );
           testBannerJunction.Publish_Date__c = myDate;
           insert testBannerJunction;
           
           controller.BannerSetName='testBannerRotator';

                                    
           List<Banners__c> bannerItems = [SELECT Banner__r.Name, Banner__r.Image__c, Banner__r.Intro_Text__c, Banner__r.Headline_Text__c, Banner__r.Supporting_Text__c, Banner__r.Text_Position__c, Banner__r.Link__c, Banner__r.Banner_Type__c, Banner__r.Text_Items_OFF__c, Banner__r.Text_Background_Box_OFF__c,Banner__r.Link_One_Text__c,
                                    Banner__r.Link_One_URL__c,
                                    Banner__r.Link_Two_Text__c,
                                    Banner__r.Link_Two_URL__c,
                                    Banner__r.Link_Three_Text__c,
                                    Banner__r.Link_Three_URL__c
                                    FROM Banners__c
                                    Where Parent_Rotator__r.Rotator_Name__c = 'testBannerRotator' and Active__c = true 
                                    Order By Publish_Date__c
                                    LIMIT 15  ];
                                 
           /* System.assert(controller.getBanners() == bannerItems); */
           controller.getBanners();
           
           
           List<Banners__c> bannerItemsPreview = [SELECT Banner__r.Name, Banner__r.Image__c, Banner__r.Intro_Text__c, Banner__r.Headline_Text__c, Banner__r.Supporting_Text__c, Banner__r.Text_Position__c, Banner__r.Banner_Type__c, Banner__r.Text_Items_OFF__c, Banner__r.Text_Background_Box_OFF__c,Banner__r.Link_One_Text__c,
                                    Banner__r.Link_One_URL__c,
                                    Banner__r.Link_Two_Text__c,
                                    Banner__r.Link_Two_URL__c,
                                    Banner__r.Link_Three_Text__c,
                                    Banner__r.Link_Three_URL__c
                                    FROM Banners__c
                                    Where Parent_Rotator__r.id = :testRotator.id and Active__c = true 
                                    Order By Publish_Date__c
                                    LIMIT 15  ];
           
           /* System.assert(controller.getBannersPreview() != bannerItemsPreview); */
           controller.getBannersPreview();
           
            
    }//end of test method
    
}//end of class