/*******************************************************************************************
// Class Name: TerritoryPlanShareHelper
//
// Description: This class is the trigger handler for TerritoryPlanteamtrigger.
// This class add or delete Territory Plan Shares based on Territory Team Member under Territory plan
//
// Created By: Nikhil Singhal (Perficient) - 7/10/2014
*******************************************************************************************/





public with sharing class TerritoryPlanShareHelper {
  public static void assignTerritoryPlanShare(List<Territory_Team__c> TerritoryPlanteamlist,Boolean isupdate,Boolean Isdelete) {
    Set<Id> orgTerPlanteamIdSet = new Set<Id> ();
    Set<Id> orgTerPlanIdSet = new Set<Id> ();
    Set<Id> orgTerPlanuserIdSet = new Set<Id> ();
    if(!TerritoryPlanteamlist.isEmpty() && !isupdate && !Isdelete){//After Insert
    for(Territory_Team__c tpt: TerritoryPlanteamlist) {
      orgTerPlanteamIdSet.add(tpt.id);
      }
    insertTerritoryPlanShare(orgTerPlanteamIdSet);
    } else if(isupdate && !TerritoryPlanteamlist.isEmpty() && !Isdelete){//After Update
      for(Territory_Team__c tpt: TerritoryPlanteamlist) {
      orgTerPlanteamIdSet.add(tpt.id);
      orgTerPlanIdSet.add(tpt.Territory_Plan__c);
      orgTerPlanuserIdSet.add(tpt.user__c);
      }
    delteTerritoryPlanShare(orgTerPlanteamIdSet,orgTerPlanIdSet,orgTerPlanuserIdSet,true);  
    } else if(!isupdate && !TerritoryPlanteamlist.isEmpty() && Isdelete){//After Delete
      for(Territory_Team__c tpt: TerritoryPlanteamlist) {
      orgTerPlanteamIdSet.add(tpt.id);
      orgTerPlanIdSet.add(tpt.Territory_Plan__c);
      orgTerPlanuserIdSet.add(tpt.user__c);
      }
    delteTerritoryPlanShare(orgTerPlanteamIdSet,orgTerPlanIdSet,orgTerPlanuserIdSet,false);  
    }
    
  }
  
  //Add Share
  public static void insertTerritoryPlanShare(Set<Id> orgTerPlanteamIdSet) {
    Map<id,Territory_Team__c> TpTMap = new Map<id,Territory_Team__c>([Select Id,Access__c,Territory_Plan__c,Role__c,User__c from Territory_Team__c where id=:orgTerPlanteamIdSet]);
    List<Territory_Plan__Share> TptShrs = new List<Territory_Plan__Share>();
    for(id aptid :TpTMap .keyset()){
      Territory_Plan__Share TPshare = new Territory_Plan__Share();
      TPshare.UserOrGroupId=TpTMap.get(aptid).User__c;
      TPshare.ParentId=TpTMap.get(aptid).Territory_Plan__c;
      if(TpTMap.get(aptid).Access__c=='Read/Edit'){
      TPshare.AccessLevel ='Edit';
      } else {
      TPshare.AccessLevel ='Read';  
      }
      TptShrs .add(TPshare);
    }
    if(!TptShrs .isEmpty()){
    insert TptShrs ;
    }
  }
  
  //Delete Share
  public static void delteTerritoryPlanShare(Set<Id> orgTerPlanteamIdSet,Set<Id> orgTerPlanIdSet,Set<Id> orgTerPlanuserIdSet,Boolean doinsert) {
              
    Map<id,Territory_Plan__Share> TpTshareMap = new Map<id,Territory_Plan__Share>([Select Id,ParentId from Territory_Plan__Share where ParentId in :orgTerPlanIdSet and UserOrGroupId in :orgTerPlanuserIdSet and RowCause = :Schema.Account_Plan__Share.RowCause.Manual]);
    if(!TpTshareMap .isEmpty()){
    delete TpTshareMap .values();
    }
    if(doinsert){
    insertTerritoryPlanShare(orgTerPlanteamIdSet);
    }
  }

}