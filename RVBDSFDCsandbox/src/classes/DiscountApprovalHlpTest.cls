/* Rev:1 Added by Anil Madithati NPI 4.0 11.14.2014 */
@isTest
private class DiscountApprovalHlpTest {

    static testMethod void testValidation() {
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        System.assert(q.Discount_Status__c == 'Required');
        Quote__c oldq = q.clone();
        q.Discount_Status__c = 'Pending Approval';
        DiscountApprovalHlp.validateSubmittedQuotes(new List <Quote__c> {q}, new Map <Id, Quote__c> {oldq.Id => oldq});
    }

    static testMethod void testSharing() {
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        System.assert(q.Discount_Status__c == 'Required');
        
        Quote__c oldq = q.clone();
        q.Share_To_Partner__c = true;
        
        DiscountApprovalHlp.shareToPartner(new Map <Id, Quote__c> {q.Id => q}, new Map <Id, Quote__c> {oldq.Id => oldq});
        
        q.Share_To_Partner__c = false;
        oldq.Share_To_Partner__c = true;
        
        DiscountApprovalHlp.shareToPartner(new Map <Id, Quote__c> {q.Id => q}, new Map <Id, Quote__c> {oldq.Id => oldq});   
    }
   

    static testMethod void testHistory() {
        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        System.assert(q.Discount_Status__c == 'Required');
        //DiscountApprovalTestHlp.submitQuote(q); 
    } 
     static  testMethod  void testEmailServiceApproves() {    
        DiscountApproval.isTest = true;

        Quote__c q = DiscountApprovalTestHlp.createQuote(null);
        //System.assert(false, 'T: ' + [SELECT User__r.Name, Level__c FROM Approvers_Routing_Matrix__c WHERE Quote__c = :q.Id]);
        System.assertEquals('Required', q.Discount_Status__c);
         
       // DiscountApprovalTestHlp.submitQuote(q);
        
        User app = getApprover(q);
        
         System.debug(LoggingLevel.INFO,'cc------' +q.Approver2__C +'1--'+q.Approver3__C+'2--'+q.Approver1__C);
        DiscountApprovalEmail esvc = new DiscountApprovalEmail();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        DiscountApprovalEmail.isTest=true;
        email.plainTextBody = 'approve my comments\n' + DiscountApprovalEmail.QUOTE_DELIM + q.Id;
        email.fromAddress = app.Email;
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        //esvc.handleInboundEmail(email, envelope);
    }  
    
     public static User getApprover(Quote__c q){
        return [SELECT Email FROM User WHERE Id = :q.Approver1__c ];
    } 
}