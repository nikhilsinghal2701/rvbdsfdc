public class SendEmailNotifications {
	
	//SB
	//public static final Id EMAIL_TEMPLATE = '00XS0000000DiSQ';
	
	//PROD
	public static final Id EMAIL_TEMPLATE = '00X70000001BOXe';
	
	public static void sendEmails(List<Id> contacts) {
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		for (Id contactId : contacts) {
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setTargetObjectId(contactId);
			mail.setTemplateId(EMAIL_TEMPLATE);
			mails.add(mail);
		}
		Messaging.sendEmail(mails);
	}
}