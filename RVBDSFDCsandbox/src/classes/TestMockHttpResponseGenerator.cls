@isTest
                        
global class TestMockHttpResponseGenerator implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        //System.assertEquals('testAsset', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"appliances":null,"evalDate":"2014-09-13","disabled":false,"status":"SUCCESS","progress":null,"error":null,"action":null,"error_id":null,"error_text":null,"actionMessage":null,"debugInfo":null}');
        res.setStatusCode(200);
        Date eDate=Date.newInstance(2011, 01, 01);
        return res;
    }
}