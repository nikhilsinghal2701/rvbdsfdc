/*
    Unit test for SetCustomCaseOwnerField trigger (Case 30708)

    06/07/12 SV@IC - Created
*/
@isTest(SeeAllData=true)
public class TestSetCustomCaseOwnerField {
    static testMethod void testTrigger() {
      Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
      User u1 = new User(Alias = 'standt1', Email='testtriggeruser1@test.test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='testtriggeruser1@test.test.com');
      insert u1;
      String queueId = [SELECT QueueId FROM QueueSobject WHERE SobjectType = 'Case' LIMIT 1].QueueId;
        
        
      Case c1 = new Case(OwnerId = u1.id);
      insert c1;
       
      c1 = [SELECT Case_Owner__c FROM Case WHERE id=:c1.id];
      System.assertEquals(u1.id, c1.Case_Owner__c, 'Trigger failed to assign user to Case_Owner__c.');
        
      c1.OwnerId = queueId;
      update c1;
      c1 = [SELECT Case_Owner__c FROM Case WHERE id=:c1.id];
      System.assertEquals(null, c1.Case_Owner__c, 'Trigger failed to null Case_Owner__c on Queue change.');
        
    }

}