@isTest(SeeAllData=true)
public with sharing class Test_LeadRemoveAssociation {
    
    static testmethod void testLeadRemoveAssociation(){
        
        RecordType genLead = [select id from RecordType where name = 'General Leads' and sObjectType = 'Lead'];
        RecordType p2pLead = [select id from RecordType where name = 'Lead Passed To Partner' and sObjectType = 'Lead'];
        User eloquaUser = [select id, name from User where name = 'Eloqua Administrator'];
        Lead newLead;
        Account acc;
        Contact con;
        test.startTest();
        System.runAs(eloquaUser){
            newLead = new Lead(firstname = 'Tony', lastname = 'Montana', company = 'new company', 
                                leadsource = 'Web - Riverbed.com', recordTypeid = genLead.id );
            insert newLead;
            
        }
        acc = new Account( Name ='Test Account',Industry= 'Software');
        insert acc;
        con = new Contact(firstname = 'Steve', lastname = 'Jobs',accountId = acc.Id );
        insert con;
        Lead l = [select id, ownerid,Lead_Score__c from Lead where id = :newLead.id];
        l.recordTypeid = p2pLead.id ;
        update l;
        
        l.recordTypeid = genLead.id;
        update l;
        
        //l.AutoConvertFlag__c = true;
        //l.TestContactId__c = con.Id;
        update l;
        
        
        Test.stopTest();
    }

}