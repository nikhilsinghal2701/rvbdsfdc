@isTest
private class TestCertificationSummaryTapQualifyUpdate {

        static testMethod void testtrigger() {
    		TestDataFactory tdf = new TestDataFactory();
    		RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
    		insert rvbdEmail;
            Account a = new Account();
            a.Name = 'test account';
            a.Type ='Partner Account';
            a.Partner_Level__c='Authorized';
            a.Partner_Status1__c='Good Standing';
            insert a;
            Account ac = new Account();
            ac.Name = 'test account1';
            ac.Type ='Cloud Service Provider';
            ac.Partner_Level__c='Authorized';
            ac.Partner_Status1__c='Good Standing';
            insert ac;
            Contact cn1 = new Contact();
            cn1.AccountId = a.id;
            cn1.FirstName = 'test';
            cn1.LastName = 'tester';
            Contact cn2 = new Contact();
            cn2.AccountId = a.id;
            cn2.firstName = 'testmore';
            cn2.LastName = 'testing';
            Contact cn3 = new Contact();
            cn3.AccountId = a.id;
            cn3.firstName = 'testmore';
            cn3.LastName = 'testing';
            Contact cn4 = new Contact();
            cn4.AccountId = a.id;
            cn4.FirstName = 'test';
            cn4.LastName = 'tester';
            Contact cn5 = new Contact();
            cn5.AccountId = a.id;
            cn5.firstName = 'testmore';
            cn5.LastName = 'testing';
            Contact cn6 = new Contact();
            cn6.AccountId = a.id;
            cn6.FirstName = 'test';
            cn6.LastName = 'tester';

            list <contact> conList = new Contact[]{cn1, cn2, cn3, cn4, cn5, cn6};
            insert ConList;
            Acct_Cert_Summary__c c1 = new Acct_Cert_Summary__c();
            c1.Account__c = a.id;
            c1.TAP__c = 'Application Delivery (AD)';
            Acct_Cert_Summary__c c2 = new Acct_Cert_Summary__c();
            c2.Account__c = ac.id;
            c2.TAP__c = 'Cloud Storage Delivery (CSD)';
            list<Acct_Cert_Summary__c> csList = new Acct_Cert_Summary__c[]{c1, c2};
            insert csList;
            c1 = [Select a.TAP_short__c, a.TAP__c, a.RTSS__c, a.RTSA__c, a.RSS__c, a.RSA__c, a.RCSP__c, a.RCSA_W__c, a.RCSA_NPM__c
                                    From Acct_Cert_Summary__c a
                                    where id = :c1.id];
            c2 = [Select a.TAP_short__c, a.TAP__c, a.RTSS__c, a.RTSA__c, a.RSS__c, a.RSA__c, a.RCSP__c, a.RCSA_W__c, a.RCSA_NPM__c
                                    From Acct_Cert_Summary__c a
                                    where id = :c2.id];
        test.startTest();
            update csList;

        //first test
        Certificate__c ce1 = new Certificate__c();
                ce1.Name = 'RSS-W';
                ce1.Contact__c = cn1.id;
                ce1.Certification_Summary__c = c1.id;
                ce1.Status__c = 'Complete';
                ce1.Contact_Left_Account__c = 'no';

                Certificate__c ce2 = new Certificate__c();
                ce2.Name = 'RSS-W';
                ce2.Contact__c = cn2.id;
                ce2.Certification_Summary__c = c1.id;
                ce2.Status__c = 'Complete';
                ce2.Contact_Left_Account__c = 'no';

                Certificate__c ce3 = new Certificate__c();
                ce3.Name = 'RTSA-W';
                ce3.Contact__c = cn2.id;
                ce3.Certification_Summary__c = c1.id;
                ce3.Status__c = 'Complete';
                ce3.Contact_Left_Account__c = 'no';

                Certificate__c ce7 = new Certificate__c();
                ce7.Name = 'RTSA-NPM';
                ce7.Contact__c = cn2.id;
                ce7.Certification_Summary__c = c1.id;
                ce7.Status__c = 'Complete';
                ce7.Contact_Left_Account__c = 'no';

                Certificate__c ce8 = new Certificate__c();
                ce8.Name = 'RTSA-APM';
                ce8.Contact__c = cn1.id;
                ce8.Certification_Summary__c = c1.id;
                ce8.Status__c = 'Complete';
                ce8.Contact_Left_Account__c = 'no';

                Certificate__c ce9 = new Certificate__c();
                ce9.Name = 'RCSA-W';
                ce9.Contact__c = cn1.id;
                ce9.Certification_Summary__c = c1.id;
                ce9.Status__c = 'Complete';
                ce9.Contact_Left_Account__c = 'no';

                Certificate__c ce10 = new Certificate__c();
                ce10.Name = 'RCSP-W';
                ce10.Contact__c = cn2.id;
                ce10.Certification_Summary__c = c1.id;
                ce10.Status__c = 'Complete';
                ce10.Contact_Left_Account__c = 'no';

                Certificate__c ce11 = new Certificate__c();
                ce11.Name = 'RTSS-NPM';
                ce11.Contact__c = cn2.id;
                ce11.Certification_Summary__c = c1.id;
                ce11.Status__c = 'Complete';
                ce11.Contact_Left_Account__c = 'no';

                Certificate__c ce12 = new Certificate__c();
                ce12.Name = 'RTSS-PM';
                ce12.Contact__c = cn1.id;
                ce12.Certification_Summary__c = c1.id;
                ce12.Status__c = 'Complete';
                ce12.Contact_Left_Account__c = 'no';


                list<Certificate__c> certList = new Certificate__c[]{ce1, ce2, ce3, ce7, ce8,ce9,ce10,ce11,ce12};
                insert certList;

                Certificate__c ce13 = new Certificate__c();
                ce13.Name = 'RTSS-PM';
                ce13.Contact__c = cn3.id;
                ce13.Certification_Summary__c = c1.id;
                ce13.Status__c = 'Complete';
                ce13.Contact_Left_Account__c = 'no';
                insert ce13;
                ce8.Status__c = 'Expired';
                ce13.Status__c = 'Expired';
                update ce8;
                update ce13;


        c1 = [Select a.TAP_Requirement_Satisfied__c,Status_Tap__c From Acct_Cert_Summary__c a where id = :c1.id];
            system.debug('********'+c1);
        a = [select Authorizations_Specializations__c,type from Account
                                where id = :a.id];
                            ac.Type ='';
                            update ac;


        //system.assert(a.Authorizations_Specializations__c.contains('TAP - AD'));

        //second test
        Certificate__c ce4 = new Certificate__c();
                ce4.Name = 'RSS-AD';
                ce4.Contact__c = cn4.id;
                ce4.Certification_Summary__c = c2.id;
                ce4.Status__c = 'Complete';
                ce4.Contact_Left_Account__c = 'no';

                Certificate__c ce5 = new Certificate__c();
                ce5.Name = 'RSS-AD';
                ce5.Contact__c = cn5.id;
                ce5.Certification_Summary__c = c2.id;
                ce5.Status__c = 'Complete';
                ce5.Contact_Left_Account__c = 'no';

                Certificate__c ce6 = new Certificate__c();
                ce6.Name = 'RTSS-AD';
                ce6.Contact__c = cn2.id;
                ce6.Certification_Summary__c = c2.id;
                ce6.Status__c = 'Complete';
                ce6.Contact_Left_Account__c = 'no';
                Certificate__c ce14 = new Certificate__c();
                ce14.Name = 'RTSS-AD';
                ce14.Contact__c = cn2.id;
                ce14.Certification_Summary__c = c2.id;
                ce14.Status__c = 'Complete';
                ce14.Contact_Left_Account__c = 'no';
            certList = new Certificate__c[]{ce4, ce5, ce6,ce14};
                insert certList;

        a = [select Authorizations_Specializations__c from Account
                                where id = :a.id];

        //system.assert(a.Authorizations_Specializations__c.contains('TAP - AD;TAP - CSD'));

        test.stopTest();
        }
        //Rev:1 test for cert adding        
    static testMethod void phase2UpdateTest() {
        TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
         Account grandParent = tdf.createAccount();
        insert grandParent;
        Account parentAcct = tdf.createAccount();
    	parentAcct.ParentId = grandParent.Id;
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        listOfAcctCerts.addAll(tdf.retriveCertSummary(parentAcct.Id));
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            String rsaStr = pc.TAP_short__c != 'General' ? 'RSA'+'-'+pc.TAP_short__c: 'RSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rsaStr));
            }
        }
        // Create Partner level config records
        PartnerLevelConfig__c plc = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR');
        insert plc;
        PartnerLevelConfig__c plcRegular = new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'Regular');
        insert plcRegular;
        //Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        insert certs;
        Test.startTest();
            ApexPages.StandardController ctrl = new ApexPages.Standardcontroller(parentAcct);
            InstantBatchCtrl ctrlRef = new InstantBatchCtrl(ctrl);
            ctrlRef.init();
        Test.stopTest();
        for(Acct_Cert_Summary__c pc :tdf.retriveCertSummary(parentAcct.Id)){
            System.assert(pc.Roll_Up_RSS__c >=0);
            System.assert(pc.Roll_Up_RTSS__c >=0);
            System.assert(pc.Roll_Up_RCSA_RCSP__c >= 0);
        }
        
    }
    // cert decreased
    static testMethod void phase2DecreaseTest() {
        TestDataFactory tdf = new TestDataFactory();
        RVBD_Email_Properties__c rvbdEmail = tdf.creatervbdEmail();
        insert rvbdEmail;
        Probation_Partner_Type__c ppType = tdf.createProbationPartnerType('VAR');
        insert ppType;
        Account grandParent = tdf.createAccount();
        insert grandParent;
        Account parentAcct = tdf.createAccount();
    	parentAcct.ParentId = grandParent.Id;
        insert parentAcct;
        Account acct = tdf.createAccount();
        acct.ParentId = parentAcct.Id;
        insert acct;
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0 ; i<5 ; i++){
            String cntLname = 'cnt_email_00'+i;
            String emailStr = 'cntemail00'+i+'@gmail.com';
            contactList.add(tdf.createContact(acct.Id, emailStr ,cntLname));
        }
        if(!contactList.isEmpty()){
            insert contactList;
        }
        List<Certificate_Master__c> listOfCertMaster = new List<Certificate_Master__c>();
        List<String> certList = new List<String>{'RSS','RSA','RTSS','RTSA','RCSP','RCSA'};
        for(String str: certList){
            listOfCertMaster.add(tdf.createMasterCerts(str));
        }
        insert listOfCertMaster;
        List<Certificate__c> certs = new List<Certificate__c>(); 
        List<Acct_Cert_Summary__c> listOfAcctCerts = tdf.retriveCertSummary(acct.Id);
        listOfAcctCerts.addAll(tdf.retriveCertSummary(parentAcct.Id));
        for(Acct_Cert_Summary__c pc: listOfAcctCerts){
            String rssstr = pc.TAP_short__c != 'General' ? 'RSS'+'-'+pc.TAP_short__c: 'RSS';
            String rtssstr = pc.TAP_short__c != 'General' ? 'RTSS'+'-'+pc.TAP_short__c: 'RTSS';
            String rcspstr = pc.TAP_short__c != 'General' ? 'RCSP'+'-'+pc.TAP_short__c: 'RCSP';
            String rcsastr = pc.TAP_short__c != 'General' ? 'RCSA'+'-'+pc.TAP_short__c: 'RCSA';
            String rsaStr = pc.TAP_short__c != 'General' ? 'RSA'+'-'+pc.TAP_short__c: 'RSA';
            System.debug('rssstr:'+rssstr+' rtssstr:'+rtssstr+' rcspstr:'+rcspstr);
            for(Integer i=0; i<=pc.RSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rssstr));
            }
            for(Integer i=0; i<=pc.RTSS_Needed__c; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rtssstr));
            }
            for(Integer i=0; i<=2; i++){
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcspstr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rcsastr));
                certs.add(tdf.createCert(pc.Id, contactList[0].Id, rsaStr));
            }
        }
        // Create Partner level config records
        List<PartnerLevelConfig__c> plcs = new List<PartnerLevelConfig__c>();
        plcs.add(new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Authorized',Number_Of_Competencies__c = 1,Type__c = 'VAR',Partner_Level__c = 'Authorized'));
        plcs.add(new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Cloud-Authorized',Number_Of_Competencies__c = 1,Type__c = 'Regular',Partner_Level__c = 'Authorized'));
        plcs.add(new PartnerLevelConfig__c(Automate_Promotion__c = true,Name = 'Federal-Authorized',Number_Of_Competencies__c = 1,Type__c = 'Federal',Partner_Level__c = 'Authorized'));
        insert plcs;
        //Constant.disablePartnerCompetencyTrigger = true;// this is to avoid the partner competency trigger execution.
        insert certs;
        Test.startTest();
        	 List<Acct_Cert_Summary__c> certSum = new List<Acct_Cert_Summary__c>();
        	 for(Acct_Cert_Summary__c cert : tdf.retriveCertSummary(parentAcct.Id)){
        	 	cert.RSA__c = 0;
        	 	cert.RSS__c = 0;
        	 	cert.RTSA__c = 0;
        	 	cert.RTSS__c = 0;
        	 	certSum.add(cert);
        	 }
        	 update certSum;
        Test.stopTest();
        for(Acct_Cert_Summary__c pc :tdf.retriveCertSummary(acct.Id)){
            System.assert(pc.Roll_Up_RSS__c >=0,'RSS count Decreased in Child');
            System.assert(pc.Roll_Up_RTSS__c >=0,'RTSS count Decreased in child');
            System.assert(pc.Roll_Up_RCSA_RCSP__c >=0,'RCSA/RCSP count Decreased in child');
        }
        
    }
}