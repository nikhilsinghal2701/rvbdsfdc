@isTest
public class SupportSystemUnitTests
{   
    public static testMethod void testChangeStatusOnEscalation(){
        //RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
                
        Case c = new Case();
        c.AccountId=c.AccountId = [ SELECT Id FROM Account LIMIT 1 ].Id;
        c.ContactId=[Select Id from contact limit 1].Id;
        c.Status = 'New';
        c.Priority='P2 - Serious';
        c.Further_Investigation__c = 'Yes';
        c.Platform__c = 'Linux'; c.Products__c = 'Steelhead'; c.Version__c = '6.5.1';
        insert(c);
        
        c.isEscalated = true;
        update(c);
        c.isEscalated = false;
        c.Status = 'Awaiting customer';
        update(c);
    }
   
    public static testMethod void testHandleEscalationMessage(){
        ZeusUtility.isTest=true;
        Case c = new Case();
        c.AccountId = [ SELECT Id FROM Account LIMIT 1 ].Id;
        c.Platform__c = 'Linux'; c.Products__c = 'ZXTM'; c.Version__c = '5.0r0';
        c.Status = 'Active';
        insert(c);
        
        c.EscalationMessage__c = 'I am a message';
        c.Status = 'Development';
        update(c);
        
        c.EscalationMessage__c = 'I am a 3rd line message';
        c.Status = '3rd Line';
        update(c);
    }
    
    public static testMethod void testCheckFieldsBeforeReassignment(){
        Case c = new Case();
        c.AccountId = [ SELECT Id FROM Account LIMIT 1 ].Id;
        //c.Platform__c = 'Linux'; c.Products__c = 'ZXTM'; c.Version__c = '5.0r0';
        c.Status = 'Active';
        insert(c);
        
        c.EscalationMessage__c = 'I am a message';
        c.Status = 'Development';
        
        try{
            update(c); 
        }catch(Exception e){
        }        
        c.Status = 'Awaiting bug fix/implementation';        
        try{
            update(c); 
        }catch(Exception e){
        }
    }
    
    public static testMethod void testSetAccountNumber(){
        Generator.isTest=true;
        RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=accCusRT.Id;
        cusAcc.name='CustomeraAAccount';
        cusAcc.Industry='Education';
        cusAcc.Type='Customer EC2';
        //User usr=[select Id,name from User where username='mgeldert@riverbed.com.test1'];
        //System.runAs(usr){
        Test.startTest();
        insert cusAcc;
        Test.stopTest();
        //}
    }
  //test method for leadEmailNotification trigger  
    public static testMethod void testLeadEmailNotifications(){ 
        ZeusUtility.isTest=true;
        Generator.isTest=true;   
        RecordType dealReg = [ SELECT Id FROM RecordType WHERE Name = 'Deal Registration' ];       
        Lead l = new Lead();
        //l.OwnerId = [ SELECT Id FROM User WHERE Name = 'Matthew Geldert' ].Id;
        l.OwnerId = [select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' AND Isactive=true limit 1].Id;
        l.Company = 'Test Company';
        l.LastName = 'MyLastName';
        l.Street='199 fremont st.';
        l.city='San Francisco';
        l.Country='USA';
        l.Email='test@test.com';
        l.Phone='4152347654';
        l.Partner_Sales_Rep__c='test';
        l.Primary_App__c='Replicator';
        l.Secondary_Application__c='Accelarator';
        l.Partner_Sales_Rep_Phone__c='4152348765';
        l.Verified_Budget__c='yes';
        l.Project_Close_Date__c=system.today().addDays(45);
        l.RecordTypeId = dealReg.Id;        
        //try{
            insert l;
            l.Status='Qualified Lead';
            update l;
        //}catch(DMLException e){
        //    System.debug(e.getMessage());
        //}
    }
    //test method for emailLoginDetails -- originally testDeveloperTriggers
    public static testMethod void testEmailLoginDetails(){
        ZeusUtility.isTest=true;
        Developer__c d1 = new Developer__c(); 
        Developer__c d2 = new Developer__c();
        d1.Name = 'TEST 123'; 
        d2.Name = 'TEST 123';
        d1.FirstName__c = 'Joe'; 
        d2.FirstName__c = 'Joe';
        d1.Surname__c = 'Blogs'; 
        d2.Surname__c = 'Blogs';
        d1.Email__c = 'madeup@riverbed.com'; 
        d2.Email__c = 'madeup@riverbed.com';        
        try{
            insert(d1);
            insert(d2);
        }catch(Exception e){}
    }
    
    //test method for amazonNewProductEmailConfirmations
    public static testMethod void testNewProductEmailConfirmations(){
        ZeusUtility.isTest=true;
        Generator.isTest=true;
        Account a = new Account(Name='TEST', Type='Customer EC2');
        insert(a);
        
        AmazonAccount__c aa = new AmazonAccount__c(Name='TEST',Account__c=a.Id, Products__c='abc;def');
        insert(aa);
        
        aa.ContactEmail__c = 'test@test.com';
        update(aa);
        
        aa.Support__c = 'TESTSUPPORT';
        update(aa);
    }
    //test method of setGlbLicenseKeyNameOnCreation and setLicenseKeyNameOnCreation
    public static testMethod void testLicenseKeyNamingTriggers(){
        ZeusUtility.isTest=true;
        License_Key__c lk = new License_Key__c();
        lk.AccountId__c = [ SELECT Id FROM Account LIMIT 1 ].Id;
        lk.Platform__c = 'Linux';
        lk.IP_address__c = '1.1.1.1';
        lk.Name = 'Awaiting activation';
        insert(lk);
        
        lk.Name = '12345';
        lk.Key__c = 'I am a key';
        update(lk);
        
        License_Key_GLB__c lkGlb = new License_Key_GLB__c();
        lkGlb.AccountId__c = [ SELECT Id FROM Account LIMIT 1 ].Id;
        lkGlb.Platform__c = 'Linux';
        lkGlb.IP_address__c = '1.1.1.1';
        insert(lkGlb);
        
        lkGlb.Name = '12345';
        lkGlb.Key__c = 'I am a key';
        update(lkGlb);
    }
    /* 
    public static testMethod void testSupportSystem()
    {
        SupportSystem sse = new SupportSystem();
        Case myCase = new Case();
        myCase.OwnerId='00520000000y0Px';
        myCase.ContactId = [SELECT Id FROM Contact WHERE LastName = 'Geldert' LIMIT 1][0].Id;
        insert(myCase);
        // Another SF bug - when you match the correct page ref the assert still fails, even though debug
        // output shows they DO match.  So, uselessly, we do a negative match of non-matching things  Brilliant!
        System.assertNotEquals(new PageReference('/500/e?retURL=/apex/Bu115h!t'), sse.newCase());
        
        // Same bug as above!
        System.assertNotEquals(new PageReference('/apex/waitingtickets/Bu115h!t'), sse.showWait());
        System.assertNotEquals(new PageReference('/apex/waitingtickets/Bu115h!t'), sse.showSearch());
        
        // Same bug as above!
        System.currentPageReference().getParameters().put('ticketToView', myCase.Id);
        System.currentPageReference().getParameters().put('userName', myCase.OwnerId);
        System.assertNotEquals(new PageReference('/' + myCase.Id + '/Bu115h!t'), sse.viewCase());        
        
        System.assertEquals('Support', sse.getCurrentQueue());
        
        //For some reason, doesn't match with assertEquals??
        System.assertEquals(sse.getDevTickets(), 0);
        sse.getDevTickets();
        sse.getThirdLineTickets();
        sse.getSupportTickets();
        
        // Just running the methods...
        List<Case> lc = sse.getAssignedCases();
        lc = sse.getShortpendingCases();
        lc = sse.getUnassignedCases();
        
        myCase.Status = 'Closed';
        update(myCase);
    }
    /*
    public static testMethod void testJoinCasesExtension()
    {
        JoinCasesExtension jce = new JoinCasesExtension();
        Id accId = [ SELECT Id FROM Account LIMIT 1 ].Id;
        Case destinationCase = new Case (AccountId=accId);
        Case sourceCase = new Case(AccountId=accId);
        
        try
        {
            insert(destinationCase);
            insert(sourceCase);
            jce.setSourceId(sourceCase.Id);
            jce.setDestinationId(destinationCase.Id);
            String junk = jce.getSourceId();
            junk = jce.getDestinationId();
            junk = jce.getSourceTicket();
            
            String str = jce.getTicketNumber();
            str = jce.getTicketNumber(sourceCase.Id);
            
            List<Case> lc = jce.getSameCompanyTickets();
        }
        catch(Exception e)
        {
        }
        
        //jce.setDestinationId('001R0000002tFQ5');
        //jce.setSourceId('001R0000002tFQ5');
               
        //System.assertEquals('001R0000002tFQ5', jce.getDestinationId());
        //System.assertEquals('001R0000002tFQ5', jce.getSourceId());
        
        //System.currentPageReference().getParameters().put('id', '001R0000002tFQ5');
        //System.assertEquals('001R0000002tFQ5', jce.getSourceTicket());
        
        
        PageReference pr = jce.joinTickets();
    }
    
    public static testMethod void testWaitingTicketController()
    {
        WaitingTicketsController wtc = new WaitingTicketsController();
        System.assertNotEquals(new PageReference('/apex/SupportSystem/Bu115h!t'), wtc.viewQueue());
    
        String response;
        List<Case> lc;
        
        response = wtc.getCustomerExpand(); System.assertEquals(response, 'Show all...');
        response = wtc.getThirdPartyExpand(); System.assertEquals(response, 'Show all...');
        response = wtc.getBugFixExpand(); System.assertEquals(response, 'Show all...');
        
        response = wtc.getCustomerSwitch(); System.assertEquals(response, 'false');
        response = wtc.getThirdPartySwitch(); System.assertEquals(response, 'false');
        response = wtc.getBugFixSwitch(); System.assertEquals(response, 'false');
        
        lc = wtc.getAwaitingBugFix();
        lc = wtc.getAwaitingThirdParty();
        lc = wtc.getAwaitingCustomer();
        
        System.currentPageReference().getParameters().put('customerLimitList', 'true');
        System.currentPageReference().getParameters().put('thirdPartyLimitList', 'true');
        System.currentPageReference().getParameters().put('bugFixLimitList', 'true');
        
        response = wtc.getCustomerExpand(); System.assertEquals(response, 'Show all...');
        response = wtc.getThirdPartyExpand(); System.assertEquals(response, 'Show all...');
        response = wtc.getBugFixExpand(); System.assertEquals(response, 'Show all...');
        
        response = wtc.getCustomerSwitch(); System.assertEquals(response, 'false');
        response = wtc.getThirdPartySwitch(); System.assertEquals(response, 'false');
        response = wtc.getBugFixSwitch(); System.assertEquals(response, 'false');
        
        lc = wtc.getAwaitingBugFix();
        lc = wtc.getAwaitingThirdParty();
        lc = wtc.getAwaitingCustomer();
        
        System.currentPageReference().getParameters().put('customerLimitList', 'false');
        System.currentPageReference().getParameters().put('thirdPartyLimitList', 'false');
        System.currentPageReference().getParameters().put('bugFixLimitList', 'false');
        
        response = wtc.getCustomerExpand(); System.assertEquals(response, 'Show fewer...');
        response = wtc.getThirdPartyExpand(); System.assertEquals(response, 'Show fewer...'); 
        response = wtc.getBugFixExpand(); System.assertEquals(response, 'Show fewer...'); 
        
        response = wtc.getCustomerSwitch(); System.assertEquals(response, 'true');
        response = wtc.getThirdPartySwitch(); System.assertEquals(response, 'true'); 
        response = wtc.getBugFixSwitch(); System.assertEquals(response, 'true');
  
        lc = wtc.getAwaitingBugFix();
        lc = wtc.getAwaitingThirdParty();
        lc = wtc.getAwaitingCustomer();
        
        Case myCase = new Case(Status='Active');
        insert(myCase);
        myCase.Status = 'Development';
        myCase.Platform__c = 'Linux'; myCase.Products__c = 'ZXTM'; myCase.Version__c = '5.0r0';
        
        try
        {
            update(myCase);
        }
        catch(DmlException e)
        {
            System.assert(e.getMessage().contains('first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, Please add platform: [Platform__c]'),
                e.getMessage());
        }       
    }

    public static testMethod void testAlertOnPoorResponse()
    {
        Case c = [ SELECT Id, CaseNumber FROM CASE WHERE IsClosed = true LIMIT 1 ];
        
        Satisfaction_Survey__c ss = new Satisfaction_Survey__c();
        ss.CaseId__c = c.CaseNumber;
        ss.Overall__c = 2;
        
        insert(ss); 
    }
    
    public static testMethod void testSupportAdvancedSearchController()
    {
        SupportAdvancedSearchController sc = new SupportAdvancedSearchController();
        List<Case> lC = sc.getPrintSearchResults('Get cases by customer');
        System.currentPageReference().getParameters().put('customer', 'ARRGGGHHHH!!!!!');
        lC = sc.getPrintSearchResults('Get cases by customer');
        System.currentPageReference().getParameters().put('refString', 'ref:00D2NO4.999999999:ref');
        lC = sc.getPrintSearchResults('Get cases by reference');
        lC = sc.getPrintSearchResults();
    }
    */    
}