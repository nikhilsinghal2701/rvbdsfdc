global without sharing class BatchContactScoring  implements Database.Batchable<sObject>, Database.Stateful {

	global String Query;

	global BatchContactScoring(){
   		
   		Query = 'Select Id, IsDeleted, Invalid_Email__c, Support_Role__c, zContact_DateFlex_1__c, Relevance_to_RVBD__c FROM Contact WHERE (IsDeleted = False)';
   
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }
   
   global void execute(Database.BatchableContext BC, List<sObject> scope) {
   	   	   		
   		List<Contact> contactToUpdate = new List<Contact>();
   		Set<Id> contactIds = new Set<Id>();
   		
   		//get a list of the contacts in this batch
   		for(sObject so : scope)
   		{
   			Contact c = (Contact)so;
   			
   			contactIds.add(c.Id);
   		}
   		
   		Map<Id,Integer> mahMap = new Map<Id,Integer>();
   		Map<Id,DateTime> mahDateMap = new Map<Id,DateTime>();
   		
   		//get the latest EventDateTime from Manticore_Marketing_Activity_History__c and store it in a map
   		for(AggregateResult ar : [SELECT Contact__c, Max(EventDateTime__c)  dt
   			FROM Manticore_Marketing_Activity_History__c 
   			WHERE Contact__c in : contactIds   			
   			Group by Contact__c])
   		{
   			try {
   				mahDateMap.put(String.valueOf(ar.get('Contact__c')),DateTime.valueOf(ar.get('dt')));
   			}
   			catch (Exception e) {
   				//mahDateMap.put(String.valueOf(ar.get('Contact__c')),DateTime.valueOf(ar.get('dt')));
   			}
   		}

		//add up a score for each contact by activity type
   		for(AggregateResult ar : [SELECT Contact__c, ActivityType__c, Count(Id) 
   			FROM Manticore_Marketing_Activity_History__c 
   			WHERE Contact__c in : contactIds
   			AND ActivityType__c in ('Web Visit', 'Email Forward', 'Form Submit')
   			Group by Contact__c, ActivityType__c])
   		{
   			if(mahMap.get(String.valueOf(ar.get('Contact__c'))) != null)
   			{
   				Integer currentCount = mahMap.get(String.valueOf(ar.get('Contact__c')));
   				mahMap.remove(String.valueOf(ar.get('Contact__c')));
   				if(String.valueOf(ar.get('ActivityType__c')) == 'Web Visit')
   				{
   					currentCount += 2;
   					mahMap.put(String.valueOf(ar.get('Contact__c')), currentCount);
   				}
   				else
   				{
   					currentCount += 1;
   					mahMap.put(String.valueOf(ar.get('Contact__c')), currentCount);
   				}   				
   			}
   			else{
   				if(String.valueOf(ar.get('ActivityType__c')) == 'Web Visit')
   				{
   					mahMap.put(String.valueOf(ar.get('Contact__c')), 2);
   				}
   				else
   				{
   					mahMap.put(String.valueOf(ar.get('Contact__c')), 1);
   				}
   			}
   		}   		
   		
   		Map<Id,Integer> taskMap = new Map<Id,Integer>();
   		//get a count of tasks related to the contact, BDR Task and Connect Task record types only -  and store in a map
   		for (AggregateResult ar : [SELECT WhoId, count(Id) cd
			FROM Task WHERE WhoId in :contactIds and RecordType.Name in ('BDR Task','Connect Task')
			group by WhoId]) 
	   	{
	   		
	   		taskMap.put(String.valueOf(ar.get('WhoId')),Integer.valueOf(ar.get('cd')));	
	   	}	   	
   		//Task[] tasks = [SELECT Id, WhoId FROM Task WHERE WhoId in :contactIds and RecordType.Name in ('BDR Task','Connect Task')];
   		
   		Map<Id,Integer> caseMap = new Map<Id,Integer>();
   		//get a count of cases related to the contact and store in a map
   		for (AggregateResult ar : [SELECT ContactId, count(Id) cd
			FROM Case WHERE ContactId in :contactIds
			group by ContactId]) 
	   	{
	   		
	   		caseMap.put(String.valueOf(ar.get('ContactId')),Integer.valueOf(ar.get('cd')));	
	   	}	   	
   		//Case[] cases = [SELECT Id, ContactId FROM Case WHERE ContactId in :contactIds];
   		
   		Map<Id,Integer> ocMap = new Map<Id,Integer>();
   		//get a count of OpportunityContactRoles related to the contact and store in a map
   		for (AggregateResult ar : [SELECT ContactId, count(Id) cd
			FROM OpportunityContactRole
			WHERE (ContactId in :contactIds)
				AND (Opportunity.CreatedBy.Profile.Name not in ('System Administrator','Sales Ops - Dashboard'))
			group by ContactId]) 
	   	{	   		
	   		ocMap.put(String.valueOf(ar.get('ContactId')),Integer.valueOf(ar.get('cd')));	
	   	} 
   		//OpportunityContactRole[] oppRoles = [SELECT Id, ContactId FROM OpportunityContactRole WHERE ContactId in :contactIds];
   		
   		for(sObject so : scope)
   		{
   			Contact c = (Contact)so;
   			
   			Integer score = 0;   			
   			  			
   			//DateTime mahDT = null;
   			/*			
   			for (Manticore_Marketing_Activity_History__c m:mah)
			{				
	   			if(m.Contact__c == c.Id)
	   			{				
					if(m.ActivityType__c == 'Web Visit')
					{
						score += 2;						
					}
					else
					{
						score += 1;
					}
					
		   			if(mahDT == null || mahDT < m.EventDateTime__c)
	   				{
	   					mahDT = m.EventDateTime__c;
	   				
	   				}
				}				
			}
			*/
			//is the latest mah record less than six months ago?  **rate high
			//if(mahDT > System.now().addMonths(-6))
			if(mahDateMap.get(c.Id) > System.now().addMonths(-6))
			{
				score += 10;
			}

			if(mahMap.get(c.Id) != null && mahMap.get(c.Id) > 0)
			{			
				score += mahMap.get(c.Id);
			}
			
			if(taskMap.get(c.Id) != null && taskMap.get(c.Id) > 0)
			{			
				score += 10;
			}
			
			if(caseMap.get(c.Id) != null && caseMap.get(c.Id) > 0)
			{			
				score += 5;
			}
			
			if(ocMap.get(c.Id) != null && ocMap.get(c.Id) > 0)
			{			
				score += 10;
			}			
			
/*			
			Integer taskCount = 0;
			Integer caseCount = 0;
			Integer oppCCount = 0;
			
   			for (Task t:tasks)
			{				
	   			if(t.WhoId == c.Id)
	   			{				
					taskCount += 1;
				}				
			}
			
			//**rate high
			if(taskCount > 0)
			{
				score += 10;
			}
			
   			for (Case cs:cases)
			{				
	   			if(cs.ContactId == c.Id)
	   			{				
					caseCount += 1;
				}				
			}
			
			if(caseCount > 0)
			{
				score += 5;
			}
			
   			for (OpportunityContactRole oc:oppRoles)
			{				
	   			if(oc.ContactId == c.Id)
	   			{				
					oppCCount += 1;
				}				
			}
			
			//**rate high
			if(oppCCount > 0)
			{
				score += 10;
			}						
*/


			if(c.Support_Role__c != null && c.Support_Role__c.Contains('Support Access'))
			{
				score += 3;
			}
			
			if(c.Support_Role__c != null && c.Support_Role__c.Contains('Support Admin'))
			{
				score += 6;
			}

			if(c.zContact_DateFlex_1__c != null)
			{
				score += 10;
			}
			
			if(c.Invalid_Email__c == true)
			{
				score += -5;
			}
			
			Integer Relevance_to_RVBD = Math.Min(99, Math.Max(0,score));
			if (c.Relevance_to_RVBD__c != Relevance_to_RVBD) {
				c.Relevance_to_RVBD__c = Relevance_to_RVBD;
				ContactToUpdate.add(c);
			}
   		}
   		
   		if (!ContactToUpdate.IsEmpty())
			update ContactToUpdate;
								
   }
   
   global void finish(Database.BatchableContext BC){

   }
	
}