@isTest
private class TestOpportunityCloneController {
	static testMethod void testNewOpportunityCloneController() { 
       // setup a reference to the page the controller is expecting with the parameters
        PageReference pref = Page.NewOpportunityClone;
        Test.setCurrentPage(pref);
         
        // // create new account record
        RecordType accPatRT=[select id,name from recordtype where SobjectType ='Account'  and name='Partner Account'];
        RecordType accCusRT=[select id,name from recordtype where SobjectType ='Account'  and name='Customer Account'];
        List<Account> lstAcc=new List<Account>();
        
        Account cusAcc=new Account();
        cusAcc.RecordTypeId=accCusRT.Id;
        cusAcc.name='CustomerAccount';
        cusAcc.Type='Customer';
        cusAcc.Industry='Education';
        lstAcc.add(cusAcc);
        
        Account stpAcc=new Account();
        stpAcc.RecordTypeId=accPatRT.Id;
        stpAcc.name='stpAccount';
        stpAcc.Type='Distributor';
        stpAcc.Industry='Education';
        stpAcc.RASP_Status__c='Authorized';
        stpAcc.Authorizations_Specializations__c='RASP;RVSP';
        insert stpAcc; 
        
        Account tier2Acc=new Account();
        tier2Acc.RecordTypeId=accPatRT.Id;
        tier2Acc.name='Trace 3';
        tier2Acc.Type='VAR';
        tier2Acc.Industry='Education';
        tier2Acc.RASP_Status__c='Authorized';
        tier2Acc.Authorizations_Specializations__c='RASP';
        lstAcc.add(tier2Acc);
        
        Account tier3Acc=new Account();
        tier3Acc.RecordTypeId=accPatRT.Id;
        tier3Acc.name='stpAccount';
        tier3Acc.Type='VAR';
        tier3Acc.Industry='Education';
        tier3Acc.RASP_Status__c='Not Reviewed';
        lstAcc.add(tier3Acc);
        
        Account influAcc=new Account();
        influAcc.RecordTypeId=accPatRT.Id;
        influAcc.name='stpAccount';
        influAcc.Type='VAR';
        influAcc.Industry='Education';
        influAcc.RASP_Status__c='Not Reviewed';
        lstAcc.add(influAcc);
        
        insert lstAcc;
 
        // create new opportunity record
        User primaryISR=[select id,name,DefaultCurrencyIsoCode from User limit 1];
        User[] powerPartner=[select id,name,DefaultCurrencyIsoCode from User where UserType='PowerPartner' limit 2];
        
        // setup user record
        Opportunity testOpp=new Opportunity();
        testOpp.AccountId=cusAcc.Id;
        testOpp.Channel__c='Reseler';
        testOpp.Sold_To_Partner__c=stpAcc.Id;
        testOpp.Sold_To_Partner_User__c=powerPartner[0].Id;
        testOpp.Primary_ISR__c=primaryISR.Id;
        testOpp.Name='TestOpportunity';
        testOpp.Type='New';
        testOpp.CurrencyIsoCode=primaryISR.DefaultCurrencyIsoCode;
        testOpp.CloseDate=System.today();
        testOpp.StageName='0-Prospect';
        testOpp.Forecast_Category__c='PipeLine';
        testOpp.Competitors__c='cisco';
        testOpp.LeadSource='Email';
 
        // Construct the standard controller
        ApexPages.StandardController con = new ApexPages.StandardController(testOpp); 
        // create the controller
        NewOpportunityCloneController ext = new NewOpportunityCloneController(con); 
        // Switch to test context
        Test.startTest(); 
        // call the cloneWithItems method
        PageReference ref = ext.cloneWithoutProducts();
        // create the matching page reference
        PageReference redir = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId); 
        //PageReference ref1 = ext.quickcloneWithoutProducts();
        //PageReference redir1 = new PageReference('/'+ext.newRecordId+'/e?retURL=%2F'+ext.newRecordId);
 		PageReference ref2 = ext.newcancel();
        // make sure the user is sent to the correct url
        //System.assertEquals(ref.getUrl(),redir.getUrl()); 
        // check that the new opp was created successfully
        Opportunity newOpp = [select id from Opportunity where id = :ext.newRecordId];
        System.assertNotEquals(newOpp, null); 		
        // Switch back to runtime context
        Test.stopTest(); 
    }	
}