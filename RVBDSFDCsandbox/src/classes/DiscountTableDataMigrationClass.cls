/*****************************************************************************************************
Author: Santoshi Mishra
Purpose: This class is the controller class for DiscountTableDataMigration
Created Date : Oct 2011
*******************************************************************************************************/

global class DiscountTableDataMigrationClass  implements Database.Batchable <Sobject> {
		global Final String query;
		Boolean Test;
	
	global DiscountTableDataMigrationClass()
		{
				query = 'Select d.Standard_Discount_Support__c,d.Standard_Discount_Support_250k__c, d.Standard_Discount_Support_1M__c, d.Standard_Discount_Spares__c,d.Standard_Discount_Services__c, d.Standard_Discount_Products__c, d.Standard_Discount_None__c,d.Standard_Discount_Demo__c, d.StandardDiscountSupportBand3__c, d.StandardDiscountSupportBand2__c, d.StandardDiscountSupportBand1__c, d.OwnerId, d.Name, d.Id, d.DiscountSupportBand3__c, d.DiscountSupportBand2__c,d.DiscountSupportBand1__c, d.DiscountSupportBand0__c, d.DiscountSpare__c, d.DiscountServices__c, d.DiscountProducts__c,d.DiscountNone__c, d.DiscountDemos__c From DiscountCategory__c d ';
				test=false;
		}
		
	global DiscountTableDataMigrationClass(Boolean FromTest) //Constructor called from test class
		{
			query = 'Select d.Standard_Discount_Support__c,d.Standard_Discount_Support_250k__c, d.Standard_Discount_Support_1M__c, d.Standard_Discount_Spares__c,d.Standard_Discount_Services__c, d.Standard_Discount_Products__c, d.Standard_Discount_None__c,d.Standard_Discount_Demo__c, d.StandardDiscountSupportBand3__c, d.StandardDiscountSupportBand2__c, d.StandardDiscountSupportBand1__c, d.OwnerId, d.Name, d.Id, d.DiscountSupportBand3__c, d.DiscountSupportBand2__c,d.DiscountSupportBand1__c, d.DiscountSupportBand0__c, d.DiscountSpare__c, d.DiscountServices__c, d.DiscountProducts__c,d.DiscountNone__c, d.DiscountDemos__c From DiscountCategory__c d  order by createddate desc limit 1';
			test=true;
		}	
		
	global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
	}
	
	 global void execute(Database.BatchableContext BC,List<DiscountCategory__c> queryList){
       
      Map<Id,DiscountCategory__c> disCategoryMap = new Map<Id,DiscountCategory__c>(queryList);
      	Map<String,Discount_Category_Detail__C> existingdetailMap = new Map<String,Discount_Category_Detail__C>();	
      	List<Category_Master__C> catList ;
      List<String> catSet = new List<String>{'A','B','B-2','B-3','B-4','C','D','E','F'};
			List<Discount_Category_Detail__C> detailList = new List<Discount_Category_Detail__C>();	
			if(test==true)
			{
			//catSet.clear();
			catSet.add('B1');
			catSet.add('B2');
			catList = [select Id,Name from Category_Master__C where name in : catSet order by createddate desc ];
			}
			else
		catList = [select Id,Name from Category_Master__C where name in : catSet];
		Map<String,Category_Master__C> catMap = new Map<String,Category_Master__C>();
		for(Category_Master__C cat : catList )
			{
				catMap.put(cat.Name,cat);
			}
		
	
		
	List<Discount_Category_Detail__C> existingList = [select Id,Name,category__C,category__r.Name,Discount_Table__C,Standard_Discount__C, 
							Registered_Discount__C from Discount_Category_Detail__c where Discount_Table__C in : disCategoryMap.keySet() and category__r.Name in :catMap.keyset() ];
							
						
								
		for(Discount_Category_Detail__c d : existingList)
		{
			existingdetailMap.put(d.category__r.Name+','+d.discount_table__c, d);
			//detailIdsSet.add(d.Id);
			System.debug('checking---'+d);
		}		
		for(DiscountCategory__c dis : disCategoryMap.values())
		{
			Discount_Category_Detail__C det;
			for( Category_Master__C cat : catList)
			{
				if(existingdetailMap.containsKey(cat.Name+','+dis.Id))
				det =existingdetailMap.get(cat.Name+','+dis.Id);
				else
				{
					det = new Discount_Category_Detail__C();
				 	det.Discount_Table__c = dis.Id;
				  	det.Active__c=true;	
				}
					det.Category__c = cat.Id;
				if(cat.Name=='A')
				{
					
					det.Standard_Discount__c = dis.Standard_Discount_Products__c;
					det.Registered_Discount__c = dis.DiscountProducts__c;
					
					System.debug('checking---inside--'+det);
					detailList.add(det);
					
				}
				
				else if(cat.Name=='B')
				{
					
					det.Standard_Discount__c = dis.Standard_Discount_Support__c;
					det.Registered_Discount__c = dis.DiscountSupportBand0__c;
					detailList.add(det);
					
				}
				
				else if(cat.Name=='B-2')
				{
					
					det.Standard_Discount__c = dis.StandardDiscountSupportBand1__c;
					det.Registered_Discount__c = dis.DiscountSupportBand1__c;
					detailList.add(det);
					
				}
				
				else if(cat.Name=='B-3')
				{
					
					det.Standard_Discount__c = dis.StandardDiscountSupportBand2__c;
					det.Registered_Discount__c = dis.DiscountSupportBand2__c;
					detailList.add(det);
					
				}
				
				else if(cat.Name=='B-4')
				{
					
					det.Standard_Discount__c = dis.StandardDiscountSupportBand3__c;
					det.Registered_Discount__c = dis.DiscountSupportBand3__c;
					detailList.add(det);
					
				}
				
				
				else if(cat.Name=='C')
				{
					det.Standard_Discount__c = dis.Standard_Discount_Demo__c;
					det.Registered_Discount__c = dis.DiscountDemos__c;
					detailList.add(det);
					
				}
				
				 else if(cat.Name=='D')
				{
					det.Standard_Discount__c = dis.Standard_Discount_Spares__c;
					det.Registered_Discount__c = dis.DiscountSpare__c;
					detailList.add(det);
					
				}
				
				else if(cat.Name=='E')
				{
				
					det.Standard_Discount__c = dis.Standard_Discount_Services__c;
					det.Registered_Discount__c = dis.DiscountServices__c;
					detailList.add(det);
					
				}
				
				else if(cat.Name=='F')
				{
				
					det.Standard_Discount__c = dis.Standard_Discount_None__c;
					det.Registered_Discount__c = dis.DiscountNone__c;
					detailList.add(det);
					
				}
				
				
				
				
			}
			
			
						
		}
		
		upsert(detailList);
		
		System.debug('Data updated Successfully..'+detailList+'--'+detailList.size());  
   }

   global void finish(Database.BatchableContext BC){
   	
   	

   }
	
	


}