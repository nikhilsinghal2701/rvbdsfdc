/*
This is a helper class used by the AccountMergeCertificateSummaryJoin tigger
to avoid infinite recursion.
*/
public with sharing class AccountMergeCertificateSummaryJoinHelper {

	public static boolean alreadyRun = false;
	
	public static boolean hasAlreadyRun(){
		return alreadyRun;
	}
	
	public static void setAlreadyRun(){
		alreadyRun = true;
	}
	
	private static testmethod void testhelper(){
		AccountMergeCertificateSummaryJoinHelper.hasAlreadyRun();
		AccountMergeCertificateSummaryJoinHelper.setAlreadyRun();
		system.assert(AccountMergeCertificateSummaryJoinHelper.hasAlreadyRun() == true);
	}
}