public class extServerDate{

    //fields
    private final Case myCase;
    
    //constructor
    public extServerDate(ApexPages.StandardController stdController) {
        this.myCase = (Case)stdController.getRecord();
    }
    
    public PageReference updateField() {
        String strResponse = '';
        DateTime d = System.now();
        //get case
        Case c = [select Id,Initial_Response__c,IsClosed from Case where id = :myCase.id limit 1];

        //update case
        if (c.IsClosed == false && c.Initial_Response__c == null){
            c.Initial_Response__c = d;
            update c;
            PageReference pageRef = Page.E2CP__New_Comment;
            pageRef.getParameters().put('id', c.Id);
            pageRef.setRedirect(true);
            return pageRef;
        } else {
            return null;
        }
       
    }
    
    static testMethod void extServerDateTest() {
        
        Case c = new Case(
            Initial_Response__c = null
        );
        
        insert c;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(c);
        extServerDate esd = new extServerDate(stdController);
        PageReference pg;
        
        //field is blank
        pg = esd.updateField();
        System.Assert(pg != null);
        System.Assert([select Initial_Response__c from case where id=:c.id][0].Initial_Response__c != null);
        
        //field has value
        pg = esd.updateField();
        System.Assert(pg == null);

     }
    
    
    
}