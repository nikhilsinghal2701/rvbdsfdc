@isTest
private class TestAssetCasesCtrl    {

    static testMethod void unitTest() {
        Account acc = new Account(Name = 'Defaut Account');
        insert acc;
        Contact con = new Contact(
            LastName = 'Test',
            AccountId = acc.id,
            Email = 'test@ctm.com'
        );
        insert con;
        Asset a = new Asset(Name = '20567', AccountId = acc.Id, ContactId = con.Id);
        insert a;
        Case c = new Case (
            AssetId = a.Id,
            Status = 'New',
            Priority = 'High'
        );
        insert c;
        ApexPages.currentPage().getParameters().put('sn', a.Name);
        AssetCasesCtrl  obj = new AssetCasesCtrl ();

    }

}